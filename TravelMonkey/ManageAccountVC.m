//  ManageAccountVC.m
//  Party Buzz
//  Created by Surendra Sharma on 05/07/16.
//  Copyright © 2016 Uros Lorencic. All rights reserved.

#import "ManageAccountVC.h"
#import "BuysGuide.h"
#import "UNIRest.h"

@interface ManageAccountVC()
@end

@implementation ManageAccountVC

-(void)viewDidLoad
{
    [super viewDidLoad];
    //Do any additional setup after loading the view from its nib.
    
    appdel=(AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.title=@"Account Detail";
    
    winSize=[UIScreen mainScreen].bounds.size;
    if(winSize.height<500)
    {
        [scrlViewShowDetail setContentSize:CGSizeMake(scrlView.contentSize.width, 600)];
        [scrlView setContentSize:CGSizeMake(scrlView.contentSize.width, 1050)];
    }
    else
    {
        [scrlView setContentSize:CGSizeMake(scrlView.contentSize.width, 1050)];
    }
    
    UIBarButtonItem *cButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_info.png"] style:UIBarButtonItemStyleDone target:self action:@selector(infoHandler:)];
    self.navigationItem.rightBarButtonItem = cButton;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_back.png"] style:UIBarButtonItemStyleBordered target: self action: @selector(btn_back:)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnHideDate:)];
    tapGesture.numberOfTapsRequired = 1;
    [imgOverly addGestureRecognizer:tapGesture];
    
    
    NSDateFormatter *dateFormatterApi=[[NSDateFormatter alloc] init];
    [dateFormatterApi setDateFormat:@"dd - MM - yyyy"];
    NSString *strDate=[dateFormatterApi stringFromDate:dtSelect.date];
    lblDateShow.text=strDate;
    
//  [btnChange setTitle:@"Add Account" forState:UIControlStateNormal];
    
    //Add observer for TextField Search....
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextViewTextDidChangeNotification object:nil];

    //Add observer for TextField Search....
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChangeAll:) name:UITextFieldTextDidChangeNotification object:nil];
    
    btnRetry.hidden=YES;
    viwFillForm.hidden=YES;
    viwShowDetail.hidden=YES;
    
    btnRetry.layer.cornerRadius = 3;
    btnDoneSelectDate.layer.cornerRadius = 3;
    btnDone.layer.cornerRadius = 3;
    btnUpload.layer.cornerRadius = 3;

    viwNeed.hidden=YES;
    lblVerify.text=@"";

    dtSelect.maximumDate=[NSDate date];
    
}

- (IBAction)btn_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)infoHandler:(id) sender
{
    BuysGuide *vc = [[BuysGuide alloc] initWithNibName: @"BuysGuide" bundle: nil];
    [self.navigationController pushViewController: vc animated: YES];
}

-(void)textFieldDidChangeAll:(NSNotification *)txtFld
{
    UITextField *txt=(UITextField*)txtFld.object;
    
    if(txt.tag==222)
    {
    //SSN
        if(txt.text.length>4)
        {
            NSString *str=txt.text;
            
            str=[str substringToIndex:4];
            txtSSN.text=str;
        }
    }
    else if (txt.tag==333)
    {
        //Routing
        if(txt.text.length>9)
        {
            NSString *str=txt.text;
            
            str=[str substringToIndex:9];
            txtRoutingNumber.text=str;
        }
    }
}


-(void)textFieldDidChange:(NSNotification *)txtFld
{
    if(txtAddress.text.length>0)
    {
        lblAddress.hidden=YES;
    }
    else
    {
        lblAddress.hidden=NO;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    NSDateFormatter *dateFormatterApi=[[NSDateFormatter alloc] init];
    [dateFormatterApi setDateFormat:@"dd - MM - yyyy"];
    NSString *strDate=[dateFormatterApi stringFromDate:dtSelect.date];
    lblDateShow.text=strDate;
    
    [self GetAccuntdetails];
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setModeLoading:(BOOL)active
{
    if(active)
    {
        [MBProgressHUD showHUDAddedTo:Obj_AppDelegate.window animated:YES];
    }
    else
    {
        [MBProgressHUD hideHUDForView:Obj_AppDelegate.window animated:YES];
    }
}

-(void)hideHud
{
    [MBProgressHUD hideHUDForView:Obj_AppDelegate.window animated:YES];
}

-(IBAction)btnDoneEntery:(id)sender
{
    [self SendAccuntdetails];
}

-(IBAction)btnHideDate:(id)sender
{
    NSDateFormatter *dateFormatterApi=[[NSDateFormatter alloc] init];
    [dateFormatterApi setDateFormat:@"dd - MM - yyyy"];
    NSString *strDate=[dateFormatterApi stringFromDate:dtSelect.date];
    lblDateShow.text=strDate;
    
    ViewdateSelect.hidden=true;
    imgOverly.hidden=true;
}

-(void)GetAccuntdetails
{
    if(![appdel isInternetAvailable])
    {
        return;
    }
    
    NSString *device =[Utility encryptString:[Utility getDeviceAppId]];
    NSString *session=[Utility encryptString:[Utility getSession]];
    NSDictionary *parameters = @{@"device": device, @"session": session};
    
    [self setModeLoading:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:@"http://partybuzzapp.com/app/1.2/user/getUserData.php" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [Obj_AppDelegate NS_Debug_Log:responseObject];
         [self setModeLoading:false];
        
         NSDictionary *resDic=(NSDictionary*)[responseObject objectForKey:@"user_data"];
         if(resDic)
         {
             NSString *strr=[resDic objectForKey:@"stripe_staus"];
             if(![strr isKindOfClass:[NSNull class]] && [[strr lowercaseString] isEqualToString:@"verified"])
             {
                 appdel.isUserAccountDetailsSuccess=st_Verifiy;
                 NSString *strName=[resDic objectForKey:@"username"];
                 NSString *strBankName=[resDic objectForKey:@"bank_name"];
                 NSString *strLastDigit=[resDic objectForKey:@"last_digit"];
                 NSString *strDescription=[resDic objectForKey:@"stripe_message"];
                 shwUserName.text=strName;
                 shwBankName.text=strBankName;
                 shwAccountNumber.text=[NSString stringWithFormat:@"******%@",strLastDigit];
                 shwDescription.text=strDescription;
                 
                 viwFillForm.hidden=YES;
                 viwShowDetail.hidden=NO;
                 btnRetry.hidden=YES;
                 btnUpload.hidden=YES;
                 btnInfo.hidden=NO;
                 [btnInfo setImage:[UIImage imageNamed:@"btn_checkgreen.png"] forState:UIControlStateNormal];
                 
                 viwNeed.hidden=YES;
                 lblVerify.text=@"Verified";
                 [[NSNotificationCenter defaultCenter]postNotificationName:@"updateAccountImage" object:[NSString stringWithFormat:@"1"]];
             }
             else if(![strr isKindOfClass:[NSNull class]] && [[strr lowercaseString] isEqualToString:@"unverified"])
             {
                 appdel.isUserAccountDetailsSuccess=st_Unverifiy;
                 NSString *strName=[resDic objectForKey:@"username"];
                 NSString *strBankName=[resDic objectForKey:@"bank_name"];
                 NSString *strLastDigit=[resDic objectForKey:@"last_digit"];
                 NSString *strDescription=[resDic objectForKey:@"stripe_message"];
                 
                 shwUserName.text=strName;
                 shwBankName.text=strBankName;
                 shwAccountNumber.text=[NSString stringWithFormat:@"******%@",strLastDigit];
                 shwDescription.text=strDescription;
                 
                 viwFillForm.hidden=YES;
                 viwShowDetail.hidden=NO;
                 btnRetry.hidden=YES;
                 btnUpload.hidden=NO;
                 btnInfo.hidden=NO;
                 [btnInfo setImage:[UIImage imageNamed:@"btn_info.png"] forState:UIControlStateNormal];
                 
                 viwNeed.hidden=YES;
                 lblVerify.text=@"Unverified";
                 
                 [[NSNotificationCenter defaultCenter]postNotificationName:@"updateAccountImage" object:[NSString stringWithFormat:@"0"]];
             }
             else if(![strr isKindOfClass:[NSNull class]] && [[strr lowercaseString] isEqualToString:@"pending"])
             {
                 appdel.isUserAccountDetailsSuccess=st_Pending;
                 NSString *strName=[resDic objectForKey:@"username"];
                 NSString *strBankName=[resDic objectForKey:@"bank_name"];
                 NSString *strLastDigit=[resDic objectForKey:@"last_digit"];
                 NSString *strDescription=[resDic objectForKey:@"stripe_message"];
                 
                 shwUserName.text=strName;
                 shwBankName.text=strBankName;
                 shwAccountNumber.text=[NSString stringWithFormat:@"******%@",strLastDigit];
                 shwDescription.text=strDescription;
                 
                 viwFillForm.hidden=YES;
                 viwShowDetail.hidden=NO;
                 btnRetry.hidden=YES;
                 btnUpload.hidden=NO;
                 btnInfo.hidden=NO;
                 [btnInfo setImage:[UIImage imageNamed:@"btn_check.png"] forState:UIControlStateNormal];
                 
                 viwNeed.hidden=YES;
                 lblVerify.text=@"Pending";
                 [[NSNotificationCenter defaultCenter]postNotificationName:@"updateAccountImage" object:[NSString stringWithFormat:@"0"]];
             }
             else
             {
                 appdel.isUserAccountDetailsSuccess=st_Error;
                 shwUserName.text=@"";
                 shwBankName.text=@"";
                 shwAccountNumber.text=@"";
                 
                 viwFillForm.hidden=NO;
                 viwShowDetail.hidden=YES;
                 btnRetry.hidden=YES;
                 
                 btnUpload.hidden=YES;
                 btnInfo.hidden=YES;
                 
                 viwNeed.hidden=YES;
                 lblVerify.text=@"";
             }
         }
         else
         {
             appdel.isUserAccountDetailsSuccess=st_Error;
             viwFillForm.hidden=YES;
             viwShowDetail.hidden=YES;
             btnRetry.hidden=NO;
             
             btnUpload.hidden=YES;
             btnInfo.hidden=YES;
             
             viwNeed.hidden=YES;
             lblVerify.text=@"";
         }
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         appdel.isUserAccountDetailsSuccess=st_Error;
         viwFillForm.hidden=YES;
         viwShowDetail.hidden=YES;
         btnRetry.hidden=NO;
         
         btnUpload.hidden=YES;
         btnInfo.hidden=YES;
         
         viwNeed.hidden=YES;
         lblVerify.text=@"";
     }];
}

- (IBAction)btnShowNeedPop:(id)sender
{
    viwNeed.hidden=NO;
    [self performSelector:@selector(hideNeedPop) withObject:nil afterDelay:1.0];
}

- (void)hideNeedPop
{
    viwNeed.hidden=YES;
}

- (IBAction)btnUploadedDoc:(id)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery", nil];
    sheet.tag=101;
    [sheet showInView:self.view];
}


- (IBAction)btnShowDatePicker:(id)sender
{
    CGPoint pt=ViewdateSelect.center;
    
    ViewdateSelect.hidden=false;
    [self.view bringSubviewToFront:ViewdateSelect];
    imgOverly.hidden=false;
    
    ViewdateSelect.center=CGPointMake(ViewdateSelect.center.x, winSize.height+50+ViewdateSelect.frame.size.height/2.0);
    
    [UIView animateWithDuration:0.4 animations:^{
        ViewdateSelect.center=pt;
    }];
}

-(IBAction)btnRetry:(id)sender
{
    [self GetAccuntdetails];
}

-(void)showAlertMessage:(NSString*)msg
{
    UIAlertView *alt=[[UIAlertView alloc] initWithTitle:appname message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alt show];
}

-(void)SendAccuntdetails
{
    NSDate *seleDate=dtSelect.date;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    dtDay=[NSString stringWithFormat:@"%ld",(long)[calendar component:NSCalendarUnitDay fromDate:seleDate]];
    dtMonth=[NSString stringWithFormat:@"%ld",(long)[calendar component:NSCalendarUnitMonth fromDate:seleDate]];
    dtYear=[NSString stringWithFormat:@"%ld",(long)[calendar component:NSCalendarUnitYear fromDate:seleDate]];
    
    if(txtFirstname.text.length<1)
    {
        [self showAlertMessage:@"Please enter first name."];
        return;
    }
    else if (txtLastname.text.length<1)
    {
        [self showAlertMessage:@"Please enter last name."];
        return;
    }
    else if (txtAddress.text.length<1)
    {
        [self showAlertMessage:@"Please enter address."];
        return;
    }
    else if(txtCity.text.length<1)
    {
        [self showAlertMessage:@"Please enter city."];
        return;
    }
    else if(txtState.text.length<1)
    {
        [self showAlertMessage:@"Please enter state."];
        return;
    }
    else if (txtSSN.text.length<4)
    {
        [self showAlertMessage:@"Please enter 4 digit SSN."];
        return;
    }
    else if (txtRoutingNumber.text.length<1)
    {
        [self showAlertMessage:@"Please enter 9 digit routing number."];
        return;
    }
    else if (txtAccountNumber.text.length<1)
    {
        [self showAlertMessage:@"Please enter account number."];
        return;
    }
    else if (dtDay.length<1 || dtMonth.length<1 || dtYear.length<1)
    {
        [self showAlertMessage:@"Please select date"];
        return;
    }
    else if (txtIP.text.length<1)
    {
        [self showAlertMessage:@"Please enter valid IP address."];
        return;
    }
    else if (txtType.text.length<1)
    {
        [self showAlertMessage:@"Please enter type."];
        return;
    }
    else if (txtpPostalCode.text.length<1)
    {
        [self showAlertMessage:@"Please enter postal code."];
        return;
    }
    else if (txtPersonalId.text.length<1)
    {
        [self showAlertMessage:@"Please enter personal id."];
        return;
    }
    
    NSString *timestamp =[NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
//  timestamp=[timestamp stringByReplacingOccurrencesOfString:@"." withString:@""];
    
//  NSString *timestamp = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970] * 1000];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:txtAddress.text forKey:@"address"];
    [parameters setObject:txtCity.text forKey:@"city"];
    [parameters setObject:txtState.text forKey:@"state"];
    [parameters setObject:@"USA" forKey:@"country"];
    [parameters setObject:txtSSN.text forKey:@"ssn"];
    [parameters setObject:txtRoutingNumber.text forKey:@"routing_number"];
    [parameters setObject:txtAccountNumber.text forKey:@"account_number"];
    [parameters setObject:timestamp forKey:@"date"];    //
    
    
    
    [parameters setObject:txtIP.text forKey:@"ip"];
    [parameters setObject:dtDay forKey:@"day"];
    [parameters setObject:dtMonth forKey:@"month"];
    [parameters setObject:dtYear forKey:@"year"];
    [parameters setObject:txtFirstname.text forKey:@"first_name"];
    [parameters setObject:txtLastname.text forKey:@"last_name"];
    [parameters setObject:txtType.text forKey:@"type"];
    [parameters setObject:txtpPostalCode.text forKey:@"postal_code"];
    [parameters setObject:txtPersonalId.text forKey:@"personal_id"];
    
    AppDelegate *appDelegate=[[UIApplication sharedApplication] delegate];
    
    NSMutableDictionary *jsonDic=[[NSMutableDictionary alloc] init];
    [jsonDic setObject:[appDelegate.userObject objectForKey: @"uid"] forKey:@"user_id"];
    
    NSString *strData=[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil] encoding:NSUTF8StringEncoding];
    
    [jsonDic setObject:strData forKey:@"data"];
    
/*
    "address": "3180 17 st",
    "city": "San Francisco",
    "country": "USA",
    "ssn": "6789",          // 4 Digit
    "routing_number": "110000000",
    "account_number": "000123456789",
    "date": "1467281032",
    "ip": "220.225.144.234",
    "day": "01",
    "month": "05",
    "year": "2015",
    "first_name": "Priyanka",
    "last_name": "Sankhala",
    "type": "individual",
    "postal_code": "94110",
    "personal_id": "123456789"
*/
    [self setModeLoading:YES];
    [manager POST:@"http://partybuzzapp.com/app/1.2/StripeManageAccount.php" parameters:jsonDic success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [Obj_AppDelegate NS_Debug_Log:responseObject];
        
        [self setModeLoading:false];
        
        NSDictionary *resDic=(NSDictionary*)responseObject;
        if([[resDic objectForKey:@"status"] integerValue]==1)
        {
            [self GetAccuntdetails];
        }
        
        if([resDic objectForKey:@"message"])
        {
            UIAlertView *alt=[[UIAlertView alloc] initWithTitle:appname message:[resDic objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alt show];
        }
        else
        {
            UIAlertView *alt=[[UIAlertView alloc] initWithTitle:appname message:@"Some thing went wrong." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Retry", nil];
            [alt show];
        }
        
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"error %@", error.description);
     }];
}

#pragma mark - UIActionSheet Delegates
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==101) //For Camera
    {
        if (buttonIndex==1) //Gallery
        {
            [self openPhotoLibrary];
        }
        else if (buttonIndex==0) //Camera
        {
            [self openCamera];
        }
    }
}

-(void)openPhotoLibrary
{
    BOOL op=[Obj_AppDelegate toCheckPhotoStatus];
    if(op)
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        imagePicker.allowsEditing=YES;
        [imagePicker setEditing:YES];
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self presentViewController:imagePicker animated:YES completion:nil];
        });
    }
}

-(void)openCamera
{
    BOOL op=[Obj_AppDelegate toCheckCameraStatus];
    if(op)
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        imagePicker.allowsEditing=YES;
        [imagePicker setEditing:YES];
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self presentViewController:imagePicker animated:YES completion:nil];
        });
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [Obj_AppDelegate saveImage:@"verifyDoc" image:image];

    [self uploadDocumentImage];
}

-(void)uploadDocumentImage
{
    NSString *strUrl=@"http://partybuzzapp.com/app/1.2/Uploaddoc.php";
    
    NSURL *imgUrl=[NSURL fileURLWithPath:[Obj_AppDelegate getImagePath:@"verifyDoc"]];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setObject:imgUrl forKey:@"doc"];
    [dict setObject:Obj_AppDelegate.uid forKey:@"user_id"];
    
    [self setModeLoading:YES];
    
    [[UNIRest post:^(UNISimpleRequest* request)
    {
         [request setUrl:strUrl];
         [request setParameters:dict];
    }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
    {
        [Obj_AppDelegate NS_Debug_Log:response.body.object];
        
        NSDictionary *resDic=(NSDictionary*)response.body.object;
        if(response)
        {
            if([[resDic objectForKey:@"status"] integerValue]==1)
            {
                NSString *fileId=[resDic objectForKey:@"file_id"];

                dispatch_async(dispatch_get_main_queue(), ^{
                    //update UI in main thread.
                    
                    [self VerifyDoc:fileId];
                });
            }
            else
            {
                [self performSelector:@selector(showAlert:) withObject:[resDic objectForKey:@"messgae"] afterDelay:0.5];
                [self performSelectorOnMainThread:@selector(hideHud) withObject:nil waitUntilDone:YES];
            }
        }
        else
        {
            [self performSelector:@selector(showAlert:) withObject:@"Some thing went wrong. please try again." afterDelay:0.5];
            [self performSelectorOnMainThread:@selector(hideHud) withObject:nil waitUntilDone:YES];
        }
    }];
}

-(void)VerifyDoc:(NSString*)fileId
{
    NSString *strUrl=@"http://partybuzzapp.com/app/1.2/Docverify.php";
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setObject:[NSString stringWithFormat:@"%@",fileId] forKey:@"file_id"];
    [dict setObject:[NSString stringWithFormat:@"%@",Obj_AppDelegate.uid] forKey:@"user_id"];
    
    [[UNIRest post:^(UNISimpleRequest* request)
      {
          [request setUrl:strUrl];
          [request setParameters:dict];
      }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
     {
         [self performSelectorOnMainThread:@selector(hideHud) withObject:nil waitUntilDone:YES];

         [Obj_AppDelegate NS_Debug_Log:response.body.object];
         NSDictionary *resDic=(NSDictionary*)response.body.object;
         if(response)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 //update UI in main thread.
                 [self performSelector:@selector(showAlert:) withObject:[resDic objectForKey:@"messgae"] afterDelay:0.5];
                 
                 [self GetAccuntdetails];
             });
         }
         else
         {
             [self performSelector:@selector(showAlert:) withObject:@"Some thing went wrong. please try again." afterDelay:0.5];
         }
     }];
}


-(void)showAlert:(NSString*)msg
{
    UIAlertView *alt=[[UIAlertView alloc] initWithTitle:appname message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alt show];
}


@end