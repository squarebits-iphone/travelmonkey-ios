//
//  ManageAccountVC.h
//  Party Buzz
//
//  Created by Surendra Sharma on 05/07/16.
//  Copyright © 2016 Uros Lorencic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperationManager.h"
#import "AppDelegate.h"

@class AppDelegate;

@interface ManageAccountVC : UIViewController<UITextViewDelegate,UIPickerViewDelegate,UIActionSheetDelegate>
{
    //////   ---- Show Details ----
    IBOutlet UILabel *shwAccountNumber;
    IBOutlet UILabel *shwBankName;
    IBOutlet UILabel *shwUserName;
    IBOutlet UILabel *shwDescription;
    
    AppDelegate *appdel;
    
    //////   ---- Entry ScrollView ----
    IBOutlet UIView *viwFillForm;

    IBOutlet UIView *viwShowDetail;
    IBOutlet UIScrollView *scrlViewShowDetail;

    
    IBOutlet UIImageView *imgOverly;
    IBOutlet UIView *ViewdateSelect;
    CGSize winSize;
    
    IBOutlet UIButton *btnDoneSelectDate;
    IBOutlet UIDatePicker *dtSelect;
    
    IBOutlet UITextField *txtFirstname;
    IBOutlet UITextField *txtLastname;
    
    IBOutlet UITextView *txtAddress;
    IBOutlet UILabel *lblAddress;
    
    IBOutlet UITextField *txtCity;
    IBOutlet UITextField *txtState;
    IBOutlet UITextField *txtSSN;
    IBOutlet UITextField *txtRoutingNumber;
    IBOutlet UITextField *txtAccountNumber;
    
    IBOutlet UILabel *lblDateShow;
    NSString *dtDay;
    NSString *dtMonth;
    NSString *dtYear;
    
    IBOutlet UITextField *txtIP;
    IBOutlet UITextField *txtType;
    IBOutlet UITextField *txtpPostalCode;
    IBOutlet UITextField *txtPersonalId;
    
    IBOutlet UIButton *btnDone;
    IBOutlet UIScrollView *scrlView;
    IBOutlet UIButton *btnRetry;
    
    IBOutlet UIButton *btnInfo;
    IBOutlet UIButton *btnUpload;
    IBOutlet UIView *viwNeed;
    
    IBOutlet UILabel *lblVerify;
}

- (IBAction)btnDoneEntery:(id)sender;
- (IBAction)btnShowDatePicker:(id)sender;
- (IBAction)btnHideDate:(id)sender;

- (IBAction)btnShowNeedPop:(id)sender;
- (IBAction)btnUploadedDoc:(id)sender;
@end