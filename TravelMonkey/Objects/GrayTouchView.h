//
//  GrayTouchView.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 2/27/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GrayTouchView : UIView

-(void)addToView:(UIView*)superView shouldRemoveAutomatically:(BOOL)removeAutomatically;

@end
