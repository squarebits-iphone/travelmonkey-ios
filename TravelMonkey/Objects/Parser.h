//
//  Parser.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/11/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TripDetails.h"
#import "UserObject.h"

@interface Parser : NSObject

-(UserObject*)getUserDetail:(NSDictionary*)dictionary;
-(NSMutableArray*)getUserArray:(NSArray*)array;
-(NSMutableArray*)getTripArray:(NSArray*)array;
-(TripDetails*)getTripDetails:(NSMutableDictionary*)dic;
-(NSMutableArray*)getPhotoArray:(NSMutableArray*)aryPhotos;

@end
