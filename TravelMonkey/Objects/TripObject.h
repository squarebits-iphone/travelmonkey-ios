//
//  TripObject.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/30/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TripObject : NSObject


@property (nonatomic,strong) NSString *strGuideId;
@property (nonatomic,strong) NSString *strBookingId;
@property (nonatomic,strong) NSString *strNumberOfPerson;
@property (nonatomic,strong) NSString *strPrice;
@property (nonatomic,strong) NSString *strStartTime;
@property (nonatomic,strong) NSString *strEndTime;
@property (nonatomic,strong) NSString *strStatus;
@property (nonatomic,strong) NSString *strDuration;
@property (nonatomic,strong) NSString *strFirstName;
@property (nonatomic,strong) NSString *strLastName;
@property (nonatomic,strong) NSString *strImageURL;
@property (nonatomic,strong) NSString *strCity;
@property (nonatomic,strong) NSString *strCountry;
@property (nonatomic,strong) NSString *strBookingTime;
@property (nonatomic,strong) NSString *strUserChatID;
@property (nonatomic,strong) NSString *strTouristID;



@end
