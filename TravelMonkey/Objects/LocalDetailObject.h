//
//  LocalDetailObject.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/10/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GuideBankDetailObject.h"

@interface LocalDetailObject : NSObject

@property (nonatomic,strong) NSString *strAbout;
@property (nonatomic,strong) NSString *strOfferTourist;
@property (nonatomic,strong) NSString *strTransport;
@property (nonatomic,strong) NSMutableArray *aryLanguage;
@property (nonatomic,strong) NSMutableArray *scheduleObjects;
@property (nonatomic,strong) GuideBankDetailObject *guideBankDetailObject;

@end
