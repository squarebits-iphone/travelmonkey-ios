//
//  PCustomScrollView.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 3/7/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCustomScrollView : UIScrollView

@end
