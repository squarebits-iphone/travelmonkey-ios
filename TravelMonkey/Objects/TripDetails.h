//
//  TripDetails.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 11/17/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TripDetails : NSObject

@property (nonatomic,strong) NSString *strBookingId;
@property (nonatomic,strong) NSString *strNumberOfPerson;
@property (nonatomic,strong) NSString *strPrice;
@property (nonatomic,strong) NSString *strStartTime;
@property (nonatomic,strong) NSString *strEndTime;
@property (nonatomic,strong) NSString *strStatus;
@property (nonatomic,strong) NSString *strDuration;

@property (nonatomic,strong) NSString *strTravellerName;
@property (nonatomic,strong) NSString *strTravellerCity;
@property (nonatomic,strong) NSString *strTravellerCountry;
@property (nonatomic,strong) NSString *strTravellerImageURL;
@property BOOL isReviewedByGuide;

@property (nonatomic,strong) NSString *strGuideName;
@property (nonatomic,strong) NSString *strGuideCity;
@property (nonatomic,strong) NSString *strGuideCountry;
@property (nonatomic,strong) NSString *strGuideImageURL;
@property (nonatomic,strong) NSString *strCardDetails;
@property BOOL isReviewedByTraveller;



@end
