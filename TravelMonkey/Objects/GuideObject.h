//
//  GuideObject.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/24/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PriceObject.h"
#import "GuideBankDetailObject.h"

@interface GuideObject : NSObject

@property (nonatomic,strong) NSString *strId;
@property (nonatomic,strong) NSString *strProfileImageURL;
@property (nonatomic,strong) NSString *strFirstName;
@property (nonatomic,strong) NSString *strLastName;
@property (nonatomic,strong) NSString *strAge;
@property (nonatomic,strong) NSString *strDistance;
@property (nonatomic,strong) NSString *strAbout;
@property (nonatomic,strong) NSString *strOfferTourist;
@property (nonatomic,strong) NSString *strTransport;
@property (nonatomic,strong) NSString *strLanguage;
@property (nonatomic,strong) NSString *strTotalReviews;

@property (nonatomic,strong) PriceObject *priceObject;
@property (nonatomic,strong) GuideBankDetailObject *guideBankDetailObject;

@property                    BOOL isMale;

//@property (nonatomic,strong) NSString *strId;
//@property (nonatomic,strong) NSString *strFirstName;
//@property (nonatomic,strong) NSString *strLastName;
//@property (nonatomic,strong) NSString *strEmail;
//@property (nonatomic,strong) NSString *strCity;
//@property (nonatomic,strong) NSString *strCountry;
//@property                    BOOL isMale;
//@property (nonatomic,strong) NSString *strUserType;
//@property (nonatomic,strong) NSString *strStatus;
//@property (nonatomic,strong) NSString *strAuthKey;
//@property (nonatomic,strong) NSString *strProfileImageURL;
//@property (nonatomic,strong) NSString *strSocialId;
//@property (nonatomic,strong) NSString *strSite;
//@property (nonatomic,strong) NSString *strLat;
//@property (nonatomic,strong) NSString *strLong;
//@property (nonatomic,strong) NSString *strAddress;
//@property (nonatomic,strong) NSString *strBirthDate;
//
//
//@property (nonatomic,strong) NSMutableArray *aryPhotos;
//@property LocalDetailObject *locatDetailObject;
//@property TravellerDetailObject *travellerDetailObject;

@end
