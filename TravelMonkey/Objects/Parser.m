//
//  Parser.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/11/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "Parser.h"
#import "UserObject.h"
#import "LocalDetailObject.h"
#import "TravellerDetailObject.h"
#import "GuideObject.h"
#import "PriceObject.h"
#import "LanguageObject.h"
#import "PhotoObject.h"
#import "CreditCard.h"
#import "TripObject.h"
#import "GuideBankDetailObject.h"
#import "TripDetails.h"
#import "ScheduleObject.h"
#import "ReviewObject.h"




@implementation Parser

-(TripDetails*)getTripDetails:(NSMutableDictionary*)dic
{
    TripDetails *object=[[TripDetails alloc]init];
    object.strBookingId=[dic objectForKey:@"booking_id"];
    object.strNumberOfPerson=[dic objectForKey:@"numberofperson"];
    object.strPrice=[dic objectForKey:@"price"];
    object.strStartTime=[App_Delegate getLocalTimeFromGMT:[dic objectForKey:@"start_time"]];
    object.strEndTime=[App_Delegate getLocalTimeFromGMT:[dic objectForKey:@"end_time"]];
    object.strStatus=[dic objectForKey:@"status"];
    object.strDuration=[dic objectForKey:@"duration"];
    
    //Traveller
    object.strTravellerName=[dic objectForKey:@"username"];
    object.strTravellerCity=[dic objectForKey:@"city"];
    object.strTravellerCountry=[dic objectForKey:@"country"];
    object.strTravellerImageURL=[dic objectForKey:@"user_profileimage"];
    
    //Guide
    object.strGuideName=[dic objectForKey:@"guide_name"];
    object.strGuideCountry=[dic objectForKey:@"guide_country"];
    object.strGuideCity=[dic objectForKey:@"guide_city"];
    object.strGuideImageURL=[dic objectForKey:@"guide_profileimage"];
    
    //Card
    object.strCardDetails=[dic objectForKey:@"cc_last_digit"];
    
    //Review
    if ([dic objectForKey:@"review"] && [dic objectForKey:@"review"]!=(id)[NSNull null])
    {
        NSDictionary *dicReview=[dic objectForKey:@"review"];
        
        if ([dicReview isKindOfClass:[NSDictionary class]] && [dicReview objectForKey:@"guide"])
        {
            if ([[dicReview objectForKey:@"guide"] objectForKey:@"review"])
            {
                NSString *strReview=[[dicReview objectForKey:@"guide"] objectForKey:@"review"];
                
                if (strReview!=nil && strReview!=(id)[NSNull null] && strReview.length>0)
                {
                    object.isReviewedByGuide=YES;
                }
            }
        }
        
        if ([dicReview isKindOfClass:[NSDictionary class]] && [dicReview objectForKey:@"traveller"])
        {
            
            if ([[dicReview objectForKey:@"traveller"] objectForKey:@"review"])
            {
                NSString *strReview=[[dicReview objectForKey:@"traveller"] objectForKey:@"review"];
                
                if (strReview!=nil && strReview!=(id)[NSNull null] && strReview.length>0)
                {
                    object.isReviewedByTraveller=YES;
                }
            }
        }
    }
    
    return object;
    
}

-(NSMutableArray*)getTripArray:(NSArray*)array
{
    NSMutableArray *aryTrips=[[NSMutableArray alloc]init];
    
    if (array && array.count>0)
    {
        for (NSDictionary *dic in array)
        {
            TripObject *object=[[TripObject alloc]init];
            object.strGuideId=[dic objectForKey:@"guide_id"];
            object.strBookingId=[dic objectForKey:@"booking_id"];
            object.strNumberOfPerson=[dic objectForKey:@"numberofperson"];
            object.strPrice=[dic objectForKey:@"price"];
            object.strStartTime=[App_Delegate getLocalTimeFromGMT:[dic objectForKey:@"start_time"]];
            object.strEndTime=[App_Delegate getLocalTimeFromGMT:[dic objectForKey:@"end_time"]];
            object.strStatus=[dic objectForKey:@"status"];
            object.strDuration=[dic objectForKey:@"duration"];
            object.strFirstName=[dic objectForKey:@"first_name"];
            object.strLastName=[dic objectForKey:@"last_name"];
            object.strImageURL=[dic objectForKey:@"profile_image"];
            object.strCity=[dic objectForKey:@"city"];
            object.strCountry=[dic objectForKey:@"country"];
            object.strBookingTime=[App_Delegate getLocalTimeFromGMT:[dic objectForKey:@"booking_time"]];
            object.strUserChatID=[dic objectForKey:@"chat_username"];
            object.strTouristID=[dic objectForKey:@"user_id"];
            
            [aryTrips addObject:object];
            
        }
    }
    
    return aryTrips;
}

-(UserObject*)getUserDetail:(NSDictionary*)dictionary1
{
    
    NSMutableDictionary *dictionary=[dictionary1 mutableCopy];
    
    
    UserObject *object=[[UserObject alloc]init];
    
    NSDictionary *userDictionary=[[NSDictionary alloc]init];
    userDictionary=[dictionary objectForKey:@"user"];
    
    if ([userDictionary objectForKey:@"email"])
    {
        object.strEmail=[userDictionary objectForKey:@"email"];
    }
    
    if ([userDictionary objectForKey:@"chat_username"])
    {
        object.strUserChatID=[userDictionary objectForKey:@"chat_username"];
    }
    
    if ([userDictionary objectForKey:@"first_name"])
    {
        object.strFirstName=[userDictionary objectForKey:@"first_name"];
    }
    
    if ([userDictionary objectForKey:@"last_name"])
    {
        object.strLastName=[userDictionary objectForKey:@"last_name"];
    }
    
    if ([userDictionary objectForKey:@"id"])
    {
        object.strId=[userDictionary objectForKey:@"id"];
    }
    
    if ([userDictionary objectForKey:@"offset"])
    {
        object.strOffset=[NSString stringWithFormat:@"%@",[userDictionary objectForKey:@"offset"]];
    }
    
    if ([userDictionary objectForKey:@"profile_image"])
    {
        object.strProfileImageURL=[userDictionary objectForKey:@"profile_image"];
    }
    
    if ([userDictionary objectForKey:@"role"])
    {
        object.strUserType=[userDictionary objectForKey:@"role"];
    }
    
    if ([userDictionary objectForKey:@"site"])
    {
        object.strSite=[userDictionary objectForKey:@"site"];
    }
    
    if ([userDictionary objectForKey:@"auth_key"])
    {
        object.strAuthKey=[userDictionary objectForKey:@"auth_key"];
    }
    
    if ([userDictionary objectForKey:@"city"])
    {
        object.strCity=[userDictionary objectForKey:@"city"];
    }
    
    if ([userDictionary objectForKey:@"country"])
    {
        object.strCountry=[userDictionary objectForKey:@"country"];
    }
    
    if ([userDictionary objectForKey:@"dob"])
    {
        object.strBirthDate=[userDictionary objectForKey:@"dob"];
    }
    
    if ([userDictionary objectForKey:@"location"])
    {
        object.strAddress=[userDictionary objectForKey:@"location"];
    }
    
    if ([userDictionary objectForKey:@"gender"])
    {
        if ([[userDictionary objectForKey:@"gender"] isEqualToString:@"M"])
        {
            object.isMale=YES;
        }
        else
        {
            object.isMale=NO;
        }
    }
    
    if ([userDictionary objectForKey:@"latitude"])
    {
        object.strLat=[userDictionary objectForKey:@"latitude"];
    }
    
    if ([userDictionary objectForKey:@"longitude"])
    {
        object.strLong=[userDictionary objectForKey:@"longitude"];
    }
    
    if ([userDictionary objectForKey:@"status"])
    {
        object.strStatus=[userDictionary objectForKey:@"status"];
    }
    
    //Detail
    if ([dictionary objectForKey:@"detail"] && [[dictionary objectForKey:@"detail"] isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dicDetail=[dictionary objectForKey:@"detail"];
        
        if ([dicDetail objectForKey:@"guide"] && [dicDetail isKindOfClass:[NSDictionary class]])
        {
            object.locatDetailObject=[self getLocalDetails:[dicDetail objectForKey:@"guide"]];
        }
        
        if ([dicDetail objectForKey:@"tourist"] && [dicDetail isKindOfClass:[NSDictionary class]])
        {
            object.travellerDetailObject=[self getTouristDetails:[dicDetail objectForKey:@"tourist"]];
        }
        
        if ([dicDetail objectForKey:@"images"] && [dicDetail isKindOfClass:[NSDictionary class]])
        {
            object.aryPhotos=[self getPhotoArray:[dicDetail objectForKey:@"images"]];
        }
    }
    
    //Reviews ReviewObject
    if ([dictionary objectForKey:@"review_new"] && [[dictionary objectForKey:@"review_new"] isKindOfClass:[NSDictionary class]])
    {
        NSMutableDictionary *aryReviews=[dictionary objectForKey:@"review_new"];
        
        if (aryReviews!=nil && aryReviews!=(id)[NSNull null])
        {
            NSMutableArray *aryreview=[[NSMutableArray alloc]init];
            
            for (NSString *dic in aryReviews)
            {
                [aryreview addObject:[aryReviews objectForKey:dic]];
            }
            
            object.aryReviews=[self getReviewsObjectArray:aryreview];
        }
    }
    else
    {
        [dictionary setObject:@"" forKey:@"review"];
    }
    
    return object;
    
}

-(NSMutableArray*)getReviewsObjectArray:(NSMutableArray*)aryReviews
{
    NSMutableArray *aryReviewObject=[[NSMutableArray alloc]init];
    
    for (NSDictionary *dic in aryReviews)
    {
        ReviewObject *objReview=[[ReviewObject alloc]init];
        
        //GuideReview
        NSMutableDictionary *dicGuideReview=[dic objectForKey:@"guide_review"];
        if (dicGuideReview && [[dicGuideReview objectForKey:@"is_review"] isEqualToString:@"Y"])
        {
            objReview.strGuideName=[dicGuideReview objectForKey:@"review_by"];
            objReview.strGuidePlace=@"City, Country"; //temp
            objReview.strGuideProfilePicURL=[dicGuideReview objectForKey:@"profile_image"];
            objReview.isReviewedByGuide=YES;
            objReview.isPositiveByGuide=[[dicGuideReview objectForKey:@"review"] isEqualToString:@"LIKE"];
            objReview.strGuideComment=[dicGuideReview objectForKey:@"comment"];
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm"];
            NSDate *date = [dateFormat dateFromString:[dicGuideReview objectForKey:@"date"]];
            [dateFormat setDateFormat:@"MMM yyyy"];
            objReview.strGuideDate=[dateFormat stringFromDate:date];
            
            objReview.strGuideCity=[dicGuideReview objectForKey:@"city"];
            objReview.strGuideCountry=[dicGuideReview objectForKey:@"country"];
            
        }
        
        //TravellerReview
        NSMutableDictionary *dicTravellerReview=[dic objectForKey:@"traveller_review"];
        if (dicTravellerReview && [[dicTravellerReview objectForKey:@"is_review"] isEqualToString:@"Y"])
        {
            objReview.strTravellerName=[dicTravellerReview objectForKey:@"review_by"];
            objReview.strTravellerPlace=@"City, Country"; //temp
            objReview.strTravellerProfilePicURL=[dicTravellerReview objectForKey:@"profile_image"];
            objReview.isReviewedByTraveller=YES;
            objReview.isPositiveByTraveller=[[dicTravellerReview objectForKey:@"review"] isEqualToString:@"LIKE"];
            objReview.strTravellerComment=[dicTravellerReview objectForKey:@"comment"];
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm"];
            NSDate *date = [dateFormat dateFromString:[dicGuideReview objectForKey:@"date"]];
            [dateFormat setDateFormat:@"MMM yyyy"];
            objReview.strTravellerDate=[dateFormat stringFromDate:date];
            
            objReview.strTravellerCity=[dicTravellerReview objectForKey:@"city"];
            objReview.strTravellerCountry=[dicTravellerReview objectForKey:@"country"];
        }
        
        
        [aryReviewObject addObject:objReview];
        
    }
    
    return aryReviewObject;
}

-(LocalDetailObject*)getLocalDetails:(NSDictionary*)dicLocal
{
    LocalDetailObject *object=[[LocalDetailObject alloc]init];
    
    if ([dicLocal objectForKey:@"about_us"])
    {
        object.strAbout=[dicLocal objectForKey:@"about_us"];
    }
    
    if ([dicLocal objectForKey:@"offer_tourist"])
    {
        object.strOfferTourist=[dicLocal objectForKey:@"offer_tourist"];
    }
    
    if ([dicLocal objectForKey:@"transport"])
    {
        object.strTransport=[dicLocal objectForKey:@"transport"];
    }
    
    if ([dicLocal objectForKey:@"language"])
    {
        object.aryLanguage=[self getLanguageArray:[dicLocal objectForKey:@"language"]];
    }
    
    if ([dicLocal objectForKey:@"schedule"] && [dicLocal isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dicPrice=[dicLocal objectForKey:@"schedule"];
        
        if ([dicPrice isKindOfClass:[NSDictionary class]])
        {
//            NSString *str=@"{\"1\":[{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"},{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"}],\"2\":[{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"},{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"}],\"3\":[{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"},{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"}],\"4\":[{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"},{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"}],\"5\":[{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"},{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"}],\"6\":[{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"},{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"}],\"7\":[{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"},{\"price\":\"50\",\"start_time\":\"7:00\",\"end_time\":\"10:00\",\"is_open\":\"1\"}]}";
//            
//            NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
//            dicPrice = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            object.scheduleObjects=[self getScheduleAry:[dicPrice mutableCopy]];
        }
    }
    
    if ([dicLocal objectForKey:@"back_detail"] && [dicLocal isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dicPrice=[dicLocal objectForKey:@"back_detail"];
        
        if ([dicPrice isKindOfClass:[NSDictionary class]])
        {
            object.guideBankDetailObject=[self getGuideBankDetails:dicPrice];
        }
    }
    
    return object;
}

-(NSMutableArray*)getLanguageArray:(NSString*)string
{
    NSMutableArray *aryLang=[[NSMutableArray alloc]init];
    
    NSData* data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *values = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    if (values && values.count>0)
    {
        for (NSDictionary *dicLang in values)
        {
            LanguageObject *object=[[LanguageObject alloc]init];
            object.strLanguageName=[dicLang objectForKey:@"Language"];
            object.strLanguageLevel=[dicLang objectForKey:@"type"];
            
            [aryLang addObject:object];
        }
    }
    
    return aryLang;
    
}

-(NSMutableArray*)getPhotoArray:(NSMutableArray*)aryPhotos
{
    
    NSMutableArray *aryLang=[[NSMutableArray alloc]init];
    
    
    if (aryPhotos && aryPhotos.count>0)
    {
        for (NSDictionary *dicLang in aryPhotos)
        {
            PhotoObject *object=[[PhotoObject alloc]init];
            
            NSData *data = [[dicLang objectForKey:@"caption"] dataUsingEncoding:NSUTF8StringEncoding];
            NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
            
            object.strCaption=goodValue;
            object.strImageURL=[dicLang objectForKey:@"large"];
            object.strThumbURL=[dicLang objectForKey:@"thumb"];
            
            if ([dicLang objectForKey:@"id"])
            {
                object.strId=[dicLang objectForKey:@"id"];
            }
            
            if ([dicLang objectForKey:@"role"])
            {
                object.strRole=[dicLang objectForKey:@"role"];
            }
            
            [aryLang addObject:object];
        }
    }
    
    return aryLang;
    
}

-(TravellerDetailObject*)getTouristDetails:(NSDictionary*)dicTourist
{
    
    TravellerDetailObject *object=[[TravellerDetailObject alloc]init];
    
    if ([dicTourist objectForKey:@"about_us"])
    {
        object.strAbout=[dicTourist objectForKey:@"about_us"];
    }
    
    if ([dicTourist objectForKey:@"counties_visited"])
    {
        object.strVisitedPlaces=[dicTourist objectForKey:@"counties_visited"];
    }
    
    if ([dicTourist objectForKey:@"travel_interest"])
    {
        object.strTravelIntrested=[dicTourist objectForKey:@"travel_interest"];
    }
    
    if ([dicTourist objectForKey:@"card_detail"])
    {
        NSArray *aryCards=[dicTourist objectForKey:@"card_detail"];
        
        if (aryCards && aryCards.count>0)
        {
            object.aryCards=[self getCarddetail:aryCards];
        }
    }
    
    return object;
}

-(NSMutableArray*)getCarddetail:(NSArray*)cards
{
    
    NSMutableArray *aryCards=[[NSMutableArray alloc]init];
    
    for (NSDictionary *dictionary in cards)
    {
        CreditCard *creditCard=[[CreditCard alloc]init];
        
        if ([dictionary objectForKey:@"brand"])
        {
            creditCard.strBrand=[dictionary objectForKey:@"brand"];
        }
        
        if ([dictionary objectForKey:@"last_digit"])
        {
            creditCard.strLastdigits=[dictionary objectForKey:@"last_digit"];
        }
        
        if ([dictionary objectForKey:@"id"])
        {
            creditCard.strId=[dictionary objectForKey:@"id"];
        }
        
        if ([dictionary objectForKey:@"status"])
        {
            creditCard.strActive=[dictionary objectForKey:@"status"];
        }
        
        [aryCards addObject:creditCard];
    }
    
    return aryCards;
}

-(GuideBankDetailObject*)getGuideBankDetails:(NSDictionary*)dicTourist
{
    GuideBankDetailObject *object=[[GuideBankDetailObject alloc]init];
    
    if ([dicTourist objectForKey:@"bank_name"])
    {
        object.strBankName=[dicTourist objectForKey:@"bank_name"];
    }
    
    if ([dicTourist objectForKey:@"last_digit"])
    {
        object.strLastDigit=[dicTourist objectForKey:@"last_digit"];
    }
    
    if ([dicTourist objectForKey:@"status"])
    {
        object.strStatus=[dicTourist objectForKey:@"status"];
    }
    
    if ([dicTourist objectForKey:@"reason"])
    {
        object.strReason=[dicTourist objectForKey:@"reason"];
    }
    
    if (object.strReason==(id)[NSNull null])
    {
            object.strReason=@"";
    }
    
    if (object.strStatus==(id)[NSNull null])
    {
        object.strStatus=@"";
    }
    
    if (object.strBankName==(id)[NSNull null])
    {
        object.strBankName=@"";
    }
    
    if (object.strLastDigit==(id)[NSNull null])
    {
        object.strLastDigit=@"";
    }
    
    return object;
}

-(NSMutableArray *)getScheduleAry:(NSMutableDictionary*)dictionary
{
    NSMutableArray *arraySchedule=[[NSMutableArray alloc]init];
    
    for (int day=1;day<8; day++)
    {
        if ([dictionary objectForKey:[NSString stringWithFormat:@"%d",day]])
        {
            ScheduleObject *object=[[ScheduleObject alloc]init];
            object.strDay=[NSString stringWithFormat:@"%d",day];
            object.aryPriceObject=[[NSMutableArray alloc]init];
            
            NSArray *ary=[dictionary objectForKey:[NSString stringWithFormat:@"%d",day]];
            
            if (ary && ary!=(id)[NSNull null] && [ary isKindOfClass:[NSArray class]] && ary.count>0)
            {
                for (NSDictionary *dic in ary)
                {
                    PriceObject *obj=[self getPriceDetails:dic];
                    [object.aryPriceObject addObject:obj];
                }
            }
            
            [arraySchedule addObject:object];
        }
        else
        {
            NSLog(@"Day not found adding it - %d",day);
            
            ScheduleObject *newObject=[[ScheduleObject alloc]init];
            newObject.strDay=[NSString stringWithFormat:@"%d",day];
            newObject.aryPriceObject=[[NSMutableArray alloc]init];
            
            [arraySchedule addObject:newObject];
        }
    }
    
    return arraySchedule;
}

-(PriceObject*)getPriceDetails:(NSDictionary*)dicTourist
{
    PriceObject *object=[[PriceObject alloc]init];
    
    if ([dicTourist objectForKey:@"price"])
    {
        object.strPrice=[dicTourist objectForKey:@"price"];
    }
    
    if ([dicTourist objectForKey:@"start_time"] && [dicTourist objectForKey:@"start_time"]!=(id)[NSNull null])
    {
        NSLog(@"GMT StartTime : %@ EndTime : %@",[dicTourist objectForKey:@"start_time"],[dicTourist objectForKey:@"end_time"]);
        NSLog(@"Local StartTime : %@ EndTime %@",[App_Delegate getLocalTimeFromGMT:[dicTourist objectForKey:@"start_time"] andDateFormat:@"HH:mm:ss"],[App_Delegate getLocalTimeFromGMT:[dicTourist objectForKey:@"end_time"] andDateFormat:@"HH:mm:ss"]);
        //object.strStartTime=[App_Delegate getLocalTimeFromGMT:[dicTourist objectForKey:@"start_time"] andDateFormat:@"HH:mm:ss"];
        object.strStartTime=[App_Delegate getConvertedDateStringFromDateString:[dicTourist objectForKey:@"local_start_time"] inputDateString:@"HH:mm:ss" andOutputDateFormat:@"HH:mm:ss"];
    }
    
    if ([dicTourist objectForKey:@"end_time"] && [dicTourist objectForKey:@"end_time"]!=(id)[NSNull null])
    {
        //object.strEndTime=[App_Delegate getLocalTimeFromGMT:[dicTourist objectForKey:@"end_time"] andDateFormat:@"HH:mm:ss"];
        object.strEndTime=[App_Delegate getConvertedDateStringFromDateString:[dicTourist objectForKey:@"local_end_time"] inputDateString:@"HH:mm:ss" andOutputDateFormat:@"HH:mm:ss"];
    }
    
    if ([dicTourist objectForKey:@"is_open"])
    {
        object.strIsOpen=[dicTourist objectForKey:@"is_open"];
    }
    
    return object;
}

-(NSMutableArray*)getUserArray:(NSArray*)array
{
    NSMutableArray *ary=[[NSMutableArray alloc]init];
    
    for (NSDictionary *userDictionary in array)
    {
        GuideObject *object=[[GuideObject alloc]init];
        
        if ([userDictionary objectForKey:@"id"])
        {
            object.strId=[userDictionary objectForKey:@"id"];
        }
        
        if ([userDictionary objectForKey:@"first_name"])
        {
            object.strFirstName=[userDictionary objectForKey:@"first_name"];
        }
        
        if ([userDictionary objectForKey:@"last_name"])
        {
            object.strLastName=[userDictionary objectForKey:@"last_name"];
        }
        
        if ([userDictionary objectForKey:@"id"])
        {
            object.strId=[userDictionary objectForKey:@"id"];
        }
        
        if ([userDictionary objectForKey:@"profile_image"])
        {
            object.strProfileImageURL=[userDictionary objectForKey:@"profile_image"];
        }
        
        if ([userDictionary objectForKey:@"gender"])
        {
            if ([[userDictionary objectForKey:@"gender"] isEqualToString:@"M"])
            {
                object.isMale=YES;
            }
            else
            {
                object.isMale=NO;
            }
        }
        
        if ([userDictionary objectForKey:@"about_us"])
        {
            object.strAbout=[userDictionary objectForKey:@"about_us"];
        }
        
        if ([userDictionary objectForKey:@"offer_tourist"])
        {
            object.strOfferTourist=[userDictionary objectForKey:@"offer_tourist"];
        }
        
        if ([userDictionary objectForKey:@"transport"])
        {
            object.strTransport=[userDictionary objectForKey:@"transport"];
        }
        
        if ([userDictionary objectForKey:@"language"])
        {
            object.strLanguage=[userDictionary objectForKey:@"language"];
        }
        
        if ([userDictionary objectForKey:@"age"])
        {
            object.strAge=[userDictionary objectForKey:@"age"];
        }
        
        if ([userDictionary objectForKey:@"distance"])
        {
            object.strDistance=[userDictionary objectForKey:@"distance"];
        }
        
        if ([userDictionary objectForKey:@"schedule"] && [userDictionary isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dicPrice=[userDictionary objectForKey:@"schedule"];
            
            if ([dicPrice isKindOfClass:[NSDictionary class]])
            {
                object.priceObject=[self getPriceDetails:dicPrice];
            }
        }
        
        if ([userDictionary objectForKey:@"back_detail"] && [userDictionary isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dicPrice=[userDictionary objectForKey:@"back_detail"];
            
            if ([dicPrice isKindOfClass:[NSDictionary class]])
            {
                object.guideBankDetailObject=[self getGuideBankDetails:dicPrice];
            }
        }
        
        if ([userDictionary objectForKey:@"review"] && [userDictionary objectForKey:@"review"]!=(id)[NSNull null])
        {
            object.strTotalReviews=[userDictionary objectForKey:@"review"];
        }
        
        
        [ary addObject:object];

    }
    
    return ary;
}

@end
