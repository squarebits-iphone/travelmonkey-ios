//
//  ScheduleObject.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 12/30/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScheduleObject : NSObject

@property (nonatomic,strong) NSString *strDay;
@property (nonatomic,strong) NSMutableArray *aryPriceObject;

@end
