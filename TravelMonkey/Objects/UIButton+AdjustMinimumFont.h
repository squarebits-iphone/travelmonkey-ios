//
//  UIButton+AdjustMinimumFont.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/15/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (AdjustMinimumFont)

@end
