//
//  GuideBankDetailObject.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/21/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GuideBankDetailObject : NSObject

@property (nonatomic,strong) NSString *strBankName;
@property (nonatomic,strong) NSString *strStatus;
@property (nonatomic,strong) NSString *strLastDigit;
@property (nonatomic,strong) NSString *strReason;


@end
