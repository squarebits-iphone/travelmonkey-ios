//
//  UserObject.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/10/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocalDetailObject.h"
#import "TravellerDetailObject.h"

@interface UserObject : NSObject

@property (nonatomic,strong) NSString *strId;
@property (nonatomic,strong) NSString *strOffset;
@property (nonatomic,strong) NSString *strFirstName;
@property (nonatomic,strong) NSString *strLastName;
@property (nonatomic,strong) NSString *strEmail;
@property (nonatomic,strong) NSString *strCity;
@property (nonatomic,strong) NSString *strCountry;
@property                    BOOL isMale;
@property (nonatomic,strong) NSString *strUserType;
@property (nonatomic,strong) NSString *strStatus;
@property (nonatomic,strong) NSString *strAuthKey;
@property (nonatomic,strong) NSString *strProfileImageURL;
@property (nonatomic,strong) NSString *strSocialId;
@property (nonatomic,strong) NSString *strSite;
@property (nonatomic,strong) NSString *strLat;
@property (nonatomic,strong) NSString *strLong;
@property (nonatomic,strong) NSString *strAddress;
@property (nonatomic,strong) NSString *strBirthDate;
@property (nonatomic,strong) NSString *strUserChatID;


@property (nonatomic,strong) NSMutableArray *aryPhotos;
@property (nonatomic,strong) NSMutableArray *aryReviews;
@property (nonatomic,strong) LocalDetailObject *locatDetailObject;
@property (nonatomic,strong) TravellerDetailObject *travellerDetailObject;

@end
