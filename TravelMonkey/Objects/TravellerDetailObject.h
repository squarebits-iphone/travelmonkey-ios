//
//  TravellerDetailObject.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/10/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CreditCard.h"

@interface TravellerDetailObject : NSObject

@property (nonatomic,strong) NSString *strAbout;
@property (nonatomic,strong) NSString *strTravelIntrested;
@property (nonatomic,strong) NSString *strVisitedPlaces;
@property (nonatomic,strong) NSMutableArray *aryCards;


@end
