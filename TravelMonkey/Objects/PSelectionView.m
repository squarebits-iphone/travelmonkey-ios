//
//  PSelectionView.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/3/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "PSelectionView.h"
#import "PSelectionCell.h"

@interface PSelectionView ()

@end

@implementation PSelectionView

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Here's where we create our UISearchController
    
    aryDisplayData = self.arrayData;
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation=NO;
    [self.searchController.searchBar setBackgroundImage:[UIImage new]];
    //[self.searchController.searchBar setScopeBarBackgroundImage:[UIImage imageNamed:@"txt_box.png"]];
    [self.searchController.searchBar setScopeBarBackgroundImage:[UIImage new]];
    [self.searchController.searchBar sizeToFit];
    
    // Add the UISearchBar to the top header of the table view
    tableViewSelection.tableHeaderView = self.searchController.searchBar;
    
    // Hides search bar initially.  When the user pulls down on the list, the search bar is revealed.
    //[tableViewSelection setContentOffset:CGPointMake(0, self.searchController.searchBar.frame.size.height)];
    
    if (self.arraySelectedData==nil)
    {
        self.arraySelectedData=[[NSMutableArray alloc]init];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (self.pageTitle)
    {
        [lblTitle setText:self.pageTitle];
    }
    
    if (self.viewColor)
    {
        [viewTitle setBackgroundColor:self.viewColor];
        [viewStatusBar setBackgroundColor:self.viewColor];
    }
}

#pragma mark- Button Action

-(IBAction)btnBackPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btnSavePressed:(id)sender
{
    [self.arraySelectedData sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self.delegate userSelectedArray:self.arraySelectedData];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark- UISearchController

// When the user types in the search bar, this method gets called.
- (void)updateSearchResultsForSearchController:(UISearchController *)aSearchController {
    NSLog(@"updateSearchResultsForSearchController");
    
    NSString *searchString = aSearchController.searchBar.text;
    NSLog(@"searchString=%@", searchString);
    
    // Check if the user cancelled or deleted the search term so we can display the full list instead.
    if (![searchString isEqualToString:@""]) {
        [arySearchData removeAllObjects];
        arySearchData=[[NSMutableArray alloc]init];
        for (NSString *str in self.arrayData) {
            if ([searchString isEqualToString:@""] || [str localizedCaseInsensitiveContainsString:searchString] == YES) {
                NSLog(@"str=%@", str);
                [arySearchData addObject:str];
            }
        }
        aryDisplayData = arySearchData;
    }
    else {
        aryDisplayData = self.arrayData;
    }
    
    [tableViewSelection reloadData];
}

#pragma mark- Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return aryDisplayData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *strCellIdentifier=@"PSelectionCell";
    
    PSelectionCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
    
    if (cell==nil)
    {
        cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if ([self.arraySelectedData containsObject:[aryDisplayData objectAtIndex:indexPath.row]])
    {
        [cell.imgSelection setHidden:NO];
    }
    else
    {
        [cell.imgSelection setHidden:YES];
    }
    
    [cell.labelName setText:[aryDisplayData objectAtIndex:indexPath.row]];
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([self.arraySelectedData  containsObject:[aryDisplayData objectAtIndex:indexPath.row]])
    {
        [self.arraySelectedData removeObject:[aryDisplayData objectAtIndex:indexPath.row]];
    }
    else
    {
        [self.arraySelectedData addObject:[aryDisplayData objectAtIndex:indexPath.row]];
    }
    
    [tableViewSelection reloadData];
    //[self.delegate userSelected:[NSString stringWithFormat:@"%@",[aryDisplayData objectAtIndex:indexPath.row]]];
    //[self dismissViewControllerAnimated:YES completion:nil];
}



@end
