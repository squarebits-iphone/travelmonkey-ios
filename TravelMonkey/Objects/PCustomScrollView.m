//
//  PCustomScrollView.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 3/7/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "PCustomScrollView.h"
#import "PageView_AboutVCViewController.h"

@implementation PCustomScrollView

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *result = [super hitTest:point withEvent:event];
    
    if ([result isKindOfClass:[UITableView class]])
    {
        UITableView *tableView = (UITableView *) [[result.superview superview] superview];
        NSLog(@"TableView touch");
        return tableView;
    }
    
    NSLog(@"Scrollview touch");
    return result;
}

@end
