//
//  PriceObject.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/25/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PriceObject : NSObject

@property (nonatomic,strong) NSString *strPrice;
@property (nonatomic,strong) NSString *strStartTime;
@property (nonatomic,strong) NSString *strEndTime;
@property (nonatomic,strong) NSString *strIsOpen;


@end
