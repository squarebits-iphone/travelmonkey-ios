//
//  PSelectionView.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/3/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PSelectionDelegate <NSObject>

@optional
-(void)userSelectedArray:(NSMutableArray*)arySelectedData;

@end

@interface PSelectionView : UIViewController<UISearchControllerDelegate,UISearchResultsUpdating,UISearchBarDelegate>
{
    IBOutlet UITableView *tableViewSelection;
    IBOutlet UILabel *lblTitle;
    IBOutlet UIView *viewStatusBar;
    IBOutlet UIView *viewTitle;
    NSMutableArray *arySearchData;
    NSMutableArray *aryDisplayData;
    UISearchBar *searchBar;
    
}

@property (nonatomic, strong) UISearchController * searchController;
@property (nonatomic,strong) NSMutableArray *arrayData;
@property (nonatomic,strong) NSMutableArray *arraySelectedData;
@property (nonatomic,strong) NSString *pageTitle;
@property (nonatomic,strong) UIColor *viewColor;


//Delegate
@property (nonatomic, weak) id <PSelectionDelegate> delegate;


@end
