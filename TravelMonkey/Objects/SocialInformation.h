//
//  SocialInformation.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SocialInformation : NSObject

@property (nonatomic,strong) NSString *strFirstName;
@property (nonatomic,strong) NSString *strLastName;
@property (nonatomic,strong) NSString *strEmail;
@property (nonatomic,strong) NSString *strImageURL;
@property (nonatomic,strong) NSString *strSite;
@property (nonatomic,strong) NSString *strId;

@end
