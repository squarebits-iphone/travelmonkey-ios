//
//  APIsList.h
//  AnswerIt
//
//  Created by Ram on 30/10/14.
//  Copyright (c) 2014 Ramprakash. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIsList : NSObject

/*
 
 All the web services will be called by this class. It is having diffrent methods.
 The basic logic is these methods get all the values which are needed to call a webservice/api & use UNIREST to call the API.
 Result is passed to the respective call (callback object).
 
 */

//GET API without header.
+(void)getAPIWithURL:(NSString *)url Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject;

//GET API with HEADER.
+(void)getAPIWithURL:(NSString *)url andHeader:(NSDictionary*)header Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject;

//POST API
+(void)postAPIWithURL:(NSString *)url andParameters:(NSDictionary*)parameters Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject;

//POST API with HEADER.
+(void)postAPIWithURL:(NSString *)url andHeader:(NSDictionary*)header andParameters:(NSDictionary*)parameters Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject;

+(void)postAPIWithURL:(NSString *)url andHeader:(NSDictionary*)header andParametersJson:(NSString*)parametersJson Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject;

//Delete API with HEADER
+(void)deleteAPIWithURL:(NSString *)url andHeader:(NSDictionary*)header Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject;


//DELETE API with HEADER.
+(void)deleteAPIWithURL:(NSString *)url andHeader:(NSDictionary*)header andParameters:(NSDictionary*)parameters Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject;

+(void)putAPIWithURL:(NSString *)url andHeader:(NSDictionary*)header andParameters:(NSDictionary*)parameters Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject;


@end
