//
//  ReviewObject.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 1/3/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReviewObject : NSObject

@property (nonatomic,strong) NSString *strGuideName;
@property (nonatomic,strong) NSString *strGuidePlace;
@property (nonatomic,strong) NSString *strGuideProfilePicURL;
@property (nonatomic,strong) NSString *strGuideReviewDate;
@property (nonatomic,strong) NSString *strGuideComment;
@property (nonatomic,strong) NSString *strGuideDate;
@property (nonatomic,strong) NSString *strGuideCity;
@property (nonatomic,strong) NSString *strGuideCountry;
@property BOOL isPositiveByGuide;
@property BOOL isReviewedByGuide;

@property (nonatomic,strong) NSString *strTravellerName;
@property (nonatomic,strong) NSString *strTravellerPlace;
@property (nonatomic,strong) NSString *strTravellerProfilePicURL;
@property (nonatomic,strong) NSString *strTravellerReviewDate;
@property (nonatomic,strong) NSString *strTravellerComment;
@property (nonatomic,strong) NSString *strTravellerDate;
@property (nonatomic,strong) NSString *strTravellerCity;
@property (nonatomic,strong) NSString *strTravellerCountry;
@property BOOL isPositiveByTraveller;
@property BOOL isReviewedByTraveller;


@end
