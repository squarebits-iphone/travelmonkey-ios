//
//  APIsList.m
//  AnswerIt
//
//  Created by Ram on 30/10/14.
//  Copyright (c) 2014 Ramprakash. All rights reserved.
//

#import "APIsList.h"
#import "UNIRest.h"
#import "Reachability.h"


@implementation APIsList


#pragma mark- GET

//Get API
+(void)getAPIWithURL:(NSString *)url Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject
{
    
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    //Checking if network is available or not.
    if (networkStatus==NotReachable)
    {
        //Network not available show user a alert.
        [App_Delegate showAlertWithMessage:@"No internet connection." inViewController:objcallbackObject];
        [objcallbackObject performSelectorOnMainThread:selector withObject:@"" waitUntilDone:NO];
    }
    else
    {
        //Network is available.
        __block NSMutableArray *array;
        __block NSMutableDictionary *dictionary;
        
        //ENCODE url
        
        //url=[url stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        //Initializing request.
        [[UNIRest get:^(UNISimpleRequest * request)
          {
              [request setUrl:url];
              
              NSLog(@"Global Get API : %@",request.url);
              
          }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
         {
             
             //Fetching Reponse.
             array=[response.body.array mutableCopy];
             dictionary=[response.body.object mutableCopy];
             
             NSLog(@"Response - %@",response.body.object);
             
             if (objcallbackObject && [objcallbackObject respondsToSelector:selector])
             {
                 if (dictionary!=nil)
                 {
                     [objcallbackObject performSelectorOnMainThread:selector withObject:dictionary waitUntilDone:NO];
                 }
                 else
                 {
                     [objcallbackObject performSelectorOnMainThread:selector withObject:array waitUntilDone:NO];
                 }
             }
             
         }];
    }
    
}

+(void)getAPIWithURL:(NSString *)url andHeader:(NSDictionary*)header Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    //Network check
    if (networkStatus==NotReachable)
    {
        [App_Delegate showAlertWithMessage:@"No internet connection." inViewController:objcallbackObject];
        [objcallbackObject performSelectorOnMainThread:selector withObject:@"" waitUntilDone:NO];
    }
    else
    {
        //__block NSMutableArray *array;
        __block NSDictionary *dicResponse;
        
        url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        url=[url stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        
        
        //Initializing request
        [[UNIRest get:^(UNISimpleRequest * request)
          {
              [request setUrl:url];
              [request setHeaders:header];
              
              NSLog(@"Global Get API : %@",request.url);
              
          }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
         {
             NSData *rawBody = response.rawBody;
             NSString *strResponse = [[NSString alloc] initWithData:rawBody encoding:NSUTF8StringEncoding];
             NSLog(@"Response %@",strResponse);
             
             dicResponse=response.body.object;
             
             if (dicResponse==nil)
             {
                 dicResponse=[response.body.array objectAtIndex:0];
             }
             
             if (objcallbackObject && [objcallbackObject respondsToSelector:selector])
                 [objcallbackObject performSelectorOnMainThread:selector withObject:dicResponse waitUntilDone:NO];
             
         }];
        
    }

}

#pragma mark- PUT

+(void)putAPIWithURL:(NSString *)url andHeader:(NSDictionary*)header andParameters:(NSDictionary*)parameters Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    //Network check
    if (networkStatus==NotReachable)
    {
        [App_Delegate showAlertWithMessage:@"No internet connection." inViewController:objcallbackObject];
        [objcallbackObject performSelectorOnMainThread:selector withObject:@"" waitUntilDone:NO];
    }
    else
    {
        __block NSDictionary *dicResponse;
        
        //Content-Type: application/json
        NSMutableDictionary *headers =[[NSMutableDictionary alloc]init];
        [headers setObject:@"application/json" forKey:@"Content-Type"];
        [headers setObject:@"utf-8" forKey:@"Accept-Encoding"];
        [headers addEntriesFromDictionary:header];
        
        
        NSError * error;
        NSData *json = [NSJSONSerialization dataWithJSONObject:parameters
                                                       options:0
                                                         error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        // This will be the json string in the preferred format
        //jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
        
        // And this will be the json data object
        NSData *processedData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
        
        [[UNIRest putEntity:^(UNIBodyRequest *request){
            
            [request setUrl:url];
            [request setHeaders:headers];
            [request setBody:processedData];
            
        }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
         {
             
             // NSInteger code = response.code;
             //NSDictionary *responseHeaders = response.headers;
             //UNIJsonNode *body = response.body;
             
             
             NSData *rawBody = response.rawBody;
             NSString *strResponse = [[NSString alloc] initWithData:rawBody encoding:NSUTF8StringEncoding];
             NSLog(@"Response %@",strResponse);
             
             dicResponse=response.body.object;
             
             if (dicResponse==nil)
             {
                 if (response.body.array.count>0)
                 {
                     dicResponse=[response.body.array objectAtIndex:0];
                 }
             }
             
             if (objcallbackObject && [objcallbackObject respondsToSelector:selector])
                 [objcallbackObject performSelectorOnMainThread:selector withObject:dicResponse waitUntilDone:NO];
             
         }];
    }
}


#pragma mark- POST

+(void)postAPIWithURL:(NSString *)url andParameters:(NSDictionary*)parameters Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    //Network check
    if (networkStatus==NotReachable)
    {
        [App_Delegate showAlertWithMessage:@"No internet connection." inViewController:objcallbackObject];
        [objcallbackObject performSelectorOnMainThread:selector withObject:@"" waitUntilDone:NO];
    }
    else
    {
        __block NSDictionary *dicResponse;
        
        //Content-Type: application/json
        NSMutableDictionary *headers =[[NSMutableDictionary alloc]init];
        [headers setObject:@"application/json" forKey:@"Content-Type"];
        [headers setObject:@"utf-8" forKey:@"Accept-Encoding"];
        
        NSError * error;
        NSData *json = [NSJSONSerialization dataWithJSONObject:parameters
                                                       options:0
                                                         error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        // This will be the json string in the preferred format
        //jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
        
        // And this will be the json data object
        NSData *processedData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        [[UNIRest postEntity:^(UNIBodyRequest *request){
            
            [request setUrl:url];
            [request setHeaders:headers];
            [request setBody:processedData];
            
        }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
         {
             
             //NSInteger code = response.code;
             //NSDictionary *responseHeaders = response.headers;
             //UNIJsonNode *body = response.body;
             NSData *rawBody = response.rawBody;
             NSString *strResponse = [[NSString alloc] initWithData:rawBody encoding:NSUTF8StringEncoding];
             NSLog(@"Response %@",strResponse);
             
             dicResponse=response.body.object;
             
             if (dicResponse==nil)
             {
                 dicResponse=[response.body.array objectAtIndex:0];
             }
             
             if (objcallbackObject && [objcallbackObject respondsToSelector:selector])
                 [objcallbackObject performSelectorOnMainThread:selector withObject:dicResponse waitUntilDone:NO];
             
         }];
    }
}

+(void)postAPIWithURL:(NSString *)url andHeader:(NSDictionary*)header andParameters:(NSDictionary*)parameters Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    //Network check
    if (networkStatus==NotReachable)
    {
        [App_Delegate showAlertWithMessage:@"No internet connection." inViewController:objcallbackObject];
        [objcallbackObject performSelectorOnMainThread:selector withObject:@"" waitUntilDone:NO];
    }
    else
    {
        __block NSDictionary *dicResponse;
        
        //Content-Type: application/json
        NSMutableDictionary *headers =[[NSMutableDictionary alloc]init];
        [headers setObject:@"application/json" forKey:@"Content-Type"];
        [headers setObject:@"utf-8" forKey:@"Accept-Encoding"];
        [headers addEntriesFromDictionary:header];
        
        
        NSError * error;
        NSData *json = [NSJSONSerialization dataWithJSONObject:parameters
                                                       options:0
                                                         error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        // This will be the json string in the preferred format
        //jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
        NSLog(@"Json : %@",jsonString);
        // And this will be the json data object
        NSData *processedData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        [[UNIRest postEntity:^(UNIBodyRequest *request){
            
            [request setUrl:url];
            [request setHeaders:headers];
            [request setBody:processedData];
            
        }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
         {
             
            // NSInteger code = response.code;
             //NSDictionary *responseHeaders = response.headers;
             //UNIJsonNode *body = response.body;
             
             NSData *rawBody = response.rawBody;
             NSString *strResponse = [[NSString alloc] initWithData:rawBody encoding:NSUTF8StringEncoding];
             NSLog(@"Response %@",strResponse);
             
             dicResponse=response.body.object;
             
             if (dicResponse==nil)
             {
                 if (response.body.array.count>0)
                 {
                    dicResponse=[response.body.array objectAtIndex:0];
                 }
             }
             
             if (objcallbackObject && [objcallbackObject respondsToSelector:selector])
                 [objcallbackObject performSelectorOnMainThread:selector withObject:dicResponse waitUntilDone:NO];
             
         }];
    }
}

#pragma mark- DELETE

+(void)deleteAPIWithURL:(NSString *)url andHeader:(NSDictionary*)header Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject
{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    //Network check
    if (networkStatus==NotReachable)
    {
        [App_Delegate showAlertWithMessage:@"No internet connection." inViewController:objcallbackObject];
        [objcallbackObject performSelectorOnMainThread:selector withObject:@"" waitUntilDone:NO];
    }
    else
    {
       // __block NSMutableArray *array;
        __block NSDictionary *dicResponse;
        
        url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        url=[url stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        
        
        //Initializing request
        [[UNIRest delete:^(UNISimpleRequest * request)
          {
              [request setUrl:url];
              [request setHeaders:header];
              
              NSLog(@"Global Get API : %@",request.url);
              
          }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
         {
             NSData *rawBody = response.rawBody;
             NSString *strResponse = [[NSString alloc] initWithData:rawBody encoding:NSUTF8StringEncoding];
             NSLog(@"Response %@",strResponse);
             
             dicResponse=response.body.object;
             
             if (dicResponse==nil)
             {
                 dicResponse=[response.body.array objectAtIndex:0];
             }
             
             if (objcallbackObject && [objcallbackObject respondsToSelector:selector])
                 [objcallbackObject performSelectorOnMainThread:selector withObject:dicResponse waitUntilDone:NO];
         }];
    }
    
}

+(void)deleteAPIWithURL:(NSString *)url andHeader:(NSDictionary*)header andParameters:(NSDictionary*)parameters Selector:(SEL )selector WithCallBackObject:(id)objcallbackObject
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //Network check
    if (networkStatus==NotReachable)
    {
        [App_Delegate showAlertWithMessage:@"No internet connection." inViewController:objcallbackObject];
        [objcallbackObject performSelectorOnMainThread:selector withObject:@"" waitUntilDone:NO];
    }
    else
    {
        __block NSDictionary *dicResponse;
        
        //Content-Type: application/json
        NSMutableDictionary *headers =[[NSMutableDictionary alloc]init];
        [headers setObject:@"application/json" forKey:@"Content-Type"];
        [headers setObject:@"utf-8" forKey:@"Accept-Encoding"];
        [headers addEntriesFromDictionary:header];
        
        NSError * error;
        NSData *json = [NSJSONSerialization dataWithJSONObject:parameters
                                                       options:0
                                                         error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        // This will be the json string in the preferred format
        //jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
        
        // And this will be the json data object
        NSData *processedData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        [[UNIRest deleteEntity:^(UNIBodyRequest *request){
            
            [request setUrl:url];
            [request setHeaders:headers];
            [request setBody:processedData];
            
        }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
         {
             
             //NSInteger code = response.code;
             //NSDictionary *responseHeaders = response.headers;
             //UNIJsonNode *body = response.body;
             NSData *rawBody = response.rawBody;
             NSString *strResponse = [[NSString alloc] initWithData:rawBody encoding:NSUTF8StringEncoding];
             NSLog(@"Response %@",strResponse);
             
             dicResponse=response.body.object;
             
             if (dicResponse==nil)
             {
                 if (response.body.array.count>0)
                 {
                     dicResponse=[response.body.array objectAtIndex:0];
                 }
             }
             
             if (objcallbackObject && [objcallbackObject respondsToSelector:selector])
                 [objcallbackObject performSelectorOnMainThread:selector withObject:dicResponse waitUntilDone:NO];
             
         }];
    }
}



@end
