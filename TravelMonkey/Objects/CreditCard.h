//
//  CreditCard.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/29/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreditCard : NSObject

@property (nonatomic,strong) NSString *strLastdigits;
@property (nonatomic,strong) NSString *strBrand;
@property (nonatomic,strong) NSString *strId;
@property (nonatomic,strong) NSString *strActive;



@end
