//
//  PhotoObject.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/1/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotoObject : NSObject

@property (nonatomic,strong) NSString *strImageURL;
@property (nonatomic,strong) NSString *strThumbURL;
@property (nonatomic,strong) NSString *strCaption;
@property (nonatomic,strong) NSString *strRole;
@property (nonatomic,strong) NSString *strId;

@end
