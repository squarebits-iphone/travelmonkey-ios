//
//  GrayTouchView.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 2/27/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "GrayTouchView.h"

@implementation GrayTouchView

-(void)addToView:(UIView*)superView shouldRemoveAutomatically:(BOOL)removeAutomatically
{
    [superView addSubview:self];
    [self setFrame:superView.frame];
    [self setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.4]];
    [superView bringSubviewToFront:self];
    
    if (removeAutomatically)
    {
        [self performSelector:@selector(hideView) withObject:self afterDelay:0.2];
    }
}

-(void)hideView
{
    [self removeFromSuperview];
}

@end
