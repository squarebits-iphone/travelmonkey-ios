//
//  LanguageObject.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/2/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LanguageObject : NSObject

@property (nonatomic,strong) NSString *strLanguageName;
@property (nonatomic,strong) NSString *strLanguageLevel;


@end
