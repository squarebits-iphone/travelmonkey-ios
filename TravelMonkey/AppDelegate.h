//
//  AppDelegate.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/11/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import "IIViewDeckController.h"
#import "UserObject.h"
#import "GuideObject.h"
#import "SCLAlertView.h"



//Chat
#import "XMPPManager.h"
#import "DBManager.h"

@import GooglePlaces;

typedef enum : NSUInteger
{
    Tourist,
    Guide,
    Guest
} UserType;


@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate>
{
    CLLocationManager *locationManager;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) UINavigationController *navigationController;
@property UserType userType;
@property (strong,nonatomic) IIViewDeckController *deckController;
@property (strong,nonatomic) NSMutableDictionary *dictRootController;
@property (nonatomic,strong) UserObject *userObject;
@property (strong,nonatomic) CLLocation *CurrentUserLocation; //User location
@property BOOL shouldDisableScrolling;
@property (nonatomic,strong) CLPlacemark *currentPlacemark;

//To Book guide
@property (nonatomic,strong) NSString *strStartTime;
@property (nonatomic,strong) NSString *strEndTime;
@property (nonatomic,strong) GuideObject *selectedGuide;

-(UserType)getCurrentUser;
-(void)GetcurrentLocation;

//Alerts
-(SCLAlertView*)getAlertInstance;
-(void)dropDownalertWithMessage:(NSString*)strMessage;
-(void)showTMAlertWithMessage:(NSString*)message andTitle:(NSString*)strTitle doneButtonTitle:(NSString*)strDoneButtonTitle inViewController:(UIViewController*)viewContorller;
-(void)showAlertWithMessage:(NSString*)message inViewController:(UIViewController*)viewContorller;
-(void)showCustomAlert:(NSString *)strMessage isSucess:(BOOL)isSucess;


-(IIViewDeckController *)setupDeckViewController;
-(BOOL) NSStringIsValidEmail:(NSString *)checkString;
-(int)getAgeFromBirthdate:(NSDate *)birthday;
-(int)getMinutesDifferenceBetweenStartDate:(NSString *)strStartDate endDate:(NSString*)strEndDate withFormaterStyle:(NSString*)style;
-(void)internalLogin;
-(void)askForPush;
-(void)connectToXMPP;
- (void)logUserCrashlytics; //Fabric

+(void)rippleWithView:(UIView *)view center:(CGPoint)center  colorFrom:(UIColor *)colorFrom colorTo:(UIColor *)colorTo;
-(NSDateFormatter *)setLocaleForIOS8:(NSDateFormatter*)dateFormate; //Chat

-(void)setStatusBarBackgroundColor:(UIColor*)color;
-(NSString*)getGMTFromLocalTime:(NSString*)gmtDateString;
-(NSString*)getLocalTimeFromGMT:(NSString*)gmtDateString;
-(NSString*)getGMTFromLocalTime:(NSString*)gmtDateString andDateFormat:(NSString*)strFormat;
-(NSString*)getLocalTimeFromGMT:(NSString*)gmtDateString andDateFormat:(NSString*)strFormat;
-(NSString*)getConvertedDateStringFromDateString:(NSString*)inputDate inputDateString:(NSString*)inputDateFormat andOutputDateFormat:(NSString*)outputDateFormat;
-(NSDate*)getDateWithZeroSecond:(NSDate*)inputDate;

//Chat
-(BOOL)isInternetAvailable;
@property (nonatomic, strong) NSMutableArray *aryChat;
@property (nonatomic, strong) NSMutableArray *aryUnreadChat;
@property (nonatomic, strong) NSMutableArray *aryUnsentMessages;
@property (nonatomic,retain)  NSMutableArray *contactList;
@property (nonatomic,strong)  NSMutableArray *arrAvt;
@property (nonatomic,strong)  NSMutableArray *arrAct;
@property (nonatomic, strong) NSMutableArray *aryFriends;
@property (nonatomic, strong) NSMutableArray *arrOnlineUsers;
@property (nonatomic, strong) NSMutableArray *aryEventChat;
@property (nonatomic) BOOL isPlaySonundForContinueMessage;
@property double lastIncomingMsgTimestamp;
@property (strong, nonatomic) XMPPManager *xmppManager;
@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) NSString *strCurrentChatFriend;
@property (nonatomic, strong) NSString *strCurrentBookingID;
@property (nonatomic, strong) NSString *strGroupCurrentChatFriend;
@property (nonatomic) BOOL isChatOpened;
@property (nonatomic) BOOL isGroupChatOpened;
@property (nonatomic, strong) NSMutableDictionary *dictUnreadMessages;
@property (nonatomic, strong) NSMutableDictionary *dictUnreadMessagesActivity;
@property (nonatomic, strong) NSMutableDictionary *dictUnreadMessagesEvent;
@property (nonatomic) BOOL isChatRowSelected;
@property (nonatomic,strong) NSString *dBName;
@property (nonatomic,strong) NSString *dBPath;



@end

