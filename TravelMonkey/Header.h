//
//  Header.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/11/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#ifndef Header_h
#define Header_h

#import "AppDelegate.h"
#import "APIsList.h"
#import "MBProgressHUD.h"
#import "SCLAlertView.h"
#import "GrayTouchView.h"
#import <Crashlytics/Crashlytics.h>


//Important Things
#define KApplicationId @"1155530292" //temp
#define KPinterestApplicationID @"4881885240819198500"
#define KAppStoreURL @"https://itunes.apple.com/us/app/video-splicer-fast-movie-making/id1062870986?mt=8"
#define KFacebookPageID @"1722378518048691" //temp
#define KFacebookPageName @"videosplicerapp" //temp
#define KTermsAndConditionPage @"http://travelmonkeyapp.com/travelmonkeyapp/App/TermCondition"
#define KPrivacyPolicyPage @"http://travelmonkeyapp.com/travelmonkeyapp/App/PrivatePolicy"
#define KSharingText @"Download Travel Monkey"
#define KSharingImageURL @"http://travelmonkey-dev.s3.amazonaws.com/feature-image.jpg"
#define KInstagramPageURL @"https://www.instagram.com/travelmonkeyapp/"
#define KTwitterPageURL @"https://twitter.com/travelmonkeyapp"
#define KPinterestPageURL @"https://www.pinterest.com/travelmonkeyapp/"

//Keys
#define kGoogleKeyForMap @"AIzaSyBRqdoyPEwVQMy02g5Y1m29LwrEi0cymT0"
#define kStripeTestKey @"pk_test_2jRTXEuM0381rb1OINszleoz"


//Constant
#define KAppName @"Travel Monkey"
#define KGuestAuth @"ef3511b14aeebbe3c5c67042cfff8a060c171635"
#define KNoBookingDetailFound @"You have no booking requests"
#define KNoGuideFound @"Sorry, there are no local guides matching to your filter settings, please adjust them and try again"
#define KNoGuideInCity @"Sorry, we do not have any local guides in this city yet. Please try a different city."
#define App_Delegate ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define KAppColor [UIColor colorWithRed:(230.0/255.0) green:(81.0/255.0) blue:(38.0/255.0) alpha:1.0]
#define KAppColor_Blue [UIColor colorWithRed:(76.0/255.0) green:(132.0/255.0) blue:(209.0/255.0) alpha:1.0]
#define KAppColor_GrayBlue [UIColor colorWithRed:(118.0/255.0) green:(137.0/255.0) blue:(149.0/255.0) alpha:1.0]
#define RGBCOLOR(r, g, b) [UIColor colorWithRed:r/225.0f green:g/225.0f blue:b/225.0f alpha:1]
#define KDangerScrollingTag 950955


#define KUserTourist @"Tourist"
#define KUserGuide @"Guide"
#define KUserGuest @"Guest"

//Notification
#define K_Notification_DoneButtonPressed @"doneButtonPressed"
#define K_Notification_ReloadView @"reloadView"

//APIs
#define mainurl @"http://travelmonkeyapp.com/travelmonkeyapp/index.php" //Development
//#define mainurl @"http://13.56.2.199/travelmonkey" //Staging
#define KBaseURL (mainurl @"/api/")
#define KNativRegister @"user/register"
#define KUploadProfilePic @"Fileupload/ImageUpload"
#define KNativeLogin @"user/login"
#define KSocialLogin @"user/checksociallogin"
#define KSocialRegister @"User/Social_login"
#define KUpdateProfile @"user/profile_update"
#define KSwitchRole @"user/switch_role"
#define KSearchGuide @"guide/search"
#define KCheckGuideInCity @"guide/Guide_Available"
#define KSaveWorkSchedule @"guide/schedule"
#define KChangePassword @"user/changepassword"
#define KAddPhoto @"Fileupload/protfolioimage"
#define KGetUserDetail @"user/get_user_detail"
#define KBookGuide @"booking/GuideBook"
#define KSendStripeToken @"user/MakeStripeCustomer"
#define KGetFutureBooking @"Booking/GetFutureBooking"
#define KGetPastBooking @"Booking/getpastBooking"
#define KAddGuideBankDetail @"BankDetail/AddBank"
#define KGuideBankAccountVerify @"BankDetail/AccountVerify"
#define KGuideBankUploadDoc @"BankDetail/FileUpload"
#define KLogoutUser @"user/Logout"
#define KDeleteCardAPI @"user/DeleteCC"
#define KActiveCardAPI @"user/MakeCCActive"
#define KDeletePhotosAPI @"Fileupload/delete_protfolio_image"

#define KAcceptBookingRequest @"Booking/Accept"
#define KRejectBookingRequest @"Booking/Reject"
#define KCancelBookedRequest @"booking/CancelBooking"
#define KStartTrip @"Booking/GuideStart"
#define KEndTrip @"booking/TripEnd"
#define KGetTripDetail @"Booking/GetBooking"
#define KPostReview @"Booking/Review"
#define KReportUser @"user/ReportUser"
#define KForgetPassword @"user/forgetPassword"
#define KContactUsAPI @"user/Supportmail"

#define KRegisterDeviceTokenForPushNotification @"User/DeviceRegister"
//________________________________________________________________APIs


//Defaults
#define Default_UserType @"userType"
#define Default_AuthKey @"auth_key"
#define Default_SaveUserObject @"userObject"
#define Default_SavedPush @"pushToken"

//Version of OS
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

//Chat _____________________________________________________________________
//https://github.com/robbiehanson/XMPPFramework/wiki/GettingStarted_iOS

typedef enum
{
    MSG_STATUS_CREATED      = 0,
    MSG_STATUS_UPLOADING    = 1,
    MSG_STATUS_UPLOADED     = 2,
    MSG_STATUS_SENT         = 3,
    MSG_STATUS_RECEIVED     = 4,
    MSG_STATUS_READ         =5
} UPLOADSTATUS;


//#define DomainUrl       @"https://api-dev.zyngme.net/sayhello/sayhelloservice/v1";
//#define DomainPushUrl   @"http://65.111.165.6/push_notification"
//#define DOMAIN_NAME     @"travelmonkeyapp.com"
#define DOMAIN_NAME     @"52.9.32.197"

//event
#define SH_Api_UserEvent            @"/user/activity/event/byme"
#define SH_Api_AddUserEvent         @"/user/activity/event"
#define SH_Api_UploadImage          @"/assets/image"
#define SH_Api_GetEventMember       @"/user/activity/event/"
#define SH_Api_DeleteUserEvent      @"/user/activity/event/"
#define SH_Api_SearchEventForActivity   @"/user/activity/event/search"
#define SH_Api_DeleteEventMember    @"/user/activity/event/"
#define SH_Api_GetAllUserEvent      @"/user/activity/event"
#define SH_Api_InviteEvent          @"/user/activity/event/"
#define SH_Api_ExitFromEvent        @"/user/activity/event/"
#define SH_Api_JoinPublicEvent      @"/user/activity/event/"
#define SH_Api_JoinEventByCode      @"/user/activity/event/invite/"

#define SH_Api_DeviceInfo           @"/device_info.php"
#define SH_Api_UpdateBadgeCount     @"/updateBadgeCount.php"

#define SH_Api_StartSession         @"/user/startSession"
#define SH_Api_AddPhone             @"/user/phone/add"
#define SH_Api_PhoneValidate        @"/user/phone/validate"
#define SH_Api_UploadingAvatar      @"/user/avatar"
#define SH_Api_UserRegister         @"/user/register"
#define SH_Api_UserEditRegister     @"/user/edit"
#define SH_Api_UserDetailByPhone    @"/user/"
#define SH_Api_UserActivities       @"/user/activities"
#define SH_Api_UpdateLatLong        @"/user/location/current"
#define SH_Api_AddUserActivity      @"/user/activity"
#define SH_Api_MatchActivities      @"/users/match?activityid="
#define SH_Api_DeleteUserActivity   @"/user/activity/"
#define SH_Api_ReportAbuse          @"/user/abuse/"
#define SH_Api_CheckUserIsBlock     @"/user/block/"
#define SH_Api_UserUnblock          @"/user/block/"
#define SH_Api_GetBlockList         @"/user/block"
#define SH_Api_GetAvatarList        @"/user/avatar"
#define SH_Api_GetActivityTypeList  @"/activityTypes"

#define SH_Defaults [NSUserDefaults standardUserDefaults]
#define kMessageUnreadClickedNotification       @"NewUnreadMessageClicked"
#define kInternetNotAvailable                   @"Internet is not available. Please try later."
#define kTYPE_IMAGE                             @"FILE_TRANSFER"
#define kTYPE_VIDEO                             @"FILE_TRANSFER_VIDEO"
#define kTYPE_AUDIO                             @"FILE_TRANSFER_AUDIO"
#define kFileUploadDownloadNotification         @"FileUploadedDownloaded"

#define kSHChatID                               @"SHChatID"
#define kServerLoginSuccess                     @"loginSuccessNotification"
#define kServerLoginFail                        @"loginFailedNotification"
#define kTYPE_BLOCK                             @"BLOCK"
#define kTYPE_UNBLOCK                           @"UNBLOCK"
#define kTYPE_UPDATE                            @"UPDATE"
#define kTYPE_TEXT                              @"CHAT"
#define kXMPPResource                           @"openfireiOS"
#define kSHUnreadMessage                        @"SHUnreadMessages"
#define kSHUnreadMessageActivity                @"SHUnreadMessagesActivity"
#define kSHUnreadMessageEvent                   @"SHUnreadMessagesEvent"
#define kMessageUnreadReceivedNotification      @"NewUnreadMessageReceived"
#define kMessageUnreadReceivedNotificationActivity      @"NewUnreadMessageReceivedActivity"
#define kMessageUnreadReceivedNotificationEvent      @"NewUnreadMessageReceivedEvent"
#define kMessageSentNotification                @"MessageSent"
#define kSHInAppPreview                         @"SHInAppPreview"
#define kSHUserID                               @"SHUserID"
#define kSHChatID                               @"SHChatID"
#define kMessageReceivedNotification            @"NewMessageReceived"
#define kGroupMessageReceivedNotification       @"NewGroupMessageReceived"
#define kSHInAppVibrate                         @"SHInAppVibrate"
#define kMessageReceiptNotification             @"MessageReceivedToFriend"
#define kNickNameConflictNotification           @"nickNameConflictNotification"
#define XMPP_ERROR_CONFILCT                     @"conflict"
#define XMPP_ERROR                              @"error"
#define kServerDisconnect                       @"serverDisconnect"
#define kSHXMPPmyJID                            @"kXMPPmyJID"
#define kSHNewaerID                             @"SHNewaerID"
#define kSHXMPPmyPassword                       @"kXMPPmyPassword"
#define kSelectMessageNotification              @"SelectMessageRecognizer"
#define kTYPE_SEND_READ_STATUS                  @"SEND_READ_STATUS"
#define kCopyButtonClickedOnPopUp               @"COPYCLICKED"
#define kDeleteButtonClickedOnPopUp             @"DELETECLICKED"
#define kForwardButtonClickedOnPopUp            @"FORWARDCLICKED"
#pragma mark - Group Chat Constant String

#define kSHMyXMPPBot                                 @"group-bot"
#define kTYPE_GROUP_CHAT                             @"GROUP_CHAT"
#define kTYPE_REMOVE_MEMBER                          @"GROUP_MEMBER_REMOVED"
#define kTYPE_EXIT_MEMBER                            @"GROUP_MEMBER_LEFT"
#define kTYPE_ADD_MEMBER                             @"GROUP_MEMBER_ADDED"
#define kTYPE_JOIN_MEMBER                            @"GROUP_MEMBER_JOIN"
#define kTYPE_UPDATE_EVENT_INFO                      @"UPDATE_EVENT_INFORMATION"

#define kTYPE_EVENT_INVITATION                       @"EVENT_INVITATION"
#define kTYPE_EVENT_DELETE                           @"EVENT_DELETE"
#define kBlockedUsers                           @"BlockedUsers"
#define kSetOnline                              @"SetOnline"
#define kHeaderSecurity                         @"HeaderSecurity"

#define kIsVibrateOn                            @"isVibrateOn"
#define kIsAudioOn                              @"isAudioOn"

#define kIsFirstTimeLaunch                      @"IsFirstTimeLaunch"
#define kIsFirstTimeLaunchUpdate                @"IsFirstTimeLaunchUpdate"
#define kSHEvent_Join_NewMember   @"JOIN_NEW_MEMBER_NOTIFICATION"
#define kGroupMessageUnreadReceivedNotification @"NewUnreadGroupMessageReceived"
#define PORT                        @"9443"

#define IS_IOS_7 fabs( ( double ) [[[UIDevice currentDevice] systemVersion] floatValue] >= ( double ) 7 )
#define IS_IOS_8                fabs( ( double )(( double ) [[[UIDevice currentDevice] systemVersion] floatValue] >= ( double ) 8 ))
#define SH_SearchActivityListCont 50
#define SH_SearchEventListCont 50
#define SH_EventMemberListCont 50
#pragma mark - Alert Message

#define SH_Alert_NoInternet         @"Please reset your Internet connection and try again."

#define SH_Alert_InvalidNumber      @"Oops! Invalid phone number, please re-enter your phone number."
#define SH_Alert_IncorrectCode      @"Please check your SMS message and re-enter the four digit verification code."
#define SH_Alert_Device_Auth        @"Awesome! Your device has been authenticated."
#define SH_Alert_CreateProfile      @"Let’s create a profile, use the avatars to be discrete."
#define SH_Alert_EnterNickName      @"Weird flow, this time around, let’s start by entering the nickname first."
#define SH_Alert_SelectProfileImage @"You can be discrete and upload a cool avatar or upload a picture, let’s get started."
#define SH_Alert_CurrentLocationNotFound @"Oh wait! Our searches for this location have failed. Try entering a zip code instead."
#define SH_Alert_SelectDays         @"Please select the days that you wish to conduct your activities."
#define SH_Alert_LocationNotFound   @"Select between current location or enter the zip code to save the activity."
#define SH_Alert_SelectActivity     @"Let’s start always by selecting your activity type"
#define SH_Alert_6Activities        @"Awesome! A max of 6 activities can be active. Lets get started."
#define SH_Alert_AvoidWordsDesc     @"Oops lets avoid words that are considered profane."
#define SH_Alert_AvoidLinksDesc     @"No web links please, update your description."
#define SH_Alert_AvoidPhoneNumber   @"Please update your description, Phone numbers are prohibited."
#define SH_Alert_ZipLocationNotFound @"Oh wait! Something is not right, be sure to verify and re-enter the zip code."
#define SH_Alert_ApiFailedError     @"It looks like something went wrong, please try again."

#define SH_Alert_LocationServiceDisable @"The location service on your phone is disabled. Please enable location services."
#define SH_Alert_LocationServiceDisableForApp @"The location services for Sayhello is disabled. Please enable location services for your device."
#define SH_Alert_6ActivitiesLimit   @"You are allowed to have 6 activities at a given time, please delete an activity and create a new one."
#define SH_Alert_DeleteActivity     @"Are you sure you want to delete this activity? If yes! Go ahead press OK."
#define SH_Alert_DeleteActivitySuccess @"You request to delete the activity has been successfully completed."
#define SH_Alert_UserIsBlocked      @"It appears that this user is blocked, you cannot communicate with this user anymore."
#define SH_Alert_NoMatchFound       @"We have not found any matches. We suggest you delete this activity or try again after a few days."
#define SH_Alert_UserNoAvailable    @"Something is not right, the chat user does not seem to be available anymore."
#define SH_Alert_AvoidWordsChat     @"We are trying to ensure that using this app is a civil experience, get off the naughty “Oops lets avoid words that are considered profane.”"

#define SH_Alert_AvoidLinksChat     @"We do not allow any web links in the chat, please update your chat."
#define SH_Alert_AvoidPhoneNumberChat @"We discourage sending out phone numbers for your safety, please update your chat."
#define SH_Alert_BlockUser          @"We understand your decision to block this user, This action cannot be undone anymore. Press OK to block this user."
#define SH_Alert_ChatServerDown     @"This is embarrassing! The server does not seem to be responding. Our team is looking into this, try back using the application in some time."
#define SH_Alert_NotSupportMail     @"Your device is not configured to send out emails, select and configure a default email program and try sending out an email."
#define SH_Alert_NotSupportSMS      @"Your device does not currently support sending out SMS. Please reach out to your carrier to resolve this issue."
#define SH_Alert_FailedToSendSMS    @"We are sorry! We are currently not able to send out an SMS. Request you to try again."

#define SH_Alert_FBPostSuccess      @"Congratulations! The Facebook post was successful."
#define SH_Alert_FBPostFailed       @"We are sorry! We are not able to post your request on Facebook."
//even alert
#define SH_Alert_CreateEventAlert   @"Apologies, you need to first create an activity and then create an event or a group against the activity."

#define SH_Alert_EventDesc          @"Please Enter the event or group description."
#define SH_Alert_EventName          @"Please enter an event or group name."
#define SH_Alert_EventAddress       @"Please Enter the event or group address."
#define SH_Alert_EventZip           @"Please enter the zip code where the event or group is scheduled to be located."
#define SH_Alert_EventStartDate     @"Please select a valid event or group start date."
#define SH_Alert_EventEndDate       @"Please Select the  event or group end date."
#define SH_Alert_EventEndDateValid  @"Please select a valid event or group end date."
#define SH_Alert_EventTime          @"Please Select the  event time."
#define SH_Alert_EventSponsorName   @"Please enter the event or group sponsor name."
#define SH_Alert_EventSponsorImage  @"Please Select the event sponsor image."
#define SH_Alert_DeleteEvent        @"Are you sure you want to delete this event or group? If yes! Go ahead press OK."
#define SH_Alert_DeleteEventSuccess @"Your request to delete the event or group has been successfully completed."
#define SH_Alert_EventMember        @"We have not found any members in this event. We suggest you invite to your friend and other people."
#define SH_Alert_BlockEventMember   @"Are you sure you want to block this user from this event or group? If yes, go ahead press OK."
#define SH_Alert_NoEventMatchFound  @"We have not found any event or group to match your search criteria. We suggest try again after a few days."
#define SH_Alert_ExitEvent          @"Are you sure you want to exit this event or group? If yes, go ahead press OK."
#define SH_Alert_JoinEvent          @"You are currently not a member of this event or group, to join this event/group press OK."

#define SH_Alert_SelectActivityInCreateEvent     @"Select an activity that would like the event or group to be associated."
#define SH_Alert_CreateNewEventAlert     @"Congratulations group XXXXXXX has been created-Invite all users in the activity send invites via textPost this group information on social media"

#define SH_Alert_EnterInviteUserLimit @"Please enter valid number between 0 to 1000. If you would wish to invite all user press All."
#define SH_Alert_EventFBPostSuccess @"Congratulations! Your Facebook post request was successful completed."
#define SH_Alert_EventFBPostFailed  @"We are sorry! We are unable to post your request on Facebook."


#define SH_InviteFriendText         @"SayHello meet & chat with  activity partners in privacy or groups"

#define SH_ShareMail_Text           @"Hi,Meet SayHello the mobile app to meet activity partners across the globe,Chat with activity partners in private or group chat."

#define SH_ShareTwitterText         @"Meet & chat with activity partners in privacy or groups"

#define SH_ShareMessage_Text        @"SayHello meet & chat with activity partners in privacy or groups."


#define SH_ShareFB_Text             @"Hi All,Meet SayHello the mobile app to meet activity partners across the globe,Chat with activity partners in private or group chat."

#define IS_IPHONE_4             fabs( ( double )(( double )[ [ UIScreen mainScreen ] bounds ].size.height == ( double )480 ))

#define IS_IPHONE_5             fabs( ( double )(( double )[ [ UIScreen mainScreen ] bounds ].size.height >= ( double )568 ))
#define IS_IPHONE_6             fabs( ( double )(( double )[ [ UIScreen mainScreen ] bounds ].size.height >= ( double )667 ))
#define IS_IPHONE_6_PLUS        fabs( ( double )(( double )[ [ UIScreen mainScreen ] bounds ].size.height >= ( double )736 ))

#endif /* Header_h */

