//
//  CreditCardCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/29/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreditCardCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *imgCardPicture;
@property (nonatomic,strong) IBOutlet UILabel *lblCardNumber;
@property (nonatomic,strong) IBOutlet UIButton *btnEdit;
@property (nonatomic,strong) IBOutlet UIButton *btnDelete;


@end
