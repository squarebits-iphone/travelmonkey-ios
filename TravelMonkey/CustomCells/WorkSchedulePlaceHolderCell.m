//
//  WorkSchedulePlaceHolderCell.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 12/30/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "WorkSchedulePlaceHolderCell.h"

@implementation WorkSchedulePlaceHolderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
