//
//  PSelectionCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/3/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSelectionCell : UITableViewCell

@property IBOutlet UILabel *labelName;
@property IBOutlet UIImageView *imgSelection;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_ImgSelection_width;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_ImgSelection_height;

@end
