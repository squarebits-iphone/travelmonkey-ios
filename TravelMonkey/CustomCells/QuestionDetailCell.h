//
//  QuestionDetailCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionDetailCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIView *viewBackGround;
@property (nonatomic,strong) IBOutlet UILabel *lblAnswer;
@property (nonatomic,strong) IBOutlet UILabel *lblQuestion;

@end
