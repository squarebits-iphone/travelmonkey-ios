//
//  HelpCellVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpCellVC : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblTitle;
@property (nonatomic,strong) IBOutlet UIImageView *imgViewBorder;
@property (nonatomic,strong) IBOutlet UIImageView *imgViewArrow;


@end
