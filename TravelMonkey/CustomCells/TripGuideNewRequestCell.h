//
//  TripGuideNewRequestCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/5/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripGuideNewRequestCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *imgUserProfile;
@property (nonatomic,strong) IBOutlet UIImageView *imgCurrentStatus;
@property (nonatomic,strong) IBOutlet UILabel *lblName;
@property (nonatomic,strong) IBOutlet UILabel *lblPlace;
@property (nonatomic,strong) IBOutlet UILabel *lblDate;
@property (nonatomic,strong) IBOutlet UILabel *lblTime;
@property (nonatomic,strong) IBOutlet UILabel *lblCurrentStatus;
@property (nonatomic,strong) IBOutlet UIButton *btnAcceptRequest;
@property (nonatomic,strong) IBOutlet UIButton *btnRejectRequest;
@property (nonatomic,strong) IBOutlet UILabel *lblRemainingTime;
@property (nonatomic,strong) IBOutlet UIImageView *imgNewChatMessage;
@property (nonatomic,strong) IBOutlet UILabel *lblViewProfile;

@end
