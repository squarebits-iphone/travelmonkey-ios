//
//  TotalReviewsCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/9/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TotalReviewsCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblNegativeCount;
@property (nonatomic,strong) IBOutlet UILabel *lblPositiveCount;


@end
