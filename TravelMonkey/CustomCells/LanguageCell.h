//
//  LanguageCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/2/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIButton *btnLanguage;
@property (nonatomic,strong) IBOutlet UIButton *btnLevel;


@end
