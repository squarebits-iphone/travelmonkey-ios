//
//  DeckCustomCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/11/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeckCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewHighLight;
@property (weak, nonatomic) IBOutlet UILabel *lblOptionName;
@property (weak, nonatomic) IBOutlet UIView *viewRipple;
@property (weak, nonatomic) IBOutlet UIImageView *imgStatus;


@end
