//
//  AddCountryCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/3/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCountryCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIButton *btnAddCountry;


@end
