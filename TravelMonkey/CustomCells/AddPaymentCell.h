//
//  AddPaymentCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/30/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddPaymentCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblAddPayment;


@end
