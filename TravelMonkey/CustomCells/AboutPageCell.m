//
//  AboutPageCell.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/8/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "AboutPageCell.h"

@implementation AboutPageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)layoutSubviews
{
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    [self.lblDescription setPreferredMaxLayoutWidth:CGRectGetWidth(self.lblDescription.frame)];
}

@end
