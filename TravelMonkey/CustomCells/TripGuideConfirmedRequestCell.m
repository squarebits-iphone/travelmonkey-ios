//
//  TripGuideConfirmedRequestCell.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/7/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "TripGuideConfirmedRequestCell.h"

@implementation TripGuideConfirmedRequestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

-(void)layoutSubviews
{
    for (UIButton *btn in self.contentView.subviews)
    {
        if ([btn isKindOfClass:[UIButton class]])
        {
            btn.titleLabel.numberOfLines = 1;
            btn.titleLabel.adjustsFontSizeToFitWidth = YES;
            btn.titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        }
        
        if ([btn isKindOfClass:[UILabel class]])
        {
            UILabel *lbl=btn;
            lbl.minimumScaleFactor=0.5;
            lbl.adjustsFontSizeToFitWidth = YES;
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
