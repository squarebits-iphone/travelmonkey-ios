//
//  BookMeCell_CollectionView.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookMeCell_CollectionView : UICollectionViewCell

@end
