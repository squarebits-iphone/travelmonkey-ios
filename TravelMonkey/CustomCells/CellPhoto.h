//
//  CellPhoto.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/1/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellPhoto : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblCaption;
@property (nonatomic,strong) IBOutlet UIImageView *imgView;
@property (nonatomic,strong) IBOutlet UIImageView *imgViewSelected;



@end
