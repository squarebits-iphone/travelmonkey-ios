//
//  CollectionViewCell.h
//  ShoppingDeals
//
//  Created by Purushottam Sain on 4/14/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblName;
@property (nonatomic,strong) IBOutlet UILabel *lblReviews;
@property (nonatomic,strong) IBOutlet UILabel *lblRatePerHour;
@property (nonatomic,strong) IBOutlet UIImageView *imgviewGuide;


@end
