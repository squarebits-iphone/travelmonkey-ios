//
//  AddLanguageCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/2/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddLanguageCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIButton *lblLabel;


@end
