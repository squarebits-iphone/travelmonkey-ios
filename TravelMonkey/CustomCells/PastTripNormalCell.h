//
//  PastTripNormalCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 11/1/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PastTripNormalCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *imgUserProfile;
@property (nonatomic,strong) IBOutlet UIImageView *imgCurrentStatus;
@property (nonatomic,strong) IBOutlet UILabel *lblName;
@property (nonatomic,strong) IBOutlet UILabel *lblPlace;
@property (nonatomic,strong) IBOutlet UILabel *lblDate;
@property (nonatomic,strong) IBOutlet UILabel *lblTime;
@property (nonatomic,strong) IBOutlet UILabel *lblCurrentStatus;


@end
