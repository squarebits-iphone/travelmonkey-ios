//
//  DaysCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/24/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DaysCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblName;
@property (nonatomic,strong) IBOutlet UIButton *btnSelect;


@end
