//
//  PhotosHeaderView.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 1/31/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosHeaderView : UICollectionReusableView

@property (nonatomic,strong) IBOutlet UILabel *lblTitle;
@property (nonatomic,strong) IBOutlet UIButton *btnAddPhotos;


@end
