//
//  AboutUsCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/11/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUsCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *imgIcon;
@property (nonatomic,strong) IBOutlet UILabel *lblTitle;
@property (nonatomic,strong) IBOutlet UIImageView *imgViewBorder;
@property (nonatomic,strong) IBOutlet UIImageView *imgViewArrow;

@end
