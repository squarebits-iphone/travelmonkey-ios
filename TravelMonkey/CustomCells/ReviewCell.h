//
//  ReviewCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/9/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewCell : UITableViewCell

@property IBOutlet UIImageView *imgProfile;
@property IBOutlet UIImageView *imgLikeDislike;
@property IBOutlet UILabel *lblReviewerName;
@property IBOutlet UILabel *lblReviewerAddress;
@property IBOutlet UILabel *lblReview;
@property IBOutlet UILabel *lblDate;

@property IBOutlet UIImageView *imgProfile_Opposite;
@property IBOutlet UIImageView *imgLikeDislike_Opposite;
@property IBOutlet UILabel *lblReviewerName_Opposite;
@property IBOutlet UILabel *lblReviewerAddress_Opposite;
@property IBOutlet UILabel *lblReview_Opposite;
@property IBOutlet UILabel *lblDate_Opposite;


@property IBOutlet UIView *viewBackground;



@end
