//
//  WorkScheduleCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 12/29/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkScheduleCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIButton *btnONOFF;
@property (nonatomic,strong) IBOutlet UIButton *btnFrom;
@property (nonatomic,strong) IBOutlet UIButton *btnTo;
@property (nonatomic,strong) IBOutlet UITextField *btnPrice;
@property (nonatomic,strong) IBOutlet UIButton *btnDelete;
@property (nonatomic,strong) IBOutlet NSLayoutConstraint *btnDeleteLeading;

@end
