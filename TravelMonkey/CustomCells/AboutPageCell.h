//
//  AboutPageCell.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/8/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutPageCell : UITableViewCell

@property IBOutlet UILabel *lblTitle;
@property IBOutlet UILabel *lblDescription;


@end
