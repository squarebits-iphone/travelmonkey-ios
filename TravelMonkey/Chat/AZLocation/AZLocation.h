//
//  AZLocation.h
//  SayHello
//
//  Created by Alex on 26.08.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^CompletionBlock)(CLLocationCoordinate2D coordinate);
typedef void (^FailureBlock)(NSError *error);

@interface AZLocation : NSObject <CLLocationManagerDelegate>

@property (nonatomic, copy) CompletionBlock completionBlock;
@property (nonatomic, copy) FailureBlock failureBlock;

+ (void)getUserLocation:(CompletionBlock)complete;
+ (void)getUserLocation:(CompletionBlock)complete fail:(FailureBlock)fail;
- (void)startLocation;

+ (void)getCoordinatesFromAddress:(NSString *)address complete:(CompletionBlock)complete fail:(FailureBlock)fail;

@end
