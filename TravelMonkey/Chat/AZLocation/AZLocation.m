//
//  AZLocation.m
//  SayHello
//
//  Created by Alex on 26.08.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import "AZLocation.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"


static AZLocation *azlocation;

@interface AZLocation (){
    CLLocationManager *_locationManager;
}
@end

@implementation AZLocation

+(AZLocation*)sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        azlocation = [[self alloc] init];
    });
    
    return azlocation;
}

+(void)getUserLocation:(CompletionBlock)complete{
    [AZLocation sharedInstance].completionBlock = complete;
    [[AZLocation sharedInstance] startLocation];
}

+(void)getUserLocation:(CompletionBlock)complete fail:(FailureBlock)fail{
    [AZLocation sharedInstance].completionBlock = complete;
    [AZLocation sharedInstance].failureBlock = fail;
    [[AZLocation sharedInstance] startLocation];
}

- (void)startLocation{
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
    }
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
    [_locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    [_locationManager stopUpdatingLocation];
    
    if (_completionBlock) {
        _completionBlock(newLocation.coordinate);
        _completionBlock = nil;
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [_locationManager stopUpdatingLocation];
    if (_failureBlock) {
        _failureBlock(error);
        _failureBlock = nil;
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
        [_locationManager startUpdatingLocation];
}


#pragma mark - Handle Address

+ (void)getCoordinatesFromAddress:(NSString *)address complete:(CompletionBlock)complete fail:(FailureBlock)fail{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSString *r = @"http://maps.google.com/maps/api/geocode/json?";
    
    NSDictionary *params = @{@"sensor" : @"false", @"address" : address};
    
    //NSLog(@"%@\n\n%@\n\n\n\n", r, [params toJSON]);
    
    [manager GET:r parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"getCoordinatesFromAddress: %@\n\n", [responseObject toJSON]);
        
        if ([responseObject[@"results"] count] > 0) {
            //NSDictionary *location = responseObject[@"results"][0][@"geometry"][@"location"];
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(1.0,1.0);
            //CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([location[@"lat"] doubleValue], [location[@"lng"] doubleValue]);
            complete(coordinate);
        }else{
            NSError *error = [NSError errorWithDomain:@"localhost" code:-1 userInfo:@{NSLocalizedDescriptionKey : @"We can't find your address"}];
            fail(error);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@", error.localizedDescription);
        fail(error);
    }];
}

@end
