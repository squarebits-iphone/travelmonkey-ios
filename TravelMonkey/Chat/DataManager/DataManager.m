//
//  DataManager.m
//  SayHello
//
//  Created by Ram on 05/05/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "DataManager.h"
#import "AZLoader.h"
#import "Extend.h"


@implementation DataManager

static DataManager * manager;

#pragma mark - Initialization
+(DataManager*)sharedManager
{
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

#pragma mark - Call Api for get event member list

//Call Api for get event member list
-(void)getEventMember:(NSString*)strEventId
{
    NSString *strEventID = [NSString stringWithFormat:@"%@",strEventId];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:[NSString stringWithFormat:@"EventMemberCountForID-%@",strEventID]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    int randomNumber = arc4random()%1000;
    NSString *strRequestID = [NSString stringWithFormat:@"%.4d",randomNumber];
    
    [[NSUserDefaults standardUserDefaults] setObject:strRequestID forKey:[NSString stringWithFormat:@"RequestForEventID-%@",strEventID]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if ([App_Delegate isInternetAvailable])
    {
        [AZLoader showWithText:@"Loading"];
        [[AFN DataRequestWithDelegate:self] requestEventMember:strEventID associateRequestID:strRequestID];
    }
}

//Call Api for get event member list for more member clicked
-(void)getEventMemberNextPage:(NSString*)strEventId
{
    NSString *strEventID = [NSString stringWithFormat:@"%@",strEventId];
    int randomNumber = arc4random()%1000;
    NSString *strRequestID = [NSString stringWithFormat:@"%.4d",randomNumber];
    [[NSUserDefaults standardUserDefaults] setObject:strRequestID forKey:[NSString stringWithFormat:@"RequestForEventID-%@",strEventID]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSInteger alreadyCount = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"EventMemberCountForID-%@",strEventID]];
    NSInteger totalCount = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"EventMemberTotalCountForID-%@",strEventID]];
    
    if (alreadyCount<totalCount)
    {
        int pageNumber = (int)alreadyCount/SH_EventMemberListCont;
        if ([App_Delegate isInternetAvailable]) {
            [AZLoader showWithText:@"Loading"];
            [[AFN DataRequestWithDelegate:self]requestEventMember:strEventID orPage:pageNumber+1 associateRequestID:strRequestID];
        }
    }
}

//Response of get event member list api
-(void)dataRequestEventMember:(NSString *)error answer:(NSDictionary *)answer forEvent:(NSString *)eventId andRequestID:(NSString *)requestID
{
    [AZLoader dismiss];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopRefreshControls" object:nil];
    NSString *strEventID = [NSString stringWithFormat:@"%@",eventId];
    NSString *strMemberCount= [NSString stringWithFormat:@"%@",[answer objectForKey:@"totalCount"]];
    [[NSUserDefaults standardUserDefaults] setObject:strMemberCount forKey:[NSString stringWithFormat:@"MemberTotalCountForEventID%@",strEventID]];
    if (error.length >0 || answer == nil)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlertMsg" object:SH_Alert_ApiFailedError];
        return;
    }
    NSString *strCurrentRequestID = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"RequestForEventID-%@",strEventID]];
    if (![strCurrentRequestID isEqualToString:requestID])
    {
        return;
    }
    if ([[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"EventMemberCountForID-%@",eventId]] == 0)
    {
        NSString *queryRemoveEventMemeber = [NSString stringWithFormat:@"delete from EventMemberList where userEventId = %d",[eventId intValue]];
        //[App_Delegate.dbManager executeQuery:queryRemoveEventMemeber];
    }
    int totalCount = [[answer objectForKey:@"totalCount"] intValue];
    if (totalCount == 0)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlertMsg" object:SH_Alert_EventMember];
        return;
    }
    NSArray *listDisc = [answer objectForKey:@"listentity"];
    for (NSDictionary *userInfo in listDisc)
    {
        NSString *query;
        if (![[[userInfo objectForKey:@"user"]objectForKey:@"chatAccount"] isKindOfClass:[NSNull class]])
        {
            NSString *strUserName = [[[userInfo objectForKey:@"user"]objectForKey:@"chatAccount"]objectForKey:@"userName"];
            if (strUserName && strUserName.length > 0)
            {
                [App_Delegate.xmppManager sendPresense:@"subscribe" :strUserName];
            }
        }
        NSString *strUserInfo =![[userInfo objectForKey:@"user"]isKindOfClass:[NSNull class]] ? [[userInfo objectForKey:@"user"] toJSON]:@"";
        NSString *strChatInfo =![[[userInfo objectForKey:@"user"] objectForKey:@"chatAccount"]isKindOfClass:[NSNull class]] ?[[[userInfo objectForKey:@"user"] objectForKey:@"chatAccount"] toJSON]:@"";
        NSString *strDeviceInfo =![[[userInfo objectForKey:@"user"]objectForKey:@"devices"]isKindOfClass:[NSNull class]] ?[[[userInfo objectForKey:@"user"] objectForKey:@"devices"] toJSON] :@"";
        NSString *strActType = ![[userInfo objectForKey:@"activityType"]isKindOfClass:[NSNull class]] ?[userInfo objectForKey:@"activityType"]:@"";
        NSString *strActInfo = ![[userInfo objectForKey:@"activities"]isKindOfClass:[NSNull class]] ?[[userInfo objectForKey:@"activities"] toJSON]:@"";
        NSString *strCreatedTimeStamp = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"createdTimestamp"]];
        NSString *strFullUserInfo = ![userInfo isKindOfClass:[NSNull class]] ?[userInfo toJSON]:@"";
        if ([strActInfo containsString:@"'"])
        {
            strActInfo = [strActInfo stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
        }
        if ([strFullUserInfo containsString:@"'"])
        {
            strFullUserInfo = [strFullUserInfo stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
        }
        query = [NSString stringWithFormat:@"insert into EventMemberList(userEventId,admin,avatarUrl,memberId,memberName,memberChatUserName,memberChatPassword,userInfo,chatInfo,deviceInfo,actType,actInfo,createdTime,fullInfo) values('%@','%d','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",eventId,[[userInfo objectForKey:@"admin"] intValue],[[userInfo objectForKey:@"user"] objectForKey:@"avatarUri"],[[userInfo objectForKey:@"user"] objectForKey:@"id"],[[userInfo objectForKey:@"user"] objectForKey:@"nickName"],[[[userInfo objectForKey:@"user"] objectForKey:@"chatAccount"] objectForKey:@"userName"],[[[userInfo objectForKey:@"user"] objectForKey:@"chatAccount"] objectForKey:@"accessHash"],strUserInfo,strChatInfo,strDeviceInfo,strActType,strActInfo,strCreatedTimeStamp,strFullUserInfo];
        //[App_Delegate.dbManager executeQuery:query];
    }
    strCurrentRequestID = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"RequestForEventID-%@",eventId]];
    if (![strCurrentRequestID isEqualToString:requestID])
    {
        [AZLoader dismiss];
        return;
    }
    NSInteger countMemberForThisEvent = listDisc.count;
    NSInteger alreadyCount = 0;
    NSInteger newTotalCount = countMemberForThisEvent;
    if ([[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"EventMemberCountForID-%@",eventId]])
    {
        alreadyCount = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"EventMemberCountForID-%@",eventId]];
        newTotalCount = countMemberForThisEvent + alreadyCount;
        [[NSUserDefaults standardUserDefaults] setInteger:newTotalCount forKey:[NSString stringWithFormat:@"EventMemberCountForID-%@",eventId]];
        [[NSUserDefaults standardUserDefaults] setInteger:totalCount forKey:[NSString stringWithFormat:@"EventMemberTotalCountForID-%@",eventId]];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EventMemberNextPageDataUpdated" object:eventId];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setInteger:newTotalCount forKey:[NSString stringWithFormat:@"EventMemberCountForID-%@",eventId]];
        [[NSUserDefaults standardUserDefaults] setInteger:totalCount forKey:[NSString stringWithFormat:@"EventMemberTotalCountForID-%@",eventId]];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EventMemberDataUpdated" object:eventId];
    }
}

-(void)showAlert:(NSString*)msg
{
    //[App_Delegate showAlertWithMessage:<#(NSString *)#> inViewController:<#(UIViewController *)#>];
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:KAppName message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alertView show];
                            
}

@end
