//
//  DataManager.h
//  SayHello
//
//  Created by Ram on 05/05/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFN.h"

@interface DataManager : NSObject<AFNDelegate>
+ (DataManager*)sharedManager;
-(void)getEventMember:(NSString*)strEventId;
-(void)getEventMemberNextPage:(NSString*)strEventId;
@end
