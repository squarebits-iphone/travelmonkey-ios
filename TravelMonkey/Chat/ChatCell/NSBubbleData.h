//
//  NSBubbleData.h
//
//  Created by Alex Barinov
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
#import "MessageObject.h"



typedef enum _NSBubbleType
{
    BubbleTypeMine = 0,
    BubbleTypeSomeoneElse = 1,
    BubbleTypeNotification = 2
} NSBubbleType;

typedef enum NSBubbleContentType
{
    BubbleContentText = 0,
    BubbleContentImage = 1,
    BubbleContentVideo = 2,
    BubbleContentAudio = 3,
    BubbleContentLocation=4,
    BubbleContentContact=5
} NSBubbleContentType;

@interface NSBubbleData : NSObject

@property (nonatomic) NSBubbleContentType contentType;
@property (nonatomic, strong) MessageObject *msgObj;
@property ( nonatomic, strong) NSString *timeSent;
@property ( nonatomic, strong) NSString *timeSeen;
@property (nonatomic,strong )NSString *strSenderName;
@property ( nonatomic) BOOL isSeen;
@property ( nonatomic) double timeSeenStamp;

@property (readonly, nonatomic, strong) NSDate *date;
@property (readonly, nonatomic) NSBubbleType type;
@property (readonly, nonatomic, strong) UIView *view;
@property (readonly, nonatomic) UIEdgeInsets insets;
@property (nonatomic, strong) UIImage *avatar;
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) UISlider *sliderView;
@property (nonatomic, strong) UIButton *btnView;
@property (nonatomic, strong) UIImageView *imageView;
@property BOOL shouldShowProgress;
@property BOOL isSent;
@property BOOL isReceived;
@property (nonatomic, strong) UILabel *lblStatus;
@property (nonatomic, strong) NSString *strMessageType;
@property (nonatomic, strong) MBProgressHUD *HUD;

-(void)hideProgressView :(NSBubbleData *)data;

- (id)initWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type;
+ (id)dataWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type;

- (id)initWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type;
+ (id)dataWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type;

- (id)initWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress;
+ (id)dataWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress;

+ (id)dataWithVideo:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress;
- (id)initWithVideo:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress;

- (id)initWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets;
+ (id)dataWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets;

+ (id)dataWithDate:(NSDate *)date type:(NSBubbleType)type;
- (id)initWithDate:(NSDate *)date type:(NSBubbleType)type;

+ (id)dataWithDate:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress;
- (id)initWithDate:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress;


#pragma mark - New CHAT bubble

+ (id)dataWithImageView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress;
- (id)initWithImageView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress;

+ (id)dataWithVideoView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress;
- (id)initWithVideoView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress;

+ (id)dataWithAudio:(NSDate *)date type:(NSBubbleType)type;
- (id)initWithAudio:(NSDate *)date type:(NSBubbleType)type;

+ (id)dataWithAudio:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress;
- (id)initWithAudio:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress;


-(UIFont *)setFont:(NSBubbleType)type;

@end
