//
//  NSBubbleData.m
//
//  Created by Alex Barinov
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/


#import "NSBubbleData.h"
#import <QuartzCore/QuartzCore.h>

@implementation NSBubbleData

#pragma mark - Properties

@synthesize date = _date;
@synthesize type = _type;
@synthesize view = _view;
@synthesize insets = _insets;
@synthesize avatar = _avatar;
@synthesize indicatorView = _indicatorView;
@synthesize progressView=_progressView;


#pragma mark - Lifecycle


#pragma mark - Text bubble

const UIEdgeInsets textInsetsMine = {8, 15, 16, 22};
const UIEdgeInsets textInsetsSomeone = {8, 22, 16, 15};

+ (id)dataWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type
{
    return [[NSBubbleData alloc] initWithText:text date:date type:type];
}

- (id)initWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type
{
    
    NSLog(@"Text : %@",text);
    
    UIFont *font =[self setFont:type];
    
    CGSize size = [(text ? text : @"") sizeWithFont:font constrainedToSize:CGSizeMake(220, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    
    UILabel *label;
    
    
    if (size.width<40)
    {
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,40, size.height)];
    }
    else
    {
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    }
    
    NSLog(@"Width size : %f",size.width);
    
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.text = (text ? text : @"");
    label.font = font;
    label.textColor=(type == BubbleTypeMine ? [UIColor whiteColor] : [UIColor blackColor]);
    label.backgroundColor=[UIColor clearColor];
    [self setColor:label type:type];
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? textInsetsMine : textInsetsSomeone);
    return [self initWithView:label date:date type:type insets:insets];
}
#pragma mark - GROUP CHAT Text bubble

+ (id)dataWithTextForGroup:(NSString *)text friend:(NSString *)friend date:(NSDate *)date type:(NSBubbleType)type
{
    return [[NSBubbleData alloc] initWithTextForGroup:text friend:(NSString *)friend date:date type:type];
}

- (id)initWithTextForGroup:(NSString *)text friend:(NSString *)friend date:(NSDate *)date type:(NSBubbleType)type
{
    UIFont *font =[self setFont:type];
    UIFont *font2 = [UIFont fontWithName:@"AvenirNext-Regular" size:14.0];

    CGSize size = [(text ? text : @"") sizeWithFont:font constrainedToSize:CGSizeMake(220, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    CGSize size2 = [(friend ? friend : @"") sizeWithFont:font2 constrainedToSize:CGSizeMake(220, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    
    UIView *newView;
    
    newView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, size2.width, size.height+size2.height)];
    if (size.width>size2.width)
    {
        newView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height+size2.height)];
    }

    
    UILabel *label2= [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size2.width, size2.height)];
    label2.numberOfLines = 0;
    label2.lineBreakMode = NSLineBreakByWordWrapping;
    label2.text = (friend ? friend : @"");
    label2.font = font2;
    label2.textColor=[UIColor blueColor];
    label2.backgroundColor = [UIColor clearColor];

		NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:(friend ? friend : @"")];
		[attributeString addAttribute:NSUnderlineStyleAttributeName
						    value:[NSNumber numberWithInt:1]
						    range:(NSRange){0,[attributeString length]}];
		
		label2.attributedText = [attributeString copy];
		[newView addSubview:label2];
		
	
	
    UILabel *label= [[UILabel alloc] initWithFrame:CGRectMake(0, size2.height, size.width, size.height)];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.text = (text ? text : @"");
    label.font = font;
    [self setColor:label type:type];
 
	
    [newView addSubview:label2];
    [newView addSubview:label];
    UIEdgeInsets insets = (type == BubbleTypeMine ? textInsetsMine : textInsetsSomeone);
    return [self initWithView:newView date:date type:type insets:insets];
}


#pragma mark - Image bubble

const UIEdgeInsets imageInsetsMine = {8, 15, 16, 22};
const UIEdgeInsets imageInsetsSomeone = {8, 22, 16, 15};

+ (id)dataWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type
{
    return [[NSBubbleData alloc] initWithImage:image date:date type:type];
}

- (id)initWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type
{
    CGSize size = image.size;
    float imageRatio=1.0;
    if (size.width>0)
    {
        imageRatio = size.height/size.width;
    }
    {
        size.height /= (size.width / 220);
        size.width = 220;
    }
    
    float maxImageWidth=80;
    UIView *newView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 220, maxImageWidth*imageRatio)];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(137, 0, maxImageWidth, maxImageWidth*imageRatio)];
    newView.backgroundColor=[UIColor clearColor];
    imageView.image = image;
    imageView.layer.cornerRadius = 5.0;
    imageView.layer.masksToBounds = YES;
    
    self.indicatorView= [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.indicatorView.center = CGPointMake(imageView.bounds.size.width / 2, imageView.bounds.size.height /2);
    self.indicatorView.tag=10;
    [self.indicatorView startAnimating];
    [imageView addSubview:self.indicatorView];
    imageView.tag=5;
    [newView addSubview:imageView];
    
    
    self.btnView =[[UIButton alloc] initWithFrame:CGRectMake(20, newView.frame.size.height/2.0, 90, 30)];
    [self.btnView setTitle:@"" forState:UIControlStateNormal];
    [self.btnView setBackgroundColor:[UIColor lightGrayColor]];
    [self.btnView.layer setCornerRadius:4.0];
    [self.btnView setClipsToBounds:YES];
    [self.btnView setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    self.btnView.tag=11;
    self.btnView.hidden=YES;
    [newView addSubview:self.btnView];
    
    self.progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(10, newView.frame.size.height/2.0, 120, 30)];
    [newView addSubview: self.progressView];
    
    
    if (type==BubbleTypeSomeoneElse)
    {
        imageView.frame=CGRectMake(7, 0, maxImageWidth, maxImageWidth*imageRatio);
        self.progressView.frame=CGRectMake(90, newView.frame.size.height/2.0, 120, 30);
        self.btnView.frame=CGRectMake(100, newView.frame.size.height/2.0, 90, 30);
    }
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    return [self initWithView:newView date:date type:type insets:insets];
}


+ (id)dataWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress
{
    return [[NSBubbleData alloc] initWithImage:image date:date type:type shouldShowProgress:shouldShowProgress];
}


- (id)initWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress
{
    CGSize size = image.size;
    float imageRatio=1.0;
    if (size.width>0)
    {
        imageRatio = size.height/size.width;
    }
    {
        size.height /= (size.width / 220);
        size.width = 220;
    }
    
    
    float maxImageWidth=80;
    UIView *newView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 220, maxImageWidth*imageRatio)];
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(137, 0, maxImageWidth, maxImageWidth*imageRatio)];
    newView.backgroundColor=[UIColor clearColor];
    self.imageView.image = image;
    self.imageView.layer.cornerRadius = 5.0;
    self.imageView.layer.masksToBounds = YES;
    self.imageView.tag=5;
    [newView addSubview:self.imageView];
    
    
    self.btnView =[[UIButton alloc] initWithFrame:CGRectMake(20, newView.frame.size.height/2.0-15, 90, 30)];

    self.btnView.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnView.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.btnView setBackgroundColor:[UIColor lightGrayColor]];
    [self.btnView.layer setCornerRadius:4.0];
    [self.btnView setClipsToBounds:YES];
    [self.btnView setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnView addTarget:self action:@selector(viewButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.btnView.tag=11;
    self.btnView.hidden=YES;
    [newView addSubview:self.btnView];
    
    
    if (shouldShowProgress)
    {
        self.indicatorView= [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        self.indicatorView.center = CGPointMake(self.imageView.bounds.size.width / 2, self.imageView.bounds.size.height /2);
        self.indicatorView.tag=10;
        
        [self.indicatorView startAnimating];
        [self.imageView addSubview:self.indicatorView];
        
        self.progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(10, newView.frame.size.height/2.0, 120, 30)];
        
        [newView addSubview: self.progressView];
        
        if (type==BubbleTypeSomeoneElse)
        {
            self.imageView.frame=CGRectMake(7, 0, maxImageWidth, maxImageWidth*imageRatio);
            self.progressView.frame=CGRectMake(90, newView.frame.size.height/2.0, 120, 30);
            self.btnView.frame=CGRectMake(100, newView.frame.size.height/2.0-15, 90, 30);
        }
    }
    else
    {
        self.btnView.hidden=NO;
        [self.indicatorView stopAnimating];
        self.indicatorView.hidden=YES;
        self.progressView.hidden=YES;
        
    }
    
    if (type==BubbleTypeSomeoneElse)
    {
        self.imageView.frame=CGRectMake(7, 0, maxImageWidth, maxImageWidth*imageRatio);
        self.btnView.frame=CGRectMake(113, newView.frame.size.height/2.0-15, 90, 30);
    }
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    return [self initWithView:newView date:date type:type insets:insets];
}

+ (id)dataWithVideo:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress
{
    return [[NSBubbleData alloc] initWithVideo:image date:date type:type shouldShowProgress:shouldShowProgress];
}


- (id)initWithVideo:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress
{
    CGSize size = image.size;
    float imageRatio=1.0;
    if (size.width>0)
    {
        imageRatio = size.height/size.width;
    }
    {
        size.height /= (size.width / 220);
        size.width = 220;
    }
    
    float maxImageWidth=80;
    UIView *newView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 220, maxImageWidth*imageRatio)];
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(137, 0, maxImageWidth, maxImageWidth*imageRatio)];
    newView.backgroundColor=[UIColor clearColor];
    self.imageView.image = image;
    self.imageView.layer.cornerRadius = 5.0;
    self.imageView.layer.masksToBounds = YES;
    self.imageView.tag=5;
    [newView addSubview:self.imageView];
    
    
    self.btnView =[[UIButton alloc] initWithFrame:CGRectMake(20, newView.frame.size.height/2.0-15, 90, 30)];

    self.btnView.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnView.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.btnView setBackgroundColor:[UIColor lightGrayColor]];
    [self.btnView.layer setCornerRadius:4.0];
    [self.btnView setClipsToBounds:YES];
    [self.btnView setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnView addTarget:self action:@selector(viewButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.btnView.tag=11;
    self.btnView.hidden=YES;
    [newView addSubview:self.btnView];
    
    
    if (shouldShowProgress)
    {
        self.indicatorView= [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        self.indicatorView.center = CGPointMake(self.imageView.bounds.size.width / 2, self.imageView.bounds.size.height /2);
        self.indicatorView.tag=10;
        
        [self.indicatorView startAnimating];
        [self.imageView addSubview:self.indicatorView];
        
        self.progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(10, newView.frame.size.height/2.0, 120, 30)];
        
        [newView addSubview: self.progressView];
        
        if (type==BubbleTypeSomeoneElse)
        {
            self.imageView.frame=CGRectMake(7, 0, maxImageWidth, maxImageWidth*imageRatio);
            self.progressView.frame=CGRectMake(90, newView.frame.size.height/2.0, 120, 30);
            self.btnView.frame=CGRectMake(100, newView.frame.size.height/2.0-15, 90, 30);
        }
    }
    else
    {
        self.btnView.hidden=NO;
        [self.indicatorView stopAnimating];
        self.indicatorView.hidden=YES;
        self.progressView.hidden=YES;
        
    }
    
    if (type==BubbleTypeSomeoneElse)
    {
        self.imageView.frame=CGRectMake(7, 0, maxImageWidth, maxImageWidth*imageRatio);
        self.btnView.frame=CGRectMake(113, newView.frame.size.height/2.0-15, 90, 30);
    }
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    return [self initWithView:newView date:date type:type insets:insets];
}


+ (id)dataWithDate:(NSDate *)date type:(NSBubbleType)type
{
    return [[NSBubbleData alloc] initWithDate:date type:type];
}

- (id)initWithDate:(NSDate *)date type:(NSBubbleType)type
{
    UIView *newView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 220, 45)];
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(150, 0, 60, 45)];
    newView.backgroundColor=[UIColor clearColor];
    self.imageView.image = [UIImage imageNamed:@"img_audio"];
    self.imageView.layer.cornerRadius = 5.0;
    self.imageView.layer.masksToBounds = YES;
    self.imageView.tag=5;
    [newView addSubview:self.imageView];
    
    
    self.btnView =[[UIButton alloc] initWithFrame:CGRectMake(0,0, 45, 45)];
    [self.btnView setTitle:@"" forState:UIControlStateNormal];
    [self.btnView setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    [self.btnView setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateSelected];
    [self.btnView setBackgroundColor:[UIColor clearColor]];
    [self.btnView addTarget:self action:@selector(viewButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.btnView.tag=11;
    [newView addSubview:self.btnView];
    
    self.sliderView = [[UISlider alloc] initWithFrame:CGRectMake(40, 0, 100, 45)];
    [self.sliderView addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.sliderView setThumbImage:[UIImage imageNamed:@"sliderThumb"] forState:UIControlStateNormal];
    [self.sliderView setMinimumValue:0];
    [self.sliderView setMinimumTrackTintColor:[UIColor grayColor]];
    [newView addSubview: self.sliderView];
    
    if (type==BubbleTypeSomeoneElse)
    {
        self.imageView.frame=CGRectMake(7, 0, 60, 45);
        self.sliderView.frame=CGRectMake(120, 0, 100, 45);
        self.btnView.frame=CGRectMake(75, 0, 45, 45);
    }
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    return [self initWithView:newView date:date type:type insets:insets];
}


+ (id)dataWithDate:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress
{
    return [[NSBubbleData alloc] initWithDate:date type:type shouldShowProgress:shouldShowProgress];
}


- (id)initWithDate:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress
{
    
    UIView *newView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 220,45)];
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(150, 0, 60, 45)];
    newView.backgroundColor=[UIColor clearColor];
    self.imageView.image = [UIImage imageNamed:@"img_audio"];
    self.imageView.layer.cornerRadius = 5.0;
    self.imageView.layer.masksToBounds = YES;
    self.imageView.tag=5;
    [newView addSubview:self.imageView];
    
    
    self.btnView =[[UIButton alloc] initWithFrame:CGRectMake(20, 2, 90, 30)];
    
    self.btnView.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnView.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.btnView setBackgroundColor:[UIColor lightGrayColor]];
    [self.btnView.layer setCornerRadius:4.0];
    [self.btnView setClipsToBounds:YES];
    [self.btnView setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnView addTarget:self action:@selector(viewButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.btnView.tag=11;
    self.btnView.hidden=YES;
    [newView addSubview:self.btnView];
    
    
    if (shouldShowProgress)
    {
        self.indicatorView= [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        self.indicatorView.center = CGPointMake(self.imageView.bounds.size.width / 2, self.imageView.bounds.size.height /2);
        self.indicatorView.tag=10;
        
        [self.indicatorView startAnimating];
        [self.imageView addSubview:self.indicatorView];
        
        self.progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(10, newView.frame.size.height/2.0, 120, 30)];
        [self.progressView setProgressTintColor:[UIColor greenColor]];
        [newView addSubview: self.progressView];
        
        if (type==BubbleTypeSomeoneElse)
        {
            self.imageView.frame=CGRectMake(7, 0, 60, 45);
            self.progressView.frame=CGRectMake(90, newView.frame.size.height/2.0, 120, 30);
            self.btnView.frame=CGRectMake(100, 2, 90, 30);
        }
    }
    else
    {
        self.btnView.hidden=NO;
        [self.indicatorView stopAnimating];
        self.indicatorView.hidden=YES;
        self.progressView.hidden=YES;
        
    }
    
    if (type==BubbleTypeSomeoneElse)
    {
        self.imageView.frame=CGRectMake(7, 0, 60, 45);
        self.btnView.frame=CGRectMake(113, 2, 90, 30);
    }
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    return [self initWithView:newView date:date type:type insets:insets];
}



-(void)hideProgressView :(NSBubbleData *)data
{
    [self.progressView removeFromSuperview];
    [self.indicatorView removeFromSuperview];
    self.btnView.hidden=NO;
}
#pragma mark - Custom view bubble

+ (id)dataWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets
{
    return [[NSBubbleData alloc] initWithView:view date:date type:type insets:insets];
}

- (id)initWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets  
{
    self = [super init];
    if (self)
    {
        _view = view;
        _date = date;
        _type = type;
        _insets = insets;
    }
    return self;
}



-(IBAction)viewButtonEvent:(UIButton *)sender
{
    NSString *strTag=[NSString stringWithFormat:@"%ld",sender.superview.tag];
}

-(IBAction)sliderValueChanged:(UISlider *)slider
{
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:[NSString stringWithFormat:@"%ld",slider.superview.tag] forKey:@"tag"];
    [dic setObject:[NSString stringWithFormat:@"%f",slider.value] forKey:@"value"];

}



-(UIFont *)setFont:(NSBubbleType)type
{
    UIFont *font;
    font=[UIFont systemFontOfSize:[UIFont systemFontSize]];
    font=[UIFont fontWithName:@"AvenirNext-Regular" size:14.0];
    return font;
    
}

-(void)setColor:(UILabel*)label type:(NSBubbleType)type
{
}

#pragma mark -
#pragma mark - Vleep Custom view bubble
#pragma mark -

+ (id)dataWithImageView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress
{
    return [[NSBubbleData alloc] initWithImageView:image date:date type:type shouldShowProgress:shouldShowProgress];
}


- (id)initWithImageView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress
{
    UIView *newView=[[UIView alloc] initWithFrame:CGRectMake(0, 0,200, 200)];
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 196, 196)];
    newView.backgroundColor=[UIColor clearColor];
    self.imageView.image = image;
    self.imageView.layer.cornerRadius = 5.0;
    self.imageView.layer.masksToBounds = YES;
    self.imageView.tag=5;
    [newView addSubview:self.imageView];
    
    
    self.btnView =[[UIButton alloc] initWithFrame:CGRectMake(2, 2, 195, 195)];
    
    self.btnView.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnView.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.btnView setBackgroundColor:[UIColor clearColor]];
    [self.btnView.layer setCornerRadius:4.0];
    [self.btnView setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.3f]];
    [self.btnView setClipsToBounds:YES];
    [self.btnView setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnView addTarget:self action:@selector(viewButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.btnView.tag=11;
    [newView addSubview:self.btnView];
    
    
    self.lblStatus= [[UILabel alloc] initWithFrame:CGRectMake(5, 180, 100, 20)];
    self.lblStatus.tag=55;
    self.lblStatus.numberOfLines = 1;
    self.lblStatus.lineBreakMode = NSLineBreakByClipping;
    self.lblStatus.text = @"";
    
    self.lblStatus.textColor=[UIColor colorWithRed:244.0f/255.0f green:244.0f/255.0f blue:244.0f/255.0f alpha:1.0f];
    self.lblStatus.backgroundColor = [UIColor clearColor];
    self.lblStatus.font=[UIFont fontWithName:@"AvenirNext-Regular" size:10.0];
    self.lblStatus.textAlignment=NSTextAlignmentLeft;
    
    [newView addSubview:self.lblStatus];
    [newView bringSubviewToFront:self.lblStatus];

    self.HUD = [[MBProgressHUD alloc] init];
    [newView addSubview:self.HUD];
    //self.HUD.mode = MBProgressHUDModeAnnularDeterminate;
    self.HUD.tag=110;
 
    if (shouldShowProgress)
         [self.HUD show:YES];
    
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    return [self initWithView:newView date:date type:type insets:insets];
}

+ (id)dataWithVideoView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress
{
    return [[NSBubbleData alloc] initWithVideoView:image date:date type:type shouldShowProgress:shouldShowProgress];
}


- (id)initWithVideoView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress
{
    UIView *newView=[[UIView alloc] initWithFrame:CGRectMake(0, 0,200, 200)];
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 196, 196)];
    newView.backgroundColor=[UIColor clearColor];
    self.imageView.image = image;
    self.imageView.layer.cornerRadius = 5.0;
    self.imageView.layer.masksToBounds = YES;
    self.imageView.tag=5;
    [newView addSubview:self.imageView];
    
    

    self.btnView =[[UIButton alloc] initWithFrame:CGRectMake(2, 2, 195, 195)];
    
    self.btnView.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnView.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.btnView setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.7f]];
    [self.btnView.layer setCornerRadius:4.0];
    [self.btnView setClipsToBounds:YES];
    [self.btnView setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnView addTarget:self action:@selector(viewButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.btnView.tag=11;
    self.btnView.layer.cornerRadius = 5.0;
    self.btnView.layer.masksToBounds = YES;
    [newView addSubview:self.btnView];
    [newView bringSubviewToFront:self.btnView];
    
    self.lblStatus= [[UILabel alloc] initWithFrame:CGRectMake(5, 180, 100, 20)];
    self.lblStatus.tag=55;
    self.lblStatus.numberOfLines = 1;
    self.lblStatus.lineBreakMode = NSLineBreakByClipping;
    self.lblStatus.text = @"";
    
    self.lblStatus.textColor=[UIColor colorWithRed:244.0f/255.0f green:244.0f/255.0f blue:244.0f/255.0f alpha:1.0f];
    self.lblStatus.backgroundColor = [UIColor clearColor];
    self.lblStatus.font=[UIFont fontWithName:@"AvenirNext-Regular" size:10.0];

    self.lblStatus.textAlignment=NSTextAlignmentLeft;
    
    [newView addSubview:self.lblStatus];
    [newView bringSubviewToFront:self.lblStatus];
   
    
    self.HUD = [[MBProgressHUD alloc] init];
    [newView addSubview:self.HUD];
    //self.HUD.mode = MBProgressHUDModeAnnularDeterminate;
    self.HUD.tag=110;
    if (shouldShowProgress)
         [self.HUD show:YES];
    
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    return [self initWithView:newView date:date type:type insets:insets];
}

+ (id)dataWithAudio:(NSDate *)date type:(NSBubbleType)type
{
    return [[NSBubbleData alloc] initWithAudio:date type:type];
}

- (id)initWithAudio:(NSDate *)date type:(NSBubbleType)type
{
    UIView *newView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 220, 45)];
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(155, 3.5, 38, 38)];
    newView.backgroundColor=[UIColor clearColor];
    self.imageView.image = [UIImage imageNamed:@"img_audio"];
    self.imageView.layer.cornerRadius = 5.0;
    self.imageView.layer.masksToBounds = YES;
    self.imageView.tag=5;
    [newView addSubview:self.imageView];
    
    
    self.btnView =[[UIButton alloc] initWithFrame:CGRectMake(0,0, 45, 45)];
    [self.btnView setTitle:@"" forState:UIControlStateNormal];
    [self.btnView setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    [self.btnView setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateSelected];
    [self.btnView setBackgroundColor:[UIColor clearColor]];
    [self.btnView addTarget:self action:@selector(viewButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.btnView.tag=11;
    [newView addSubview:self.btnView];
    
    self.lblStatus= [[UILabel alloc] initWithFrame:CGRectMake(5, 25, 100, 20)];
    self.lblStatus.tag=55;
    self.lblStatus.numberOfLines = 1;
    self.lblStatus.lineBreakMode = NSLineBreakByClipping;
    self.lblStatus.text = @"";
    
    self.lblStatus.textColor=[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
    self.lblStatus.backgroundColor = [UIColor clearColor];
    self.lblStatus.font=[UIFont fontWithName:@"AvenirNext-Regular" size:10.0];
    self.lblStatus.textAlignment=NSTextAlignmentLeft;
    
    [newView addSubview:self.lblStatus];
    [newView bringSubviewToFront:self.lblStatus];
    
    self.sliderView = [[UISlider alloc] initWithFrame:CGRectMake(40, 0, 100, 45)];
    [self.sliderView addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.sliderView setThumbImage:[UIImage imageNamed:@"sliderThumb"] forState:UIControlStateNormal];
    [self.sliderView setMinimumValue:0];
    [self.sliderView setMinimumTrackTintColor:[UIColor greenColor]];
    [self.sliderView setMaximumTrackTintColor:[UIColor grayColor]];
    [newView addSubview: self.sliderView];
    
    self.HUD = [[MBProgressHUD alloc] init];
    [newView addSubview:self.HUD];
    //self.HUD.mode = MBProgressHUDModeAnnularDeterminate;
    self.HUD.tag=110;

    if (type==BubbleTypeSomeoneElse)
    {
        self.imageView.frame=CGRectMake(7, 3.5, 38, 38);
        self.lblStatus.frame=CGRectMake(117, 3.5, 100, 20);
        self.sliderView.frame=CGRectMake(100, 0, 100, 45);
        self.btnView.frame=CGRectMake(55, 0, 45, 45);
        self.lblStatus.textAlignment=NSTextAlignmentRight;
    }
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    return [self initWithView:newView date:date type:type insets:insets];
}

+ (id)dataWithAudio:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress
{
    return [[NSBubbleData alloc] initWithAudio:date type:type shouldShowProgress:shouldShowProgress];
}


- (id)initWithAudio:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress
{
    
    UIView *newView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 220,45)];
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(155, 3.5, 38, 38)];
    newView.backgroundColor=[UIColor clearColor];
    self.imageView.image = [UIImage imageNamed:@"img_audio"];
    self.imageView.layer.cornerRadius = 5.0;
    self.imageView.layer.masksToBounds = YES;
    self.imageView.tag=5;
    [newView addSubview:self.imageView];

    self.btnView =[[UIButton alloc] initWithFrame:CGRectMake(0, 7, 155, 30)];
    
    self.btnView.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnView.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.btnView setBackgroundColor:[UIColor clearColor]];
    [self.btnView.layer setCornerRadius:10.0];
    [self.btnView setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.0f]];
    [self.btnView setClipsToBounds:YES];
    [self.btnView setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [self.btnView addTarget:self action:@selector(viewButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.btnView.tag=11;
    [newView addSubview:self.btnView];
    
    
    self.lblStatus= [[UILabel alloc] initWithFrame:CGRectMake(5, 30, 100, 15)];
    self.lblStatus.tag=55;
    self.lblStatus.numberOfLines = 1;
    self.lblStatus.lineBreakMode = NSLineBreakByClipping;
    self.lblStatus.text = @"";
    
    self.lblStatus.textColor=[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
    self.lblStatus.backgroundColor = [UIColor clearColor];
    self.lblStatus.font=[UIFont fontWithName:@"AvenirNext-Regular" size:10.0];
    self.lblStatus.textAlignment=NSTextAlignmentLeft;
    
    [newView addSubview:self.lblStatus];
    [newView bringSubviewToFront:self.lblStatus];

    
    self.HUD = [[MBProgressHUD alloc] init];
    [newView addSubview:self.HUD];
    //self.HUD.mode = MBProgressHUDModeAnnularDeterminate;
    self.HUD.tag=110;
    if (shouldShowProgress)
        [self.HUD show:YES];

 
    if (type==BubbleTypeSomeoneElse)
    {
        self.imageView.frame=CGRectMake(7, 3.5, 38, 38);
        self.btnView.frame=CGRectMake(50,  7, 155, 30);
        self.lblStatus.frame=CGRectMake(117, 3.5, 100, 20);
        self.lblStatus.textAlignment=NSTextAlignmentRight;
   }
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    return [self initWithView:newView date:date type:type insets:insets];
}


@end
