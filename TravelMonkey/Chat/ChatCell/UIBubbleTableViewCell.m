          //
//  UIBubbleTableViewCell.m
//
//  Created by Alex Barinov
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import <QuartzCore/QuartzCore.h>
#import "UIBubbleTableViewCell.h"
#import "NSBubbleData.h"
#import "MessageObject.h"
@interface UIBubbleTableViewCell ()

@property (nonatomic, retain) UIView *customView;
@property (nonatomic, retain) UIImageView *bubbleImage;
@property (nonatomic, retain) UIImageView *avatarImage;
@property (nonatomic, retain) UIButton *avatarButton;

- (void) setupInternalData;

@end

@implementation UIBubbleTableViewCell

@synthesize data = _data;
@synthesize customView = _customView;
@synthesize bubbleImage = _bubbleImage;
@synthesize showAvatar = _showAvatar;
@synthesize avatarImage = _avatarImage;
@synthesize avatarButton = _avatarButton;

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
	[self setupInternalData];
}


- (void)setDataInternal:(NSBubbleData *)value
{
	self.data = value;
	[self setupInternalData];
}

- (void) setupInternalData
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor=[UIColor clearColor];
    
    if (!self.bubbleImage)
    {
        self.bubbleImage = [[UIImageView alloc] init];

        [self addSubview:self.bubbleImage];
    }
    
    NSBubbleType type = self.data.type;
    
    CGFloat width = self.data.view.frame.size.width;
    CGFloat height = self.data.view.frame.size.height;

    CGFloat x = (type == BubbleTypeSomeoneElse) ? 0 : self.frame.size.width - width - self.data.insets.left - self.data.insets.right;
    CGFloat y = 0;
    
    //temp
    self.showAvatar=NO;
    
    // Adjusting the x coordinate for avatar
    if (self.showAvatar)
    {
        [self.avatarImage removeFromSuperview];
        [self.avatarButton removeFromSuperview];
        self.avatarImage = [[UIImageView alloc] initWithImage:(self.data.avatar ? self.data.avatar : [UIImage imageNamed:@"ic_user_defualt.png"])];
        self.avatarButton = [[UIButton alloc] init];
        self.avatarButton = [UIButton buttonWithType:UIButtonTypeCustom];

        self.avatarImage.layer.cornerRadius = 25.0;
        self.avatarImage.layer.masksToBounds = YES;
        self.avatarButton.layer.cornerRadius = 5.0;
        self.avatarButton.layer.masksToBounds = YES;
        CGFloat avatarX = (type == BubbleTypeSomeoneElse) ? 2 : self.frame.size.width - 52;
        CGFloat avatarY = (self.frame.size.height -50)-2;
        
        self.avatarImage.frame = CGRectMake(avatarX, avatarY, 50, 50);
        [self addSubview:self.avatarImage];
       
        self.avatarButton.frame = CGRectMake(avatarX, avatarY, 50, 50);
        [self addSubview:self.avatarButton];
        [self bringSubviewToFront:self.avatarButton];
        
        
        CGFloat delta = self.frame.size.height - (self.data.insets.top + self.data.insets.bottom + self.data.view.frame.size.height);
        if (delta > 0) y = delta/2;
        
        if (type == BubbleTypeSomeoneElse) x += 54;
        if (type == BubbleTypeMine) x -= 54;
    }

    
    UILongPressGestureRecognizer *onLongTap=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPressGesture:)] ;
    
    onLongTap.minimumPressDuration=1.0;
    [self.contentView addGestureRecognizer:onLongTap];
    
    self.imageViewSelected = [[UIImageView alloc] initWithFrame:self.contentView.frame];
    self.imageViewSelected.backgroundColor=[UIColor blueColor];
    self.imageViewSelected.alpha=0.4f;
    self.imageViewSelected.hidden=YES;
    [self.contentView addSubview:self.imageViewSelected];
    [self.contentView bringSubviewToFront:self.imageViewResponse];
    
    if (type == BubbleTypeSomeoneElse)
    {
        self.bubbleImage.image = [[UIImage imageNamed:@"bubbleSomeone.png"] stretchableImageWithLeftCapWidth:self.bubbleImage.image.size.width*0.5 topCapHeight:15];
        [self.avatarButton addTarget:self action:@selector(avtarButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        self.bubbleImage.image = [[UIImage imageNamed:@"bubbleMine.png"] stretchableImageWithLeftCapWidth:self.bubbleImage.image.size.width*0.5 topCapHeight:15];
    }
    
    if (type == BubbleTypeSomeoneElse)
    {
        //self.backgroundColor=[UIColor yellowColor]; //color
        self.bubbleImage.frame = CGRectMake(x+5, y, width + self.data.insets.left + self.data.insets.right, height + self.data.insets.top + self.data.insets.bottom-8);
    }
    else
    {
        //self.backgroundColor=[UIColor greenColor]; //color
         self.bubbleImage.frame = CGRectMake(x-5, y, width + self.data.insets.left + self.data.insets.right, height + self.data.insets.top + self.data.insets.bottom-8);
    }
    
    //color
    //[self.bubbleImage setBackgroundColor:[UIColor orangeColor]];
    
    if (type == BubbleTypeMine)
    {
        
        //self.data.msgObj.isRead=YES; //temp
        
        if (self.data.msgObj.status)
        {
            self.lblMsgSeen = [[UILabel alloc]initWithFrame:CGRectMake(self.bubbleImage.frame.origin.x, self.bubbleImage.frame.origin.y + self.bubbleImage.frame.size.height+1, self.bubbleImage.frame.size.width,10)];
            
            if (self.data.msgObj.status==MSG_STATUS_UPLOADING)
            {
                [self.lblMsgSeen setText:@"Sending"];
            }
            else if (self.data.msgObj.status==MSG_STATUS_UPLOADED)
            {
                [self.lblMsgSeen setText:[NSString stringWithFormat:@"Sent, %@",self.data.timeSent]];
            }
            else if (self.data.msgObj.status==MSG_STATUS_RECEIVED)
            {
                [self.lblMsgSeen setText:[NSString stringWithFormat:@"Delivered, %@",self.data.timeSent]];
            }
            else if (self.data.msgObj.status==MSG_STATUS_READ)
            {
                [self.lblMsgSeen setText:[NSString stringWithFormat:@"Read, %@",self.data.timeSent]];
            }
            
            [self.lblMsgSeen setFont:[UIFont systemFontOfSize:8.0]];
            [self.lblMsgSeen setMinimumScaleFactor:0.5];
            [self.lblMsgSeen setTextColor:[UIColor lightGrayColor]];
            [self.lblMsgSeen setTextAlignment:NSTextAlignmentLeft];
            //[self.lblMsgSeen setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.8]]; //temp
            
            self.imageViewResponse = [[UIImageView alloc] initWithFrame:CGRectMake(self.lblMsgSeen.frame.origin.x-10, self.bubbleImage.frame.origin.y + self.bubbleImage.frame.size.height+5, 10,10)];
            self.imageViewResponse.image=[UIImage imageNamed:@"icn_checkMsg"];
            [self.imageViewResponse setContentMode:UIViewContentModeCenter];
            
            if (self.data.isSent)
            {
                [self.imageViewResponse setContentMode:UIViewContentModeScaleAspectFit];
                self.imageViewResponse.image=[UIImage imageNamed:@"sent"];
            }
            
            if (self.data.isReceived)
            {
                [self.imageViewResponse setContentMode:UIViewContentModeScaleAspectFit];
                self.imageViewResponse.image=[UIImage imageNamed:@"received"];
            }
            self.imageViewResponse.layer.cornerRadius = 5.0;
            self.imageViewResponse.layer.masksToBounds = YES;
            self.imageViewResponse.tag=15;
            //[self.contentView addSubview:self.imageViewResponse]; //Puru
            //[self.contentView addSubview:self.lblMsgSeen];
            
        }
     }

    if (type == BubbleTypeSomeoneElse)
    {
        self.lblMsgGetTime = [[UILabel alloc]initWithFrame:CGRectMake(self.bubbleImage.frame.origin.x + self.bubbleImage.frame.size.width + 20, self.bubbleImage.frame.origin.y + self.bubbleImage.frame.size.height/2.0-5, 30,10)];
        [self.lblMsgGetTime setText:self.data.timeSent];
        [self.lblMsgGetTime setFont:[UIFont systemFontOfSize:8.0]];
        [self.lblMsgGetTime setTextColor:[UIColor lightGrayColor]];
        [self.lblMsgGetTime setTextAlignment:NSTextAlignmentLeft];
        
        self.imgWatch = [[UIImageView alloc] initWithFrame:CGRectMake(self.bubbleImage.frame.origin.x + self.bubbleImage.frame.size.width + 5, self.lblMsgGetTime.frame.origin.y, 10,10)];
        self.imgWatch.image=[UIImage imageNamed:@"icn_clock"];
        [self.imgWatch setContentMode:UIViewContentModeCenter];
        
        self.lblName = [[UILabel alloc]initWithFrame:CGRectMake(self.bubbleImage.frame.origin.x +5, self.bubbleImage.frame.origin.y -20, 150,20)];
        [self.lblName setText:self.data.strSenderName];
        [self.lblName setFont:[UIFont systemFontOfSize:12.0]];
        [self.lblName setTextColor:KAppColor];
        [self.lblName setTextAlignment:NSTextAlignmentLeft];
        
        //Puru
        if (self.data.msgObj.status)
        {
            self.lblMsgSeen = [[UILabel alloc]initWithFrame:CGRectMake(self.bubbleImage.frame.origin.x, self.bubbleImage.frame.origin.y + self.bubbleImage.frame.size.height+1, self.bubbleImage.frame.size.width,10)];
            
            [self.lblMsgSeen setText:[NSString stringWithFormat:@"Received, %@",self.data.timeSent]];
            
            [self.lblMsgSeen setFont:[UIFont systemFontOfSize:8.0]];
            [self.lblMsgSeen setMinimumScaleFactor:0.5];
            [self.lblMsgSeen setTextColor:[UIColor lightGrayColor]];
            [self.lblMsgSeen setTextAlignment:NSTextAlignmentRight];
            //[self.lblMsgSeen setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.8]]; //temp
            //[self.contentView addSubview:self.lblMsgSeen];
        }

    }
    else
    {
        self.lblMsgGetTime = [[UILabel alloc]initWithFrame:CGRectMake(self.bubbleImage.frame.origin.x - 30, self.bubbleImage.frame.origin.y + self.bubbleImage.frame.size.height/2.0-5, 30,10)];

        [self.lblMsgGetTime setText:self.data.timeSent];
        [self.lblMsgGetTime setFont:[UIFont systemFontOfSize:8.0]];
        [self.lblMsgGetTime setTextColor:[UIColor lightGrayColor]];
        [self.lblMsgGetTime setTextAlignment:NSTextAlignmentLeft];
        
        self.imgWatch = [[UIImageView alloc] initWithFrame:CGRectMake(self.lblMsgGetTime.frame.origin.x-13, self.lblMsgGetTime.frame.origin.y, 10,10)];
        self.imgWatch.image=[UIImage imageNamed:@"icn_clock"];
        [self.imgWatch setContentMode:UIViewContentModeCenter];

    }
    
    //[self.contentView addSubview:self.imgWatch];
    //[self.contentView addSubview:self.lblMsgGetTime]; //Puru
    [self.contentView addSubview:self.lblName];
    [self.customView removeFromSuperview];
    self.customView = self.data.view;
    self.customView.frame = CGRectMake(x + self.data.insets.left, y + self.data.insets.top, width, height);
    [self.contentView addSubview:self.customView];

}

-(IBAction)avtarButtonEvent:(id)sender
{
    
}

-(void)handleLongPressGesture:(UIGestureRecognizer*)gestureRecognizer
{
    [self setSelectionStyle:UITableViewCellSelectionStyleGray];
}

-(void)initilizeMenuViewcontroller
{
    UIMenuItem* miCustom1 = [[UIMenuItem alloc] initWithTitle: @"Forward" action:@selector(Forward: )];
    UIMenuController* mc = [UIMenuController sharedMenuController];
    mc.menuItems = [NSArray arrayWithObjects: miCustom1, nil];
}

-(BOOL) canPerformAction:(SEL)action withSender:(id)sender
{
    App_Delegate.isChatRowSelected = YES;
    if (self.data.contentType==BubbleContentContact||self.data.contentType==BubbleContentLocation)
    {
        return (action == @selector(Forward:) ||action == @selector(delete:));
    }    
    return (action == @selector(copy:) ||action == @selector(delete:));
}

-(BOOL)canBecomeFirstResponder
{
    return YES;
}

/// this methods will be called for the cell menu items
-(void) copy:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kCopyButtonClickedOnPopUp object:self.data];
}

-(void) delete:(id)sender
{
    if ([self.data.msgObj.strMsgId isEqualToString:@""])
    {
        NSString *str= [NSString stringWithFormat:@"%.0f",self.data.msgObj.ID];
        NSArray *arr = [str componentsSeparatedByString:@"."];
        str =[arr objectAtIndex:0];
        self.data.msgObj.strMsgId =str;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kDeleteButtonClickedOnPopUp object:self.data];
}

- (void) Forward: (UIMenuController*) sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kForwardButtonClickedOnPopUp object:self.data];
}


@end
