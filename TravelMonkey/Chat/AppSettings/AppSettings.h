//
//
//
//
//
//
//

#import <Foundation/Foundation.h>

@interface AppSettings : NSObject

+ (AppSettings*)sharedInstance;
@property (nonatomic,assign) BOOL isTCReached;
@property (nonatomic,assign) BOOL isEULAReached;
- (void)savePhone:(NSString*)phone;
- (void)saveTermsRead;
- (void)saveEULARead;
- (void)saveChatAccount: (NSString*) username password: (NSString*)password;

- (NSString*)getPhone;
- (NSNumber*)getTermsRead;
-(NSNumber*)getEulaRead;

- (NSString*)getChatUsername;
- (NSString*)getChatPassword;
+(NSString * ) urlDecode : (NSString *) raw;
+(NSInteger)checkProfinityWords :(NSString *)textString;

@end
