//
//  AppSettings.m
//  Entwicklungsprogramm
//
//  Created by Анна on 28.01.13.
//  Copyright (c) 2013 Анна. All rights reserved.
//

#import "AppSettings.h"

static AppSettings* Settings;

static NSString* const kPhone = @"UserPhone";
static NSString* const kTerms = @"ReadTerms";
static NSString* const kEula = @"ReadEULA";
@interface AppSettings(){
    dispatch_queue_t read_sync_queue;
    dispatch_queue_t write_sync_queue;
}

@property(nonatomic, strong)NSDictionary* settingsDictionary;


@end

@implementation AppSettings

+(AppSettings*)sharedInstance {
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        Settings = [[self alloc] init];
    });
    
    return Settings;
}

-(id)init{
    self = [super init];
    if(self){
        read_sync_queue = dispatch_queue_create("read_queue", NULL);
        write_sync_queue = dispatch_queue_create("write_queue", NULL);
    }
    return self;
}

#pragma mark - Setters

-(void)savePhone:(NSString*)phone{
    [[NSUserDefaults standardUserDefaults] setObject:phone forKey:kPhone];
}

-(void)saveTermsRead{
    dispatch_async(write_sync_queue, ^{
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:[NSNumber numberWithBool:YES] forKey:kTerms];
        [userDefaults synchronize];
    });
}

-(void)saveEULARead
{
    dispatch_async(write_sync_queue, ^{
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:[NSNumber numberWithBool:YES] forKey:kEula];
        [userDefaults synchronize];
    });
}

- (void)saveChatAccount: (NSString*) username password: (NSString*)password{
    dispatch_async(write_sync_queue, ^{
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:username forKey:kSHXMPPmyJID];
        [userDefaults setObject:password forKey:kSHXMPPmyPassword];
        [userDefaults synchronize];
    });
}

+(NSString * ) urlDecode : (NSString *) raw
{
    NSString *preparedString = [raw stringByReplacingOccurrencesOfString: @"%20" withString: @" "];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%7B" withString: @"{"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%2F" withString: @"/"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%3A" withString: @":"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%2C" withString: @","];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%7D" withString: @"}"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%22" withString: @" "];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%0A" withString: @""];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"+" withString: @" "];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%5C" withString: @""];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%27" withString: @"'"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%24" withString: @"$"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%3F" withString: @"?"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%3A" withString: @":"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%2F" withString: @"/"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%3F" withString: @"?"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%3D" withString: @"="];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%26" withString: @"&"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%3B" withString: @";"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"&amp;" withString: @"&"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%28" withString: @"("];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%29" withString: @")"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%2A" withString: @"*"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%2B" withString: @"+"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%2E" withString: @"."];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%2D" withString: @"-"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%40" withString: @"@"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%3C" withString: @"<"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%C3" withString: @""];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%83" withString: @""];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%C2" withString: @""];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%A9" withString: @""];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%21" withString: @"!"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%0D" withString: @" "];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%09" withString: @" "];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%3E" withString: @">"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%7E" withString: @"~"];
    preparedString = [preparedString stringByReplacingOccurrencesOfString: @"%EF%BF%A5" withString: @"￥"];
    return preparedString;
    
}

#pragma mark - Getters

- (NSString*)getPhone{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kPhone];
}

- (NSNumber*)getTermsRead{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kTerms];
}

-(NSNumber*)getEulaRead
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:kEula];
}

- (NSString*)getChatUsername{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"chat_username"];
}

- (NSString*)getChatPassword{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"chat_password"];
}

+(NSInteger)checkProfinityWords :(NSString *)textString  {
    textString =[textString lowercaseString];
    NSInteger flg=0;
    NSArray * listOfProfaneWords = @[@"anal", @"anus", @"arse", @"ass", @"ballsack", @"balls", @"bastard", @"bitch", @"biatch", @"bloody", @"blowjob",  @"blow job", @"bollock", @"bollok", @"boner", @"boob",@"bugger", @"bum", @"butt", @"buttplug", @"clitoris", @"cock", @"coon", @"crap", @"cunt", @"damn", @"dick",  @"dildo", @"dyke", @"fag", @"feck", @"fellate", @"fellatio", @"felching", @"fuck",  @"fudgepacker", @"fudge packer", @"flange", @"Goddamn", @"God damn", @"homo", @"jerk", @"jizz", @"knobend", @"knob end", @"labia", @"lmao", @"lmfao", @"muff", @"nigger", @"nigga", @"omg", @"penis", @"piss", @"poop", @"prick", @"pube", @"pussy", @"queer", @"scrotum", @"sex", @"shit", @"s hit", @"sh1t", @"slut", @"smegma", @"spunk", @"tit", @"tosser", @"turd", @"twat", @"vagina", @"wank", @"whore", @"wtf",@"fucked",@"hell",@"bullshit"];
    if ([textString localizedCaseInsensitiveContainsString:@" f u c k "])
    {
        flg = 1;
        return flg;
    }
    if ([textString localizedCaseInsensitiveContainsString:@"f u c k "])
    {
        flg = 1;
        return flg;
    }
    if ([textString localizedCaseInsensitiveContainsString:@" f u c k"])
    {
        flg = 1;
        return flg;
    }
    if ([textString localizedCaseInsensitiveContainsString:@"f u c k"])
    {
        flg = 1;
        return flg;
    }
    NSArray *arr = [textString componentsSeparatedByString:@" "];
    for (NSString *strText in arr)
    {
        if ([strText containsString:@"."])
        {
            NSArray *subArray = [textString componentsSeparatedByString:@"."];
            for (NSString *strSubText in subArray)
            {
                if ([listOfProfaneWords containsObject:strSubText])
                {
                    flg = 1;
                    return flg;
                }

            }
        }
        else if ([strText containsString:@","])
        {
            NSArray *subArray = [textString componentsSeparatedByString:@","];
            for (NSString *strSubText in subArray)
            {
                if ([listOfProfaneWords containsObject:strSubText])
                {
                    flg = 1;
                    return flg;
                }
                
            }
        }
        else if ([listOfProfaneWords containsObject:strText])
        {
            flg = 1;
            return flg;
        }
    }
    return flg;
}

@end