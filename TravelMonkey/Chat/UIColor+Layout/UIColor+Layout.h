//
//  UIColor+Layout.h
//  SwopShop
//
//  Created by Andrey Dorosh on 17.01.14.
//  Copyright (c) 2014 Andrey Dorosh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Layout)

+ (UIColor*)projectGreenColor;
+ (UIColor*)projectNavBarColor;
+ (UIColor*)projectTextGrayColor;
+ (UIColor*)projectLightGrayColor;
+ (UIColor*)projectLoadingTextColor;
+ (UIColor*)projectColor;
@end
