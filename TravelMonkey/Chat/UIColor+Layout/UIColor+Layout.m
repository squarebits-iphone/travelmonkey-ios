//
//  UIColor+Layout.m
//  SwopShop
//
//  Created by Andrey Dorosh on 17.01.14.
//  Copyright (c) 2014 Andrey Dorosh. All rights reserved.
//

#import "UIColor+Layout.h"

@implementation UIColor (Layout)

+ (UIColor*)projectGreenColor{
    return [UIColor colorWithRed:0.094 green:0.737 blue:0.612 alpha:1.000];
}

+ (UIColor*)projectNavBarColor{
    return [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1.000];
}

+ (UIColor*)projectTextGrayColor{
    return [UIColor colorWithWhite:0.753 alpha:1.000];
}

+ (UIColor*)projectLightGrayColor{
    return [UIColor colorWithRed:0.945 green:0.945 blue:0.953 alpha:1.000];
}

+ (UIColor*)projectLoadingTextColor
{
    return [UIColor colorWithRed:0.262 green:0.698 blue:0.349 alpha:1.000];
}

+ (UIColor*)projectColor
{
    return [UIColor colorWithRed:86.0/255.0 green:196.0/255.0 blue:212.0/255.0 alpha:1.000];
}

@end
