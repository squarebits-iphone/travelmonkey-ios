//
//  ChatLeftCell.m
//  SayHello
//
//  Created by Alex on 10.09.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import "ChatLeftCell.h"

@implementation ChatLeftCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        // Initialization code
    }
    
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setUpCell:(NSString*)message
{
    self.textLbl.superview.layer.cornerRadius = 17;
    self.textLbl.text = message;
    self.layer.rasterizationScale = [[UIScreen mainScreen] scale];
}

- (CGFloat)getTextWidth:(UILabel*)label
{
    NSDictionary *attributes = @{NSFontAttributeName:label.font};
    CGRect rect = [label.text boundingRectWithSize:CGSizeMake(self.frame.size.width - 70 - 10 - 10 - 4, CGFLOAT_MAX)
                                           options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                        attributes:attributes
                                           context:nil];
    rect.size.width = rect.size.width <= 40 ? 40 : rect.size.width;
    return rect.size.width;
}


@end
