//
//  ChatLeftCell.h
//  SayHello
//
//  Created by Alex on 10.09.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatLeftCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *textLbl;
@property (weak, nonatomic) IBOutlet UILabel *deliveredLbl;

- (void)setUpCell:(NSString*)message;

@end
