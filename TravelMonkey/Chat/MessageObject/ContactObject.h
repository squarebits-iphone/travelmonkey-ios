//
//  ContactObject.h
//  SimsMessenger
//
//  Created by Ram on 28/09/15.
//  Copyright (c) 2015 Squarebits. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactObject : NSObject

@property(nonatomic,strong) NSString *strFirstName;
@property(nonatomic,strong) NSString *strLastName;
@property(nonatomic,strong) NSString *strGender;

@property(nonatomic,strong) NSString *strProfileImage;
@property(nonatomic,strong) NSString *strJID;
@property(nonatomic,strong) NSString *strLoginUserJID;
@property(nonatomic,strong) NSString *strUserID;
@property(nonatomic,strong) NSString *strFriendFromType;
@property(nonatomic,strong) NSString *strLastMessage;
@property(nonatomic,strong) NSString *strLastMessageFromUID;
@property(nonatomic,strong) NSString *strFullUserInfo;
@property(nonatomic) double lastMsgReceivedOn;

-(id)initWithDefault;

@end

