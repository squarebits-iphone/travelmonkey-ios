//
//  ContactObject.m
//  SimsMessenger
//
//  Created by Ram on 28/09/15.
//  Copyright (c) 2015 Squarebits. All rights reserved.
//

#import "ContactObject.h"

@implementation ContactObject


-(id)initWithDefault
{
    self=[super init];
    self.strFirstName=@"";
    self.strLastName=@"";
    self.strGender=@"";
    self.strProfileImage=@"";
    self.strJID=@"";
    //self.strLoginUserJID = [[NSUserDefaults standardUserDefaults]objectForKey:kSkyXMPPmyJID];
    self.strUserID=@"";
    self.lastMsgReceivedOn=0;
    self.strFriendFromType=@"";
    self.strLastMessage=@"";
    self.strLastMessageFromUID=@"";
    return self;
}

@end
