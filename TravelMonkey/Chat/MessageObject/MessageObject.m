//
//  MessageObject.m
//  Vleep
//
//  Created by Ram on 19/07/14.
//  Copyright (c) 2014 squarebits. All rights reserved.
//

#import "MessageObject.h"

@implementation MessageObject


-(id)initWithDefault
{
    self=[super init];
    self.ID=0;
    self.timeStamp=0;
    self.status=MSG_STATUS_CREATED;
    self.strFriendUserName=@"";
    self.strBody=@"";
    self.strType=@"";
    self.strTime=@"";
    self.strTypeOfSending=@"";
    self.strOfUser=[NSString stringWithFormat:@"%@",App_Delegate.userObject.strEmail];
    self.strThumbUrl=@"NULL";
    self.strMainUrl=@"NULL";
    self.strSizeofFile=@"NULL";
    self.strDurationofFile=@"NULL";
    self.strMsgId =@"";
    self.strGroupId=@"";
    self.strNickName =@"";
    return self;
}

@end
