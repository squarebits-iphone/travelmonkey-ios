//
//  MessageObject.h
//  Vleep
//
//  Created by Ram on 19/07/14.
//  Copyright (c) 2014 squarebits. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageObject : NSObject

@property(nonatomic) double ID;
@property(nonatomic) double timeStamp;
@property(nonatomic) UPLOADSTATUS status;
@property(nonatomic,strong) NSString *strFriendUserName;
@property(nonatomic,strong) NSString *strBody;
@property(nonatomic,strong) NSString *strType;
@property(nonatomic,strong) NSString *strTime;
@property(nonatomic,strong) NSString *strTypeOfSending;
@property(nonatomic,strong) NSString *strOfUser;
@property(nonatomic,strong) NSString *strThumbUrl;
@property(nonatomic,strong) NSString *strMainUrl;
@property(nonatomic,strong) NSString *strSizeofFile;
@property(nonatomic,strong) NSString *strDurationofFile;
@property(nonatomic,strong) NSString *strMsgId;
@property(nonatomic,strong) NSString *strGroupId;
@property(nonatomic,strong) NSString *strNickName;
@property(nonatomic,strong) NSString *strBookingID;


@property(nonatomic) BOOL isRead;
@property(nonatomic,strong) NSString *strReadTime;
@property(nonatomic) double readTimeStamp;


-(id)initWithDefault;

@end
