//
//  GroupObject.m
//  NukeMessenger
//
//  Created by Ram on 14/10/15.
//  Copyright (c) 2015 Squarebits. All rights reserved.
//

#import "GroupObject.h"

@implementation GroupObject

-(id)initWithDefault
{
    self=[super init];
    
    self.strGroupName=@"";
    self.strGroupID=@"";
    self.strGroupImage=@"";
    self.strGroupType=@"";
    self.aryGroupMembers=[[NSMutableArray alloc] init];
    return self;
}

@end
