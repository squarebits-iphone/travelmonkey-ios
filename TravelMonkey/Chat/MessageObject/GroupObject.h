//
//  GroupObject.h
//  NukeMessenger
//
//  Created by Ram on 14/10/15.
//  Copyright (c) 2015 Squarebits. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupObject : NSObject
@property(nonatomic,strong) NSString *strGroupName;
@property(nonatomic,strong) NSString *strGroupType;
@property(nonatomic,strong) NSString *strGroupImage;
@property(nonatomic,strong) NSString *strGroupID;
@property(nonatomic,strong) NSMutableArray *aryGroupMembers;
@property(nonatomic,strong) NSString *strGroupMember;
@property(nonatomic,strong) NSString *strGroupCreatedOn;

@property(nonatomic) double LastUpdated;

-(id)initWithDefault;

@end
