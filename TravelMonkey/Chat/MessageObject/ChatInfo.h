//
//  ChatInfo.h
//  NukeMessenger
//
//  Created by Manish Sawlot on 27/11/15.
//  Copyright (c) 2015 Squarebits. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ChatInfo : NSObject


@property(nonatomic,strong) NSString *strName;
@property(nonatomic,strong) NSString *strGroupID;
@property(nonatomic) double timeStamp;
//@property(nonatomic) UPLOADSTATUS status;
@property(nonatomic,strong) NSString *strBody;
@property(nonatomic,strong) NSString *strType;
@property(nonatomic,strong) NSString *strSendingType;
@property(nonatomic,strong) NSString *strImage;

@property(nonatomic,strong) NSString *strFriendId;
@property(nonatomic,strong) NSString *strContactNumber;
@property(nonatomic,strong) NSString *strMsgTime;

-(id)initWithDefault;

@end
