//
//  ChatInfo.m
//  NukeMessenger
//
//  Created by Manish Sawlot on 27/11/15.
//  Copyright (c) 2015 Squarebits. All rights reserved.
//

#import "ChatInfo.h"

@implementation ChatInfo

-(id)initWithDefault
{
    self.strName=@"";
    self.strGroupID=@"";
    self.timeStamp=0;
    //self.status=MSG_STATUS_CREATED;
    self.strBody=@"";
    self.strType=@"";
    self.strImage=@"";
    self.strSendingType=@"";
    self.strFriendId=@"";
    self.strContactNumber=@"";
    self.strMsgTime=@"";
    return self;
}
@end
