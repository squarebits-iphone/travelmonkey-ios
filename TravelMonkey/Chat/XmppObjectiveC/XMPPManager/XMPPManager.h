//
//  XMPPManager.h
//  Vleep
//
//  Created by Ram on 07/04/14.
//  Copyright (c) 2014 Ramprakash. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPFramework.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
#import "XMPPStream.h"
#import "XMPPPing.h"
#import "XMPPMessageArchiving.h"
#import "XMPPMessageArchivingCoreDataStorage.h"
#import "XMPPIQ.h"
#import "XMPPIQ+XEP_0055.h"
#import "XMPPMessage+XEP_0184.h"
#import "XMPPMessage+XEP0045.h"
#import "XMPPProcessOne.h"
#import "XMPPLastActivity.h"
#import "XMPPMessageDeliveryReceipts.h"
#import <AudioToolbox/AudioServices.h>
#import <AVFoundation/AVFoundation.h>

@class MessageNotificationScreen;
@interface XMPPManager : NSObject<XMPPRosterDelegate,NSFetchedResultsControllerDelegate,AVAudioPlayerDelegate>
{
    AVAudioPlayer *_audioPlayer;

    XMPPLastActivity *xmppLastActivity;
    NSMutableArray *onlineUsers;
    XMPPStream *xmppStream;
	XMPPReconnect *xmppReconnect;
    XMPPRoster *xmppRoster;
    XMPPMUC *xmppMUC;
    XMPPRoom *xmppRoom;
    XMPPPing *xmppPing;
	
    XMPPRosterCoreDataStorage *xmppRosterStorage;
    XMPPvCardCoreDataStorage *xmppvCardStorage;
    XMPPRoomCoreDataStorage *xmppRoomDataStorage;
    
    XMPPMessageArchiving *xmppMessageArchiving;
    XMPPMessageArchivingCoreDataStorage *xmppMessageArchivingStorage;
    
	XMPPvCardTempModule *xmppvCardTempModule;
	XMPPvCardAvatarModule *xmppvCardAvatarModule;
    
	XMPPCapabilities *xmppCapabilities;
	XMPPCapabilitiesCoreDataStorage *xmppCapabilitiesStorage;
    XMPPProcessOne *xmppProcessOne;
    XMPPMessageDeliveryReceipts *xmppMessageDeliveryReceipts;
   
    XMPPJID *myJID;
    
    NSString *password;
    BOOL isXmppConnected;
    
    BOOL allowSelfSignedCertificates;
	BOOL allowSSLHostNameMismatch;
    
    BOOL isAnoymous;
    
    NSManagedObjectContext *managedObjectContext_roster;
    NSManagedObjectContext *managedObjectContext_messages;
	NSManagedObjectContext *managedObjectContext_capabilities;
    
    NSFetchedResultsController *rosterFetchedResultsController;
    NSFetchedResultsController *messagesFetchedResultsController;
//    NSMutableDictionary *buddyListDic;
//    NSMutableDictionary *chatroomListDic;
    
    NSArray *selectedBuddy;
    
}

@property (nonatomic, readonly) XMPPStream *xmppStream;
@property (nonatomic, readonly) XMPPReconnect *xmppReconnect;
@property (nonatomic, readonly) XMPPRoster *xmppRoster;
@property (nonatomic, readonly) XMPPRosterCoreDataStorage *xmppRosterStorage;
@property (nonatomic, readonly) XMPPvCardTempModule *xmppvCardTempModule;
@property (nonatomic, readonly) XMPPvCardAvatarModule *xmppvCardAvatarModule;
@property (nonatomic, readonly) XMPPCapabilities *xmppCapabilities;
@property (nonatomic, readonly) XMPPCapabilitiesCoreDataStorage *xmppCapabilitiesStorage;
@property (nonatomic, readonly) XMPPRoomCoreDataStorage *xmppRoomDataStorage;
@property (nonatomic, strong) XMPPJID *myJID;
@property (nonatomic, strong) MessageNotificationScreen *messageNotification;
@property (nonatomic, strong) NSMutableDictionary *dictStatus;
-(void)reachabilityChanged:(NSNotification*)note;

- (id)init;
+ (XMPPManager *)sharedManager;
- (void)setupStream;
- (BOOL)connectWithJID:(NSString *)JID password:(NSString *)myPassword;
- (BOOL)anoymousConnection;
- (void)disconnect;

- (void)sendMessageTo:(XMPPJID *)targetBareID withMessage:(NSString *)newMessage;

- (NSFetchedResultsController *)messagesFetchedResultsController:(NSString *)bareJidStr addDelegate:(id)delegate;
- (NSFetchedResultsController *)rosterFetchedResultsController;
- (void)sendSearchRequest:(NSString *)searchField;
- (NSManagedObjectContext *)managedObjectContext_roster;

-(NSString*)getMessageID;
-(void)setTabBadgeValue;
-(XMPPJID*)getJIDfromUsername:(NSString*)strUserName;

// ** XMPP Message
-(void)sendMessage:(XMPPJID *)to Body:(NSString *)body withID:(NSString *)messageID;

-(void)sendImage:(XMPPJID *)to :(NSString *)body withID:(NSString *)messageID;
-(void)sendVideo:(XMPPJID *)to :(NSString *)body withID:(NSString *)messageID;
-(void)sendAudio:(XMPPJID *)to :(NSString *)body withID:(NSString *)messageID;
-(void)sendMessageTo:(XMPPJID *)to message:(NSString *)body withID:(NSString *)messageID withUserInfo:(NSString*)userInfo;

-(void)sendMessage:(XMPPJID *)to :(NSString *)body withID:(NSString *)messageID withUserDetail:(NSString*)user;

-(void)sendCustomMessageWithoutReceipt:(NSString *)to withSubject:(NSString *)subject Body:(NSString *)body withID:(NSString *)messageID;

-(void)sendPresense:(NSString *)type :(NSString *)to;
-(BOOL)isUserOnline:(NSString *)friend_user_name;
//Send message read confirmation
-(void)sendReadStatus:(NSString *)strFriendId;
-(void)sendCustomMessage:(NSString *)to withSubject:(NSString *)subject Body:(NSString *)body withID:(NSString *)messageID;



@end
