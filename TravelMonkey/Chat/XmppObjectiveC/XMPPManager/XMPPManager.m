//
//  XMPPManager.m
//  Vleep
//
//  Created by Ram on 07/04/14.
//  Copyright (c) 2014 Ramprakash. All rights reserved.
//

#import "XMPPManager.h"
#import "MessageNotificationScreen.h"
#import "UIImageView+WebCache.h"
#import "MessageObject.h"
#import "FriendObject.h"
#import "DBManager.h"
#import "NSXMLElement+XEP_0203.h"
#import "Reachability.h"
#import "XMPPStream.h"
#import "GroupObject.h"
#import "DataManager.h"
#import "Extend.h"


#if DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_WARN;
#endif

static XMPPManager *sharedManager = nil;

@implementation XMPPManager

@synthesize xmppStream;
@synthesize xmppReconnect;
@synthesize xmppRoster;
@synthesize xmppRosterStorage;
@synthesize xmppvCardTempModule;
@synthesize xmppvCardAvatarModule;
@synthesize xmppCapabilities;
@synthesize xmppCapabilitiesStorage;
@synthesize xmppRoomDataStorage;
@synthesize myJID;

#pragma mark -
#pragma mark singleton
- (id)init
{
    self=[super init];
    if(self){
        [DDLog addLogger:[DDTTYLogger sharedInstance]];
        [self setupStream];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChanged:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
        
        Reachability * reach = [Reachability reachabilityWithHostName:@"www.google.com"];
        [reach startNotifier];
        
    }
    return self;
}
+ (XMPPManager *)sharedManager
{
    @synchronized(self) {
        if (sharedManager == nil) {
            sharedManager = [[self alloc] init];
        }
    }
    return sharedManager;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (sharedManager == nil) {
            sharedManager = [super allocWithZone:zone];
            return sharedManager;  // assignment and return on first allocation
        }
    }
    return nil; // on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

#pragma mark -
#pragma mark Manage Private
- (void)setupStream
{
    
    NSAssert(xmppStream == nil, @"Method setupStream invoked multiple times");
    xmppStream = [[XMPPStream alloc]init];
    
    xmppReconnect = [[XMPPReconnect alloc] init];
    
    xmppRosterStorage = [[XMPPRosterCoreDataStorage alloc] initWithDatabaseFilename:@"XMPPRoster.sqlite"];
    xmppRoster = [[XMPPRoster alloc] initWithRosterStorage:xmppRosterStorage];
    xmppRoster.autoFetchRoster = YES;
    xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = YES;
    
    xmppRoomDataStorage = [[XMPPRoomCoreDataStorage alloc] initWithInMemoryStore];
    xmppMUC = [[XMPPMUC alloc]initWithDispatchQueue:dispatch_get_main_queue()];
    
    xmppvCardStorage = [XMPPvCardCoreDataStorage sharedInstance];
    xmppvCardTempModule = [[XMPPvCardTempModule alloc] initWithvCardStorage:xmppvCardStorage];
    xmppvCardAvatarModule = [[XMPPvCardAvatarModule alloc] initWithvCardTempModule:xmppvCardTempModule];
    
    xmppMessageArchivingStorage = [[XMPPMessageArchivingCoreDataStorage alloc] initWithDatabaseFilename:@"XMPPMessageArchiving.sqlite"];
    
    xmppMessageArchiving = [[XMPPMessageArchiving alloc]initWithMessageArchivingStorage:xmppMessageArchivingStorage dispatchQueue:dispatch_get_main_queue()];
    xmppMessageArchiving.clientSideMessageArchivingOnly = YES;
    
    xmppCapabilitiesStorage = [XMPPCapabilitiesCoreDataStorage sharedInstance];
    xmppCapabilities = [[XMPPCapabilities alloc] initWithCapabilitiesStorage:xmppCapabilitiesStorage];
    
    xmppCapabilities.autoFetchHashedCapabilities = YES;
    xmppCapabilities.autoFetchNonHashedCapabilities = NO;
    
    xmppPing = [[XMPPPing alloc] initWithDispatchQueue:dispatch_get_main_queue()];
    xmppPing.respondsToQueries = YES;
    
    xmppMessageDeliveryReceipts = [[XMPPMessageDeliveryReceipts alloc] initWithDispatchQueue:dispatch_get_main_queue()];
    
    xmppProcessOne=[xmppProcessOne initWithDispatchQueue:dispatch_get_main_queue()];
    
    xmppMessageDeliveryReceipts.autoSendMessageDeliveryReceipts = YES;
    xmppMessageDeliveryReceipts.autoSendMessageDeliveryRequests = YES;
    // Activate xmpp modules
    // create last activity instance, For Last seen
    //sohan
    xmppLastActivity = [[XMPPLastActivity alloc] initWithDispatchQueue:dispatch_get_main_queue()];
    
    [xmppLastActivity addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    // Activate xmpp modules
    // add last scene module
    //sohan
    [xmppLastActivity activate:xmppStream];
    [xmppMUC               activate:xmppStream];
    [xmppReconnect         activate:xmppStream];
    [xmppRoster            activate:xmppStream];
    [xmppvCardTempModule   activate:xmppStream];
    [xmppvCardAvatarModule activate:xmppStream];
    [xmppCapabilities      activate:xmppStream];
    [xmppPing              activate:xmppStream];
    [xmppMessageArchiving  activate:xmppStream];
    [xmppMessageDeliveryReceipts activate:xmppStream];
    
    [xmppCapabilities addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [xmppRoster addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [xmppMUC addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [xmppvCardAvatarModule addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    allowSelfSignedCertificates = YES;
    allowSSLHostNameMismatch = YES;
    
}

- (void)teardownStream
{
    [xmppStream disconnect];
    
    [xmppStream removeDelegate:self];
    [xmppRoster removeDelegate:self];
    
    [xmppReconnect         deactivate];
    [xmppRoster            deactivate];
    [xmppvCardTempModule   deactivate];
    [xmppvCardAvatarModule deactivate];
    [xmppCapabilities      deactivate];
    
    xmppStream = nil;
    xmppReconnect = nil;
    xmppRoster = nil;
    xmppRosterStorage = nil;
    xmppvCardStorage = nil;
    xmppvCardTempModule = nil;
    xmppvCardAvatarModule = nil;
    xmppCapabilities = nil;
    xmppCapabilitiesStorage = nil;
}

- (void)goOnline
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    XMPPPresence *presence = [XMPPPresence presence];
    [presence addChild:[XMPPElement elementWithName:@"priority" stringValue:@"1"]];
    [presence addChild:[XMPPElement elementWithName:@"device" stringValue:@"mobile"]];
    [presence addChild:[XMPPElement elementWithName:@"status" stringValue:@"Online"]];
    [xmppStream sendElement:presence];
    [[NSNotificationCenter defaultCenter] postNotificationName:kServerLoginSuccess object:self];
    [self requestSearchFields];
    NSLog(@"goOnline");
}

- (void)goOffline
{
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
    [xmppStream sendElement:presence];
    NSLog(@"goOffline");
}


- (void)failedToConnect
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kServerLoginFail object:self];
    [xmppStream disconnect];
}


- (void)requestSearchFields
{
    NSXMLElement *query = [NSXMLElement elementWithName:@"query"];
    [query addAttributeWithName:@"xmlns" stringValue:@"jabber:iq:search"];
    
    NSXMLElement *iq = [NSXMLElement elementWithName:@"iq"];
    [iq addAttributeWithName:@"type" stringValue:@"get"];
    [iq addAttributeWithName:@"id" stringValue:@"search"];
    [iq addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"search.%@",DOMAIN_NAME]];
    [iq addAttributeWithName:@"from" stringValue:[myJID full]];
    [iq addChild:query];
    [xmppStream sendElement:iq];
}


- (void)sendSearchRequest:(NSString *)searchField
{
    NSXMLElement *query = [NSXMLElement elementWithName:@"query"];
    [query addAttributeWithName:@"xmlns" stringValue:@"jabber:iq:search"];
    
    
    NSXMLElement *x = [NSXMLElement elementWithName:@"x" xmlns:@"jabber:x:data"];
    [x addAttributeWithName:@"type" stringValue:@"submit"];
    
    NSXMLElement *formType = [NSXMLElement elementWithName:@"field"];
    [formType addAttributeWithName:@"type" stringValue:@"hidden"];
    [formType addAttributeWithName:@"var" stringValue:@"FORM_TYPE"];
    [formType addChild:[NSXMLElement elementWithName:@"value" stringValue:@"jabber:iq:search" ]];
    
    NSXMLElement *userName = [NSXMLElement elementWithName:@"field"];
    [userName addAttributeWithName:@"var" stringValue:@"Username"];
    [userName addChild:[NSXMLElement elementWithName:@"value" stringValue:@"1" ]];
    
    NSXMLElement *name = [NSXMLElement elementWithName:@"field"];
    [name addAttributeWithName:@"var" stringValue:@"Name"];
    [name addChild:[NSXMLElement elementWithName:@"value" stringValue:@"1"]];
    
    NSXMLElement *email = [NSXMLElement elementWithName:@"field"];
    [email addAttributeWithName:@"var" stringValue:@"Email"];
    [email addChild:[NSXMLElement elementWithName:@"value" stringValue:@"1"]];
    
    NSXMLElement *search = [NSXMLElement elementWithName:@"field"];
    [search addAttributeWithName:@"var" stringValue:@"search"];
    [search addChild:[NSXMLElement elementWithName:@"value" stringValue:searchField]];
    
    [x addChild:formType];
    [x addChild:userName];
    [x addChild:name];
    [x addChild:email];
    [x addChild:search];
    [query addChild:x];
    
    NSXMLElement *iq = [NSXMLElement elementWithName:@"iq"];
    [iq addAttributeWithName:@"type" stringValue:@"set"];
    [iq addAttributeWithName:@"id" stringValue:[xmppStream generateUUID]];
    [iq addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"search.%@",DOMAIN_NAME ]];
    [iq addAttributeWithName:@"from" stringValue:[myJID full]];
    [iq addChild:query];
    [xmppStream sendElement:iq];
}


- (void)pushLocalNotification:(XMPPMessage *)message
{
    NSString *displayName =[[message from]user];
    NSArray *arr  = [displayName componentsSeparatedByString:@"_"];
    displayName = [[arr objectAtIndex:0] uppercaseString];
    NSString *body = [[message elementForName:@"body"] stringValue];
    NSString *messagetype = [[message elementForName:@"subject"] stringValue];
    
    NSString *alertMessage = [NSString stringWithFormat:@"%@ says: %@",
                              displayName ,
                              body ];
    
    if([messagetype isEqualToString:kTYPE_BLOCK] || [messagetype isEqualToString:kTYPE_UNBLOCK] || [messagetype isEqualToString:kTYPE_UPDATE])
        return;
    
    
    UIApplication *application = [UIApplication sharedApplication];
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif) {
        if([messagetype isEqualToString:kTYPE_TEXT]||[messagetype isEqualToString:kTYPE_IMAGE]||[messagetype isEqualToString:kTYPE_VIDEO]||[messagetype isEqualToString:kTYPE_AUDIO])
            localNotif.applicationIconBadgeNumber = [application applicationIconBadgeNumber]+1;
        localNotif.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
        
        if ([messagetype isEqualToString:kTYPE_IMAGE])
            alertMessage =[NSString stringWithFormat:@"%@ has sent you an image.",displayName];
        else if ([messagetype isEqualToString:kTYPE_VIDEO])
            alertMessage =[NSString stringWithFormat:@"%@ has sent you a video.",displayName];
        else if ([messagetype isEqualToString:kTYPE_AUDIO])
            alertMessage =[NSString stringWithFormat:@"%@ has sent you an audio.",displayName];
        else
            alertMessage=[NSString stringWithFormat:@"%@ says: %@",displayName,body];
        
        
        UIApplication *application = [UIApplication sharedApplication];
        UILocalNotification *localNotif = [[UILocalNotification alloc] init];
        if (localNotif)
        {
            if([messagetype isEqualToString:kTYPE_TEXT])
                localNotif.applicationIconBadgeNumber = [application applicationIconBadgeNumber]+1;
            localNotif.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
            
            localNotif.alertBody = alertMessage;
            localNotif.alertAction =NSLocalizedString(@"Ok", nil);
            localNotif.soundName = UILocalNotificationDefaultSoundName;
            
            
            [application presentLocalNotificationNow:localNotif];
        }
    }
}

#pragma mark -
#pragma mark XMPPStream Connect/Disconnect

- (BOOL)connectWithJID:(NSString *)JID password:(NSString *)myPassword
{
    if (![xmppStream isDisconnected])
    {
        return YES;
    }
    
    
    NSLog(@"Input JID : %@ Password : %@",JID,myPassword);
    
    if(JID == nil || myPassword == nil)
        return NO;
    NSString *resource = [NSString stringWithFormat:@"%@",[self getMessageID]];
    myJID = [XMPPJID jidWithString:JID resource:resource]; NSLog(@"MyJID : %@",myJID);
    [xmppStream setMyJID:myJID];
    [xmppStream setHostName:DOMAIN_NAME];
    [xmppStream setHostPort:5222];
    password = myPassword;
    NSError *error;
    isAnoymous = NO;
    
    NSLog(@"Xmpp Stream : %@",xmppStream);
    
    if (![xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error connecting"
                                                            message:@"See console for error details."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        DDLogError(@"Error connecting: %@", error);
        
        return NO;
    }
    return YES;
}

- (BOOL)anoymousConnection
{
    if (![xmppStream isDisconnected]) {
        return YES;
    }
    NSString *resource = [NSString stringWithFormat:@"%@",kXMPPResource];
    myJID = [XMPPJID jidWithString:[NSString stringWithFormat:@"anoymous@%@",DOMAIN_NAME] resource:resource];
    [xmppStream setMyJID:myJID];
    [xmppStream setHostName:DOMAIN_NAME];
    [xmppStream setHostPort:5222];
    NSError *error;
    isAnoymous = YES;
    if (![xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error connecting"
                                                            message:@"See console for error details."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        DDLogError(@"Error connecting: %@", error);
        
        return NO;
    }
    
    return YES;
}

- (void)disconnect
{
    [self goOffline];
    [xmppStream disconnect];
}

#pragma mark -
#pragma mark XMPPStream Delegate

- (void)xmppStreamDidSecure:(XMPPStream *)sender
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStream:(XMPPStream *)sender willSecureWithSettings:(NSMutableDictionary *)settings
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    if (allowSelfSignedCertificates)
    {
        [settings setObject:[NSNumber numberWithBool:YES] forKey:(NSString *)kCFStreamSSLAllowsAnyRoot];
    }
    
    if (allowSSLHostNameMismatch)
    {
        [settings setObject:[NSNull null] forKey:(NSString *)kCFStreamSSLPeerName];
    }
    else
    {
        // Google does things incorrectly (does not conform to RFC).
        // Because so many people ask questions about this (assume xmpp framework is broken),
        // I've explicitly added code that shows how other xmpp clients "do the right thing"
        // when connecting to a google server (gmail, or google apps for domains).
        
        
        NSString *expectedCertName = nil;
        NSString *serverDomain = xmppStream.hostName;
        NSString *virtualDomain = [xmppStream.myJID domain];
        
        if ([serverDomain isEqualToString:DOMAIN_NAME])
        {
            if ([virtualDomain isEqualToString:DOMAIN_NAME])
            {
                expectedCertName = virtualDomain;
            }
            else
            {
                expectedCertName = serverDomain;
            }
        }
        else if (serverDomain == nil)
        {
            expectedCertName = virtualDomain;
        }
        else
        {
            expectedCertName = serverDomain;
        }
        
        if (expectedCertName)
        {
            [settings setObject:expectedCertName forKey:(NSString *)kCFStreamSSLPeerName];
        }
    }
}


- (void)xmppStreamDidConnect:(XMPPStream *)sender
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    NSError *secureError = nil;
    NSError *authenticationError = nil;
    BOOL isSecureAble = (![sender isSecure])&& [sender supportsStartTLS];
    if (isSecureAble) {
        [sender secureConnection:&secureError];
    }
    if (isAnoymous) {
        if (![[self xmppStream] authenticateAnonymously:&authenticationError])
        {
            if (![[self xmppStream] supportsAnonymousAuthentication]) {
                return;
            }
            DDLogError(@"Can't anoymous: %@", authenticationError);
            isXmppConnected = NO;
            return;
        }
    }else{
        if (![[self xmppStream] authenticateWithPassword:password error:&authenticationError])
        {
            DDLogError(@"Error authenticating: %@", authenticationError);
            isXmppConnected = NO;
            return;
        }
    }
    
    isXmppConnected = YES;
    
    if (isXmppConnected)
    {
        NSLog(@"XMPP is connected now");
    }
}

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    [self goOnline];
    [self sendAllUnsentMessages];
}

- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    [self failedToConnect];
}

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq
{
    return NO;
}

#pragma mark --
#pragma mark -- All Unsent Messages Send
#pragma mark --
-(void)setTabBadgeValue
{
    //    NSDictionary *dictUnread = [SH_Defaults objectForKey:kSHUnreadMessage];
    //    int badgeCount = 0;
    //    for (NSString *strValue in dictUnread.allValues)
    //    {
    //        badgeCount += [strValue intValue];
    //    }
    //
    //    NSNumber *badgeCnt = [NSNumber numberWithInt:badgeCount];
    //    [App_Delegate callApiForBadgeCountInAppIcon:badgeCnt];
    //
    //    MainNavigationController *navController = (MainNavigationController *)App_Delegate.window.rootViewController;
    //    if ([navController isKindOfClass:[MainNavigationController class]])
    //    {
    //        NSArray *arrViewController = [navController viewControllers];
    //        for (int i=0 ;i<arrViewController.count;i++)
    //        {
    //            UIViewController *view = [arrViewController objectAtIndex:i];
    //            if ([view isKindOfClass:[MainTabBarViewController class]])
    //            {
    //                NSArray *arrViewController2 = [(UITabBarController *)view viewControllers];
    //                if (arrViewController2.count > 2)
    //                {
    //                    UINavigationController *nav = [arrViewController2 objectAtIndex:2];
    //                    nav.tabBarItem.badgeValue =[NSString stringWithFormat:@"%d",badgeCount];
    //                    if (badgeCount <= 0)
    //                    {
    //                        nav.tabBarItem.badgeValue =nil;
    //                    }
    //                }
    //                break;
    //            }
    //        }
    //    }
}

-(void)sendAllUnsentMessages
{
    //    [App_Delegate.dbManager getAllUnsentMessagesList];
    //    for (int i=0; i<App_Delegate.aryUnsentMessages.count; i++)
    //    {
    //        MessageObject *message=[App_Delegate.aryUnsentMessages objectAtIndex:i];
    //
    //        NSString *strMessageID=[NSString stringWithFormat:@"%f",message.ID];
    //        if (strMessageID.length<17)
    //            strMessageID=[NSString stringWithFormat:@"0%@",strMessageID];
    //
    //        if ([message.strType isEqualToString:kTYPE_TEXT])
    //        {
    //            [self sendMessage:[self getJIDfromUsername:message.strFriendUserName] Body:message.strBody withID:strMessageID];
    //        }
    //        else if ([message.strType isEqualToString:kTYPE_IMAGE])
    //        {
    //            [self sendImage:[self getJIDfromUsername:message.strFriendUserName] :message.strBody withID:strMessageID];
    //        }
    //        else if ([message.strType isEqualToString:kTYPE_VIDEO])
    //        {
    //            [self sendVideo:[self getJIDfromUsername:message.strFriendUserName] :message.strBody withID:strMessageID];
    //        }
    //        else if ([message.strType isEqualToString:kTYPE_AUDIO])
    //        {
    //            [self sendAudio:[self getJIDfromUsername:message.strFriendUserName] :message.strBody withID:strMessageID];
    //        }
    //    }
}

#pragma mark --
#pragma mark -- Message Sent
#pragma mark --

- (void)xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message
{
    NSString *displayName = [message toStr];
    
    NSArray* arr = [displayName componentsSeparatedByString: @"@"];
    displayName = [arr objectAtIndex: [arr count]-2];
    
    
    NSString *strMsgID=[NSString stringWithFormat:@"%@",[message messageID]];
    NSString *messagetype = [[message elementForName:@"subject"] stringValue];
    NSString *body = [[message elementForName:@"body"] stringValue];
    
    if ([message isChatMessageWithBody] && ![message isErrorMessage])
    {
        displayName=[displayName stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAIN_NAME ] withString:@""];
        
        if(([messagetype isEqualToString:kTYPE_TEXT]||[messagetype isEqualToString:kTYPE_IMAGE]||[messagetype isEqualToString:kTYPE_VIDEO]||[messagetype isEqualToString:kTYPE_AUDIO]) && body )
        {
            if ([self updateMessageofID:[strMsgID doubleValue] Status:MSG_STATUS_SENT])
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:kMessageSentNotification object:strMsgID];
            }
        }
    }
}

#pragma mark --
#pragma mark -- Message Received
#pragma mark --

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    
    if (![message hasReceiptResponse])
    {
        //        //Temp
        //        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Puru" message:[NSString stringWithFormat:@"Received message : %@",[message elementsForName:@"body"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        //        [alertView show];
        //        //------------------------------------------------------------
    }
    
    double currentt = [[NSDate new] timeIntervalSince1970];
    NSTimeInterval differ= [[NSDate dateWithTimeIntervalSince1970:currentt] timeIntervalSinceDate:[NSDate dateWithTimeIntervalSince1970:App_Delegate.lastIncomingMsgTimestamp]];
    
    if (differ >= 0.5)
    {
        App_Delegate.isPlaySonundForContinueMessage = YES;
    }
    else
    {
        App_Delegate.isPlaySonundForContinueMessage = NO;
    }
    
    
    NSString *userStr =[[message elementForName:@"user"] stringValue];
    
    if (userStr != nil)
    {
        userStr =[userStr stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        NSData *dd = [userStr dataUsingEncoding:NSUTF8StringEncoding];
        
        NSDictionary *dictUsr = [NSJSONSerialization JSONObjectWithData:dd options:NSJSONReadingMutableContainers error:nil];
        
        //Save to contact list
        User *user = [User new];
        user.idUser = [dictUsr objectForKey:@"userId"];
        user.name = [dictUsr objectForKey:@"name"];
        user.chatAccountID=[dictUsr objectForKey:@"chatACId"];
        user.chatAccountUserName = [dictUsr objectForKey:@"chatACName"];
        user.chatAccountHashPwd = [dictUsr objectForKey:@"chatACPass"];
        user.avatarUri = [dictUsr objectForKey:@"profilePic"];
        user.gender = [dictUsr objectForKey:@"gender"];
        user.status = [dictUsr objectForKey:@"status"];
        
        NSDate *date=[NSDate date];
        
        double timeStamp= [date timeIntervalSince1970];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:date forKey:@"lastMsgTime"];
        [dict setObject:[NSString stringWithFormat:@"%f",timeStamp] forKey:@"lastMsgTimeStamp"];
        if ([dictUsr objectForKey:@"activityType"])
        {
            [dict setObject:[dictUsr objectForKey:@"activityType"] forKey:@"activityType"];
        }
        
        //[App_Delegate.dbManager insertContactDetail:user dict:dict];
    }
    
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    formatter = [App_Delegate setLocaleForIOS8:formatter];
    [formatter setDateFormat:@"ddMMyyyyHHmmssSSS"];
    NSString *displayName = [message fromStr];
    
    NSArray* arr = [displayName componentsSeparatedByString: @"@"];
    displayName = [arr objectAtIndex: [arr count]-2];
    
    displayName=[displayName stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAIN_NAME ] withString:@""];
    
    FriendObject *frnd=[self getFriendObject:displayName];
    
    NSString *body = [[message elementForName:@"body"] stringValue];
    NSData *data=[[[message elementForName:@"userinfo"] stringValue] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *strUserInfo;
    if (data)
    {
        strUserInfo=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    }
    
    NSString *strBookingId=@"";
    
    if (strUserInfo && [strUserInfo isKindOfClass:[NSMutableDictionary class]] && [strUserInfo objectForKey:@"bookingId"])
    {
        strBookingId=[strUserInfo objectForKey:@"bookingId"];
    }
    
    NSString *messagetype = [[message elementForName:@"subject"] stringValue];
    
    //GROUP CHAT
    if([messagetype isEqualToString:kTYPE_GROUP_CHAT])
    {
        [self didReceiveGroupMessage:message];
        return;
    }
    
    //REMOVE MEMBER
    if([messagetype isEqualToString:kTYPE_REMOVE_MEMBER])
    {
        [self didReceiveGroupMemberRemove:message];
        return;
    }
    
    //EXIT MEMBER
    if([messagetype isEqualToString:kTYPE_EXIT_MEMBER])
    {
        [self didReceiveGroupMemberExit:message];
        return;
    }
    
    //ADD MEMBER
    if([messagetype isEqualToString:kTYPE_ADD_MEMBER])
    {
        [self didReceiveGroupAddMemeber:message];
        return;
    }
    
    //JOIN MEMBER
    if([messagetype isEqualToString:kTYPE_JOIN_MEMBER])
    {
        [self didReceiveGroupJoinMember:message];
        return;
    }
    
    //UPDATE EVENT INFO
    if([messagetype isEqualToString:kTYPE_UPDATE_EVENT_INFO])
    {
        [self didReceiveGroupUpdateEvent:message];
        return;
    }
    
    //SEND EVENT INVITATION
    if([messagetype isEqualToString:kTYPE_EVENT_INVITATION])
    {
        [self didReceiveInvitation:message];
        return;
    }
    
    //DELETE_EVENT
    if([messagetype isEqualToString:kTYPE_EVENT_DELETE])
    {
        [self didReceiveDeleteEvent:message];
        return;
    }
    
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
    {
        
        
    }
    else if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
    {
        if ((body && body.length > 0))
        {
            [self pushLocalNotification:message];
        }
    }
    
    
    //Prevent to receive messages from blocked contacts
    NSMutableArray *arrBlocked =[[SH_Defaults objectForKey:kBlockedUsers] mutableCopy];
    
    NSPredicate *namePredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",displayName];
    NSArray * filtered = [arrBlocked filteredArrayUsingPredicate:namePredicate];
    if (filtered.count>0)
    {
        return;
    }
    
    NSDate *dtMessage;
    if ([message wasDelayed])
    {
        dtMessage = [message delayedDeliveryDate];
    }
    else
    {
        dtMessage = [NSDate date];
    }
    
    if ([message isChatMessage])
    {
        
        //Check if the Chat screen is open and the user is same and check booking id also
        if (!App_Delegate.isChatOpened || ![App_Delegate.strCurrentChatFriend isEqualToString:displayName] || [strBookingId isEqualToString:App_Delegate.strCurrentBookingID]==NO)
        {
            
            #pragma mark -- Increase Count of Unread Messages
            
            if(([messagetype isEqualToString:kTYPE_TEXT]||[messagetype isEqualToString:kTYPE_IMAGE]||[messagetype isEqualToString:kTYPE_VIDEO]||[messagetype isEqualToString:kTYPE_AUDIO]) && body )
            {
                if (![messagetype isEqualToString:kTYPE_SEND_READ_STATUS])// Not increas badge when get read status msg
                {
                    if ([App_Delegate.dictUnreadMessages objectForKey:displayName])
                    {
                        int valueCount=[[App_Delegate.dictUnreadMessages objectForKey:displayName] intValue];
                        
                        [App_Delegate.dictUnreadMessages setObject:[NSString stringWithFormat:@"%d",++valueCount] forKey:displayName];
                        
                        
                        
                        int actvalueCount = [[App_Delegate.dictUnreadMessagesActivity objectForKey:displayName] intValue];
                        
                        [App_Delegate.dictUnreadMessagesActivity setObject:[NSString stringWithFormat:@"%d",++actvalueCount] forKey:displayName];
                        
                    }
                    else
                    {
                        [App_Delegate.dictUnreadMessages setObject:[NSString stringWithFormat:@"%d",1] forKey:displayName];
                        [App_Delegate.dictUnreadMessagesActivity setObject:[NSString stringWithFormat:@"%d",1] forKey:displayName];
                        
                    }
                    
                    //sound play on receive messsage
                    if (App_Delegate.isPlaySonundForContinueMessage)
                    {
                        if ([SH_Defaults boolForKey:kIsAudioOn])
                        {
                            NSString *path = [NSString stringWithFormat:@"%@/ReceiveMsg.wav", [[NSBundle mainBundle] resourcePath]];
                            NSURL *soundUrl = [NSURL fileURLWithPath:path];
                            _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
                            [_audioPlayer play];
                        }
                        if ([SH_Defaults boolForKey:kIsVibrateOn])
                        {
                            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
                        }
                        double currentt = [[NSDate new] timeIntervalSince1970];
                        App_Delegate.lastIncomingMsgTimestamp = currentt;
                    }
                    
                    [SH_Defaults setObject:App_Delegate.dictUnreadMessages forKey:kSHUnreadMessage];
                    [SH_Defaults synchronize];
                    
                    [SH_Defaults setObject:App_Delegate.dictUnreadMessagesActivity forKey:kSHUnreadMessageActivity];
                    [SH_Defaults synchronize];
                    
                    //UnreadMessage booking id to array in appdelegate
                    NSLog(@"Booking Id %@",strBookingId);
                    [App_Delegate.aryUnreadChat addObject:strBookingId];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:kMessageUnreadReceivedNotification object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kMessageUnreadReceivedNotificationActivity object:nil];
                    
                    //[self setTabBadgeValue];
                }
            }
        }
        else
        {
            if (![messagetype isKindOfClass:[NSNull class]])
            {
                if (![messagetype isEqualToString:kTYPE_SEND_READ_STATUS])
                {
                    if ([messagetype isEqualToString:kTYPE_TEXT])
                    {
                        //sound play on receive messsage
                        if ([SH_Defaults boolForKey:kIsAudioOn])
                        {
                            NSString *path = [NSString stringWithFormat:@"%@/message_send.wav", [[NSBundle mainBundle] resourcePath]];
                            NSURL *soundUrl = [NSURL fileURLWithPath:path];
                            _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
                            [_audioPlayer play];
                        }
                        if ([SH_Defaults boolForKey:kIsVibrateOn])
                        {
                            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
                        }
                        
                        [self sendReadStatus:displayName];
                    }
                }
            }
        }
        
        if([messagetype isEqualToString:kTYPE_TEXT])
        {
            
            
            #pragma mark -- Text Message Received
            if (!body || body.length <= 0)
                return;
            
            MessageObject *messageObj=[[MessageObject alloc]initWithDefault];
            NSString *strMessageID=@"";
            if ([message wasDelayed])
            {
                NSDate *date=[message delayedDeliveryDate];
                NSString *str=[formatter stringFromDate:date];
                strMessageID=str;
            }
            else
            {
                strMessageID=[self getMessageID];
            }
            
            if (strMessageID.length<17)
                strMessageID=[NSString stringWithFormat:@"0%@",strMessageID];
            
            
            messageObj.ID=[strMessageID doubleValue];
            messageObj.strMsgId = strMessageID;
            messageObj.strBody=body;
            
            NSDate *date=[formatter dateFromString:strMessageID];
            messageObj.timeStamp=[date timeIntervalSince1970];
            messageObj.strTime=[NSString stringWithFormat:@"%@",date];
            messageObj.strTypeOfSending=@"in";
            messageObj.strType=kTYPE_TEXT;
            messageObj.status=MSG_STATUS_RECEIVED;
            messageObj.strFriendUserName=displayName;
            messageObj.strBookingID=strBookingId;
            
            [App_Delegate.dbManager insertMessage:messageObj];
            NSString * str=[NSString stringWithFormat:@"%@@%@",frnd.strChatID,[SH_Defaults objectForKey:kSHChatID]];
            [SH_Defaults setObject:@"1" forKey:str];
            [SH_Defaults synchronize];
            
            BOOL bb = App_Delegate.isChatOpened;
            NSString *str1 = App_Delegate.strCurrentChatFriend;
            str1 = [str1 lowercaseString];
            displayName = [displayName lowercaseString];
            if (bb && [str1 isEqualToString:displayName] && [strBookingId isEqualToString:App_Delegate.strCurrentBookingID])
            {
                [App_Delegate.aryChat addObject:messageObj];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kMessageReceivedNotification object:nil];
            }
            else
            {
                NSString *str;
                
                if (frnd)
                {
                    if ([frnd.strUserID respondsToSelector:@selector(length)] && [frnd.strUserID length]>0)
                    {
                        if ([[SH_Defaults objectForKey:kSHInAppPreview] boolValue]==YES)
                        {
                            if([frnd.strUserID isEqualToString:[SH_Defaults objectForKey:kSHUserID]])
                                return ;
                            
                            str=[NSString stringWithFormat:@"%@ says: %@",frnd.strName,body];
                            
                            [self receiveNotification:str :frnd.strThumbUrl :displayName :frnd];
                        }
                        if ([[SH_Defaults objectForKey:kSHInAppVibrate] boolValue]==YES)
                        {
                            //   AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                        }
                    }
                    else
                    {
                    }
                }
                else
                {
                    
                }
                
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kMessageUnreadReceivedNotification object:nil];
        }
        else if([messagetype isEqualToString:kTYPE_SEND_READ_STATUS] )
        {
            #pragma mark -- Manage Read Status
            BOOL update = [self didManageReadMessages:displayName andDate:dtMessage];
            if (update) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kTYPE_SEND_READ_STATUS object:nil];
            }
            //Reload chat vc
        }
        else if([messagetype isEqualToString:kTYPE_IMAGE])
        {
            #pragma mark -- Image Message Received
            [self downloadThumbImage:message from:displayName];
        }
        else if([messagetype isEqualToString:kTYPE_VIDEO])
        {
            #pragma mark -- Video Message Received
            [self downloadThumbImage:message from:displayName];
        }
        else if([messagetype isEqualToString:kTYPE_AUDIO])
        {
            #pragma mark -- Audio Message Received
            [self downloadThumbImage:message from:displayName];
        }
    }
    
    if([message hasReceiptResponse])
    {
        #pragma mark -- Receipt Response Received
        
        NSString *strMsgID=[message receiptResponseID];
        
        NSString *strMessageID=@"";
        if ([message wasDelayed])
        {
            NSDate *date=[message delayedDeliveryDate];
            NSString *str=[formatter stringFromDate:date];
            strMessageID=str;
        }
        else
        {
            strMessageID=[self getMessageID];
        }
        
        if ([self updateMessageofID:[strMsgID doubleValue] Status:MSG_STATUS_RECEIVED])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kMessageReceiptNotification object:strMsgID];
        }
    }
    else
    {
        
    }
}

//Read status

//Manage Read messages
-(void)sendReadStatus:(NSString *)strFriendId
{
    if ([App_Delegate isInternetAvailable])
    {
        NSString *strMessageID=[App_Delegate.xmppManager getMessageID];
        [App_Delegate.xmppManager sendCustomMessage:strFriendId withSubject:kTYPE_SEND_READ_STATUS Body:strFriendId withID:strMessageID];
    }
}
-(BOOL)didManageReadMessages:(NSString*)strFriendId andDate:(NSDate *)date
{
    BOOL isUpdatedReadStatus= [App_Delegate.dbManager updateReadStatus:strFriendId];
    
    return isUpdatedReadStatus;
}

#pragma mark --
#pragma mark -- Sending XMPP Custom Message
#pragma mark --

-(void)sendCustomMessage:(NSString *)to withSubject:(NSString *)subject Body:(NSString *)body  withID:(NSString *)messageID
{
    XMPPMessage *message1 = [[XMPPMessage alloc] initWithType:@"chat" to:[self getJIDfromUsername:to] elementID:messageID];
    NSXMLElement *mainElement = [NSXMLElement elementWithName:@"subject" stringValue:subject];
    NSXMLElement *element = [NSXMLElement elementWithName:@"body" stringValue:body];
    NSXMLElement *element1 = [NSXMLElement elementWithName:@"message" stringValue:@"text"];
    
    [message1 addChild:mainElement];
    [message1 addChild:element];
    [message1 addChild:element1];
    [message1 addReceiptRequest];
    XMPPElementReceipt *receipt = [xmppProcessOne goOnStandby];
    [xmppStream sendElement:message1 andGetReceipt:&receipt];
}

#pragma mark --
#pragma mark -- Sending XMPP Custom Message (Without Receipt)
#pragma mark --

-(void)sendCustomMessageWithoutReceipt:(NSString *)to withSubject:(NSString *)subject Body:(NSString *)body withID:(NSString *)messageID
{
    XMPPMessage *message1 = [[XMPPMessage alloc] initWithType:@"chat" to:[self getJIDfromUsername:to] elementID:messageID];
    NSXMLElement *mainElement = [NSXMLElement elementWithName:@"subject" stringValue:subject];
    NSXMLElement *element = [NSXMLElement elementWithName:@"body" stringValue:body];
    NSXMLElement *element1 = [NSXMLElement elementWithName:@"message" stringValue:@"text"];
    
    [message1 addChild:mainElement];
    [message1 addChild:element];
    [message1 addChild:element1];
    
    [xmppStream sendElement:message1];
}

-(BOOL)updateMessageofID:(double)messageID Status:(int)status
{
    __block MessageObject *messageOLD;
    [App_Delegate.aryChat enumerateObjectsUsingBlock:^(id object, NSUInteger idx, BOOL *stop)
     {
         MessageObject *frnd=object;
         if(frnd.ID == messageID)
         {
             messageOLD=frnd;
         }
     }];
    
    if([App_Delegate.dbManager updateStatusMessage:status ofID:messageID])
    {
        if (App_Delegate.isChatOpened && [App_Delegate.strCurrentChatFriend isEqualToString:messageOLD.strFriendUserName])
        {
            MessageObject *messageNew=[App_Delegate.dbManager getParticularMessage:messageID];
            
            
            if ([App_Delegate.aryChat containsObject:messageOLD ])
            {
                [App_Delegate.aryChat replaceObjectAtIndex:[App_Delegate.aryChat indexOfObject:messageOLD] withObject:messageNew];
            }
            return YES;
        }
    }
    return NO;
}

-(int)isFriendExist:(NSMutableArray *)array username:(NSString *)username
{
    __block int index=-1;
    __block BOOL isFriend=NO;
    [array enumerateObjectsUsingBlock:^(id object, NSUInteger idx, BOOL *stop)
     {
         FriendObject *frnd=object;
         if([frnd.strUserID isEqualToString:username])
         {
             isFriend=YES;
             index=idx;
         }
     }];
    return index;
}
-(BOOL)isUserOnline:(NSString *)friend_user_name
{
    XMPPUserCoreDataStorageObject *user = [xmppRosterStorage userForJID:[self getJIDfromUsername:friend_user_name]
                                                             xmppStream:xmppStream
                                                   managedObjectContext:[self managedObjectContext_roster]];
    
    if ([user isOnline])
    {
        return true;
    }
    else
    {
        //[self sendPresense:@"subscribe" :friend_user_name];
    }
    
    return false;
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
    if([xmppMUC isMUCRoomPresence:presence]){
        //NSLog(@"Get Room presence");
    }
    
    NSString *presenceFromUser = [[presence from] user];
    
    if (presence && [[presence type] isEqualToString:@"subscribe"])
    {
        [self sendPresense:@"subscribed" :presenceFromUser];
    }
    if ([[presence status] isEqualToString:@"Online"])
    {
        // send post notification from here
        //notification_online
        NSString *strPresence=@"Online";
        
        NSMutableDictionary *dictPresence =[NSMutableDictionary new];
        [dictPresence setObject:presenceFromUser forKey:@"presenceOfUserJid"];
        [dictPresence setObject:strPresence forKey:@"presence"];
        
        if (![App_Delegate.arrOnlineUsers containsObject:presenceFromUser])
        {
            [App_Delegate.arrOnlineUsers addObject:presenceFromUser];
        }
    }
    else
    {
        if ([App_Delegate.arrOnlineUsers containsObject:presenceFromUser])
        {
            [App_Delegate.arrOnlineUsers removeObject:presenceFromUser];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSetOnline object:nil];
    if ([presence isErrorPresence]) {
        if ([[presence elementForName:XMPP_ERROR] elementForName:XMPP_ERROR_CONFILCT]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNickNameConflictNotification object:self];
        }
    }
    
}

- (void)xmppStream:(XMPPStream *)sender didReceiveError:(id)error
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kServerDisconnect object:self];
    if (!isXmppConnected)
    {
        DDLogError(@"Unable to connect to server. Check xmppStream.hostName");
        [self failedToConnect];
    }
    else {
    }
}

#pragma mark Core Data

- (NSManagedObjectContext *)managedObjectContext_roster
{
    
    return [xmppRosterStorage mainThreadManagedObjectContext];
    if (managedObjectContext_roster == nil)
    {
        managedObjectContext_roster = [[NSManagedObjectContext alloc] init];
        
        NSPersistentStoreCoordinator *psc = [xmppRosterStorage persistentStoreCoordinator];
        [managedObjectContext_roster setPersistentStoreCoordinator:psc];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(contextDidSave:)
                                                     name:NSManagedObjectContextDidSaveNotification
                                                   object:nil];
    }
    
    
    return managedObjectContext_roster;
}



- (NSManagedObjectContext *)managedObjectContext_messages
{
    NSAssert([NSThread isMainThread],
             @"NSManagedObjectContext is not thread safe. It must always be used on the same thread/queue");
    
    if (managedObjectContext_messages == nil)
    {
        managedObjectContext_messages = [[NSManagedObjectContext alloc] init];
        
        NSPersistentStoreCoordinator *psc = [xmppMessageArchivingStorage persistentStoreCoordinator];
        [managedObjectContext_messages setPersistentStoreCoordinator:psc];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(messagesContextDidSave:)
                                                     name:NSManagedObjectContextDidSaveNotification
                                                   object:nil];
    }
    
    return managedObjectContext_messages;
}

- (NSManagedObjectContext *)managedObjectContext_chatroom
{
    NSAssert([NSThread isMainThread],
             @"NSManagedObjectContext is not thread safe. It must always be used on the same thread/queue");
    return nil;
}


- (NSManagedObjectContext *)managedObjectContext_capabilities
{
    NSAssert([NSThread isMainThread],
             @"NSManagedObjectContext is not thread safe. It must always be used on the same thread/queue");
    
    if (managedObjectContext_capabilities == nil)
    {
        managedObjectContext_capabilities = [[NSManagedObjectContext alloc] init];
        
        NSPersistentStoreCoordinator *psc = [xmppCapabilitiesStorage persistentStoreCoordinator];
        [managedObjectContext_roster setPersistentStoreCoordinator:psc];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(contextDidSave:)
                                                     name:NSManagedObjectContextDidSaveNotification
                                                   object:nil];
    }
    
    return managedObjectContext_capabilities;
}

- (void)contextDidSave:(NSNotification *)notification
{
    NSManagedObjectContext *sender = (NSManagedObjectContext *)[notification object];
    
    if (sender != managedObjectContext_roster &&
        [sender persistentStoreCoordinator] == [managedObjectContext_roster persistentStoreCoordinator])
    {
        DDLogVerbose(@"%@: %@ - Merging changes into managedObjectContext_roster", THIS_FILE, THIS_METHOD);
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [managedObjectContext_roster mergeChangesFromContextDidSaveNotification:notification];
        });
    }
    
    if (sender != managedObjectContext_capabilities &&
        [sender persistentStoreCoordinator] == [managedObjectContext_capabilities persistentStoreCoordinator])
    {
        DDLogVerbose(@"%@: %@ - Merging changes into managedObjectContext_capabilities", THIS_FILE, THIS_METHOD);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [managedObjectContext_capabilities mergeChangesFromContextDidSaveNotification:notification];
        });
    }
}
- (void)messagesContextDidSave:(NSNotification *)notification
{
    NSManagedObjectContext *sender = (NSManagedObjectContext *)[notification object];
    if(sender != managedObjectContext_messages &&
       [sender persistentStoreCoordinator] == [managedObjectContext_messages persistentStoreCoordinator])
    {
        DDLogVerbose(@"%@: %@ - Merging changes into managedObjectContext_message", THIS_FILE, THIS_METHOD);
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [managedObjectContext_messages mergeChangesFromContextDidSaveNotification:notification];
            
        });
    }
}

#pragma mark NSuserFetchedResultsController

- (NSFetchedResultsController *)rosterFetchedResultsController
{
    if (rosterFetchedResultsController == nil)
    {
        NSManagedObjectContext *moc = [self managedObjectContext_roster];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                                  inManagedObjectContext:moc];
        
        NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"sectionNum" ascending:YES];
        NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"displayName" ascending:YES];
        
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, sd2, nil];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat: @"streamBareJidStr == %@", [[xmppStream myJID] bare]];
        [fetchRequest setEntity:entity];
        [fetchRequest setSortDescriptors:sortDescriptors];
        [fetchRequest setFetchBatchSize:10];
        [fetchRequest setPredicate:predicate];
        [fetchRequest setReturnsObjectsAsFaults:NO];
        rosterFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                             managedObjectContext:moc
                                                                               sectionNameKeyPath:@"sectionNum"
                                                                                        cacheName:nil];
        [rosterFetchedResultsController setDelegate:self];
        NSError *error = nil;
        if (![rosterFetchedResultsController performFetch:&error])
        {
            //NSLog(@"Error performing fetch: %@", error);
        }
        
    }
    
    return rosterFetchedResultsController;
}



- (NSFetchedResultsController *)messagesFetchedResultsController:(NSString *)bareJidStr addDelegate:(id)delegate
{
    if (messagesFetchedResultsController == nil)
    {
        NSManagedObjectContext *moc = [self managedObjectContext_messages];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject"
                                                  inManagedObjectContext:moc];
        
        NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:YES];
        
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, nil];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr == %@ AND bareJidStr == %@",[myJID bare], bareJidStr];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        [fetchRequest setSortDescriptors:sortDescriptors];
        [fetchRequest setFetchBatchSize:20];
        [fetchRequest setPredicate:predicate];
        [fetchRequest setReturnsObjectsAsFaults:NO];
        messagesFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                               managedObjectContext:moc
                                                                                 sectionNameKeyPath:@"sectionNum"
                                                                                          cacheName:nil];
    }else{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"streamBareJidStr == %@ AND bareJidStr == %@",[myJID bare], bareJidStr];
        [messagesFetchedResultsController.fetchRequest setPredicate:predicate];
    }
    [messagesFetchedResultsController setDelegate:delegate];
    
    NSError *error = nil;
    if (![messagesFetchedResultsController performFetch:&error])
    {
        //NSLog(@"Error performing fetch: %@", error);
    }
    
    
    return messagesFetchedResultsController;
}
#pragma mark --
#pragma mark -- Download Thumbnail of Image
#pragma mark --

-(void)downloadThumbImage:(XMPPMessage *)message from:(NSString *)displayName
{
    
    NSArray *arr3  = [displayName componentsSeparatedByString:@"_"];
    displayName = [[arr3 objectAtIndex:0] uppercaseString];
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    formatter = [App_Delegate setLocaleForIOS8:formatter];
    [formatter setDateFormat:@"ddMMyyyyHHmmssSSS"];
    
    NSString *body = [[message elementForName:@"body"] stringValue];
    NSString *messagetype = [[message elementForName:@"subject"] stringValue];
    
    MessageObject *messageObj=[[MessageObject alloc]initWithDefault];
    
    
    NSString *strMessageID=@"";
    if ([message wasDelayed])
    {
        NSDate *date=[message delayedDeliveryDate];
        NSString *str=[formatter stringFromDate:date];
        strMessageID=str;
    }
    else
    {
        strMessageID=[self getMessageID];
    }
    
    
    if (strMessageID.length<17)
        strMessageID=[NSString stringWithFormat:@"0%@",strMessageID];
    
    messageObj.ID=[strMessageID doubleValue];
    messageObj.strMsgId =strMessageID;
    messageObj.strBody=body;
    
    NSDate *date=[formatter dateFromString:strMessageID];
    messageObj.timeStamp=[date timeIntervalSince1970];
    messageObj.strTime=[NSString stringWithFormat:@"%@",date];
    messageObj.strTypeOfSending=@"in";
    messageObj.strType=messagetype;
    messageObj.status=MSG_STATUS_RECEIVED;
    messageObj.strFriendUserName=displayName;
    
    NSArray *arr=[body componentsSeparatedByString: @"|"];
    for (int i=0; i<arr.count; i++)
    {
        NSString *tmpDownlodFilePath=[arr objectAtIndex: i];
        if (i==0)
            messageObj.strMainUrl=tmpDownlodFilePath;
        else if (i==1)
            messageObj.strThumbUrl=tmpDownlodFilePath;
        else if (i==2)
            messageObj.strSizeofFile=tmpDownlodFilePath;
        else if (i==3)
            messageObj.strDurationofFile=tmpDownlodFilePath;
        
    }
    
    [App_Delegate.dbManager insertMessage:messageObj];
    if (App_Delegate.isChatOpened && [App_Delegate.strCurrentChatFriend isEqualToString:displayName] )
    {
        [App_Delegate.aryChat addObject:messageObj];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMessageReceivedNotification object:nil];
    }
    else
    {
        NSString *str;
        
        FriendObject *frnd=[self getFriendObject:displayName];
        
        
        if ([messagetype isEqualToString:kTYPE_IMAGE])
            str =[NSString stringWithFormat:@"%@ has sent you an image.",displayName];
        else if ([messagetype isEqualToString:kTYPE_VIDEO])
            str =[NSString stringWithFormat:@"%@ has sent you a video.",displayName];
        else if ([messagetype isEqualToString:kTYPE_AUDIO])
            str =[NSString stringWithFormat:@"%@ has sent you an audio.",displayName];
        else
            str=[NSString stringWithFormat:@"%@ says: %@",displayName,body];
        
        [self receiveNotification:str :@"" :displayName :frnd];
        
        if (frnd)
        {
            if ([frnd.strUserID respondsToSelector:@selector(length)] && [frnd.strUserID length]>0)
            {
                if ([[SH_Defaults objectForKey:kSHInAppPreview] boolValue]==YES)
                {
                    if([frnd.strUserID isEqualToString:[SH_Defaults objectForKey:kSHUserID]])
                        return ;
                    
                    [self receiveNotification:str :frnd.strThumbUrl :displayName :frnd];
                    
                }
                if ([[SH_Defaults objectForKey:kSHInAppVibrate] boolValue]==YES)
                {
                    //AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                }
            }
        }
    }
    if ([messagetype isEqualToString:kTYPE_AUDIO])
        return;
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    
    [dict setObject:[NSString stringWithFormat:@"%@@%@",[SH_Defaults objectForKey:kSHXMPPmyJID],DOMAIN_NAME] forKey:@"account"];
    [dict setObject:[NSString stringWithFormat:@"%@",messageObj.strThumbUrl] forKey:@"file"];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark --
#pragma mark -- Sending Text Message
#pragma mark --


- (void)sendMessageTo:(XMPPJID *)targetBareID withMessage:(NSString *)newMessage;
{
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:newMessage];
    
    XMPPMessage *message = [XMPPMessage elementWithName:@"message"];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addAttributeWithName:@"to" stringValue:[targetBareID full]];
    [message addAttributeWithName:@"from" stringValue:[myJID full]];
    int timeStamp = (int)[[NSDate date] timeIntervalSince1970];
    NSString * messageID = [NSString stringWithFormat:@"%@%d%@",[myJID user],timeStamp,[targetBareID user]];
    [message addAttributeWithName:@"id" stringValue:messageID];
    
    NSXMLElement * receiptRequest = [NSXMLElement elementWithName:@"request"];
    [receiptRequest addAttributeWithName:@"xmlns" stringValue:@"urn:xmpp:receipts"];
    [message addChild:receiptRequest];
    [message addChild:body];
    XMPPElementReceipt *receipt = nil;
    [xmppStream sendElement:message andGetReceipt:&receipt];
    if ([receipt wait:-1])
    {
        
    };
}

#pragma mark --
#pragma mark -- Sending Text Message
#pragma mark --

-(void)sendMessage:(XMPPJID *)to Body:(NSString *)body withID:(NSString *)messageID
{
    XMPPMessage *message1 = [[XMPPMessage alloc] initWithType:@"chat" to:to elementID:messageID];
    NSXMLElement *mainElement = [NSXMLElement elementWithName:@"subject" stringValue:kTYPE_TEXT];
    NSXMLElement *element = [NSXMLElement elementWithName:@"body" stringValue:body];
    NSXMLElement *element1 = [NSXMLElement elementWithName:@"message" stringValue:@"text"];
    NSXMLElement *element2 = [NSXMLElement elementWithName:@"deviceid" stringValue:[SH_Defaults objectForKey:kSHNewaerID]];
    
    [message1 addChild:mainElement];
    [message1 addChild:element];
    [message1 addChild:element1];
    [message1 addReceiptRequest];
    [message1 addChild:element2];
    XMPPElementReceipt *receipt = [xmppProcessOne goOnStandby];
    [xmppStream sendElement:message1 andGetReceipt:&receipt];
}


#pragma mark --
#pragma mark -- Sending Text Message
#pragma mark --

-(void)sendMessageTo:(XMPPJID *)to message:(NSString *)body withID:(NSString *)messageID withUserInfo:(NSString*)userInfo
{
    XMPPMessage *message1 = [[XMPPMessage alloc] initWithType:@"chat" to:to elementID:messageID];
    NSXMLElement *mainElement = [NSXMLElement elementWithName:@"subject" stringValue:kTYPE_TEXT];
    NSXMLElement *element = [NSXMLElement elementWithName:@"body" stringValue:body];
    NSXMLElement *element1 = [NSXMLElement elementWithName:@"message" stringValue:@"text"];
    NSXMLElement *element2 = [NSXMLElement elementWithName:@"userinfo" stringValue:userInfo];
    [message1 addChild:mainElement];
    [message1 addChild:element];
    [message1 addChild:element1];
    [message1 addChild:element2];
    [message1 addReceiptRequest];
    XMPPElementReceipt *receipt = [xmppProcessOne goOnStandby];
    [xmppStream sendElement:message1 andGetReceipt:&receipt];
    
    NSLog(@"send message %@",message1);
}

-(void)sendMessage:(XMPPJID *)to :(NSString *)body withID:(NSString *)messageID withUserDetail:(NSString *)user
{
    XMPPMessage *message1 = [[XMPPMessage alloc] initWithType:@"chat" to:to elementID:messageID];
    NSXMLElement *mainElement = [NSXMLElement elementWithName:@"subject" stringValue:kTYPE_TEXT];
    NSXMLElement *element = [NSXMLElement elementWithName:@"body" stringValue:body];
    NSXMLElement *element1 = [NSXMLElement elementWithName:@"message" stringValue:@"text"];
    NSXMLElement *userDetail = [NSXMLElement elementWithName:@"user" stringValue:user];
    [message1 addChild:mainElement];
    [message1 addChild:element];
    [message1 addChild:element1];
    [message1 addChild:userDetail];
    [message1 addReceiptRequest];
    XMPPElementReceipt *receipt = [xmppProcessOne goOnStandby];
    [xmppStream sendElement:message1 andGetReceipt:&receipt];
}

#pragma mark --
#pragma mark -- Sending Image Message
#pragma mark --

-(void)sendImage:(XMPPJID *)to :(NSString *)body withID:(NSString *)messageID
{
    XMPPMessage *message1 = [[XMPPMessage alloc] initWithType:@"chat" to:to elementID:messageID];
    
    NSXMLElement *mainElement = [NSXMLElement elementWithName:@"subject" stringValue:kTYPE_IMAGE];
    NSXMLElement *element = [NSXMLElement elementWithName:@"body" stringValue:body];
    NSXMLElement *element1 = [NSXMLElement elementWithName:@"message" stringValue:@"text"];
    
    [message1 addChild:mainElement];
    [message1 addChild:element];
    [message1 addChild:element1];
    [message1 addReceiptRequest];
    
    XMPPElementReceipt *receipt = [xmppProcessOne goOnStandby];
    [xmppStream sendElement:message1 andGetReceipt:&receipt];
}

#pragma mark --
#pragma mark -- Sending Video Message
#pragma mark --

-(void)sendVideo:(XMPPJID *)to :(NSString *)body withID:(NSString *)messageID
{
    XMPPMessage *message1 = [[XMPPMessage alloc] initWithType:@"chat" to:to elementID:messageID];
    
    NSXMLElement *mainElement = [NSXMLElement elementWithName:@"subject" stringValue:kTYPE_VIDEO];
    NSXMLElement *element = [NSXMLElement elementWithName:@"body" stringValue:body];
    NSXMLElement *element1 = [NSXMLElement elementWithName:@"message" stringValue:@"text"];
    
    [message1 addChild:mainElement];
    [message1 addChild:element];
    [message1 addChild:element1];
    [message1 addReceiptRequest];
    
    XMPPElementReceipt *receipt = [xmppProcessOne goOnStandby];
    [xmppStream sendElement:message1 andGetReceipt:&receipt];
}

#pragma mark --
#pragma mark -- Sending Audio Message
#pragma mark --

-(void)sendAudio:(XMPPJID *)to :(NSString *)body withID:(NSString *)messageID
{
    XMPPMessage *message1 = [[XMPPMessage alloc] initWithType:@"chat" to:to elementID:messageID];
    
    NSXMLElement *mainElement = [NSXMLElement elementWithName:@"subject" stringValue:kTYPE_AUDIO];
    NSXMLElement *element = [NSXMLElement elementWithName:@"body" stringValue:body];
    NSXMLElement *element1 = [NSXMLElement elementWithName:@"message" stringValue:@"text"];
    
    [message1 addChild:mainElement];
    [message1 addChild:element];
    [message1 addChild:element1];
    [message1 addReceiptRequest];
    
    XMPPElementReceipt *receipt = [xmppProcessOne goOnStandby];
    [xmppStream sendElement:message1 andGetReceipt:&receipt];
}

#pragma mark --
#pragma mark -- Message Notification
#pragma mark --

-(void)receiveNotification:(NSString *)textMsg :(NSString *)image :(NSString *)from :(FriendObject *)frndObj
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    self.messageNotification=[[MessageNotificationScreen alloc]init ];
    if (!IS_IOS_7)
        self.messageNotification.view.center=CGPointMake(self.messageNotification.view.frame.size.width/2,-self.messageNotification.view.frame.size.height);
    else
        self.messageNotification.view.center=CGPointMake(self.messageNotification.view.frame.size.width/2,-self.messageNotification.view.frame.size.height);
    
    [self.messageNotification.imgView sd_setImageWithURL:[NSURL URLWithString:image] placeholderImage:[UIImage imageNamed:@"default_face.png"]];
    self.messageNotification.lblMsg.font=[UIFont fontWithName:@"Raleway-Medium" size:14.0f];
    self.messageNotification.lblMsg.text=textMsg;
    self.messageNotification.strMsgFrom=from;
    self.messageNotification.frndObj=frndObj;
    
    [App_Delegate.window addSubview:self.messageNotification.view];
    [self.messageNotification.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:4.0];
    [self performSelector:@selector(unHideStatusBar) withObject:from afterDelay:4.0];
    [UIView animateWithDuration:0.7 animations:^{
        if (!IS_IOS_7)
            self.messageNotification.view.center=CGPointMake(self.messageNotification.view.frame.size.width/2, self.messageNotification.view.frame.size.height/2);
        else
            self.messageNotification.view.center=CGPointMake(self.messageNotification.view.frame.size.width/2, self.messageNotification.view.frame.size.height/2);
        
    } completion:^(BOOL finished)
     {
         
     }];
}
-(void)unHideStatusBar
{
    if (App_Delegate.isChatOpened)
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    else
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}
#pragma mark --
#pragma mark -- Get XMPP JID From Username
#pragma mark --

-(XMPPJID*)getJIDfromUsername:(NSString*)strUser
{
    strUser=[strUser stringByAppendingFormat:@"@"];
    strUser=[strUser stringByAppendingFormat:DOMAIN_NAME];
    XMPPJID *toJID = [XMPPJID jidWithString:strUser];
    return toJID;
}

#pragma mark --
#pragma mark -- Get MessageID
#pragma mark --
-(NSDate *) toLocalTime:(NSDate *)dt
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: dt];
    return [NSDate dateWithTimeInterval: seconds sinceDate: dt];
}
-(NSString*)getMessageID
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    formatter = [App_Delegate setLocaleForIOS8:formatter];
    [formatter setDateFormat:@"ddMMyyyyHHmmssSSS"];
    
    NSString *strTimeStamp=[formatter stringFromDate:[NSDate date]];
    
    NSString *messageID=[NSString stringWithFormat:@"%.0f",[strTimeStamp doubleValue]];
    if (messageID.length<17)
        messageID=[NSString stringWithFormat:@"0%@",messageID];
    return messageID;
}

#pragma mark --
#pragma mark -- Get Friend Name
#pragma mark --

-(FriendObject *)getFriendObject:(NSString *)username
{
    __block FriendObject *dic;
    [App_Delegate.aryFriends enumerateObjectsUsingBlock:^(id object, NSUInteger idx, BOOL *stop)
     {
         FriendObject *frnd=object;
         if([frnd.strChatID isEqualToString:username])
         {
             dic=frnd;
         }
     }];
    return dic;
}

-(NSString *)getFriendName:(NSString *)username
{
    __block NSString *str;
    str=username;
    return str;
}

#pragma mark --
#pragma mark -- Internet Notification
#pragma mark --


-(void)reachabilityChanged:(NSNotification*)note
{
    Reachability * reach = [note object];
    
    NetworkStatus remoteHostStatus = [reach currentReachabilityStatus];
    if (remoteHostStatus !=NotReachable)
    {
        if ([self.xmppStream isDisconnected])
        {
            NSString *completeJID = [[SH_Defaults objectForKey:kSHXMPPmyJID] stringByAppendingFormat:@"@%@",DOMAIN_NAME];
            [self connectWithJID:completeJID password:[SH_Defaults objectForKey:kSHXMPPmyPassword]];
        }
        else
        {
            [self sendAllUnsentMessages];
        }
    }
    else
    {
        [self.xmppStream disconnect];
    }
}

-(void)sendPresense:(NSString *)type :(NSString *)to
{
    XMPPJID *toJID = [self getJIDfromUsername:to];
    XMPPPresence *presence = [XMPPPresence presenceWithType:type to:toJID];
    [[self xmppStream] sendElement:presence];
}

#pragma mark - Event Chat

- (void)didReceiveGroupMessage:(XMPPMessage *)message
{
    //    NSString *jsonBody = [[message elementForName:@"body"] stringValue];
    //    NSData *dd = [jsonBody dataUsingEncoding:NSUTF8StringEncoding];
    //
    //    NSDictionary *dicRes = [NSJSONSerialization JSONObjectWithData:dd options:NSJSONReadingMutableContainers error:nil];
    //
    //    NSString *body = [dicRes objectForKey:@"message"];
    //
    //    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    //    formatter = [App_Delegate setLocaleForIOS8:formatter];
    //    [formatter setDateFormat:@"ddMMyyyyHHmmssSSS"];
    //
    //    if ([dicRes isKindOfClass:[NSDictionary class]])
    //    {
    //        NSString *messagetype = [dicRes objectForKey:@"type"];
    //        NSString *displayName =[dicRes objectForKey:@"event_id"];
    //        NSString *senderID =[dicRes objectForKey:@"sender_jid"];
    //        NSArray* arr = [senderID componentsSeparatedByString: @"@"];
    //        senderID = [arr objectAtIndex: [arr count]-2];
    //
    //        senderID=[senderID stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",DOMAIN_NAME] withString:@""];
    //
    //        //Check group exist or not when group message received
    ////        if (![App_Delegate.dbManager checkIsGroupExistOrNot:[dicRes objectForKey:@"event_id"]])// Check if group exist in our DB or not if not then create it via get group info procedure
    ////        {
    ////            //[self requestGetPerticulerGroupDetail:[dicRes objectForKey:@"event_id"]];
    ////        }
    //
    //        //GroupObject *frnd=[App_Delegate.dbManager getParticularGroup:displayName];
    //
    //        __block NSDictionary *senderDetails;
    //        NSString *query =[NSString stringWithFormat:@"SELECT * FROM EventMemberList where userEventId = \"%@\"",displayName];
    //
    ////        NSArray *aryGroupMembers = [App_Delegate.dbManager getMembersOfEvents:query];
    ////        [aryGroupMembers enumerateObjectsUsingBlock:^(id object, NSUInteger idx, BOOL *stop)
    ////         {
    ////             NSDictionary *frnd=object;
    ////             if([[frnd objectForKey:@"userChatName"] isEqualToString:senderID])
    ////             {
    ////                 senderDetails=frnd;
    ////             }
    ////         }];
    //
    //        NSString *senderName =[senderDetails objectForKey:@"name"];
    //
    //        senderName =[senderDetails objectForKey:@"name"];
    //        if ([message isChatMessage])
    //        {
    //            //Group message received and user is not on chat screen, set badge also here
    //            if (!App_Delegate.isGroupChatOpened || ![App_Delegate.strGroupCurrentChatFriend isEqualToString:displayName])
    //            {
    //#pragma mark -- Increase Count of Unread Messages
    //
    //                if ([App_Delegate.dictUnreadMessages objectForKey:displayName])
    //                {
    //                    int valueCount=[[App_Delegate.dictUnreadMessages objectForKey:displayName] intValue];
    //                    [App_Delegate.dictUnreadMessages setObject:[NSString stringWithFormat:@"%d",++valueCount] forKey:displayName];
    //
    //
    //                    int evtValueCount = [[App_Delegate.dictUnreadMessagesEvent objectForKey:displayName] intValue];
    //
    //                    [App_Delegate.dictUnreadMessagesEvent setObject:[NSString stringWithFormat:@"%d",++evtValueCount] forKey:displayName];
    //
    //                }
    //                else
    //                {
    //                    [App_Delegate.dictUnreadMessages setObject:[NSString stringWithFormat:@"%d",1] forKey:displayName];
    //                    [App_Delegate.dictUnreadMessagesEvent setObject:[NSString stringWithFormat:@"%d",1] forKey:displayName];
    //                }
    //                [SH_Defaults setObject:App_Delegate.dictUnreadMessages forKey:kSHUnreadMessage];
    //                [SH_Defaults synchronize];
    //
    //                [SH_Defaults setObject:App_Delegate.dictUnreadMessagesEvent forKey:kSHUnreadMessageEvent];
    //                [SH_Defaults synchronize];
    //
    //
    //                [[NSNotificationCenter defaultCenter] postNotificationName:kGroupMessageUnreadReceivedNotification object:nil];
    //                [[NSNotificationCenter defaultCenter] postNotificationName:kMessageUnreadReceivedNotificationEvent object:nil];
    //                [self setTabBadgeValue];
    //            }
    //            else
    //            {
    //                if ([SH_Defaults boolForKey:kIsAudioOn])
    //                {
    //                    NSString *path = [NSString stringWithFormat:@"%@/message_send.wav", [[NSBundle mainBundle] resourcePath]];
    //                    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    //                    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    //                    [_audioPlayer play];
    //                }
    //                if ([SH_Defaults boolForKey:kIsVibrateOn])
    //                {
    //                    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    //                }
    //
    //            }
    //            if (!body || body.length <= 0)
    //                return;
    //            if([messagetype isEqualToString:kTYPE_TEXT])
    //            {
    //#pragma mark -- Text Message Received
    //
    //                MessageObject *messageObj=[[MessageObject alloc]initWithDefault];
    //                NSString *strMessageID=@"";
    //                if ([message wasDelayed])
    //                {
    //                    NSDate *date=[message delayedDeliveryDate];
    //                    NSString *str=[formatter stringFromDate:date];
    //                    strMessageID=str;
    //                }
    //                else
    //                {
    //                    strMessageID=[self getMessageID];
    //                }
    //
    //                if (strMessageID.length<17)
    //                    strMessageID=[NSString stringWithFormat:@"0%@",strMessageID];
    //
    //                messageObj.ID=[strMessageID doubleValue];
    //                messageObj.strMsgId = strMessageID;
    //                messageObj.strBody=body;
    //
    //                NSDate *date=[formatter dateFromString:strMessageID];
    //                messageObj.timeStamp=[date timeIntervalSince1970];
    //                messageObj.strTime=[NSString stringWithFormat:@"%@",date];
    //                messageObj.strTypeOfSending=@"in";
    //                messageObj.strType=kTYPE_TEXT;
    //                messageObj.status=MSG_STATUS_RECEIVED;
    //                messageObj.strFriendUserName=senderID;
    //                messageObj.strGroupId=displayName;
    //                messageObj.strNickName =[dicRes objectForKey:@"userName"];
    //                [App_Delegate.dbManager insertGroupMessage:messageObj];
    //
    //
    //                //Update group's lastupdate time
    //                frnd.LastUpdated= [date timeIntervalSince1970];
    //                [App_Delegate.dbManager updateGroupLastUpdatedTime:frnd.LastUpdated grpId:frnd.strGroupID];
    //
    //                ///Reload Chat vc
    //                [[NSNotificationCenter defaultCenter] postNotificationName:kGroupMessageUnreadReceivedNotification object:nil];
    //
    //                //When User on same group chat screen, he receive chat message
    //
    //                if (App_Delegate.isGroupChatOpened && [App_Delegate.strGroupCurrentChatFriend isEqualToString:displayName])
    //                {
    //                    [App_Delegate.aryChat addObject:messageObj];
    //
    //                    [[NSNotificationCenter defaultCenter] postNotificationName:kGroupMessageReceivedNotification object:nil];
    //                }
    //                else
    //                {
    //                    NSString *str;
    //                    if (frnd)
    //                    {
    //                        if (frnd.strGroupID && [frnd.strGroupID length]>0)
    //                        {
    //                            if([frnd.strGroupID isEqualToString:[SH_Defaults objectForKey:kSHXMPPmyJID]])
    //                                return ;
    //
    //                            if (!frnd.strGroupName || [frnd.strGroupName isEqualToString:@""])
    //                                str=[NSString stringWithFormat:@"%@@%@: %@",senderName,frnd.strGroupName,body];
    //                            else
    //                                str=[NSString stringWithFormat:@"%@@%@: %@",senderName,frnd.strGroupName,body];
    //
    //
    //                            [[NSNotificationCenter defaultCenter] postNotificationName:kGroupMessageUnreadReceivedNotification object:nil];
    //
    //                            //Manage Sound And vibrate here
    //                            //sound play on receive messsage
    //                            if (App_Delegate.isPlaySonundForContinueMessage)
    //                            {
    //
    //                                if ([SH_Defaults boolForKey:kIsAudioOn])
    //                                {
    //                                    NSString *path = [NSString stringWithFormat:@"%@/ReceiveMsg.wav", [[NSBundle mainBundle] resourcePath]];
    //                                    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    //                                    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    //                                    [_audioPlayer play];
    //                                }
    //                                if ([SH_Defaults boolForKey:kIsVibrateOn])
    //                                {
    //                                    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    //                                }
    //                                double currentt = [[NSDate new] timeIntervalSince1970];
    //                                App_Delegate.lastIncomingMsgTimestamp = currentt;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }
    //    else
    //        return;
    //
    //    if([message hasReceiptResponse])
    //    {
    //#pragma mark -- Receipt Response Received
    //
    //        NSString *strMsgID=[message receiptResponseID];
    //
    //        NSString *strMessageID=@"";
    //        if ([message wasDelayed])
    //        {
    //            NSDate *date=[message delayedDeliveryDate];
    //            NSString *str=[formatter stringFromDate:date];
    //            strMessageID=str;
    //        }
    //        else
    //        {
    //            strMessageID=[self getMessageID];
    //        }
    //
    //        if ([self updateMessageofID:[strMsgID doubleValue] Status:MSG_STATUS_RECEIVED])
    //        {
    //            [[NSNotificationCenter defaultCenter] postNotificationName:kMessageReceiptNotification object:strMsgID];
    //        }
    //    }
}


-(void)requestGetPerticulerGroupDetail:(NSString *)strGroupId
{
    if ([App_Delegate isInternetAvailable])
    {
        NSMutableDictionary *dicParams=[[NSMutableDictionary alloc]init];
        
        [dicParams setObject:strGroupId forKey:@"group_id"];
    }
}


- (void)didReceiveGroupJoinMember:(XMPPMessage *)message
{
    NSString *jsonBody = [[message elementForName:@"body"] stringValue];
    NSData *dd = [jsonBody dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *dicRes = [NSJSONSerialization JSONObjectWithData:dd options:NSJSONReadingMutableContainers error:nil];
    
    NSString *strEventId =[NSString stringWithFormat:@"%@",[dicRes objectForKey:@"event_id"]];
    
    [[DataManager sharedManager]getEventMember:strEventId];
}

- (void)didReceiveGroupMemberRemove:(XMPPMessage *)message
{
    //    NSString *jsonBody = [[message elementForName:@"body"] stringValue];
    //    NSData *dd = [jsonBody dataUsingEncoding:NSUTF8StringEncoding];
    //
    //    NSDictionary *dicRes = [NSJSONSerialization JSONObjectWithData:dd options:NSJSONReadingMutableContainers error:nil];
    //
    //    NSString *strOwnUserId = [NSString stringWithFormat:@"%@",[DataSource DataSource].user.idUser];
    //    NSString *strDeletedUserId =[dicRes objectForKey:@"deleted_user_id"];
    //    NSString *strEventId = [NSString stringWithFormat:@"%@",[dicRes objectForKey:@"event_id"]];
    //    App_Delegate.isFromNotificationForRemoveMember =YES;
    //
    //    [self setBadgeOnNotificationPage:strEventId];
    //
    //    if ([strOwnUserId isEqualToString:strDeletedUserId])
    //    {
    //        NSString *queryDeleteEventChat = [NSString stringWithFormat:@"delete from tbl_grp_Chat where group_id = %@",[dicRes objectForKey:@"event_id"]];
    //        [App_Delegate.dbManager executeQuery:queryDeleteEventChat];
    //
    //        NSString *queryDeleteEvent = [NSString stringWithFormat:@"delete from tbl_grplist where g_id = %@",[dicRes objectForKey:@"event_id"]];
    //        [App_Delegate.dbManager executeQuery:queryDeleteEvent];
    //
    //        NSString *queryDeleteEventMember = [NSString stringWithFormat:@"delete from EventMemberList where userEventId = %@",[dicRes objectForKey:@"event_id"]];
    //        [App_Delegate.dbManager executeQuery:queryDeleteEventMember];
    //
    //        [[NSNotificationCenter defaultCenter]postNotificationName:@"RELOADEVENTNOTIFICATIONLIST" object:nil];
    //        [[NSNotificationCenter defaultCenter]postNotificationName:@"RELOADMEMBERTABLEVIEW" object:nil];
    //        [[NSNotificationCenter defaultCenter]postNotificationName:@"POPFROMCHATANDDETAILS" object:nil];
    //
    //    }
    //    else
    //    {
    //        NSString *queryDeleteEventMember = [NSString stringWithFormat:@"delete from EventMemberList where userEventId = %@ and memberId = %@",[dicRes objectForKey:@"event_id"],[dicRes objectForKey:@"deleted_user_id"]];
    //        [App_Delegate.dbManager executeQuery:queryDeleteEventMember];
    //
    //        [[NSNotificationCenter defaultCenter]postNotificationName:@"RELOADMEMBERTABLEVIEW" object:nil];
    //    }
}

- (void)didReceiveGroupMemberExit:(XMPPMessage *)message
{
    NSString *jsonBody = [[message elementForName:@"body"] stringValue];
    NSData *dd = [jsonBody dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *dicRes = [NSJSONSerialization JSONObjectWithData:dd options:NSJSONReadingMutableContainers error:nil];
    NSString *strEvnId = [NSString stringWithFormat:@"%@",[dicRes objectForKey:@"event_id"]];
    NSString *strDeletedUserId = [NSString stringWithFormat:@"%@",[dicRes objectForKey:@"deleted_user_id"]];
    
    NSString *queryDeleteEventMember = [NSString stringWithFormat:@"delete from EventMemberList where userEventId = %@ and memberId = %@",strEvnId,strDeletedUserId];
    //[App_Delegate.dbManager executeQuery:queryDeleteEventMember];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"RELOADEVENTNOTIFICATIONLIST" object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"RELOADMEMBERTABLEVIEW" object:nil];
    
}

- (void)didReceiveGroupUpdateEvent:(XMPPMessage *)message
{
    //    App_Delegate.isFromNotificationForUpdateEvent = YES;
    //    [[NSNotificationCenter defaultCenter]postNotificationName:@"RELOADEVENTNOTIFICATIONLIST" object:nil];
}

- (void)didReceiveInvitation:(XMPPMessage *)message
{
    
    NSString *jsonBody = [[message elementForName:@"body"] stringValue];
    NSData *dd = [jsonBody dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *dicRes = [NSJSONSerialization JSONObjectWithData:dd options:NSJSONReadingMutableContainers error:nil];
    NSString *msg = [dicRes objectForKey:@"message"];
    NSString *strEventId = [dicRes objectForKey:@"event_id"];
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    formatter = [App_Delegate setLocaleForIOS8:formatter];
    [formatter setDateFormat:@"ddMMyyyyHHmmssSSS"];
    MessageObject *messageObj=[[MessageObject alloc]initWithDefault];
    NSString *strMessageID=@"";
    if ([message wasDelayed])
    {
        NSDate *date=[message delayedDeliveryDate];
        NSString *str=[formatter stringFromDate:date];
        strMessageID=str;
    }
    else
    {
        strMessageID=[self getMessageID];
    }
    
    if (strMessageID.length<17)
        strMessageID=[NSString stringWithFormat:@"0%@",strMessageID];
    
    messageObj.ID=[strMessageID doubleValue];
    messageObj.strMsgId = strMessageID;
    messageObj.strBody=msg;
    
    NSDate *date=[formatter dateFromString:strMessageID];
    messageObj.timeStamp=[date timeIntervalSince1970];
    messageObj.strTime=[NSString stringWithFormat:@"%@",date];
    messageObj.strTypeOfSending=@"";
    messageObj.strType=kTYPE_TEXT;
    messageObj.status=MSG_STATUS_RECEIVED;
    messageObj.strFriendUserName=@"";
    messageObj.strGroupId=strEventId;
    messageObj.strNickName =[dicRes objectForKey:@"userName"];
    //[App_Delegate.dbManager insertGroupMessage:messageObj];
    
    if (App_Delegate.isPlaySonundForContinueMessage)
    {
        if ([SH_Defaults boolForKey:kIsAudioOn])
        {
            NSString *path = [NSString stringWithFormat:@"%@/ReceiveMsg.wav", [[NSBundle mainBundle] resourcePath]];
            NSURL *soundUrl = [NSURL fileURLWithPath:path];
            _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
            [_audioPlayer play];
        }
        if ([SH_Defaults boolForKey:kIsVibrateOn])
        {
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
        }
        double currentt = [[NSDate new] timeIntervalSince1970];
        App_Delegate.lastIncomingMsgTimestamp = currentt;
    }
    
    if ([App_Delegate.dictUnreadMessages objectForKey:strEventId])
    {
        int valueCount=[[App_Delegate.dictUnreadMessages objectForKey:strEventId] intValue];
        [App_Delegate.dictUnreadMessages setObject:[NSString stringWithFormat:@"%d",++valueCount] forKey:strEventId];
        
        
        int evtValueCount = [[App_Delegate.dictUnreadMessagesEvent objectForKey:strEventId] intValue];
        
        [App_Delegate.dictUnreadMessagesEvent setObject:[NSString stringWithFormat:@"%d",++evtValueCount] forKey:strEventId];
    }
    else
    {
        [App_Delegate.dictUnreadMessages setObject:[NSString stringWithFormat:@"%d",1] forKey:strEventId];
        [App_Delegate.dictUnreadMessagesEvent setObject:[NSString stringWithFormat:@"%d",1] forKey:strEventId];
    }
    [SH_Defaults setObject:App_Delegate.dictUnreadMessages forKey:kSHUnreadMessage];
    [SH_Defaults synchronize];
    
    [SH_Defaults setObject:App_Delegate.dictUnreadMessagesEvent forKey:kSHUnreadMessageEvent];
    [SH_Defaults synchronize];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kGroupMessageUnreadReceivedNotification object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kMessageUnreadReceivedNotificationEvent object:nil];
    [self setTabBadgeValue];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"RELOADEVENTNOTIFICATIONLIST" object:nil];
}

-(void)didReceiveGroupAddMemeber:(XMPPMessage *)message
{
    NSString *jsonBody = [[message elementForName:@"body"] stringValue];
    NSData *dd = [jsonBody dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *dicRes = [NSJSONSerialization JSONObjectWithData:dd options:NSJSONReadingMutableContainers error:nil];
    
    NSString *strEventId =[NSString stringWithFormat:@"%@",[dicRes objectForKey:@"event_id"]];
    
    [[DataManager sharedManager]getEventMember:strEventId];
}

-(void)didReceiveDeleteEvent:(XMPPMessage *)message
{
    
    NSString *jsonBody = [[message elementForName:@"body"] stringValue];
    NSData *dd = [jsonBody dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *dicRes = [NSJSONSerialization JSONObjectWithData:dd options:NSJSONReadingMutableContainers error:nil];
    NSString *strEvnId = [NSString stringWithFormat:@"%@",[dicRes objectForKey:@"event_id"]];
    
    NSString *queryDeleteEventChat = [NSString stringWithFormat:@"delete from tbl_grp_Chat where group_id = %@",strEvnId];
    //[App_Delegate.dbManager executeQuery:queryDeleteEventChat];
    
    NSString *queryDeleteEvent = [NSString stringWithFormat:@"delete from tbl_grplist where g_id = %@",strEvnId];
    //[App_Delegate.dbManager executeQuery:queryDeleteEvent];
    
    NSString *queryDeleteEventMember = [NSString stringWithFormat:@"delete from EventMemberList where userEventId = %@",strEvnId];
    
    //[App_Delegate.dbManager executeQuery:queryDeleteEventMember];
    
    [self setBadgeOnNotificationPage:strEvnId];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"POPFROMCHATANDDETAILS" object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"RELOADEVENTNOTIFICATIONLIST" object:nil];
    
}

-(void)setBadgeOnNotificationPage:(NSString*)strEventId
{
    //    if ([App_Delegate.dictUnreadMessages respondsToSelector:@selector(count)] && [App_Delegate.dictUnreadMessages objectForKey:strEventId])
    //        [App_Delegate.dictUnreadMessages removeObjectForKey:strEventId];
    //    [SH_Defaults setObject:App_Delegate.dictUnreadMessages forKey:kSHUnreadMessage];
    //    [SH_Defaults synchronize];
    //    
    //    if ([App_Delegate.dictUnreadMessagesEvent respondsToSelector:@selector(count)] && [App_Delegate.dictUnreadMessagesEvent objectForKey:strEventId])
    //        [App_Delegate.dictUnreadMessagesEvent removeObjectForKey:strEventId];
    //    [SH_Defaults setObject:App_Delegate.dictUnreadMessagesEvent forKey:kSHUnreadMessageEvent];
    //    [SH_Defaults synchronize];
    //    
    //    [[NSNotificationCenter defaultCenter] postNotificationName:kMessageUnreadReceivedNotificationEvent object:nil];
    //    
    //    NSDictionary *dictUnread = [SH_Defaults objectForKey:kSHUnreadMessage];
    //    int badgeCount = 0;
    //    for (NSString *strValue in dictUnread.allValues)
    //    {
    //        badgeCount += [strValue intValue];
    //    }
    //    NSNumber *badgeCnt = [NSNumber numberWithInt:badgeCount];
    //    [App_Delegate callApiForBadgeCountInAppIcon:badgeCnt];
    //    [self setTabBadgeValue];
    
}
@end
