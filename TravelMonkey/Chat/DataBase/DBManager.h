//
//  DBManager.h
//  SayHello
//
//  Created by Ram on 11/01/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactObject.h"
#import <sqlite3.h>
@class MessageObject;

@interface DBManager : NSObject
{
    NSDateFormatter *formatter;
}

-(id)initWith;
-(void)closeDBconnection;

//message
-(void)insertMessage:(MessageObject *)message;
-(void)getAllMessagesList:(NSString *)frnd By:(NSString *)byUserName andBookingID:(NSString*)strBookingID;
-(BOOL)updateStatusMessage:(int)status ofID:(double)messageID;
-(MessageObject *)getParticularMessage:(double)messageID;
-(BOOL)updateReadStatus:(NSString *)strFriendId;


//Contacts
-(void)insertContact:(ContactObject *)contact;
-(NSMutableArray *)getFriendsList:(NSString *)type;
-(NSMutableArray *)getFriendsJIDList;
@end
