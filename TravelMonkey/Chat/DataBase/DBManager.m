//
//  DBManager.m
//  SayHello
//
//  Created by Ram on 11/01/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "DBManager.h"
#import "MessageObject.h"
#import "NSDate-Utilities.h"

static  sqlite3 *database;

@implementation DBManager
#pragma mark - Initialization
-(id)initWith
{
    if (App_Delegate.dbManager)
    {
        return App_Delegate.dbManager;
    }
    self=[super init];
    formatter=[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ddMMyyyyHHmmssSSS"];
    
    if(sqlite3_open([App_Delegate.dBPath UTF8String], &database) == SQLITE_OK)
    {
         NSLog(@"Success");
    }
    else
    {
        NSLog(@"Fail");
    }
    NSLog(@"%@",App_Delegate.dBPath);
    
    return self;
}


-(void)closeDBconnection
{
    if(database) sqlite3_close(database);
}

#pragma mark- Insert Message
-(void)insertMessage:(MessageObject *)message
{
    message.strOfUser=App_Delegate.userObject.strEmail;
    
    NSString *checkString=[NSString stringWithFormat:@"SELECT rowid FROM tbl_Chat where msg_id=%f AND login_user=\"%@\" ",message.ID,message.strOfUser];
    
    const char *sqlSelect1 = [checkString  cStringUsingEncoding:NSUTF8StringEncoding];
    
    sqlite3_stmt *compiledStatement = nil;
    
    int rowid=0;
    
    if(sqlite3_prepare_v2(database, sqlSelect1, -1, &compiledStatement, NULL) != SQLITE_OK)
    {
        NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
    }
    else
    {
        // Loop through the results and add them to the feeds array
        while(sqlite3_step(compiledStatement) == SQLITE_ROW)
        {
            rowid=sqlite3_column_int(compiledStatement,0);
        }
    }
    sqlite3_finalize(compiledStatement);
    compiledStatement=nil;
    
    if (rowid==0)
    {
        NSString *queryString=[NSString stringWithFormat:@"INSERT into tbl_Chat (booking_id,msg_id, friend_id, msg_body, msg_timestamp, msg_time, msg_type, sending_type, msg_status, msg_thumb_url, msg_main_url, msg_size, msg_duration, login_user,msg_caption) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"];
        
        const char *insertSql = [queryString  cStringUsingEncoding:NSUTF8StringEncoding];
        
        if(sqlite3_prepare_v2(database, insertSql, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            int columIndex=0;
            
            sqlite3_bind_text(compiledStatement, ++columIndex, [message.strBookingID UTF8String],-1,SQLITE_TRANSIENT);
            
            sqlite3_bind_double(compiledStatement, ++columIndex, message.ID );
            
            sqlite3_bind_text(compiledStatement, ++columIndex, [message.strFriendUserName UTF8String],-1,SQLITE_TRANSIENT);
            
            sqlite3_bind_text(compiledStatement, ++columIndex, [message.strBody UTF8String],-1,SQLITE_TRANSIENT);
            
            sqlite3_bind_double(compiledStatement,++columIndex,message.timeStamp);
            
            sqlite3_bind_text(compiledStatement, ++columIndex, [message.strTime UTF8String],-1,SQLITE_TRANSIENT);
            
            sqlite3_bind_text(compiledStatement, ++columIndex, [message.strType UTF8String],-1,SQLITE_TRANSIENT);
            
            sqlite3_bind_text(compiledStatement, ++columIndex, [message.strTypeOfSending UTF8String],-1,SQLITE_TRANSIENT);
            
            sqlite3_bind_int(compiledStatement, ++columIndex, message.status);
            
            sqlite3_bind_text(compiledStatement, ++columIndex, [message.strThumbUrl UTF8String],-1,SQLITE_TRANSIENT);
            
            sqlite3_bind_text(compiledStatement, ++columIndex, [message.strMainUrl UTF8String],-1,SQLITE_TRANSIENT);
            
            sqlite3_bind_text(compiledStatement, ++columIndex, [message.strSizeofFile UTF8String],-1,SQLITE_TRANSIENT);
            
            sqlite3_bind_text(compiledStatement, ++columIndex, [message.strDurationofFile UTF8String],-1,SQLITE_TRANSIENT);
            
            sqlite3_bind_text(compiledStatement, ++columIndex, [message.strOfUser UTF8String],-1,SQLITE_TRANSIENT);
            
            //sqlite3_bind_text(compiledStatement, ++columIndex, [message.capText UTF8String],-1,SQLITE_TRANSIENT);
        }
        
        if(sqlite3_step(compiledStatement) != SQLITE_DONE)
        {
            NSAssert1(0, @"Error while creating insert statement '%s'", sqlite3_errmsg(database));
             NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
        }
        else
        {
            NSLog(@"Row Inserted!!!:");
        }
    }
}

#pragma mark - Update Message Status

-(BOOL)updateStatusMessage:(int)status ofID:(double)messageID
{
    
    NSString *queryString=[NSString stringWithFormat:@"UPDATE tbl_Chat SET msg_status=? where msg_id=? AND login_user=?"];
    
    const char *sqlSelect = [queryString  cStringUsingEncoding:NSUTF8StringEncoding];
    
    sqlite3_stmt *compiledStatement = nil;
    
    if(sqlite3_prepare_v2(database, sqlSelect, -1, &compiledStatement, NULL) != SQLITE_OK)
    {
        NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
    }
    else if(sqlite3_prepare_v2(database, sqlSelect, -1, &compiledStatement, NULL) == SQLITE_OK)
    {
        int columIndex=0;
        
        sqlite3_bind_int(compiledStatement, ++columIndex, status);
        sqlite3_bind_double(compiledStatement, ++columIndex, messageID);
        sqlite3_bind_text(compiledStatement, ++columIndex, [App_Delegate.userObject.strEmail UTF8String],-1,SQLITE_TRANSIENT);
        
        if(sqlite3_step(compiledStatement) != SQLITE_DONE)
        {
            NSLog(@"return 1");
            return NO;
            
        }
        else
        {
            NSLog(@"return 2");
            return YES;
        }
        
        return NO;
    }
    NSLog(@"return 4");
    return NO;
}

#pragma mark - Get ParticularMessage

-(MessageObject *)getParticularMessage:(double)messageID
{
    MessageObject *message=[[MessageObject alloc]initWithDefault];
    
    
    
    NSString *queryString=[NSString stringWithFormat:@"SELECT msg_id, friend_id, msg_body, msg_timestamp, msg_time, msg_type, sending_type, msg_status, msg_thumb_url, msg_main_url, msg_size, msg_duration, login_user FROM tbl_Chat where msg_id=%f AND login_user=\"%@\" ORDER BY msg_timestamp",messageID,App_Delegate.userObject.strEmail];
    NSLog(@"query %@",queryString);
    const char *sqlSelect = [queryString  cStringUsingEncoding:NSUTF8StringEncoding];
    
    sqlite3_stmt *compiledStatement = nil;
    
    if(sqlite3_prepare_v2(database, sqlSelect, -1, &compiledStatement, NULL) != SQLITE_OK)
    {
        
        //    NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
        
    }
    else if(sqlite3_prepare_v2(database, sqlSelect, -1, &compiledStatement, NULL) == SQLITE_OK)
    {
        // Loop through the results and add them to the feeds array
        while(sqlite3_step(compiledStatement) == SQLITE_ROW)
        {
            int columIndex=-1;
            
            message.ID=sqlite3_column_double(compiledStatement, ++columIndex);
            message.strFriendUserName= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strBody= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.timeStamp=sqlite3_column_double(compiledStatement, ++columIndex);
            message.strTime= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strType= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strTypeOfSending= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.status=sqlite3_column_int(compiledStatement,++columIndex);
            message.strThumbUrl= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strMainUrl= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strSizeofFile= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strDurationofFile= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strOfUser= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
        }
    }
    return message;
}

#pragma mark- Update Read Status
-(BOOL)updateReadStatus:(NSString *)strFriendId
{
    sqlite3_stmt *compiledStatement = nil;
    
    sqlite3_finalize(compiledStatement);
    compiledStatement=nil;
    
    NSString *queryString=@"";
    
    queryString=[NSString stringWithFormat:@"UPDATE tbl_Chat SET msg_status = 5 where msg_status=4 AND friend_id=\"%@\"",strFriendId];
    
    const char *sqlSelect = [queryString  cStringUsingEncoding:NSUTF8StringEncoding];
    
    if(sqlite3_prepare_v2(database, sqlSelect, -1, &compiledStatement, NULL) != SQLITE_OK)
    {
        NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
    }
    
    if(sqlite3_step(compiledStatement) != SQLITE_DONE)
    {
        //NSAssert1(0, @"Error while creating insert statement '%s'", sqlite3_errmsg(database));
        NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
    }
    else if(sqlite3_prepare_v2(database, sqlSelect, -1, &compiledStatement, NULL) == SQLITE_OK)
    {
        if(sqlite3_step(compiledStatement) != SQLITE_DONE)
        {
            //NSAssert1(0, @"Error while creating insert statement '%s'", sqlite3_errmsg(database));
            NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
            return NO;
        }
        else
        {
            return YES;
        }
        return NO;
    }
    return NO;
}


#pragma mark- Get All Messages

-(void)getAllMessagesList:(NSString *)frnd By:(NSString *)byUserName andBookingID:(NSString*)strBookingID
{
    byUserName=App_Delegate.userObject.strEmail;
    
    [App_Delegate.aryChat removeAllObjects];
    NSString *queryString=[NSString stringWithFormat:@"SELECT msg_id, friend_id, msg_body, msg_timestamp, msg_time, msg_type, sending_type, msg_status, msg_thumb_url, msg_main_url, msg_size, msg_duration, login_user,msg_caption FROM tbl_Chat where friend_id=\"%@\" AND login_user=\"%@\" AND booking_id=\"%@\" ORDER BY msg_timestamp",frnd,byUserName,strBookingID];
    NSLog(@"Get query: %@",queryString);
    
    const char *sqlSelect = [queryString  cStringUsingEncoding:NSUTF8StringEncoding];
    
    sqlite3_stmt *compiledStatement = nil;
    
    if(sqlite3_prepare_v2(database, sqlSelect, -1, &compiledStatement, NULL) != SQLITE_OK)
    {
        NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
    }
    else
    {
        // Loop through the results and add them to the feeds array
        while(sqlite3_step(compiledStatement) == SQLITE_ROW)
        {
            int columIndex=-1;
            
            MessageObject *message=[[MessageObject alloc]initWithDefault];
            message.ID=sqlite3_column_double(compiledStatement, ++columIndex);
            message.strFriendUserName= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strBody= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.timeStamp=sqlite3_column_double(compiledStatement, ++columIndex);
            message.strTime= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strType= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strTypeOfSending= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.status=sqlite3_column_int(compiledStatement,++columIndex);
            message.strThumbUrl= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strMainUrl= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strSizeofFile= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strDurationofFile= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            message.strOfUser= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            
            
            //Puru
            if (message.status==5)
            {
                message.isRead=YES;
            }
            
            //message.capText= sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, columIndex)]:@"";
            
            [App_Delegate.aryChat addObject:message];
        }
    }
    NSLog(@"all message %@",App_Delegate.aryChat);
    sqlite3_finalize(compiledStatement);
}

#pragma mark- Insert Contact
/*
-(void)insertContact:(ContactObject *)contact
{
    
    NSString *checkString=[NSString stringWithFormat:@"SELECT rowid FROM contact_list where userid=\"%@\" ", contact.strUserID];
    NSLog(@"Check Query : %@",checkString);
    const char *sqlSelect1 = [checkString  cStringUsingEncoding:NSUTF8StringEncoding];
    
    sqlite3_stmt *compiledStatement = nil;
    
    int rowid=0;
    
    if(sqlite3_prepare_v2(database, sqlSelect1, -1, &compiledStatement, NULL) != SQLITE_OK)
    {
        //    NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
    }
    else
    {
        // Loop through the results and add them to the feeds array
        while(sqlite3_step(compiledStatement) == SQLITE_ROW)
        {
            rowid=sqlite3_column_int(compiledStatement,0);
        }
    }
    sqlite3_finalize(compiledStatement);
    compiledStatement=nil;
    
    
    NSString *queryString=@"";
    if (rowid==0)
    {
        queryString=[NSString stringWithFormat:@"INSERT into contact_list (first_name, last_name, full_name, modified_date, modified_timestamp, contact_number_with_code, saved_original_number,is_nuke_user, login_user,is_in_contact_book) values(?,?,?,?,?,?,?,?,?,?)"];
    }
    else
    {
        queryString=[NSString stringWithFormat:@"UPDATE contact_list SET first_name = ?, last_name = ?, full_name = ?, modified_date = ?, modified_timestamp = ?, contact_number_with_code = ?, saved_original_number = ?, login_user = ?, is_in_contact_book = ? where rowid=%d AND login_user=\"%@\"",rowid,[[NSUserDefaults standardUserDefaults] objectForKey:kSkyXMPPmyJID]];
    }
    NSLog(@"Action Query : %@",queryString);
    const char *insertSql = [queryString  cStringUsingEncoding:NSUTF8StringEncoding];
    
    if(sqlite3_prepare_v2(database, insertSql, -1, &compiledStatement, NULL) == SQLITE_OK)
    {
        int columIndex=0;
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strFirstName UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strLastName UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strFullName UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strModifiedDate UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_double(compiledStatement, ++columIndex, contact.modifiedTimestamp);
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strMergedContactNumber UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strSavedOriginalNumber UTF8String],-1,SQLITE_TRANSIENT);
        
        if (rowid==0)
            sqlite3_bind_int(compiledStatement, ++columIndex, contact.isOnSims);
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [[Sims_Defaults objectForKey:kSimsXMPPmyJID] UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_int(compiledStatement, ++columIndex, contact.isInContactBook);
    }
    
    if(sqlite3_step(compiledStatement) != SQLITE_DONE)
    {
        //NSAssert1(0, @"Error while creating insert statement '%s'", sqlite3_errmsg(database));
        NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
    }
    else
    {
        //        //    NSLog(@"Row Inserted!!!:");
    }
    sqlite3_finalize(compiledStatement);
    compiledStatement=nil;
}
*/
-(void)insertContact:(ContactObject *)contact
{
    
    NSString *checkString=[NSString stringWithFormat:@"SELECT rowid FROM friend_list where user_id=\"%@\" AND login_user=\"%@\"",contact.strUserID,[[NSUserDefaults standardUserDefaults] objectForKey:@"kSkyXMPPmyJID"]];
    
    NSLog(@"Check Query : %@",checkString);
    
    const char *sqlSelect1 = [checkString  cStringUsingEncoding:NSUTF8StringEncoding];
    
    sqlite3_stmt *compiledStatement = nil;
    
    int rowid=0;
    
    if(sqlite3_prepare_v2(database, sqlSelect1, -1, &compiledStatement, NULL) != SQLITE_OK)
    {
        //    NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
    }
    else
    {
        // Loop through the results and add them to the feeds array
        while(sqlite3_step(compiledStatement) == SQLITE_ROW)
        {
            rowid=sqlite3_column_int(compiledStatement,0);
        }
    }
    sqlite3_finalize(compiledStatement);
    compiledStatement=nil;
    
    
    NSString *queryString=@"";
    if (rowid==0)
    {
        queryString=[NSString stringWithFormat:@"INSERT into friend_list (first_name, last_name, gender, profile_Image, user_id, user_jid,last_message_timestamp,login_user,friend_from_type,last_message,user_info,last_message_from) values(?,?,?,?,?,?,?,?,?,?,?,?)"];
    }
    else
    {
        queryString=[NSString stringWithFormat:@"UPDATE friend_list SET first_name = ?, last_name = ?, gender = ?, profile_Image = ?, user_id = ?, user_jid = ?, last_message_timestamp = ?, login_user = ? ,friend_from_type =?, last_message=?,user_info=? ,last_message_from = ? where rowid=%d AND login_user=\"%@\"",rowid,[[NSUserDefaults standardUserDefaults] objectForKey:@"kSkyXMPPmyJID"]];
    }
    
    NSLog(@"Action Query : %@",queryString);
    const char *insertSql = [queryString  cStringUsingEncoding:NSUTF8StringEncoding];
    
    if(sqlite3_prepare_v2(database, insertSql, -1, &compiledStatement, NULL) == SQLITE_OK)
    {
        int columIndex=0;
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strFirstName UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strLastName UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strGender UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strProfileImage UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strUserID UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strJID UTF8String],-1,SQLITE_TRANSIENT);

        sqlite3_bind_double(compiledStatement, ++columIndex, contact.lastMsgReceivedOn);
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [[[NSUserDefaults standardUserDefaults] objectForKey:@"kSkyXMPPmyJID"] UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strFriendFromType UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strLastMessage UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strFullUserInfo UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement, ++columIndex, [contact.strLastMessageFromUID UTF8String],-1,SQLITE_TRANSIENT);
    }
    
    if(sqlite3_step(compiledStatement) != SQLITE_DONE)
    {
        //NSAssert1(0, @"Error while creating insert statement '%s'", sqlite3_errmsg(database));
        NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
    }
    else
    {
        NSLog(@"Row Inserted!!!:");
    }
    sqlite3_finalize(compiledStatement);
    compiledStatement=nil;
}


#pragma mark - Get Contact list

-(NSMutableArray *)getFriendsList:(NSString *)type
{
   
    
    NSMutableArray *aryContacts=[[NSMutableArray alloc] init];
    
    
    NSString *queryString=[NSString stringWithFormat:@"SELECT first_name, last_name, gender, profile_Image,user_id,user_jid,last_message_timestamp ,last_message ,user_info,last_message_from FROM friend_list where login_user = '%@' AND friend_from_type ='%@' ORDER BY last_message_timestamp desc ",[[NSUserDefaults standardUserDefaults] objectForKey:@"kSkyXMPPmyJID"],type];
    NSLog(@"get friend query %@",queryString);

    const char *sqlSelect = [queryString  cStringUsingEncoding:NSUTF8StringEncoding];
    
    sqlite3_stmt *compiledStatement = nil;
    
    if(sqlite3_prepare_v2(database, sqlSelect, -1, &compiledStatement, NULL) != SQLITE_OK)
    {
            NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
    }
    else
    {
        // Loop through the results and add them to the feeds array
        while(sqlite3_step(compiledStatement) == SQLITE_ROW)
        {
            ContactObject *contact=[[ContactObject alloc]initWithDefault];
            
            int columIndex=-1;
            
            contact.strFirstName=sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement,columIndex)]:@"";
            
            contact.strLastName=sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement,columIndex)]:@"";
            
            contact.strGender=sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement,columIndex)]:@"";
            
            contact.strProfileImage=sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement,columIndex)]:@"";
            
            contact.strUserID=sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement,columIndex)]:@"";
            
            contact.strJID=sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement,columIndex)]:@"";
            
            contact.lastMsgReceivedOn=sqlite3_column_double(compiledStatement, ++columIndex);
            
            contact.strLastMessage=sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement,columIndex)]:@"";
            
            contact.strFullUserInfo=sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement,columIndex)]:@"";
            
            contact.strLastMessageFromUID=sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement,columIndex)]:@"";
            
            [aryContacts addObject:contact];
        }
    }
    sqlite3_finalize(compiledStatement);
    
    return aryContacts;
}

-(NSMutableArray *)getFriendsJIDList
{
    NSMutableArray *aryContacts=[[NSMutableArray alloc] init];
    
    
    NSString *queryString=[NSString stringWithFormat:@"SELECT user_jid,friend_from_type FROM friend_list where login_user = '%@'",[[NSUserDefaults standardUserDefaults] objectForKey:@"kSkyXMPPmyJID"]];
    NSLog(@"get friend query %@",queryString);
    
    const char *sqlSelect = [queryString  cStringUsingEncoding:NSUTF8StringEncoding];
    
    sqlite3_stmt *compiledStatement = nil;
    
    if(sqlite3_prepare_v2(database, sqlSelect, -1, &compiledStatement, NULL) != SQLITE_OK)
    {
        NSLog(@"Error Occured:'%s'",sqlite3_errmsg(database));
    }
    else
    {
        // Loop through the results and add them to the feeds array
        while(sqlite3_step(compiledStatement) == SQLITE_ROW)
        {
            NSMutableDictionary *dictFriends=[[NSMutableDictionary alloc]init];
            NSString *strJID;
            NSString *strType;
            int columIndex=-1;
            
            strJID=sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement,columIndex)]:@"";
            strType=sqlite3_column_text(compiledStatement, ++columIndex) != NULL ?[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement,columIndex)]:@"";
            strJID = [strJID lowercaseString];
            [dictFriends setObject:strJID forKey:@"jid"];
            [dictFriends setObject:strType forKey:@"type"];
            
            [aryContacts addObject:dictFriends];
        }
    }
    sqlite3_finalize(compiledStatement);
    
    return aryContacts;
}

@end