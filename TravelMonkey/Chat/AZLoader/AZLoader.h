//
//  AZLoader.h
//  SayHello
//
//  Created by Alex on 28.08.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AZLoader : UIView{
    UIView *_overlayView;
    UIImageView *_loader;
    UIImageView *_lines;
    UILabel *_text;
}

+ (void)showWithText:(NSString*)text;
+ (void)showWithLines:(CGFloat)height andText:(NSString*)text;
+ (void)showWithRound:(CGFloat)height andText:(NSString*)text;

+ (void)dismiss;

@end
