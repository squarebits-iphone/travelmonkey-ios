//
//  AZLoader.m
//  SayHello
//
//  Created by Alex on 28.08.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import "AZLoader.h"
#import "UIColor+Layout.h"

static AZLoader *azloader;

@implementation AZLoader

- (id)init
{
    
    self = [super init];
    if (self) {
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        _overlayView = [[UIView alloc] initWithFrame:window.bounds];

        _lines = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rings"]];
        _lines.center = _overlayView.center;
        _lines.contentMode = UIViewContentModeScaleAspectFit;
        
    
        _loader = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loading1"]];
        _loader.backgroundColor = [UIColor whiteColor];
        _loader.clipsToBounds = YES;
        _loader.layer.cornerRadius = _loader.frame.size.height / 2;
        _loader.center = _overlayView.center;
        [_overlayView addSubview:_loader];
        
        NSMutableArray *objects = [NSMutableArray array];
        for (int i = 1; i < 13; i++) {
            [objects addObject:[UIImage imageNamed:[NSString stringWithFormat:@"loading%d", i]]];
        }
        _loader.animationImages = objects;
        _loader.animationRepeatCount = 0;
        
        _text = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 20)];
        _text.textAlignment = NSTextAlignmentCenter;
        _text.textColor = [UIColor projectLoadingTextColor];
        _text.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        _text.center = _loader.center;
        [_overlayView addSubview:_text];
    }
    return self;
}

+(AZLoader*)sharedView{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        azloader = [[self alloc] init];
    });
    
    return azloader;
}

+ (void)showWithText:(NSString*)text{
    [[self sharedView] showLoader:text];
}

+ (void)showWithLines:(CGFloat)height andText:(NSString*)text{
    [[self sharedView] showWithLines:height andText:text];
}

+ (void)showWithRound:(CGFloat)height andText:(NSString*)text{
    [[self sharedView] showWithRound:height andText:text];
}

- (void)changeBgSize:(CGFloat)height{
    if (height < _lines.frame.size.height) {
        CGRect rect = _lines.frame;
        rect.size.height = height;
        rect.size.width = height;
        _lines.frame = rect;
        _lines.center = _overlayView.center;
    }
}

- (void)showWithLines:(CGFloat)height andText:(NSString*)text{
    [self changeBgSize:height];
    _lines.image = [UIImage imageNamed:@"rings"];
    [self showWithBack:height andText:text];
}

- (void)showWithRound:(CGFloat)height andText:(NSString*)text{
    [self changeBgSize:height];
    _lines.image = nil;
    _lines.backgroundColor = [UIColor colorWithRed:0.094 green:0.737 blue:0.612 alpha:0.500];
    _lines.layer.cornerRadius = _lines.frame.size.height / 2;
    _lines.layer.borderWidth = 15;
    _lines.layer.borderColor = [UIColor redColor].CGColor;
    
    [self showWithBack:height andText:text];
}

- (void)showWithBack:(CGFloat)height andText:(NSString*)text{
    [_overlayView insertSubview:_lines atIndex:0];
    
    [self showLoader:text];
}

- (void)showLoader:(NSString*)text{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    [window addSubview:_overlayView];
    
    _text.text = text;
    
    [_loader startAnimating];
}

+ (void)dismiss{
    [[AZLoader sharedView] dismissLoader];
}

- (void)dismissLoader{
    [_loader stopAnimating];
    [_lines removeFromSuperview];
    [_overlayView removeFromSuperview];
}

@end
