//
//  MessageNotificationScreen.h
//  Vleep
//
//  Created by Ram on 18/07/14.
//  Copyright (c) 2014 Square Bits. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "FriendObject.h"

@interface MessageNotificationScreen : UIViewController
{
    AppDelegate *appDelegateObj;
}
@property(nonatomic,strong)IBOutlet UILabel *lblMsg;
@property(nonatomic,strong)IBOutlet UIImageView *imgView;
@property(nonatomic,strong)NSString *strMsgFrom;
@property(nonatomic,strong)FriendObject *frndObj;

-(IBAction)btnNotificationView:(id)sender;

@end
