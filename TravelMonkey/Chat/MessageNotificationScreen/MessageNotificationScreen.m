//
//  MessageNotificationScreen.m
//  Vleep
//
//  Created by Ram on 18/07/14.
//  Copyright (c) 2014 Square Bits. All rights reserved.
//

#import "MessageNotificationScreen.h"
//#import "ChatScreen.h"
//#import "ChatViewController.h"

@interface MessageNotificationScreen ()

@end

@implementation MessageNotificationScreen

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnNotificationView:(id)sender
{
     [[NSNotificationCenter defaultCenter] postNotificationName:kMessageUnreadClickedNotification object:self.frndObj];
}

@end
