//
//  FriendObject.h
//  Vleep
//
//  Created by Ram on 19/07/14.
//  Copyright (c) 2014 squarebits. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FriendObject : NSObject


@property(nonatomic,strong) NSString *strChatID;
@property(nonatomic,strong) NSString *strUserID;
@property(nonatomic,strong) NSString *strNewaerDeviceID;
@property(nonatomic,strong) NSString *strBio;
@property(nonatomic,strong) NSString *strName;
@property(nonatomic,strong) NSString *strEmail;
@property(nonatomic,strong) NSString *strThumbUrl;
@property(nonatomic,strong) NSString *strImageUrl;
@property(nonatomic) double lastconnected;
@property BOOL isFriend;
@property BOOL isHeBlockUs;

-(id)initWithDefault;

@end
