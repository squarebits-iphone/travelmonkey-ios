//
//  FriendObject.m
//  Vleep
//
//  Created by Ram on 19/07/14.
//  Copyright (c) 2014 squarebits. All rights reserved.
//

#import "FriendObject.h"

@implementation FriendObject

-(id)initWithDefault
{
    self=[super init];
    
    self.strUserID=@"";
    self.strChatID=@"";
    self.strNewaerDeviceID=@"";
    self.strEmail=@"";
    self.strBio=@"";
    self.strThumbUrl=@"";
    self.strImageUrl=@"";
    self.lastconnected=0.0;
    self.isFriend=YES;
    self.isHeBlockUs=NO;
    
    return self;
}

@end
