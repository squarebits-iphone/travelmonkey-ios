//
//  AFN.m
//  TBD
//
//  Created by Kirill Kovalenko on 14.08.14.
//  Copyright (c) 2014 Kirill Kovalenko. All rights reserved.
//

#import "AFN.h"
#import "User.h"
#import "DataSource.h"
#import "Extend.h"
#import "AppSettings.h"
#import <AFNetworking/AFNetworking.h>
#import "AFHTTPRequestOperationManager.h"




NSString *const Domain = KBaseURL;
NSString *const sign = @"c1a8frfunnoo989b2oh319bo2r";
NSString *const securityHeader = @"X-USER-AUTH-TOKEN";

@interface AFN ()

@property (nonatomic, strong)AFHTTPRequestOperationManager *manager;

@end

@implementation AFN

#pragma mark - Methods


-(id)init
{
    self.manager = [AFHTTPRequestOperationManager manager];
    self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
    return self;
}

+(id)DataRequestWithDelegate:(id<AFNDelegate>)_delegate{
    AFN *Request = [[AFN alloc] init];
    if(_delegate){
        Request.delegate = _delegate;
    }
    return Request;
}

-(void)dealloc{
    self.delegate = nil;
    self.manager = nil;
}


#pragma mark - Data requests

- (void)requestVerify:(NSString*)phone code:(NSString*)code{
    [self.manager.requestSerializer setValue:sign forHTTPHeaderField:@"signature"];
    
    NSString *r = [NSString stringWithFormat:@"%@%@",Domain,SH_Api_PhoneValidate];
    
    NSDictionary *params = @{@"phoneNumber": phone, @"validationCode": code};

    
    [self.manager POST:r parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"valid"] boolValue]) {
            [self.delegate dataRequestVerify:nil answer:responseObject];
        }else{
            [self.delegate dataRequestVerify:responseObject[@"message"] answer:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if ([operation.responseData bytes]) {
        }
        [self.delegate dataRequestVerify:error.localizedDescription answer:nil];
    }];
}

- (void)requestAddPhone:(NSString*)phone
{
    NSString *r = [NSString stringWithFormat:@"%@%@", Domain,SH_Api_AddPhone];
    NSString *os = @"IOS";
    NSString *type=@"PHONE";
    NSDictionary *params = @{@"phoneNumber":phone, @"os":os,@"type":type};
    [self.manager POST:r parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"token"] length]>0) {
            [self.delegate dataRequestAddPhone:responseObject[@"token"] answer:responseObject];
        }else{
            [self.delegate dataRequestAddPhone:nil answer:responseObject];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if ([operation.responseData bytes])
        {
        }
        [self.delegate dataRequestAddPhone:error.localizedDescription answer:nil];
    }];
}

- (void)requestRegisterToUploadImg:(User*)user :(NSData *)imageData
{
    //Add security Header
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *strIds=[NSString stringWithFormat:@"%@",user.idUser];
    [self.manager.requestSerializer setValue:strIds forHTTPHeaderField:@"userid"];
    
    NSString *r = [NSString stringWithFormat:@"%@%@", Domain,SH_Api_UploadingAvatar];
    
    [self.manager POST:r parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"avatar" fileName:@"img.png" mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestRegisterForUploadImage:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.response.statusCode == 503) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
            [self.delegate dataRequestRegisterForUploadImage:dict[@"message"] answer:nil];
        }else
        {
            [self.delegate dataRequestRegisterForUploadImage:SH_Alert_ApiFailedError answer:nil];
        }
    }];
}


- (void)requestRegister:(User*)user{
    
    //Add security Header
    [self.manager.requestSerializer setValue:sign forHTTPHeaderField:@"signature"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    NSString *r = [NSString stringWithFormat:@"%@%@", Domain,SH_Api_UserRegister];
    
    user.phone= [[AppSettings sharedInstance] getPhone];
    
    NSDictionary *params;
    
    if (user.avatarUri == nil)
    {
       params = @{@"phoneNumber"     : user.phone,
                                 @"nickName"        : user.name,
                                 @"gender"          : [user.gender boolValue] ? @"MALE" : @"FEMALE",
                                 @"avatarUri"       : @"saro.jpg"};

    }
    else
    {
        params = @{@"phoneNumber"     : user.phone,
                   @"nickName"        : user.name,
                   @"gender"          : [user.gender boolValue] ? @"MALE" : @"FEMALE",
                   @"avatarUri"       : user.avatarUri};
    }
    [self.manager POST:r parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestRegister:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.response.statusCode == 503) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
            [self.delegate dataRequestRegister:dict[@"message"] answer:nil];
        }else{
            [self.delegate dataRequestRegister:SH_Alert_ApiFailedError answer:nil];
        }
    }];
}

- (void)requestEditRegisterUser:(User*)user{
    
    //Add security Header
    [self.manager.requestSerializer setValue:sign forHTTPHeaderField:@"signature"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    
    NSString *r = [NSString stringWithFormat:@"%@%@",Domain,SH_Api_UserEditRegister];
    user.phone= [[AppSettings sharedInstance] getPhone];
    NSDictionary *params;
    if (user.avatarUri == nil)
    {
        params = @{@"phoneNumber"     : user.phone,
                                 @"nickName"        : user.name,
                                 @"gender"          : [user.gender boolValue] ? @"MALE" : @"FEMALE",
                                 @"avatarUri"       : @"saro.jpg"};
    }
    else
    {
        params = @{@"phoneNumber"     : user.phone,
                   @"nickName"        : user.name,
                   @"gender"          : [user.gender boolValue] ? @"MALE" : @"FEMALE",
                   @"avatarUri"       : user.avatarUri};
    }
    [self.manager POST:r parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestEditRegisterUser:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.response.statusCode == 503) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
            [self.delegate dataRequestEditRegisterUser:dict[@"message"] answer:nil];
        }else
        {
            [self.delegate dataRequestEditRegisterUser:SH_Alert_ApiFailedError answer:nil];
        }
    }];
}

- (void)requestUserByPhone:(NSString*)phone{
    
    //Add security Header
    [self.manager.requestSerializer setValue:sign forHTTPHeaderField:@"signature"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@%@", Domain,SH_Api_UserDetailByPhone, [phone stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"]];
    [self.manager GET:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *strJsn = [NSString stringWithFormat:@"%@",[responseObject toJSON]];
        NSMutableDictionary *m_dict = [responseObject mutableCopy];
        for (NSString *key in [responseObject allKeys]) {
            if ([responseObject objectForKey:key]==[NSNull null]) {
                [m_dict setObject:@"" forKey:key];
            }
        }
        [self.delegate dataRequestUserByPhone1:nil answer:m_dict resp:strJsn];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.response.statusCode == 404 && [operation.responseData bytes])
        {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
            if (dict)
            {
                [self.delegate dataRequestUserByPhone1:dict[@"message"] answer:nil resp:nil];
            }
            else
            {
                [self.delegate dataRequestUserByPhone1:nil answer:nil resp:nil];
            }
        }
        else
        {
            [self.delegate dataRequestUserByPhone1:SH_Alert_ApiFailedError answer:nil resp:nil];
        }
    }];
}

-(void)requestLoggedInUserActivities
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@", Domain,SH_Api_UserActivities];
    [self.manager GET:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *strJson = [NSString stringWithFormat:@"%@",[responseObject toJSON]];
        if (responseObject[@"valid"])
        {
            [[DataSource DataSource].user.activities removeAllObjects];
            id dict=responseObject[@"listentity"];
            if (![dict isKindOfClass:[NSNull class]]&&dict!=nil)
            {
                [[DataSource DataSource].user activities:responseObject[@"listentity"]];
            }
            if (responseObject[@"listentity"] != nil) {
                [self.delegate dataRequestUserActivities:nil resp:strJson];
            }
            else
            {
                [self.delegate dataRequestUserActivities:@"No Activities" resp:strJson];
            }
        }
        else
        {
            [self.delegate dataRequestUserActivities:responseObject[@"message"] resp:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}

- (void)requestGetLatLong:(NSString*)zipcode
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    [self.manager.requestSerializer setValue:sign forHTTPHeaderField:@"signature"];
    NSString *r = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?components=postal_code:%@&sensor=true",zipcode];
    r = [r stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self.manager GET:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestGetLatLong:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestGetLatLong:error.localizedDescription answer:nil];
    }];
}

- (void)requestUpdateLatLong:(NSDictionary*)latLongDict{
    if([[DataSource DataSource].user.idUser intValue]==0){
        return;
    }
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue]] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    NSString *r = [NSString stringWithFormat:@"%@%@", Domain,SH_Api_UpdateLatLong];
    [self.manager.requestSerializer setValue:sign forHTTPHeaderField:@"signature"];
    
    [self.manager POST:r parameters:latLongDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestUpdateLatLong:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestUpdateLatLong:error.localizedDescription answer:nil];
    }];
}


- (void)requestAddUserActivities:(NSArray*)activities
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    NSString *r = [NSString stringWithFormat:@"%@%@", Domain,SH_Api_AddUserActivity];
    [self.manager POST:r parameters:activities success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestAddUserActivities:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestAddUserActivities:SH_Alert_ApiFailedError answer:nil];
    }];
}

- (void)requestActivityMatches:(NSString *)activityId associateRequestID:(NSString *)strRequestID
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    NSString *r1= [NSString stringWithFormat:@"%@%@%@", Domain,SH_Api_MatchActivities,activityId];
    [self.manager GET:r1 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestNearByUsers:nil answer:responseObject forActivityId:activityId andRequestID:strRequestID];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestNearByUsers:error.localizedDescription answer:nil forActivityId:activityId andRequestID:strRequestID];
    }];
}
- (void)requestActivityMatches:(NSString *)activityId forPage:(int)pageNumber  andAssociateRequestID:(NSString *)strRequestID
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r1= [NSString stringWithFormat:@"%@%@%@&page=%d", Domain,SH_Api_MatchActivities,activityId,pageNumber];
    [self.manager GET:r1 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestNearByUsers:nil answer:responseObject forActivityId:activityId andRequestID:strRequestID];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestNearByUsers:error.localizedDescription answer:nil forActivityId:activityId andRequestID:strRequestID];
    }];
}
- (void)requestUserStatus:(NSString *)jid {
    NSString *r1= [NSString stringWithFormat:@"https://%@:%@/plugins/presence/status?jid=%@",DOMAIN_NAME,PORT,jid];
    [self.manager GET:r1 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestNearByUsers:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}

- (void)requestUserDeleteActivity:(NSString*)deleteIds {
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];

    NSString *r = [NSString stringWithFormat:@"%@%@%@",Domain, SH_Api_DeleteUserActivity,deleteIds];
    [self.manager DELETE:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableDictionary *m_dict = [responseObject mutableCopy];
        for (NSString *key in [responseObject allKeys]) {
            if ([responseObject objectForKey:key]==[NSNull null])
            {
                [m_dict setObject:@"" forKey:key];
            }
        }
        [self.delegate dataRequestUserDeleteActivity:nil answer:m_dict];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.response.statusCode == 400 && [operation.responseData bytes]) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
            if (dict) {
                [self.delegate dataRequestUserDeleteActivity:dict[@"message"] answer:nil];
            }
        }else{
            [self.delegate dataRequestUserDeleteActivity:SH_Alert_ApiFailedError answer:nil];
        }
    }];
}

- (void)requestReportAbuse:(NSData *)imageData withUserID:(NSString *)userID
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@%@",Domain,SH_Api_ReportAbuse,userID];
    [self.manager POST:r parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileData:imageData name:@"images" fileName:@"Applications-icon.png" mimeType:@"image/jpeg"];
     } success:^(AFHTTPRequestOperation *operation, id responseObject) {
         [self.delegate dataRequestReportAbuse:nil answer:responseObject];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         if (operation.response.statusCode == 503) {
             NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
             [self.delegate dataRequestReportAbuse:dict[@"message"] answer:nil];
         }else{
             
             [self.delegate dataRequestReportAbuse:SH_Alert_ApiFailedError answer:nil];
         }
     }];
}

- (void)requestCheckUserIsBlock:(NSString*)strID
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@%@",Domain,SH_Api_CheckUserIsBlock,strID];
    
    [self.manager GET:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestCheckUserIsBlock:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestCheckUserIsBlock:error.localizedDescription answer:nil];
    }];
}

- (void)requestUserUnblock:(NSString*)strID
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@%@",Domain,SH_Api_UserUnblock,strID];
    
    [self.manager DELETE:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestUserUnblock:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestUserUnblock:error.localizedDescription answer:nil];
    }];
}

- (void)requestGetBlockedUser
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userIDD"]] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@",Domain,SH_Api_GetBlockList];
    
    [self.manager GET:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestGetBlockedUser:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestGetBlockedUser:error.localizedDescription answer:nil];
    }];
}


- (void)requestGetStaticAvtar
{
    NSString *r = [NSString stringWithFormat:@"%@%@",Domain,SH_Api_GetAvatarList];
    [self.manager.requestSerializer setValue:sign forHTTPHeaderField:@"signature"];
    [self.manager GET:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *strAvtarResponse = [NSString stringWithFormat:@"%@",[responseObject toJSON]];
        [self.delegate dataRequestGetStaticAvtar:nil answer:responseObject strJson:strAvtarResponse];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestGetStaticAvtar:error.localizedDescription answer:nil strJson:nil];
    }];
}


- (void)requestGetAllActivities
{
    NSString *r = [NSString stringWithFormat:@"%@%@",Domain,SH_Api_GetActivityTypeList];
    [self.manager.requestSerializer setValue:sign forHTTPHeaderField:@"signature"];
    
    [self.manager GET:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *str = [NSString stringWithFormat:@"%@",[responseObject toJSON]];
        [self.delegate dataRequestGetAllActivities:nil answer:responseObject strJson:str];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestGetAllActivities:error.localizedDescription answer:nil strJson:nil];
    }];
}


//events api
#pragma mark - Event API

- (void)requestLoggedInUserEvents
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@", Domain,SH_Api_UserEvent];
    [self.manager GET:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *strJson = [NSString stringWithFormat:@"%@",[responseObject toJSON]];
        if (responseObject[@"valid"])
        {
            [[DataSource DataSource].user.events removeAllObjects];
            id dict=responseObject[@"listentity"];
            if (![dict isKindOfClass:[NSNull class]]&&dict!=nil)
            {
                [[DataSource DataSource].user events:responseObject[@"listentity"]];
            }
            if (responseObject[@"listentity"] != nil) {
                [self.delegate dataRequestUserEvents:nil resp:strJson];
            }
            else
            {
                [self.delegate dataRequestUserEvents:@"No Activities" resp:strJson];
            }
        }
        else
        {
            [self.delegate dataRequestUserEvents:responseObject[@"message"] resp:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}

-(void)requestAddUserEvent:(NSDictionary *)event
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@",Domain,SH_Api_AddUserEvent];
    
    [self.manager POST:r parameters:event success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [self.delegate dataRequestAddUserEvent:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        if (operation.response.statusCode == 400)
        {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
            [self.delegate dataRequestAddUserEvent:dict[@"message"] answer:nil];
        }
        else
        {
            [self.delegate dataRequestAddUserEvent:SH_Alert_ApiFailedError answer:nil];
        }
    }];
}

-(void)requestEditUserEvent:(NSDictionary *)event withEventId:(NSString*)strEventId
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@/%@",Domain,SH_Api_AddUserEvent,strEventId];
    
    [self.manager POST:r parameters:event success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestEditUserEvent:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.response.statusCode == 400) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
            [self.delegate dataRequestEditUserEvent:dict[@"message"] answer:nil];
        }else
        {
            [self.delegate dataRequestEditUserEvent:SH_Alert_ApiFailedError answer:nil];
        }
    }];
}

- (void)requestEventBannerUploadImg:(NSData *)imageData
{
    //Add security Header
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    NSString *r = [NSString stringWithFormat:@"%@%@", Domain,SH_Api_UploadImage];
    
    [self.manager POST:r parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"imageBytes" fileName:@"img.png" mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestEventBannerUploadImg:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.response.statusCode == 503) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
            [self.delegate dataRequestEventBannerUploadImg:dict[@"message"] answer:nil];
        }else
        {
            [self.delegate dataRequestEventBannerUploadImg:SH_Alert_ApiFailedError answer:nil];
        }
    }];
}

- (void)requestEventMember:(NSString *)eventID associateRequestID:(NSString *)strRequestID
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    NSString *r1= [NSString stringWithFormat:@"%@%@%@/members", Domain,SH_Api_GetEventMember,eventID];
    [self.manager GET:r1 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestEventMember:nil answer:responseObject forEvent:eventID andRequestID:strRequestID];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestEventMember:error.localizedDescription answer:nil forEvent:eventID andRequestID:strRequestID];
    }];
}

-(void)requestEventMember:(NSString *)eventID orPage:(int)pageNumber associateRequestID:(NSString *)strRequestID
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    NSString *r1= [NSString stringWithFormat:@"%@%@%@/members?page=%d", Domain,SH_Api_GetEventMember,eventID,pageNumber];
    [self.manager GET:r1 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestEventMember:nil answer:responseObject forEvent:eventID andRequestID:strRequestID];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestEventMember:error.localizedDescription answer:nil forEvent:eventID andRequestID:strRequestID];
    }];
}

- (void)requestUserDeleteEvent:(NSString*)deleteIds
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@%@",Domain, SH_Api_DeleteUserEvent,deleteIds];
    
    [self.manager DELETE:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableDictionary *m_dict = [responseObject mutableCopy];
        for (NSString *key in [responseObject allKeys]) {
            if ([responseObject objectForKey:key]==[NSNull null])
            {
                [m_dict setObject:@"" forKey:key];
            }
        }
        [self.delegate dataRequestUserDeleteEvent:nil answer:m_dict];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        if (operation.response.statusCode == 400 && [operation.responseData bytes])
        {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
            if (dict)
            {
                [self.delegate dataRequestUserDeleteEvent:dict[@"message"] answer:nil];
            }
        }else
        {
            [self.delegate dataRequestUserDeleteEvent:SH_Alert_ApiFailedError answer:nil];
        }
    }];
}


- (void)requestSearchEventList:(NSDictionary *)activityDetails withActivityId:(NSString*)strActId associateRequestID:(NSString *)strRequestID
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    NSString *r1= [NSString stringWithFormat:@"%@%@",Domain,SH_Api_SearchEventForActivity];
    
    [self.manager POST:r1 parameters:activityDetails success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestSearchEventList:nil answer:responseObject forActivityId:strActId andRequestID:strRequestID];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestSearchEventList:error.localizedDescription answer:nil forActivityId:strActId andRequestID:strRequestID];
    }];
}

- (void)requestSearchEventList:(NSDictionary *)activityDetails withActivityId:(NSString*)strActId associateRequestID:(NSString *)strRequestID ForPage:(int)pageNumber
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    NSString *r1= [NSString stringWithFormat:@"%@%@?page=%d", Domain,SH_Api_SearchEventForActivity,pageNumber];
    
    [self.manager POST:r1 parameters:activityDetails success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestSearchEventList:nil answer:responseObject forActivityId:strActId andRequestID:strRequestID];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestSearchEventList:error.localizedDescription answer:nil forActivityId:strActId andRequestID:strRequestID];
    }];
}

- (void)requestEventMemberDelete:(NSString*)eventId withDeleteUserId:(NSArray*)arrUserId
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@%@/members",Domain,SH_Api_DeleteEventMember,eventId];
    
    [self.manager DELETE:r parameters:arrUserId success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableDictionary *m_dict = [responseObject mutableCopy];
        for (NSString *key in [responseObject allKeys]) {
            if ([responseObject objectForKey:key]==[NSNull null])
            {
                [m_dict setObject:@"" forKey:key];
            }
        }
        [self.delegate dataRequestEventMemberDelete:nil answer:m_dict];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if (operation.response.statusCode == 400 && [operation.responseData bytes])
         {
             NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
             if (dict)
             {
                 [self.delegate dataRequestEventMemberDelete:dict[@"message"] answer:nil];
             }
         }else
         {
             [self.delegate dataRequestEventMemberDelete:SH_Alert_ApiFailedError answer:nil];
         }
     }];
}

- (void)requestGetUserAllEvents
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@", Domain,SH_Api_GetAllUserEvent];
    [self.manager GET:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *strJson = [NSString stringWithFormat:@"%@",[responseObject toJSON]];
        if (responseObject[@"valid"])
        {
            if (responseObject[@"listentity"] != nil)
            {
                [self.delegate dataRequestGetUserAllEvents:nil resp:strJson];
            }
            else
            {
                [self.delegate dataRequestGetUserAllEvents:@"No Event Notification" resp:strJson];
            }
        }
        else
        {
            [self.delegate dataRequestGetUserAllEvents:responseObject[@"message"] resp:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}


-(void)requestInviteEventUser:(NSString*)strEventId withLimit:(int)limit
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    NSString *r = [NSString stringWithFormat:@"%@%@%@/match?limit=%d",Domain,SH_Api_InviteEvent,strEventId,limit];
    
    [self.manager POST:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestInviteEventUser:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.response.statusCode == 400) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
            [self.delegate dataRequestInviteEventUser:dict[@"message"] answer:nil];
        }else
        {
            [self.delegate dataRequestInviteEventUser:SH_Alert_ApiFailedError answer:nil];
        }
    }];
}

- (void)requestUserExitEvent:(NSString*)eventId
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@%@/exit",Domain, SH_Api_ExitFromEvent,eventId];
    
    [self.manager DELETE:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableDictionary *m_dict = [responseObject mutableCopy];
        for (NSString *key in [responseObject allKeys]) {
            if ([responseObject objectForKey:key]==[NSNull null])
            {
                [m_dict setObject:@"" forKey:key];
            }
        }
        [self.delegate dataRequestUserExitEvent:nil answer:m_dict];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if (operation.response.statusCode == 400 && [operation.responseData bytes])
         {
             NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
             if (dict)
             {
                 [self.delegate dataRequestUserExitEvent:dict[@"message"] answer:nil];
             }
         }else
         {
             [self.delegate dataRequestUserExitEvent:SH_Alert_ApiFailedError answer:nil];
         }
     }];
}

-(void)requestJoinPublicEvent:(NSString*)strEventId
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    NSString *r = [NSString stringWithFormat:@"%@%@%@/join",Domain,SH_Api_JoinPublicEvent,strEventId];
    
    [self.manager POST:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestJoinPublicEvent:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.response.statusCode == 400) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
            [self.delegate dataRequestJoinPublicEvent:dict[@"message"] answer:nil];
        }else
        {
            [self.delegate dataRequestJoinPublicEvent:SH_Alert_ApiFailedError answer:nil];
        }
    }];
}

-(void)requestJoinEventByCode:(NSString*)strCode
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%d",[[DataSource DataSource].user.idUser intValue] ] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    
    NSString *r = [NSString stringWithFormat:@"%@%@%@",Domain,SH_Api_JoinEventByCode,strCode];
    
    [self.manager POST:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestJoinEventByCode:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.response.statusCode == 400) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
            [self.delegate dataRequestJoinEventByCode:dict[@"message"] answer:nil];
        }else
        {
            [self.delegate dataRequestJoinEventByCode:SH_Alert_ApiFailedError answer:nil];
        }
    }];
}



- (void)requestUpdateUserActiveDate
{
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userIDD"]] forHTTPHeaderField:@"userid"];
    [self.manager.requestSerializer setValue:[SH_Defaults objectForKey:kHeaderSecurity] forHTTPHeaderField:securityHeader];
    [self.manager.requestSerializer setValue:sign forHTTPHeaderField:@"signature"];
    NSString *r = [NSString stringWithFormat:@"%@%@",Domain,SH_Api_StartSession];
    r = [r stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self.manager POST:r parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestUpdateUserActiveDate:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestUpdateUserActiveDate:error.localizedDescription answer:nil];
    }];
}

#pragma mark - Private API

- (void)requestPushNotification:(NSDictionary*)dict
{
    NSString *r = [NSString stringWithFormat:@"%@%@",KBaseURL,SH_Api_DeviceInfo];
    [self.manager POST:r parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestPushNotification:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestPushNotification:error.localizedDescription answer:nil];
    }];
}

- (void)requestBadgeCount:(NSDictionary*)dict
{
    NSString *r = [NSString stringWithFormat:@"%@%@",KBaseURL,SH_Api_UpdateBadgeCount];
    [self.manager POST:r parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate dataRequestBadgeCount:nil answer:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate dataRequestBadgeCount:error.localizedDescription answer:nil];
    }];
}



@end
