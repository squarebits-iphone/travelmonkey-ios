//
//  AFN.h
//  TBD
//
//  Created by Kirill Kovalenko on 14.08.14.
//  Copyright (c) 2014 Kirill Kovalenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "User.h"

@protocol AFNDelegate;

@interface AFN : NSObject

@property(nonatomic, strong) id<AFNDelegate> delegate;

+(id)DataRequestWithDelegate:(id<AFNDelegate>)delegate;

- (void)requestVerify:(NSString*)phone code:(NSString*)code;
- (void)requestAddPhone:(NSString*)phone;
- (void)requestRegister:(User*)user;
- (void)requestEditRegisterUser:(User*)user;
- (void)requestUserByPhone:(NSString*)phone;
- (void)requestAddUserActivities:(NSArray*)activities;
- (void)requestActivityMatches:(NSString *)activityId associateRequestID:(NSString *)strRequestID;
- (void)requestActivityMatches:(NSString *)activityId forPage:(int)pageNumber  andAssociateRequestID:(NSString *)strRequestID;


- (void)requestLoggedInUserActivities;
- (void)requestRegisterToUploadImg:(User*)user :(NSData *)imageData;
- (void)requestGetLatLong:(NSString*)zipcode;
- (void)requestUpdateLatLong:(NSDictionary*)latLongDict;
- (void)requestUserDeleteActivity:(NSString*)deleteIds;
- (void)requestGetStaticAvtar;
- (void)requestGetAllActivities;
- (void)requestPushNotification:(NSDictionary*)dict;
- (void)requestBadgeCount:(NSDictionary*)dict;
- (void)requestReportAbuse:(NSData *)imageData withUserID:(NSString *)userID;
- (void)requestCheckUserIsBlock:(NSString*)strID;
- (void)requestUserUnblock:(NSString*)strID;
- (void)requestGetBlockedUser;

//event
- (void)requestLoggedInUserEvents;
- (void)requestAddUserEvent:(NSDictionary*)event;
-(void)requestEditUserEvent:(NSDictionary *)event withEventId:(NSString*)strEventId;
- (void)requestEventBannerUploadImg:(NSData *)imageData;
- (void)requestEventMember:(NSString *)eventID associateRequestID:(NSString *)strRequestID;
-(void)requestEventMember:(NSString *)eventID orPage:(int)pageNumber associateRequestID:(NSString *)strRequestID;
- (void)requestUserDeleteEvent:(NSString*)deleteIds;
- (void)requestSearchEventList:(NSDictionary *)activityDetails withActivityId:(NSString*)strActId associateRequestID:(NSString *)strRequestID;
- (void)requestSearchEventList:(NSDictionary *)activityDetails withActivityId:(NSString*)strActId associateRequestID:(NSString *)strRequestID ForPage:(int)pageNumber;
- (void)requestEventMemberDelete:(NSString*)eventId withDeleteUserId:(NSArray*)arrUserId;
- (void)requestGetUserAllEvents;
-(void)requestInviteEventUser:(NSString*)strEventId withLimit:(int)limit;
- (void)requestUserExitEvent:(NSString*)eventId;
-(void)requestJoinPublicEvent:(NSString*)strEventId;
-(void)requestJoinEventByCode:(NSString*)strCode;
-(void)requestUpdateUserActiveDate;
@end

@protocol AFNDelegate <NSObject>

@optional

- (void)dataRequestVerify:(NSString*)error answer:(NSDictionary*)answer;
- (void)dataRequestRegister:(NSString*)error answer:(NSDictionary*)answer;
- (void)dataRequestEditRegisterUser:(NSString*)error answer:(NSDictionary*)answer;

- (void)dataRequestUserByPhone:(NSString*)error answer:(NSDictionary*)answer;//
- (void)dataRequestUserByPhone1:(NSString*)error answer:(NSDictionary*)answer resp:(NSString *)strResJs;//

- (void)dataRequestAddUserActivities:(NSString*)error answer:(NSDictionary*)answer;
- (void)dataRequestAddPhone: (NSString*) error answer:(NSDictionary*) answer;
- (void)dataRequestNearByUsers:(NSString*) error answer:(NSDictionary*) answer;
- (void)dataRequestNearByUsers:(NSString*) error answer:(NSDictionary*) answer forActivityId:(NSString *)activityID andRequestID:(NSString *)requestID;
- (void)requestUserStatus:(NSString *)jid;
- (void)dataRequestUserActivities:(NSString *)message resp:(NSString *)strResJs;

- (void)dataRequestUpdateLatLong:(NSString*) error answer:(NSDictionary*) answer;
- (void)dataRequestGetLatLong:(NSString*) error answer:(NSDictionary*) answer;

- (void)dataRequestRegisterForUploadImage:(NSString*)error answer:(NSDictionary*)answer;

- (void)dataRequestUserDeleteActivity:(NSString*)error answer:(NSDictionary*)answer;


- (void)dataRequestGetStaticAvtar:(NSString*)error answer:(NSDictionary*)answer strJson:(NSString *)strResJson;

- (void)dataRequestGetAllActivities:(NSString*)error answer:(NSArray*)answer strJson:(NSString *)strResJson;
- (void)dataRequestPushNotification:(NSString*)error answer:(NSDictionary*)answer ;
- (void)dataRequestBadgeCount:(NSString*)error answer:(NSDictionary*)answer ;
- (void)dataRequestReportAbuse:(NSString*)error answer:(NSDictionary*)answer ;
- (void)dataRequestCheckUserIsBlock:(NSString*)error answer:(NSDictionary*)answer ;
- (void)dataRequestUserUnblock:(NSString*)error answer:(NSDictionary*)answer ;
- (void)dataRequestGetBlockedUser:(NSString*)error answer:(NSDictionary*)answer ;

- (void)dataRequestUpdateUserActiveDate:(NSString*)error answer:(NSDictionary*)answer ;

//event
- (void)dataRequestUserEvents:(NSString *)message resp:(NSString *)strResJs;
- (void)dataRequestAddUserEvent:(NSString*)error answer:(NSDictionary*)answer;
- (void)dataRequestEditUserEvent:(NSString*)error answer:(NSDictionary*)answer;
- (void)dataRequestEventBannerUploadImg:(NSString*)error answer:(NSDictionary*)answer;
- (void)dataRequestEventMember:(NSString*)error answer:(NSDictionary*)answer forEvent:(NSString *)eventId andRequestID:(NSString *)requestID;
- (void)dataRequestUserDeleteEvent:(NSString*)error answer:(NSDictionary*)answer;
- (void)dataRequestSearchEventList:(NSString*)error answer:(NSDictionary*)answer forActivityId:(NSString *)activityID andRequestID:(NSString *)requestID;
- (void)dataRequestEventMemberDelete:(NSString*)error answer:(NSDictionary*)answer;
- (void)dataRequestGetUserAllEvents:(NSString *)message resp:(NSString *)strResJs;
- (void)dataRequestInviteEventUser:(NSString*)error answer:(NSDictionary*)answer;
- (void)dataRequestUserExitEvent:(NSString*)error answer:(NSDictionary*)answer;
- (void)dataRequestJoinPublicEvent:(NSString*)error answer:(NSDictionary*)answer;
- (void)dataRequestJoinEventByCode:(NSString*)error answer:(NSDictionary*)answer;

@end