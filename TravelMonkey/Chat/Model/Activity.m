//
//  Activity.m
//  SayHello
//
//  Created by Alex on 25.08.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import "Activity.h"

#import "AZLocation.h"

@implementation Activity

- (id)copyObj{
    Activity *activity = [[Activity alloc] init];
    activity.type = self.type;
    activity.desc = self.desc;
    activity.days = [NSMutableArray arrayWithArray:self.days];
    activity.from = self.from;
    activity.to = self.to;
    activity.zip = self.zip;
    activity.coordinates = self.coordinates;
    activity.Ids = self.Ids;
    return activity;
}

- (id)initWithType:(ActivityType*)type{
    self.type = type;
    self.days = [NSMutableArray array];
    self.from = @"00:00";
    self.to = @"00:00";
    self.desc = @"";
    self.zip = @"";
    self.Ids = @"";
    
    [AZLocation getUserLocation:^(CLLocationCoordinate2D coordinate) {
     //   self.coordinates = coordinate;
    }];
    
    return self;
}

- (NSDictionary*)getActivityDict{
    NSMutableArray *days = [NSMutableArray array];
    for (WeekDay *day in self.days) {
        [days addObject:day.fullName];
    }
    NSMutableDictionary *dict;
    if(self.Ids==nil ||[self.Ids integerValue]==0) {
        dict = [NSMutableDictionary
                dictionaryWithDictionary:@{@"description"   : self.desc,
                                           @"days"          : days,
                                           @"startTime"     : [NSString stringWithFormat:@"%@:00", self.from],
                                           @"endTime"       : [NSString stringWithFormat:@"%@:00", self.to],
                                           @"activityType"  : self.type.name}];
    }
    else
    {
        dict = [NSMutableDictionary
                dictionaryWithDictionary:@{@"description"   : self.desc,
                                           @"days"          : days,
                                           @"id"            : self.Ids,
                                           @"startTime"     : [NSString stringWithFormat:@"%@:00", self.from],
                                           @"endTime"       : [NSString stringWithFormat:@"%@:00", self.to],
                                           @"activityType"  : self.type.name}];
    }
    NSMutableDictionary *dictLocation = [[NSMutableDictionary alloc]init];
    if ([self.zip isKindOfClass:[NSNull class]])
    {
        self.zip=@"";
    }
    if (self.zip.length > 0)
    {
        
        dictLocation[@"postalCode"] = self.zip;
        dictLocation[@"type"] =@"ZIPCODE";
        dict[@"location"]=dictLocation;
        
    }else{
        dictLocation[@"latitude"] = [NSNumber numberWithDouble:self.coordinates.latitude];
        dictLocation[@"longitude"] = [NSNumber numberWithDouble:self.coordinates.longitude];
        dictLocation[@"type"] =@"LAT_LANG";
        dict[@"location"]=dictLocation;
    }
    
    return dict;
}

- (NSArray*)getDaysShortNames
{
    NSMutableArray *arr = [NSMutableArray array];
    for (WeekDay *day in self.days) {
        [arr addObject:day.shortName];
    }
    NSArray *arrSortedArray = [[NSArray alloc] initWithObjects:@"MON",@"TUE",@"WED",@"THU",@"FRI",@"SAT",@"SUN", nil];
    NSMutableArray *arrFinalArray = [[NSMutableArray alloc] init];
    for (int i=0; i<arrSortedArray.count; i++)
    {
        NSString *strDay = [arrSortedArray objectAtIndex:i];
        
        if ([arr containsObject:strDay])
        {
            strDay = [strDay lowercaseString];
            strDay = [strDay stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[strDay substringToIndex:1] uppercaseString]];
            [arrFinalArray addObject:strDay];
        }
    }
    return arrFinalArray;
}

@end
