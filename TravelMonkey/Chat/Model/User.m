//
//  User.m
//  SayHello
//
//  Created by Alex on 02.09.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import "User.h"

@implementation User

- (id)initUserWithPhone:(NSString*)phone{
    self.phone = phone;
    self.activities = [[NSMutableArray alloc] initWithObjects:nil];
    
    return self;
}

- (id)initUserWithDictionary:(NSDictionary*)info{
    if([info count]==0) {
        return self;
    }
    self.activities = [[NSMutableArray alloc] initWithObjects:nil];
    id dictObjects=[info objectForKey:@"object"];
    if ([dictObjects isKindOfClass:[NSString class]]) {
        NSString *temp=dictObjects;
        if (temp.length==0||temp==nil|| [temp isEqualToString:@""]) {
            return self;
        }
    }
    else if ([dictObjects isKindOfClass:[NSDictionary class]]) {
        //NSLog(@"DictClass");
    }
    
    
    self.idUser = [NSNumber numberWithInteger:[info[@"object"][@"id"] integerValue]];
    
    self.phone = info[@"object"][@"devices"][@"phoneNumber"];
    
    self.gender = [info[@"object"][@"gender"] isEqualToString:@"MALE"] ? @1 : @0;
    self.name = info[@"object"][@"nickName"];
    //self.activities = [self activities:info[@"object"][@"activities"]];
    self.avatarUri = info[@"object"][@"avatarUri"];
    self.status = info[@"object"][@"status"];
    self.chatAccountID = info[@"object"][@"chatAccount"][@"id"];
    self.chatAccountUserName = info[@"object"][@"chatAccount"][@"userName"];
    self.chatAccountHashPwd = info [@"object"][@"chatAccount"][@"accessHash"];
    
    return self;
}

- (id)initMatchUserWithDictionary:(NSDictionary*)info{
    if([info count]==0) {
        return self;
    }
    self.activities = [[NSMutableArray alloc] initWithObjects:nil];
    
    NSDictionary *userDisc= [info objectForKey:@"user"];
    self.idUser = [NSNumber numberWithInteger:[[userDisc objectForKey:@"id"]integerValue]];
    
    NSDictionary *deviceDisc = [info objectForKey:@"devices"];
    self.phone = [deviceDisc objectForKey:@"phoneNumber"];
    
    self.gender = [[userDisc objectForKey:@"gender"] isEqualToString:@"MALE"] ? @1 : @0;
    self.name = [userDisc objectForKey:@"nickName"];
    //self.activities = [self activities:info[@"object"][@"activities"]];
    self.avatarUri = [userDisc objectForKey:@"avatarUri"];
    self.status = [userDisc objectForKey:@"status"];
    
    NSDictionary *chatDisc=[userDisc objectForKey:@"chatAccount"];
    if([chatDisc isKindOfClass:[NSNull class]])
    {
    }
    else
    {
    self.chatAccountID = [chatDisc objectForKey:@"id"];
    self.chatAccountUserName = [chatDisc objectForKey:@"userName"];
    self.chatAccountHashPwd = [chatDisc objectForKey:@"accessHash"];
    }
    
    NSString *activity_name = [info objectForKey:@"type"];
    long indexActivity = [[ActivityType activityNamesOnServer] indexOfObject:activity_name];
    NSString *strImageName = @"";
    if (indexActivity != NSNotFound)
    {
        strImageName = [[ActivityType activityImagesForEditScreen] objectAtIndex:indexActivity];
    }
    ActivityType *type  = [[ActivityType alloc] initWithImage:strImageName andName:activity_name];
    
    Activity *newActivity = [[Activity alloc] initWithType:type];
    
    NSArray *weekDays = [[info objectForKey:@"daysInString"] componentsSeparatedByString:@","];
    
    for (NSString *str in weekDays)
    {
        if (str.length>2)
        {
            WeekDay *day = [[WeekDay alloc] initWithShort:[str substringToIndex:3] andFull:str];
            [newActivity.days addObject:day];
        }
        
    }
    NSDictionary *locationDisc=[info objectForKey:@"location"];
    
    NSNumber *lat = [locationDisc objectForKey:@"latitude"];
    NSNumber *lon = [locationDisc objectForKey:@"longitude"];
    
    //newActivity.coordinates= CLLocationCoordinate2DMake(1.0,1.0);
    newActivity.coordinates= CLLocationCoordinate2DMake(lat.doubleValue, lon.doubleValue);
    newActivity.zip = [locationDisc objectForKey:@"postalCode"];
    newActivity.from = [info objectForKey:@"startTime"];
    newActivity.to = [info objectForKey:@"endTime"];
    newActivity.desc = [info objectForKey:@"description"];
    if ([[info objectForKey:@"id"] isKindOfClass:[NSString class]])
    {
        newActivity.Ids= [info objectForKey:@"id"] ;
    }
    else
    {
        newActivity.Ids= [NSString stringWithFormat:@"%ld",[[info objectForKey:@"id"] longValue]];
    }
    
    [self.activities addObject:newActivity];
    return self;
}

- (void)activities:(NSArray *)arr
{
    if([arr class]==[NSNull null]||arr == nil){
        return;
    }

    for (NSDictionary *dict in arr) {
        NSString *activity_name = dict[@"type"];
        long indexActivity = [[ActivityType activityNamesOnServer] indexOfObject:activity_name];
        //ActivityType *type  = [[ActivityType alloc] initWithImage:[[ActivityType activityImagesForEditScreen] objectAtIndex:indexActivity] andName:activity_name];
        NSString *strImageName = @"";
        if (indexActivity != NSNotFound)
        {
            strImageName = [[ActivityType activityImagesForEditScreen] objectAtIndex:indexActivity];
        }

        ActivityType *type = [[ActivityType alloc] initWithImage:strImageName andName:activity_name];
        
        Activity *newActivity = [[Activity alloc] initWithType:type];
        NSArray *weekDays = dict[@"daysInString"];
        for (NSString *str in weekDays)
        {
            if (str.length>2)
            {
                WeekDay *day = [[WeekDay alloc] initWithShort:[str substringToIndex:3] andFull:str];
                [newActivity.days addObject:day];
                
            }
        }
        NSNumber *lat = dict[@"location"][@"latitude"];
        NSNumber *lon = dict[@"location"][@"longitude"];

        //newActivity.coordinates= CLLocationCoordinate2DMake(1.0,1.0);
        newActivity.coordinates= CLLocationCoordinate2DMake(lat.doubleValue, lon.doubleValue);
        newActivity.zip =dict[@"location"][@"postalCode"];
        newActivity.from = dict[@"startTime"];
        newActivity.to = dict[@"endTime"];
        newActivity.desc = dict[@"description"];
        newActivity.Ids=[NSString stringWithFormat:@"%ld",[dict[@"id"] longValue]];
        [self.activities addObject:newActivity];
    }
}


-(void)events:(NSArray*)arr
{
    if([arr class]==[NSNull null]||arr == nil){
        return;
    }
    self.events = [[NSMutableArray alloc]init];
    for (NSDictionary *dict in arr)
    {
        Event *newEvent =  [[Event alloc]initEvent];
        newEvent.arrEventMember =[[NSMutableArray alloc]init];
        NSNumber *lat = dict[@"location"][@"latitude"];
        NSNumber *lon = dict[@"location"][@"longitude"];
        newEvent.ActivityId = dict[@"activityId"];
        newEvent.bannerUrl =dict[@"bannerURL"];
        newEvent.code =dict[@"code"];
        newEvent.desc = dict[@"description"];
        newEvent.startDate = dict[@"startDate"];
        newEvent.endDate = dict[@"endDate"];
        newEvent.eventStatus =dict[@"eventStatus"];
        newEvent.eventType = dict[@"eventType"];
        newEvent.iconUrl =dict[@"iconURL"];
        newEvent.Ids=[NSString stringWithFormat:@"%ld",[dict[@"id"] longValue]];
        newEvent.inviteContent = dict[@"inviteContent"];
        newEvent.zip =dict[@"location"][@"postalCode"];
        newEvent.address = dict[@"location"][@"address"];
        newEvent.coordinates= CLLocationCoordinate2DMake(lat.doubleValue, lon.doubleValue);
        [newEvent.arrEventMember addObjectsFromArray:dict[@"members"]];
        newEvent.name = dict[@"name"];
        newEvent.sponsor = [dict[@"sponsored"] boolValue];
        newEvent.sponsorName = dict [@"sponsorName"];
        newEvent.ownerId = dict[@"ownerId"];
        [self.events addObject:newEvent];
    }
}

- (NSArray*)getActivitiesJSON
{
    NSMutableArray *activities = [NSMutableArray array];
    for (Activity *activity in self.activities)
    {
        [activities addObject:[activity getActivityDict]];
    }
    return activities;
}

@end
