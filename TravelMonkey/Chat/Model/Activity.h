//
//  Activity.h
//  SayHello
//
//  Created by Alex on 25.08.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "ActivityType.h"
#import "WeekDay.h"

//enum {
//    ToursUpdated = 1,
//    ParamsUpdated = 2
//};
//typedef NSUInteger TableUpdated;

@interface Activity : NSObject

@property (strong, nonatomic) ActivityType *type;
@property (strong, nonatomic) NSString *desc;
@property (strong, nonatomic) NSMutableArray *days;
@property (strong, nonatomic) NSString *from;
@property (strong, nonatomic) NSString *to;
@property (strong, nonatomic) NSString *zip;
@property (nonatomic) CLLocationCoordinate2D coordinates;
@property (strong, nonatomic) NSString *Ids;

- (id)initWithType:(ActivityType*)type;
- (id)copyObj;
- (NSDictionary*)getActivityDict;
- (NSArray*)getDaysShortNames;

@end
