//
//  Country.m
//  SayHello
//
//  Created by Alex on 22.08.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import "Country.h"

@implementation Country

- (instancetype)initWithDictionary:(NSDictionary*)info{
    self.name = info[@"name"];
    self.code = info[@"code"];
    self.phoneCode = info[@"dial_code"];
    
    return self;
}

@end
