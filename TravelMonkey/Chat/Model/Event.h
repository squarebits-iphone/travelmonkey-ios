//
//  Event.h
//  SayHello
//
//  Created by Ram on 21/03/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject

@property (strong, nonatomic) NSString *Ids;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSString *desc;
@property (strong, nonatomic) NSString *ownerId;
@property (strong, nonatomic) NSString *ActivityId;
@property  BOOL sponsor;
@property (strong, nonatomic) NSString *sponsorName;
@property (strong, nonatomic) NSString *zip;
@property (nonatomic) CLLocationCoordinate2D coordinates;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *startDate;
@property (strong, nonatomic) NSString *endDate;
@property (strong, nonatomic) NSString *iconUrl;
@property (strong, nonatomic) NSString *bannerUrl;
@property (strong, nonatomic) NSString *eventType;
@property (strong, nonatomic) NSString *eventStatus;
@property (strong, nonatomic) NSString *inviteContent;
@property (strong, nonatomic) NSString *time;
@property (strong ,nonatomic) NSString *strMemberCount;
@property(strong,nonatomic) NSMutableArray *arrEventMember;

- (id)initEvent;


@end
