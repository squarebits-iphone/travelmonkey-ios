//
//  ActivityType.m
//  SayHello
//
//  Created by Alex on 26.08.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import "ActivityType.h"
NSMutableArray *arrActivities;
@implementation ActivityType
+(void)setActivitiesData:(NSMutableArray *)arrData
{
    arrActivities = arrData;
}
+(NSArray *)activityNames
{
    NSMutableArray *list = [NSMutableArray array];
    for (int i=0; i<arrActivities.count; i++) {
        NSDictionary *dict = [arrActivities objectAtIndex:i];
        [list addObject:[dict objectForKey:@"name"]];
    }
/*    [list addObject:@"Coffee"];
    [list addObject:@"Chatting"];
    [list addObject:@"Indoor Sports"];
    [list addObject:@"Cycling etc"];
    [list addObject:@"Adventure Sports"];
    [list addObject:@"Outdoor Sports"];
    [list addObject:@"Events"];
    [list addObject:@"Travel"];
    [list addObject:@"Water Activities"];
    [list addObject:@"Dog Walking"];
    [list addObject:@"Playing Music"];
    [list addObject:@"Camping"];
    [list addObject:@"Gym"];
    [list addObject:@"Video Games"];
    [list addObject:@"Others"];
    [list addObject:@"Walking"];
    [list addObject:@"Cooking"];
    [list addObject:@"Dancing"];
    [list addObject:@"Bars,Dining"];
    [list addObject:@"Reading"];*/

    // get activity names list from realm
    return list;

}

+(NSArray *)activityNamesOnServer
{
    NSMutableArray *list = [NSMutableArray array];
    for (int i=0; i<arrActivities.count; i++) {
        NSDictionary *dict = [arrActivities objectAtIndex:i];
        [list addObject:[dict objectForKey:@"code"]];
    }
    /*NSMutableArray *list = [NSMutableArray array];
    [list addObject:@"COFFEE"];
    [list addObject:@"CHATTING"];
    [list addObject:@"INDOOR_SPORTS"];
    [list addObject:@"CYCLING_ETC"];
    [list addObject:@"ADVENTURE_SPORTS"];
    [list addObject:@"OUTDOOR_SPORTS"];
    [list addObject:@"EVENTS"];
    [list addObject:@"TRAVEL"];
    [list addObject:@"WATER_ACTIVITIES"];
    [list addObject:@"DOG_WALKING"];
    [list addObject:@"PLAYING_MUSIC"];
    [list addObject:@"CAMPING"];
    [list addObject:@"GYM"];
    [list addObject:@"VIDEO_GAMES"];
    [list addObject:@"OTHER"];
    [list addObject:@"WALKING"];
    [list addObject:@"COOKING"];
    [list addObject:@"DANCING"];
    [list addObject:@"BARS_DINING"];
    [list addObject:@"READING"];
    */
    return list;
    
}
+(NSArray *)activityImagesForSearchScreen
{
   /* NSMutableArray *list = [NSMutableArray array];
    [list addObject:[UIImage imageNamed:@"ico_coffee1"]];
    [list addObject:[UIImage imageNamed:@"ico_chatting1"]];
    [list addObject:[UIImage imageNamed:@"ico_indoor_games1"]];
    [list addObject:[UIImage imageNamed:@"ico_outdoor1"]];
    [list addObject:[UIImage imageNamed:@"ico_adventure_sports1"]];
    [list addObject:[UIImage imageNamed:@"ico_outdoor_games1"]];
    [list addObject:[UIImage imageNamed:@"ico_movie_tickets1"]];
    [list addObject:[UIImage imageNamed:@"ico_travel1"]];
    [list addObject:[UIImage imageNamed:@"ico_water_activities1"]];
    [list addObject:[UIImage imageNamed:@"ico_dog_walking1"]];
    [list addObject:[UIImage imageNamed:@"ico_play_music1"]];
    [list addObject:[UIImage imageNamed:@"ico_camping1"]];
    [list addObject:[UIImage imageNamed:@"ico_gym1"]];
    [list addObject:[UIImage imageNamed:@"ico_video_games1"]];
    [list addObject:[UIImage imageNamed:@"ico_other1"]];
    [list addObject:[UIImage imageNamed:@"ico_walking1"]];
    [list addObject:[UIImage imageNamed:@"ico_cooking1"]];
    [list addObject:[UIImage imageNamed:@"ico_dancing1"]];
    [list addObject:[UIImage imageNamed:@"ico_bars_dining1"]];
    [list addObject:[UIImage imageNamed:@"ico_reading1"]];
    */
    NSMutableArray *list = [NSMutableArray array];
    for (int i=0; i<arrActivities.count; i++) {
        NSDictionary *dict = [arrActivities objectAtIndex:i];
        [list addObject:[dict objectForKey:@"imageUri"]];
    }
    return list;
}
+(NSArray *)activityImagesForEditScreen
{
   /* NSMutableArray *list = [NSMutableArray array];
    [list addObject:[UIImage imageNamed:@"ico_coffee"]];
    [list addObject:[UIImage imageNamed:@"ico_chatting"]];
    [list addObject:[UIImage imageNamed:@"ico_indoor_games"]];
    [list addObject:[UIImage imageNamed:@"ico_outdoor"]];
    [list addObject:[UIImage imageNamed:@"ico_adventure_sports"]];
    [list addObject:[UIImage imageNamed:@"ico_outdoor_games"]];
    [list addObject:[UIImage imageNamed:@"ico_movie_tickets"]];
    [list addObject:[UIImage imageNamed:@"ico_travel"]];
    [list addObject:[UIImage imageNamed:@"ico_water_activities"]];
    [list addObject:[UIImage imageNamed:@"ico_dog_walking"]];
    [list addObject:[UIImage imageNamed:@"ico_play_music"]];
    [list addObject:[UIImage imageNamed:@"ico_camping"]];
    [list addObject:[UIImage imageNamed:@"ico_gym"]];
    [list addObject:[UIImage imageNamed:@"ico_video_games"]];
    [list addObject:[UIImage imageNamed:@"ico_other"]];
    [list addObject:[UIImage imageNamed:@"ico_walking"]];
    [list addObject:[UIImage imageNamed:@"ico_cooking"]];
    [list addObject:[UIImage imageNamed:@"ico_dancing"]];
    [list addObject:[UIImage imageNamed:@"ico_bars_dining"]];
    [list addObject:[UIImage imageNamed:@"ico_reading"]];*/
    
    NSMutableArray *list = [NSMutableArray array];
    for (int i=0; i<arrActivities.count; i++) {
        NSDictionary *dict = [arrActivities objectAtIndex:i];
        [list addObject:[dict objectForKey:@"imageUri"]];
    }
    return list;
    
 }

+ (NSArray*)activityTypes{
    /*NSMutableArray *list = [NSMutableArray array];
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_coffee"] andName:@"COFFEE"]];//COFFEE
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_chatting"] andName:@"CHATTING"]];//CHAT
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_indoor_games"] andName:@"INDOOR_SPORTS"]];//Indoor Sports
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_outdoor"] andName:@"CYCLING_ETC"]];//Cycling etc
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_adventure_sports"] andName:@"ADVENTURE_SPORTS"]];//Adventure Sports
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_outdoor_games"] andName:@"OUTDOOR_SPORTS"]];//Outdoor Sports
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_movie_tickets"] andName:@"EVENTS"]];//Events
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_travel"] andName:@"TRAVEL"]];//Travel
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_water_activities"] andName:@"WATER_ACTIVITIES"]];//Water Activities
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_dog_walking"] andName:@"DOG_WALKING"]];//Dog Walking
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_play_music"] andName:@"PLAYING_MUSIC"]];//Playing Music
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_camping"] andName:@"CAMPING"]];//Camping
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_gym"] andName:@"GYM"]];//Gym
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_video_games"] andName:@"VIDEO_GAMES"]];//Video Games
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_other"] andName:@"OTHER"]];//Others
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_walking"] andName:@"WALKING"]];//Walking
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_cooking"] andName:@"COOKING"]];//Cooking
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_dancing"] andName:@"DANCING"]];//Dancing
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_bars_dining"] andName:@"BARS_DINING"]];//Bars,Dining
    [list addObject:[[ActivityType alloc] initWithImage:[UIImage imageNamed:@"ico_reading"] andName:@"READING"]];//Reading

    return list;*/
    
    NSMutableArray *list = [NSMutableArray array];
    for (int i=0; i<arrActivities.count; i++) {
        NSDictionary *dict = [arrActivities objectAtIndex:i];
        //[list addObject:[[ActivityType alloc] initWithImage:[dict objectForKey:@"imageUri"] andName:[dict objectForKey:@"code"]]];
        [list addObject:[[ActivityType alloc]initWithImage:[dict objectForKey:@"imageUri"] andName:[dict objectForKey:@"code"] andFOrder:[[dict objectForKey:@"fSortOrder"] longValue] andMOrder:[[dict objectForKey:@"mSortOrder"] longValue]]];
    }
    return list;
}

-(id)initWithImage:(NSString*)image andName:(NSString*)name{
    self.imageurl = image;
    self.name = name;
    
    return self;
}

-(id)initWithImage:(NSString*)image andName:(NSString*)name andFOrder:(long)fOrder andMOrder:(long)mOrder
{
    self.imageurl = image;
    self.name = name;
    self.fOrder = fOrder;
    self.mOrder =mOrder;
    return self;
}

@end
