//
//  Country.h
//  SayHello
//
//  Created by Alex on 22.08.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Country : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSString *phoneCode;

- (instancetype)initWithDictionary:(NSDictionary*)info;

@end
