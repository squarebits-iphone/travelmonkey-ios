//
//  WeekDays.h
//  SayHello
//
//  Created by Alex on 16.09.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeekDay : NSObject

@property (strong, nonatomic) NSString *shortName;
@property (strong, nonatomic) NSString *fullName;

+ (NSArray*)createAllDays;
- (id)initWithShort:(NSString*)shortName andFull:(NSString*)full;

@end
