//
//  Event.m
//  SayHello
//
//  Created by Ram on 21/03/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "Event.h"

@implementation Event


- (id)initEvent
{
    self.code=@"";
    self.ownerId=@"";
    self.sponsorName=@"";
    self.address=@"";
    self.startDate=@"";
    self.endDate=@"";
    self.eventType=@"";;
    self.eventStatus=@"";
    self.inviteContent=@"";
    self.desc = @"";
    self.zip = @"";
    self.Ids = @"";
    self.iconUrl = @"";
    self.ActivityId=@"";
    self.name=@"";
    self.bannerUrl =@"";
    self.strMemberCount=@"";
    return self;
}

@end
