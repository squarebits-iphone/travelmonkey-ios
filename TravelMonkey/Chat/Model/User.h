//
//  User.h
//  SayHello
//
//  Created by Alex on 02.09.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Activity.h"
#import "Event.h"

@interface User : NSObject

@property (strong, nonatomic) NSNumber *idUser;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSNumber *gender;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSNumber *termsAccepted;
@property (strong, nonatomic) NSNumber *eulaAccepted;
@property (strong, nonatomic) NSMutableArray *activities;
@property (strong, nonatomic) NSString *avatarUri;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *chatAccountID;
@property (strong, nonatomic) NSString *chatAccountUserName;
@property (strong, nonatomic) NSString *chatAccountHashPwd;

@property (strong, nonatomic) NSMutableArray *events;

@property (nonatomic) BOOL isOnline;

- (id)initMatchUserWithDictionary:(NSDictionary*)info;
- (id)initUserWithPhone:(NSString*)phone;
- (id)initUserWithDictionary:(NSDictionary*)info;
- (NSArray*)getActivitiesJSON;
- (void)activities:(NSArray *)arr;
-(void)events:(NSArray*)arr;
@end
