//
//  ActivityType.h
//  SayHello
//
//  Created by Alex on 26.08.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityType : NSObject
{
    
    
}
@property long fOrder;
@property long mOrder;
@property (strong, nonatomic) NSString *imageurl;
@property (strong, nonatomic) NSString *name;

+(NSArray *)activityNamesOnServer;
+ (NSArray*)activityTypes;
+(NSArray *)activityNames;
+(NSArray *)activityImagesForSearchScreen;
+(NSArray *)activityImagesForEditScreen;
-(id)initWithImage:(NSString*)image andName:(NSString*)name;
+(void)setActivitiesData:(NSMutableArray *)arrData;

-(id)initWithImage:(NSString*)image andName:(NSString*)name andFOrder:(long)fOrder andMOrder:(long)mOrder;
 @end
