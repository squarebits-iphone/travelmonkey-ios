//
//  WeekDays.m
//  SayHello
//
//  Created by Alex on 16.09.14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import "WeekDay.h"

@implementation WeekDay

+ (NSArray*)createAllDays{
    NSMutableArray *days = [NSMutableArray array];
    [days addObject:[[WeekDay alloc] initWithShort:@"MON" andFull:@"MONDAY"]];
    [days addObject:[[WeekDay alloc] initWithShort:@"TUE" andFull:@"TUESDAY"]];
    [days addObject:[[WeekDay alloc] initWithShort:@"WED" andFull:@"WEDNESDAY"]];
    [days addObject:[[WeekDay alloc] initWithShort:@"THU" andFull:@"THURSDAY"]];
    [days addObject:[[WeekDay alloc] initWithShort:@"FRI" andFull:@"FRIDAY"]];
    [days addObject:[[WeekDay alloc] initWithShort:@"SAT" andFull:@"SATURDAY"]];
    [days addObject:[[WeekDay alloc] initWithShort:@"SUN" andFull:@"SUNDAY"]];
    
    return days;
}

- (id)initWithShort:(NSString*)shortName andFull:(NSString*)full{
    self.shortName = shortName;
    self.fullName = full;
    return self;
}

@end
