//
//  Implementations.h
//  Workflow
//
//  Created by Alex on 03.04.14.
//  Copyright (c) 2014 Kirill Kovalenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSDate (Convert)

- (NSString*)dateToString:(NSString*)mask;
- (NSDate*)dateOnly;

@end

@interface NSString (Convert)

- (NSDate*)stringToDate:(NSString*)mask;
- (NSString *)MD5;

@end

@interface NSDictionary (Convert)

- (NSString*)toJSON;

@end

@interface NSArray (Sort)

- (NSMutableArray*)sortArrayBy:(NSString*)byField asc:(BOOL)asc;
- (NSString*)toJSON;
- (NSUInteger)countNotNull;
- (instancetype)notNullObjects;

@end

@interface NSNumber (Round)

- (NSNumber*)roundToPrecision:(NSNumber*)precision;

@end

@interface UIImage (Create)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end

@interface UICollectionView (Select)

- (void)deselectAllItems:(BOOL)animated;
- (void)selectItemAtIndexPaths:(NSArray *)indexPaths animated:(BOOL)animated;

@end
