//
//  Implementations.m
//  Workflow
//
//  Created by Alex on 03.04.14.
//  Copyright (c) 2014 Kirill Kovalenko. All rights reserved.
//

#import "Extend.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSDate (Convert)

- (NSString*)dateToString:(NSString*)mask{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter = [App_Delegate setLocaleForIOS8:dateFormatter];
    [dateFormatter setDateFormat:mask];
    NSString *strDate = [dateFormatter stringFromDate:self];
    
    return strDate;
}

- (NSDate*)dateOnly{
    return [[self dateToString:@"dd.MM.yyyy"] stringToDate:@"dd.MM.yyyy"];
}

@end

@implementation NSString (Convert)

- (NSDate*)stringToDate:(NSString *)mask{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter = [App_Delegate setLocaleForIOS8:dateFormatter];
    [dateFormatter setDateFormat:mask];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:self];
    
    return dateFromString;
}

- (NSString*)MD5{
    // Create pointer to the string as UTF8
    const char *ptr = [self UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 byte MD5 hash value, store in buffer
    CC_MD5(ptr, (int)strlen(ptr), md5Buffer);
    
    // Convert MD5 value in the buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}

@end

@implementation NSDictionary (Convert)

- (NSString*)toJSON{
    NSError *e = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:0 error:&e];
    if (e) {
        //NSLog(@"%@", e.localizedDescription);
    }else{
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return jsonString;
    }
    
    return nil;
}

@end

@implementation NSArray (Sort)

- (NSMutableArray*)sortArrayBy:(NSString*)byField asc:(BOOL)asc{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:byField ascending:asc];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [self sortedArrayUsingDescriptors:sortDescriptors];
    return [[NSMutableArray alloc] initWithArray:sortedArray];
}

- (NSString*)toJSON{
    NSError *e = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:0 error:&e];
    if (e) {
        //NSLog(@"%@", e.localizedDescription);
    }else{
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return jsonString;
    }
    
    return nil;
}

- (NSUInteger)countNotNull{
    NSIndexSet *notNilIndexes = [self indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return obj != [NSNull null];
    }];
    
    return notNilIndexes.count;
}

- (instancetype)notNullObjects{
    NSMutableArray *arr = [NSMutableArray array];
    for (id obj in self) {
        if (![obj isKindOfClass:[NSNull class]]) {
            [arr addObject:obj];
        }
    }
    return arr;
}

@end

@implementation NSNumber (Round)

- (NSNumber*)roundToPrecision:(NSNumber*)precision{
    double valueToRound = [self doubleValue];
    double scale = pow(10, [precision integerValue]);
    double tmp = valueToRound * scale;
    tmp = (double)((int)(round(tmp)));
    double roundedValue = tmp / scale;
    
    return [NSNumber numberWithDouble:roundedValue];
}

@end

@implementation UIImage (Create)

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end

@implementation UICollectionView (Select)

- (void)deselectAllItems:(BOOL)animated{
    for (NSIndexPath *indexPath in [self indexPathsForSelectedItems]) {
        [self deselectItemAtIndexPath:indexPath animated:animated];
    }
}

- (void)selectItemAtIndexPaths:(NSArray *)indexPaths animated:(BOOL)animated{
    for (NSIndexPath *indexPath in indexPaths) {
        [self selectItemAtIndexPath:indexPath animated:animated scrollPosition:UICollectionViewScrollPositionNone];
    }
}

@end