//
//  EPDataSource.h
//  Entwicklungsprogramm
//
//  Created by Анна on 24.01.13.
//  Copyright (c) 2013 Анна. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface DataSource : NSObject

+(DataSource*)DataSource;

@property (strong, nonatomic) User *user;
@end
