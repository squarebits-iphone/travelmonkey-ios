//
//  EPDataSource.m
//  Entwicklungsprogramm
//
//  Created by Анна on 24.01.13.
//  Copyright (c) 2013 Анна. All rights reserved.
//

#import "DataSource.h"

static DataSource* dataSource;

@interface DataSource()

@end

@implementation DataSource

+(DataSource*)DataSource{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dataSource = [[self alloc] init];
    });
    
    return dataSource;
}

-(id)init{
    self = [super init];
    if(self) {
    
    }
    return self;
}

@end
