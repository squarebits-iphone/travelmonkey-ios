//
//  AppDelegate.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/11/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "AppDelegate.h"
#import "LoginVC.h"
#import "LeftVC_Local.h"
#import "LeftVC_Tourist.h"
#import "HomeVC_Tourist.h"
#import "HomeVC_Guide.h"
#import "Parser.h"
#import "LoadingVC.h"
#import "FilterScreenVC_Tourist.h"
#import "AddCreditCardDetailVC.h"
#import "IQKeyboardManager.h"
#import "TripDetailVC_Guide.h"
#import "TripDetailVC_Tourist.h"
#import "Reachability.h"
#import "SplashVC.h"
#import "VerifyEmailVC.h"
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import <Crashlytics/Crashlytics.h>
#import "AlertVC.h"
#import "LeftVC_Guest.h"
#import "TripsVC_Guide.h"
#import "ThankYouVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SCLAlertView.h"
#import "ALAlertBanner.h"
#import "PinterestSDK.h"



@import Stripe;

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize navigationController,window,dictRootController,deckController,userObject,shouldDisableScrolling,strStartTime,strEndTime;
@synthesize aryChat,isPlaySonundForContinueMessage,dBName,dBPath,selectedGuide;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    //Xmpp
    self.xmppManager = [XMPPManager sharedManager];
    
    //AryChat
    self.aryChat=[[NSMutableArray alloc] init];
    self.aryUnreadChat=[[NSMutableArray alloc] init];
    [self.aryUnreadChat addObject:@"73"];
    
    //DataBaseManager
    [self databaseInitialization];
    
    //Facebook
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    //Stripe
    [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:kStripeTestKey];
    [[STPTheme defaultTheme] setAccentColor:KAppColor];
    
    //Fabric
    [Fabric with:@[[STPAPIClient class], [Twitter class], [Crashlytics class],[Answers class]]];
    
    //Pinterest
    [PDKClient configureSharedInstanceWithAppId:KPinterestApplicationID];

    
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    
    //[GMSServices provideAPIKey:kGoogleKeyForMap];
    [GMSPlacesClient provideAPIKey:kGoogleKeyForMap];
    
    window=[[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    [window setBackgroundColor:[UIColor whiteColor]];
    SplashVC *viewController=[[SplashVC alloc]init];
    //ThankYouVC *viewController=[[ThankYouVC alloc]init];
    navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
    [navigationController setNavigationBarHidden:YES];
    [window setRootViewController:navigationController];
    
    [window makeKeyAndVisible];
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:Default_SaveUserObject])
    {
        //[self setupViewForAlreadyLoggedInUser];
        LoadingVC *loading=[[LoadingVC alloc]init];
        [window setRootViewController:loading];
    }
    
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:40];
    
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    
    //temp - Currently these flags will play sound and make vibrate
    [SH_Defaults setBool:YES forKey:kIsAudioOn];
    [SH_Defaults setBool:YES forKey:kIsVibrateOn];
    
    
    NSLog(@"Local Time 01:00:00 -> GMT: %@",[self getGMTFromLocalTime:@"01:00:00" andDateFormat:@"HH:mm:ss"]);
    NSLog(@"GMT Time 01:30:00 -> GMT: %@",[self getLocalTimeFromGMT:@"01:30:00" andDateFormat:@"HH:mm:ss"]);
    
    
    return YES;
    
}

-(void)response:(NSDictionary*)dictionary
{
    
    //    NSString *arabicWord=@"لصفحة الرئيسية";
    //    //NSStringEncoding arabicEnc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingISOLatinArabic);
    //    //arabicWord=[arabicWord stringByAddingPercentEscapesUsingEncoding:arabicEnc];
    //
    //    NSString *strUrl=[NSString stringWithFormat:@"http://suggestqueries.google.com/complete/search?q=%@&client=firefox&hl=ar&alt=json",arabicWord];
    //
    //    [APIsList getAPIWithURL:strUrl Selector:@selector(response:) WithCallBackObject:self];
    
    NSLog(@"Result: %@",dictionary);
}

void uncaughtExceptionHandler ()
{
//            NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
//            [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
//    
//            LoginVC *viewController=[[LoginVC alloc]init];
//            App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
//            [App_Delegate.navigationController setNavigationBarHidden:YES];
//            [App_Delegate.window setRootViewController:App_Delegate.navigationController];
    
    
    
    if (App_Delegate.userObject!=nil && App_Delegate.userObject.strFirstName!=nil && App_Delegate.userObject.strEmail!=nil)
    {
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        [dic setObject:App_Delegate.userObject.strEmail forKey:@"email"];
        [dic setObject:App_Delegate.userObject.strFirstName forKey:@"name"];
        
        [Answers logCustomEventWithName:@"Crash"
                       customAttributes:dic];
    }
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [self.xmppManager disconnect];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [self connectToXMPP];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
    application.applicationIconBadgeNumber=0;
}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary *)options {
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation

{
    
    if ([[url path] containsString:@"pdk"])
    {
        return [[PDKClient sharedInstance] handleCallbackURL:url];
    }
    
    
    if ([[url path] containsString:@"fb"])
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }
    
    
    
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation];
}




- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [self.xmppManager disconnect];
}

#pragma mark - Google Sign-In

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error
{
    // Perform any operations on signed in user here.
    if (error || user==nil)
    {
        //[App_Delegate showAlertWithMessage:[NSString stringWithFormat:@"%@",[error.userInfo objectForKey:NSLocalizedDescriptionKey]] inViewController:nil];
        NSLog(@"Google Sign in error : %@",[NSString stringWithFormat:@"%@",error]);
    }
    else
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"googleSign" object:user];
    }
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error
{
    // Perform any operations when the user disconnects from app here.
    // ...
}

#pragma mark- CLLocation Delegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //    if (!isLocationPrivacyAlert)
    //    {
    //        isLocationPrivacyAlert=YES;
    //
    //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:KAppName message:@"Location not found. Please turn on location from SETTING." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Go to setting", nil];
    //        [alert setDelegate:(id)self];
    //        [alert show];
    //    }
}

//For Ios 8
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation=[locations lastObject];
    NSLog(@"New location Found at %@",newLocation);
    self.CurrentUserLocation = newLocation;
}

#pragma mark - Push Notification Work

-(void)askForPush
{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        // use registerUserNotificationSettings
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    else
    {
        // use registerForRemoteNotifications
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    
    NSString *strDeviceToken=[[[[deviceToken description]
                                stringByReplacingOccurrencesOfString: @"<" withString: @""]
                               stringByReplacingOccurrencesOfString: @">" withString: @""]
                              stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"My token is: %@", strDeviceToken);
    
    //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Development" message:strDeviceToken delegate:nil cancelButtonTitle:@"Ok Puru" otherButtonTitles: nil];
    //[alert show];
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = strDeviceToken;
    
    [[NSUserDefaults standardUserDefaults] setObject:strDeviceToken forKey:Default_SavedPush];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self callPushRegistrationAPI:strDeviceToken];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler
{
    
    NSLog(@"PushNotification Detail %@",userInfo);
    //http://stackoverflow.com/questions/14968578/calling-didreceiveremotenotification-when-app-is-launching-for-the-first-time
    
    
    if (userInfo!=nil && [userInfo isKindOfClass:[NSDictionary class]] && [[userInfo objectForKey:@"aps"] objectForKey:@"category"] )
    {
        //Chat PushNotification
        return;
    }

    
    if (userInfo!=nil && [userInfo isKindOfClass:[NSDictionary class]] && [[userInfo objectForKey:@"aps"] objectForKey:@"alert"] )
    {
//        //Booking PushNotification
//        NSString *strMessage=[[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
//        
//        SCLAlertView *alert =[App_Delegate getAlertInstance];
//        
//        [alert addButton:@"Yes" actionBlock:^(void) {
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"openTripPage" object:nil];
//            
//        }];
//        
//        [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:KAppName subTitle:strMessage closeButtonTitle:@"No" duration:0.0];
        
    }
}

-(void)callPushRegistrationAPI:(NSString*)strToken
{
    if (strToken==nil || strToken==(id)[NSNull null] || App_Delegate.userObject.strId==nil || App_Delegate.userObject.strId==(id)[NSNull null] || App_Delegate.userObject.strAuthKey==nil || App_Delegate.userObject.strAuthKey==(id)[NSNull null])
    {
        return;
    }
    
    NSString *url=[NSString stringWithFormat:@"%@%@",KBaseURL,KRegisterDeviceTokenForPushNotification];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strToken forKey:@"device_token"];
    [dicParameter setObject:@"I" forKey:@"device_type"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:url andHeader:dicHeader andParameters:dicParameter Selector:@selector(pushRegistrationAPIResponse:) WithCallBackObject:self];
    
}

-(void)pushRegistrationAPIResponse:(NSDictionary*)dic
{
    NSLog(@"push registration api response : %@",dic);
}

#pragma mark- Internal Login

-(void)internalLogin
{
    NSDictionary *userDetail=[[NSUserDefaults standardUserDefaults]objectForKey:Default_SaveUserObject];
    
    Parser *parser=[[Parser alloc]init];
    App_Delegate.userObject=[parser getUserDetail:userDetail];
    
    if ([App_Delegate.userObject.strSite isEqualToString:@"NATIVE"])
    {
        [self callLogin];
    }
    else
    {
        [self callSocialLogin];
    }
}

-(void)callLogin
{
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KNativeLogin];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strEmail forKey:@"email"];
    [dicParameter setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"password"] forKey:@"password"];
    
    [APIsList postAPIWithURL:strUrl andParameters:dicParameter Selector:@selector(loginAPIResponse:) WithCallBackObject:self];
}

-(void)callSocialLogin
{
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KSocialLogin];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strEmail forKey:@"email"];
    [dicParameter setObject:App_Delegate.userObject.strSite forKey:@"site"];
    
    [APIsList postAPIWithURL:strUrl andParameters:dicParameter Selector:@selector(loginAPIResponse:) WithCallBackObject:self];
}

-(void)loginAPIResponse:(NSDictionary*)dictionary
{
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        Parser *parser=[[Parser alloc]init];
        App_Delegate.userObject=[parser getUserDetail:dictionary];
        
        if ([App_Delegate.userObject.strUserType isEqualToString:@"TRAVELER"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserTourist forKey:Default_UserType];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserGuide forKey:Default_UserType];
        }
        
        [[NSUserDefaults standardUserDefaults]setObject:dictionary forKey:Default_SaveUserObject];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshData" object:nil];
    }
    
    if (!isSuccess)
    {
        
    }
}


#pragma mark- AlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1024)
    {
        
        
        NSLog(@"Button index : %d",(int)buttonIndex);
        
        if (buttonIndex==1)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"openTripPage" object:nil];
        }
        
    }
}

#pragma mark - Methods

-(void)connectToXMPP
{
    
    if (App_Delegate.userObject==nil)
    {
        return;
    }
    
    if ([App_Delegate.userObject.strSite isEqualToString:@"NATIVE"])
    {
        //Connect to XMPP with the password which is entered at native.
        NSArray *aryString=[App_Delegate.userObject.strUserChatID componentsSeparatedByString:@"@"];
        
        if(![self.xmppManager connectWithJID:[NSString stringWithFormat:@"%@@%@",[aryString firstObject],DOMAIN_NAME] password:[[NSUserDefaults standardUserDefaults] objectForKey:@"password"]])
        {
            NSLog(@"XMPP not connected");
        }
    }
    else
    {
        //Password will be same as JID
        if(![self.xmppManager connectWithJID:[NSString stringWithFormat:@"%@@%@",App_Delegate.userObject.strUserChatID,DOMAIN_NAME] password:App_Delegate.userObject.strUserChatID])
        {
            NSLog(@"XMPP not connected");
        }
    }
}

-(BOOL)isInternetAvailable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus ==NotReachable)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

-(NSDateFormatter *)setLocaleForIOS8:(NSDateFormatter*)dateFormate
{
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < ( double ) 9 )
    {
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormate setLocale:locale];
    }
    
    return dateFormate;
}

-(NSString*)getLocalTimeFromGMT:(NSString*)gmtDateString andDateFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:strFormat];
    
    //Create the date assuming the given string is in GMT
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *date = [df dateFromString:gmtDateString];
    
    //Create a date string in the local timezone
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
    NSString *localDateString = [df stringFromDate:date];
    //NSLog(@"date = %@", localDateString);
    
    return localDateString;
}

-(NSString*)getLocalTimeFromGMT:(NSString*)gmtDateString
{
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    //Create the date assuming the given string is in GMT
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *date = [df dateFromString:gmtDateString];
    
    //Create a date string in the local timezone
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
    NSString *localDateString = [df stringFromDate:date];
    //NSLog(@"date = %@", localDateString);
    
    return localDateString;
}

-(NSString*)getGMTFromLocalTime:(NSString*)gmtDateString andDateFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:strFormat];
    
    //Create the date assuming the given string is in GMT
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
    NSDate *date = [df dateFromString:gmtDateString];
    
    //Create a date string in the local timezone
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSString *localDateString = [df stringFromDate:date];
    //NSLog(@"date = %@", localDateString);
    
    return localDateString;
}

-(NSString*)getGMTFromLocalTime:(NSString*)gmtDateString
{
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    //Create the date assuming the given string is in GMT
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
    NSDate *date = [df dateFromString:gmtDateString];
    
    //Create a date string in the local timezone
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSString *localDateString = [df stringFromDate:date];
    //NSLog(@"date = %@", localDateString);
    return localDateString;
}

-(int)getMinutesDifferenceBetweenStartDate:(NSString *)strStartDate endDate:(NSString*)strEndDate withFormaterStyle:(NSString*)style
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:style]; //coming string 2015-11-09 @ 18:45
    
    NSDate *startDate=[formatter dateFromString:strStartDate];
    NSDate *endDate=[formatter dateFromString:strEndDate];
    NSLog(@"Start Date : %@ End Date : %@",strStartDate,strEndDate);
    
    NSTimeInterval secondsBetween = [endDate timeIntervalSinceDate:startDate];
    int minBetweenTwoDates = secondsBetween /60;
    
    NSLog(@"There are %d min in between the two dates.", minBetweenTwoDates);
    
    if (minBetweenTwoDates==59)
    {
        minBetweenTwoDates=60;
    }
    
    return minBetweenTwoDates;
    
}

-(NSString*)getConvertedDateStringFromDateString:(NSString*)inputDate inputDateString:(NSString*)inputDateFormat andOutputDateFormat:(NSString*)outputDateFormat
{
    
    NSString *strDateOutput=@"";
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:inputDateFormat];
    NSDate *dateTime=[dateFormatter dateFromString:inputDate];
    [dateFormatter setDateFormat:outputDateFormat];
    strDateOutput=[dateFormatter stringFromDate:dateTime];
    
    return strDateOutput;
}

-(NSDate*)getDateWithZeroSecond:(NSDate*)inputDate
{
    NSDateComponents *comp = [[NSCalendar currentCalendar] components:NSCalendarUnitSecond
                                                             fromDate:inputDate];
    
    NSDate *output=[inputDate dateByAddingTimeInterval:-comp.second];
    
    NSLog(@"Input Date : %@",inputDate);
    NSLog(@"Output Date : %@",output);
    
    return output;
}

+(void)rippleWithView:(UIView *)view center:(CGPoint)center  colorFrom:(UIColor *)colorFrom colorTo:(UIColor *)colorTo
{
    
    if (!view)
    {
        return;
    }
    
    [view setClipsToBounds:YES];
    
    CGFloat radius = 50.0f;
    UIView *ripple = [[UIView alloc] initWithFrame:CGRectMake((view.frame.size.width-radius)/2, (view.frame.size.height-radius)/2, radius, radius)];
    ripple.layer.cornerRadius = radius * 0.5f;
    ripple.backgroundColor = colorFrom;
    ripple.alpha = 0.0f;
    ripple.center = center;
    ripple.alpha = 0.5f;
    [view insertSubview:ripple atIndex:0];
    
    CGFloat scale = 8.0f;
    [UIView animateWithDuration:0.6f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        ripple.transform = CGAffineTransformMakeScale(scale, scale);
        ripple.alpha = 0.0f;
        ripple.backgroundColor = colorTo;
    } completion:^(BOOL finished) {
        [ripple removeFromSuperview];
    }];
}

-(void)setupViewForAlreadyLoggedInUser
{
    NSDictionary *userDetail=[[NSUserDefaults standardUserDefaults]objectForKey:Default_SaveUserObject];
    
    Parser *parser=[[Parser alloc]init];
    App_Delegate.userObject=[parser getUserDetail:userDetail];
    
    if ([App_Delegate.userObject.strUserType isEqualToString:@"TRAVELER"])
    {
        [[NSUserDefaults standardUserDefaults]setObject:KUserTourist forKey:Default_UserType];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:KUserGuide forKey:Default_UserType];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.window setRootViewController:[self setupDeckViewController]];
    
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(IIViewDeckController *)setupDeckViewController
{
    
    //ConnetXMPP
    [self performSelector:@selector(connectToXMPP) withObject:nil afterDelay:2.0];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:Default_SavedPush])
    {
        [self callPushRegistrationAPI:[[NSUserDefaults standardUserDefaults] objectForKey:Default_SavedPush]];
    }
    
    UIViewController *mainVC;
    UIViewController *leftMenuVC;
    
    if ([self getCurrentUser]==Tourist)
    {
        mainVC=[[HomeVC_Tourist alloc]init];
        leftMenuVC=[[LeftVC_Tourist alloc]init];
        //[App_Delegate GetcurrentLocation];
        
        [self askForPush];
    }
    else if ([self getCurrentUser]==Guest)
    {
        mainVC=[[HomeVC_Tourist alloc]init];
        leftMenuVC=[[LeftVC_Guest alloc]init];
        App_Delegate.userObject=[[UserObject alloc]init];
        App_Delegate.userObject.strId=@"1";
        App_Delegate.userObject.strAuthKey=KGuestAuth;
    }
    else
    {
        mainVC=[[TripsVC_Guide alloc]init];
        leftMenuVC=[[LeftVC_Local alloc]init];
        
        [self askForPush];
    }
    
    //dictRootController=[[NSMutableDictionary alloc]init];
    //mainVC=[[TripDetailVC_Guide alloc]init];
    //mainVC=[[TripDetailVC_Tourist alloc]init]; //temp
    
    [self logUserCrashlytics];
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:mainVC];
    [navController setNavigationBarHidden:YES];
    
    [dictRootController setObject:navController forKey:@"0"];
    
    self.deckController =  [[IIViewDeckController alloc]
                            initWithCenterViewController:navController
                            leftViewController:leftMenuVC
                            rightViewController:nil];
    
    self.deckController.delegate=(id)self;
    self.deckController.enabled=YES;
    self.deckController.leftSize = 100;
    self.deckController.rightSize = 100;
    
    return self.deckController;
}

#pragma mark Alerts

-(SCLAlertView*)getAlertInstance
{
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    [alert setHorizontalButtons:YES];
    alert.showAnimationType = SCLAlertViewShowAnimationSlideInToCenter;
    alert.backgroundType = SCLAlertViewBackgroundBlur;
    
    return alert;
}

-(void)dropDownalertWithMessage:(NSString*)strMessage
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ALAlertBannerStyle randomStyle = ALAlertBannerStyleFailure;
    ALAlertBannerPosition position = ALAlertBannerPositionBottom;
    
    ALAlertBanner *banner = [ALAlertBanner alertBannerForView:appDelegate.window style:randomStyle position:position title:KAppName subtitle:strMessage tappedBlock:^(ALAlertBanner *alertBanner) {
        NSLog(@"tapped!");
        [alertBanner hide];
    }];
    
    [banner show];
}

-(void)showCustomAlert:(NSString *)strMessage isSucess:(BOOL)isSucess
{
    SCLAlertView *alert=[self getAlertInstance];
    
    if (isSucess)
    {
        [alert showSuccess:@"Done!" subTitle:strMessage closeButtonTitle:@"Ok" duration:0.0];
    }
    else
    {
        [alert showError:@"Oops!" subTitle:strMessage closeButtonTitle:@"Ok" duration:0.0];
    }
    
}

-(void)showTMAlertWithMessage:(NSString*)message andTitle:(NSString*)strTitle doneButtonTitle:(NSString*)strDoneButtonTitle inViewController:(UIViewController*)viewContorller
{
    
    
    SCLAlertView *alert=[self getAlertInstance];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:message closeButtonTitle:strDoneButtonTitle duration:0.0];
    
}

-(void)showAlertWithMessage:(NSString*)message inViewController:(UIViewController*)viewContorller
{
    
    SCLAlertView *alert=[self getAlertInstance];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:message closeButtonTitle:@"OK" duration:0.0];

}

-(UserType)getCurrentUser
{
    NSString *strUserType=[[NSUserDefaults standardUserDefaults] objectForKey:Default_UserType];
    
    if ([strUserType isEqualToString:KUserTourist])
    {
        return Tourist;
    }
    else if ([strUserType isEqualToString:KUserGuide])
    {
        return Guide;
    }
    else
    {
        return Guest;
    }
    
}

-(void)GetcurrentLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    locationManager.distanceFilter = 10;
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationManager requestWhenInUseAuthorization];
    }
    
    [locationManager startUpdatingLocation];
}

-(int)getAgeFromBirthdate:(NSDate *)birthday
{
    
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:birthday
                                       toDate:now
                                       options:0];
    int age = (int)[ageComponents year];
    
    return age;
    
}

-(void)setStatusBarBackgroundColor:(UIColor*)color
{
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}

#pragma mark Fabric Crashlytics

- (void)logUserCrashlytics
{
    
    if (App_Delegate.userObject)
    {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        [CrashlyticsKit setUserIdentifier:App_Delegate.userObject.strId];
        [CrashlyticsKit setUserEmail:App_Delegate.userObject.strEmail];
        [CrashlyticsKit setUserName:App_Delegate.userObject.strFirstName];
        NSLog(@"Adding user info");
    }
    else
    {
        NSLog(@"User info not available");
    }
}

#pragma mark - Database work

-(void)databaseInitialization
{
    dBName=@"TravelMonkey.sqlite";
    self.dBPath=[self getDBPath: dBName];
    NSLog(@"DBPath:%@",self.dBPath);
    [self copyDatabaseFromApplicationBundelIfNeeded];
    self.dbManager=[[DBManager alloc] initWith];
}

- (NSString*)getDBPath :(NSString*)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    //    NSLog(@"%@",[documentsDir stringByAppendingPathComponent:filename]);
    return [documentsDir stringByAppendingPathComponent:filename];
}

-(void)copyDatabaseFromApplicationBundelIfNeeded
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    Boolean success = [fileManager fileExistsAtPath:self.dBPath];
    
    if(success)
        return;
    unlink([self.dBPath UTF8String]);
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:dBName];
    [fileManager copyItemAtPath:databasePathFromApp toPath:self.dBPath error:nil];
}



@end
