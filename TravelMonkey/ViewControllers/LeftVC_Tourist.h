//
//  LeftVC_Tourist.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/9/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LeftVC_Tourist : UIViewController
{
    IBOutlet UILabel *lblUserName;
    IBOutlet UIImageView *imgProfile;
    IBOutlet UITableView *tblOption;
    IBOutlet UILabel *lblViewProfile;
    IBOutlet UIView *viewSideBar;
    NSIndexPath *currentIndex;
    
}


@end
