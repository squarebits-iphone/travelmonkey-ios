//
//  ViewPhotosVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/6/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewPhotosVC : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIScrollView *scrl;
    NSMutableArray *pageViews;
    UILabel *lblPageNumber;
    IBOutlet UILabel *lblCaption;
    BOOL isZoomed;
}

@property (nonatomic,strong) NSMutableArray *aryPhotos;
@property int currentIndex;


@end
