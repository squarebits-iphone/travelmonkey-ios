//
//  ChatViewControllerVC_Guide.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 11/9/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBubbleTableViewDataSource.h"
#import "UIBubbleTableView.h"
#import "HPGrowingTextView.h"

@interface ChatViewControllerVC_Guide : UIViewController<HPGrowingTextViewDelegate>
{
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UILabel *lblChatPersonName;
    IBOutlet UILabel *lblChatPersonPlace;
    IBOutlet UIBubbleTableView *tblMsgList;
    
    UIView *containerView;
    HPGrowingTextView *textView;
    UIButton *doneBtn;
    __weak IBOutlet NSLayoutConstraint *constraintTblBottom;
}

@property (strong,nonatomic) NSMutableArray *bubbleData; //Chat Array
@property (nonatomic,strong) NSString *strUserChatId;
@property (nonatomic,strong) NSString *strBookingID;
@property (nonatomic,strong) NSString *strOtherName;
@property (nonatomic,strong) NSString *strOtherPlace;
@property (nonatomic,strong) NSString *strOtherImageUrl;
@property (nonatomic,retain) NSMutableArray *arrChatSection;

@end
