//
//  ChatViewControllerVC_Guide.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 11/9/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "ChatViewControllerVC_Guide.h"
#import "IQKeyboardManager.h"
#import "Extend.h"
#import "UIImageView+WebCache.h"


@interface ChatViewControllerVC_Guide ()

@end

@implementation ChatViewControllerVC_Guide
@synthesize strUserChatId,arrChatSection;



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    self.bubbleData = [[NSMutableArray alloc] init];
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    //Disabling IQKeyboard Manager
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] disableToolbarInViewControllerClass:[ChatViewControllerVC_Guide class]];
    [[IQKeyboardManager sharedManager] disableInViewControllerClass:[ChatViewControllerVC_Guide class]];
    
    
    
    //Keyboard Manager
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    //Tap gesture to resign keyboard
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(resignTextView)];
    tap.delegate=self;
    [tblMsgList addGestureRecognizer:tap];
    
    //This will call when a message is arived.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addLastIncomingMessageData) name:kMessageReceivedNotification object:nil];
    
    //This will call when a status is changed of Read message
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshChatViewForSeen) name:kTYPE_SEND_READ_STATUS object:nil];
    
    //This will call when a status is changed of Delivered message
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshChatViewForSeen) name:kMessageReceiptNotification object:nil];
        
    //Loading ary chat section
    arrChatSection=[[NSMutableArray alloc] init];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    App_Delegate.isChatOpened=YES;
    NSArray *aryString=[self.strUserChatId componentsSeparatedByString:@"@"];
    NSString *strJID= [NSString stringWithFormat:@"%@",self.strUserChatId];
    NSLog(@"AryString : %@",aryString);
    
    if (self.strOtherName)
    {
        [lblChatPersonName setText:self.strOtherName];
    }
    
    if (self.strOtherPlace)
    {
        [lblChatPersonPlace setText:self.strOtherPlace];
    }
    
    if (aryString)
    {
        strJID=[aryString firstObject];
    }
    
    App_Delegate.strCurrentChatFriend=strJID;
    App_Delegate.strCurrentBookingID=self.strBookingID;
    
    
    if ([App_Delegate isInternetAvailable])
    {
        [App_Delegate.xmppManager sendReadStatus:App_Delegate.strCurrentChatFriend]; //for temp
    }

}

-(void)viewWillDisappear:(BOOL)animated
{
    App_Delegate.isChatOpened=NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    [arrChatSection removeAllObjects];
    [self inputBarForChatting];
    [self performSelector:@selector(MessageHistryFromDB) withObject:nil afterDelay:0.2];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
}

#pragma mark- Methods

-(void)refreshChatViewForSeen
{
    [self performSelectorOnMainThread:@selector(MessageHistryFromDB) withObject:nil waitUntilDone:NO];
}

-(void)resignTextView
{
    [textView resignFirstResponder];
}


- (void)inputBarForChatting
{
    UIFont *font=[UIFont systemFontOfSize:15.0];
    
    float h = 0.0;
    float hh = 0.0;
    float y =0.0;
    
    if (IS_IPHONE_6_PLUS)
    {
        h = 60.0;
        hh= 47.0;
        y = 15.0;
    }
    else if (IS_IPHONE_6)
    {
        h = 55.0;
        hh = 42.0;
        y = 12.0;
    }
    else if (IS_IPHONE_5)
    {
        h = 47.0;
        hh = 36.0;
        y = 9.0;
    }
    else
    {
        h = 47.0;
        hh = 36.0;
        y = 9.0;
    }
    
    constraintTblBottom.constant=h;
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - h, self.view.frame.size.width, h)]; //Puru h-5
    
    UIView *viewSeprator=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,1)];
    [viewSeprator setBackgroundColor:[UIColor lightGrayColor]];
    [containerView addSubview:viewSeprator];
    
    UIImageView *textbackView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 7, self.view.frame.size.width-50, hh)];
    //[textbackView setImage:[UIImage imageNamed:@"comment_box"]];
    textbackView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [containerView addSubview:textbackView];
    
    textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(10, y, self.view.frame.size.width-55, hh)];//(10, 10, 260, 30)
    textView.contentInset = UIEdgeInsetsMake(5, 7, 5, 7);
    textView.placeholder=@"Write Message";
    textView.minNumberOfLines = 1;
    textView.maxNumberOfLines = 2;
    textView.returnKeyType = UIReturnKeyDefault; //just as an example
    textView.font = font;
    textView.delegate = self;
    textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 7, 5, 7);
    textView.backgroundColor = [UIColor clearColor];
    textView.layer.cornerRadius = 5.0;
    textView.layer.masksToBounds = YES;
    
    textView.animateHeightChange = YES; //turns off animation
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [containerView addSubview:textView];
    
    doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(self.view.frame.size.width-40,(h-40)/2,40,40);//(270,10,45,35)
    doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [doneBtn setTitle:@"" forState:UIControlStateNormal];
    [doneBtn setImage:[UIImage imageNamed:@"icn_send"] forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(btnSendPressed:) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn setBackgroundColor:[UIColor clearColor]];
    [containerView addSubview:doneBtn];
    [containerView bringSubviewToFront:doneBtn];
    [doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneBtn.titleLabel setFont:font];
    
    NSString *trimmedString = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if(trimmedString.length>0)
    {
        [doneBtn setEnabled:YES];
    }
    else
    {
        [doneBtn setEnabled:NO];
    }
    
    containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    textView.internalTextView.font=font;
    textView.placeholder=@"Write Message";
    textView.hidden=NO;
    containerView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:containerView];
}

- (void)moveContentViewToEnd
{
    
    float height=0.0;
    
    if ([textView isFirstResponder])
    {
        //If keyboard is shown
        height=tblMsgList.frame.size.height-containerView.frame.size.height-230;
        
        if (tblMsgList.contentSize.height > height)
        {
            NSIndexPath* ip = [NSIndexPath indexPathForRow:[tblMsgList numberOfRowsInSection:tblMsgList.numberOfSections -1]-1  inSection:tblMsgList.numberOfSections -1];
            
            [tblMsgList scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            //[tblMsgList setContentOffset:CGPointMake(0,tblMsgList.contentSize.height)];
        }
    }
    else
    {
        
        //If Keyboard is Hidden
        height=tblMsgList.frame.size.height;
        
        if (tblMsgList.contentSize.height > height)
        {
            [tblMsgList setContentOffset:CGPointMake(0,tblMsgList.contentSize.height-height)];
        }
    }
    
    NSLog(@"Height of Table view : %f",tblMsgList.frame.size.height);
    NSLog(@"Content Size of Table view : %f",tblMsgList.contentSize.height);
}

-(void)keyboardWillShow:(NSNotification *)note
{
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    // commit animations
    [UIView commitAnimations];
    
    tblMsgList.contentInset=UIEdgeInsetsMake(0, 0,keyboardBounds.size.height+20, 0);
    [self moveContentViewToEnd];
    
}

-(void)keyboardWillHide:(NSNotification *)note
{
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
    
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    tblMsgList.contentInset=UIEdgeInsetsMake(0, 0,0, 0);
    
    // commit animations
    [UIView commitAnimations];
    
}

- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView;
{
    NSString *trimmedString = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(trimmedString.length>0)
    {
        doneBtn.enabled=YES;
    }
    else
    {
        doneBtn.enabled=NO;
    }
}
- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    
    CGRect r = containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
    containerView.frame = r;
}

#pragma mark- Chat

// Load data from database
-(void)MessageHistryFromDB
{
    NSLog(@"Friend chat name : %@",App_Delegate.strCurrentChatFriend);
    
    [App_Delegate.dbManager getAllMessagesList:App_Delegate.strCurrentChatFriend  By:[SH_Defaults objectForKey:kSHChatID] andBookingID:self.strBookingID];
    
    [self.bubbleData removeAllObjects];
    
    for (int i=0; i<[App_Delegate.aryChat count]; i++)
    {
        NSBubbleData *sayBubble;
        
        MessageObject *message=[App_Delegate.aryChat objectAtIndex:i];
        
        NSString *dateStr = message.strTime;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        dateFormat = [App_Delegate setLocaleForIOS8:dateFormat];
        
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
        NSDate *date = [dateFormat dateFromString:dateStr];
        
        if (date == nil)
        {
            [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss a Z"];
            date = [dateFormat dateFromString:dateStr];
        }
        
        if ([message.strTypeOfSending isEqualToString:@"out"])
        {
            if ([message.strType isEqualToString:kTYPE_TEXT])
            {
                sayBubble = [NSBubbleData dataWithText:message.strBody date:date type:BubbleTypeMine];
                sayBubble.msgObj = message;
            }
            
            [self setImageToBubble:sayBubble withURL:[NSURL URLWithString:App_Delegate.userObject.strProfileImageURL]];
        }
        else
        {
            if ([message.strType isEqualToString:kTYPE_TEXT])
            {
                sayBubble = [NSBubbleData dataWithText:message.strBody date:date type:BubbleTypeSomeoneElse];
                sayBubble.msgObj =message;
            }
            
            if (self.strOtherImageUrl)
            {
              [self setImageToBubble:sayBubble withURL:[NSURL URLWithString:self.strOtherImageUrl]];
            }
        }
        
        
        
        if (sayBubble)
            [self.bubbleData addObject:sayBubble];
    }
    
    [tblMsgList performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:0.1];
    [self performSelectorOnMainThread:@selector(moveContentViewToEnd) withObject:nil waitUntilDone:0.4];
    
}


// Add last incominge message
-(void)addLastIncomingMessageData
{
    if (App_Delegate.isChatRowSelected)
    {
        App_Delegate.isChatRowSelected =NO;
        [UIMenuController sharedMenuController].menuVisible = NO;
    }
    tblMsgList.typingBubble = NSBubbleTypingTypeNobody;
    NSBubbleData *sayBubble;
    
    MessageObject *message=[App_Delegate.aryChat objectAtIndex:App_Delegate.aryChat.count-1];
    
    NSString *dateStr = message.strTime;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat = [App_Delegate setLocaleForIOS8:dateFormat];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    if (date == nil)
    {
        [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss a Z"];
        date = [dateFormat dateFromString:dateStr];
    }
    
    if ([message.strType isEqualToString:kTYPE_TEXT])
    {
        sayBubble = [NSBubbleData dataWithText:message.strBody date:date type:BubbleTypeSomeoneElse];
    }
    
    if (self.strOtherImageUrl)
    {
        [self setImageToBubble:sayBubble withURL:[NSURL URLWithString:self.strOtherImageUrl]];
    }
    
    if(self.bubbleData)
    {
        
        [self.bubbleData addObject:sayBubble];
    }
    else
    {
        self.bubbleData = [[NSMutableArray alloc] init];
        [self.bubbleData addObject:sayBubble];
    }
    
    sayBubble.msgObj =message;
    NSString *senderName =message.strNickName;
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"PrevUserName"]];
    if (str == nil)
    {
        str =@"";
    }
    
    str= [str lowercaseString];
    NSString *strNew = [senderName lowercaseString];
    
    if ([str isEqualToString:strNew])
    {
        sayBubble.strSenderName =@"";
        if (![[NSUserDefaults standardUserDefaults]objectForKey:@"PrevUserName"]) {
            [[NSUserDefaults standardUserDefaults]setObject:senderName forKey:@"PrevUserName"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }
    else
    {
        sayBubble.strSenderName =senderName;
        [[NSUserDefaults standardUserDefaults]setObject:senderName forKey:@"PrevUserName"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    CGFloat height = tblMsgList.frame.size.height;
    CGFloat contentYoffset = tblMsgList.contentOffset.y;
    CGFloat distanceFromBottom = tblMsgList.contentSize.height - contentYoffset;
    [tblMsgList reloadData];
    
    if(distanceFromBottom -20<= height)
    {
        [self performSelectorOnMainThread:@selector(moveContentViewToEnd) withObject:nil waitUntilDone:NO];
    }
    else
    {
        [tblMsgList setContentOffset:CGPointMake(tblMsgList.contentOffset.x, contentYoffset+20) animated:YES];
    }
}

#pragma mark- Button Action

-(void)btnSendPressed:(UIButton *)sender
{
    
    if (App_Delegate.xmppManager.xmppStream.isAuthenticated) {
        NSLog(@"Autheticated");
    }
    else if (!App_Delegate.xmppManager.xmppStream.isAuthenticated) {
        
        NSLog(@"NOT Autheticated");
        [App_Delegate showAlertWithMessage:@"User not authenticated." inViewController:self];
        [App_Delegate connectToXMPP];
        return;
    }
    
    tblMsgList.typingBubble = NSBubbleTypingTypeNobody;
    NSString *trimmedString = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (![trimmedString isEqualToString:@"" ])
    {
        
        NSBubbleData *sayBubble;
        sayBubble = [NSBubbleData dataWithText:trimmedString date:[NSDate dateWithTimeIntervalSinceNow:0] type:BubbleTypeMine];
        
        
        //Setting Image
        [self setImageToBubble:sayBubble withURL:[NSURL URLWithString:App_Delegate.userObject.strProfileImageURL]];
        
        //Get Message ID
        NSString *strMessageID=[App_Delegate.xmppManager getMessageID];
        
        //Message
        NSString *messageStr = trimmedString;
        
        //Sender JID
        NSArray *aryString=[self.strUserChatId componentsSeparatedByString:@"@"];
        NSString *strJID= [NSString stringWithFormat:@"%@",self.strUserChatId];
        NSLog(@"AryString : %@",aryString);
        
        if (aryString)
        {
            strJID=[NSString stringWithFormat:@"%@@%@",[aryString firstObject],DOMAIN_NAME];
        }
        
        XMPPJID *xmppJID=[XMPPJID jidWithString:strJID];
        
        //temp - Priyanka asked
        //User Info
        NSMutableDictionary *dictionaryUserInfo=[[NSMutableDictionary alloc]init];
        [dictionaryUserInfo setObject:App_Delegate.userObject.strProfileImageURL forKey:@"senderimage"];
        [dictionaryUserInfo setObject:App_Delegate.userObject.strFirstName forKey:@"sendername"];
        [dictionaryUserInfo setObject:self.strBookingID forKey:@"bookingId"];
        [dictionaryUserInfo setObject:[aryString firstObject] forKey:@"to"];
        
        //Converting User info to Json
        NSString *strJson=[dictionaryUserInfo toJSON];
        
        [App_Delegate.xmppManager sendMessageTo:xmppJID message:messageStr withID:strMessageID withUserInfo:strJson];
        
        //Work for saving into DB
        if (strMessageID.length<17)
            strMessageID=[NSString stringWithFormat:@"0%@",strMessageID];
        
        MessageObject *message=[[MessageObject alloc]initWithDefault];
        message.ID=[strMessageID doubleValue];
        message.strFriendUserName=[aryString firstObject];
        message.strBody=messageStr;

        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        formatter = [App_Delegate setLocaleForIOS8:formatter];
        [formatter setDateFormat:@"ddMMyyyyHHmmssSSS"];
        
        NSDate *date=[formatter dateFromString:strMessageID];
        message.timeStamp=[date timeIntervalSince1970];
        message.strTime=[NSString stringWithFormat:@"%@",date];
        message.strTypeOfSending=@"out";
        message.strType=kTYPE_TEXT;
        message.status=MSG_STATUS_UPLOADED;
        message.strMsgId = strMessageID;
        message.strBookingID=self.strBookingID;
        
        
        [App_Delegate.aryChat addObject:message];
        [App_Delegate.dbManager insertMessage:message];
        
        sayBubble.msgObj = message;
        
        [self.bubbleData addObject:sayBubble];
        [tblMsgList reloadData];
        if (App_Delegate.aryChat.count!=0)
        {
            [self performSelector:@selector(moveContentViewToEnd) withObject:nil afterDelay:0.01];
        }
    }
    
    textView.text = @"";
}


-(IBAction)btnCallPressed:(UIButton *)senderButton
{
    [App_Delegate showAlertWithMessage:@"Under Construction" inViewController:self];
}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Bubble Table Delegates Method

-(void)setImageToBubble:(NSBubbleData *)bubble withURL:(NSURL*)imageURL
{
    UIImageView *imageView=[[UIImageView alloc]init];
    
    [imageView sd_setImageWithURL:imageURL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        NSLog(@"Image set to bubble : %@",bubble);
        [bubble setAvatar:image];
        
    }];
}

- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
    return [self.bubbleData count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableViewBubble dataForRow:(NSInteger)row
{
    NSBubbleData * bdata=[self.bubbleData objectAtIndex:row];
    bdata.view.tag=row+500;
    
    MessageObject *message=[App_Delegate.aryChat objectAtIndex:row];
    if (bdata.type == BubbleTypeMine)
    {
        if (message.status==MSG_STATUS_SENT)
        {
            bdata.isSent=YES;
        }
        if (message.status==MSG_STATUS_RECEIVED)
        {
            bdata.isReceived=YES;
        }
    }
    NSString *messageID=[NSString stringWithFormat:@"%.0f",message.ID ];
    if (messageID.length<17)
        messageID=[NSString stringWithFormat:@"0%@",messageID];
    return [self.bubbleData objectAtIndex:row];
}



@end
