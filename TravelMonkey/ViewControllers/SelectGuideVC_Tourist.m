//
//  SelectGuideVC_Tourist.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/22/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "SelectGuideVC_Tourist.h"
#import "CollectionViewCell.h"
#import "Parser.h"
#import "UserObject.h"
#import "UIImageView+WebCache.h"
#import "GuideObject.h"
#import "FilterScreenVC_Tourist.h"
#import "HomeVC_Guide.h"


@interface SelectGuideVC_Tourist ()

@end

@implementation SelectGuideVC_Tourist

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    [collectionViewTable registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"CollectionViewCell"];
    
    [self performSelector:@selector(callSearchGuideAPIWithOptionParameter:) withObject:nil afterDelay:1.0];
    dicFilter=[[NSMutableDictionary alloc]init];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(filterSearch:) name:@"filterSearch" object:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

#pragma mark- Method

-(NSString*)getDayFromDayName:(NSString*)day
{
    if ([day isEqualToString:@"monday"])
    {
        return @"1";
    }
    else if ([day isEqualToString:@"tuesday"])
    {
        return @"2";
    }
    else if ([day isEqualToString:@"wednesday"])
    {
        return @"3";
    }
    else if ([day isEqualToString:@"thursday"])
    {
        return @"4";
    }
    else if ([day isEqualToString:@"friday"])
    {
        return @"5";
    }
    else if ([day isEqualToString:@"saturday"])
    {
        return @"6";
    }
    else if ([day isEqualToString:@"sunday"])
    {
        return @"7";
    }
    
    return @"1";
    
}

-(void)filterSearch:(NSNotification*)notification
{
    NSLog(@"Dictionary %@",notification.object);
    [self callSearchGuideAPIWithOptionParameter:notification.object];
}

-(void)callGuideDetailAPIWithID:(NSString*)strGuideID
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KGetUserDetail];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strGuideID forKey:@"otheruser_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(userDetailAPIResponse:) WithCallBackObject:self];
    
}

-(void)userDetailAPIResponse:(NSDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        Parser *parser=[[Parser alloc]init];
        UserObject *guideObject=[parser getUserDetail:dictionary];
        
        HomeVC_Guide *filter=[[HomeVC_Guide alloc]init];
        //filter.isForViewProfile=YES;
        filter.guideObject=guideObject;
        [self.navigationController pushViewController:filter animated:YES];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"User details not found" inViewController:self];
        }
    }
    
    [grayView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.5];
    
}

-(void)callSearchGuideAPIWithOptionParameter:(NSMutableDictionary*)optionalDictionary
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    collectionViewTable.hidden=YES;
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KSearchGuide];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:[NSString stringWithFormat:@"%f",self.touristGMSPlace.coordinate.latitude] forKey:@"latitude"];
    [dicParameter setObject:[NSString stringWithFormat:@"%f",self.touristGMSPlace.coordinate.longitude] forKey:@"longitude"];
    [dicParameter setObject:self.strStartTime forKey:@"start_time"];
    [dicParameter setObject:self.strEndTime forKey:@"end_time"];
    [dicParameter setObject:self.touristGMSPlace.name forKey:@"location"];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    
    if (optionalDictionary)
    {
        [dicParameter addEntriesFromDictionary:optionalDictionary];
    }
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *startDate=[dateFormatter dateFromString:self.strStartTime];
    dateFormatter.dateFormat=@"EEEE";
    
    NSString *dayString = [[dateFormatter stringFromDate:startDate] lowercaseString];
    dateFormatter.dateFormat=@"ZZZZZZ";
    NSString *gmtString = [dateFormatter stringFromDate:startDate];
    NSLog(@"day: %@", gmtString);
    [dicParameter setObject:[self getDayFromDayName:dayString] forKey:@"day"];
    [dicParameter setObject:[NSString stringWithFormat:@"%ld",App_Delegate.currentPlacemark.timeZone.secondsFromGMT] forKey:@"offset"];
    
    NSLog(@"Search Guide API : %@ Auth token: %@ & Parameters : %@",strURL,dicHeader,dicParameter);
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(searchGuideAPIResponse:) WithCallBackObject:self];
    
}

-(void)searchGuideAPIResponse:(NSDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    collectionViewTable.hidden=NO;
    aryGuides=[[NSMutableArray alloc]init];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        
        NSArray *ary=[dictionary objectForKey:@"guide_detail"];
        
        if (ary && [ary isKindOfClass:[NSArray class]] && ary.count>0)
        {
            isSuccess=YES;
            Parser *parser=[[Parser alloc]init];
            aryGuides=[parser getUserArray:ary];
        }
    }
    
    if (!isSuccess)
    {
        //[App_Delegate showAlertWithMessage:KNoGuideFound inViewController:self];
        [App_Delegate showTMAlertWithMessage:KNoGuideFound andTitle:@"Oops!" doneButtonTitle:@"Continue" inViewController:self];
    }
    
    [collectionViewTable reloadData];
}

-(UILabel *)getCustomBackgroundLabel
{
    UILabel *messageLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,collectionViewTable.bounds.size.width,collectionViewTable.bounds.size.height)];
    messageLbl.text = KNoGuideFound;
    messageLbl.textAlignment = NSTextAlignmentCenter;
    messageLbl.numberOfLines=0;
    messageLbl.font=[UIFont fontWithName:@"SourceSansPro-Regular" size:15.0];
    messageLbl.textColor=KAppColor_Blue;
    //messageLbl.backgroundColor=KAppColor;
    
    return messageLbl;
}



#pragma mark- Button Action

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnFilterPressed:(id)sender
{
    NSString *strKeyName=@"filterView";
    
    if ([dicFilter objectForKey:strKeyName])
    {
        FilterScreenVC_Tourist *filter=(FilterScreenVC_Tourist*)[dicFilter objectForKey:strKeyName];
        [self.navigationController pushViewController:filter animated:YES];
    }
    else
    {
        FilterScreenVC_Tourist *filter=[[FilterScreenVC_Tourist alloc]init];
        [dicFilter setObject:filter forKey:strKeyName];
        [self.navigationController pushViewController:filter animated:YES];
    }
}

#pragma mark - collection view delegates
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (aryGuides.count==0)
    {
        //collectionViewTable.backgroundView=[self getCustomBackgroundLabel];
    }
    else
    {
        //collectionViewTable.backgroundView=nil;
    }
    
    return aryGuides.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Adjust cell size
    float widthOfView=[UIScreen mainScreen].bounds.size.width;
    widthOfView=widthOfView-5-5-5; //10+10+10
    
    float cellWidth=widthOfView/2;
    float cellHeight=cellWidth+cellWidth/2;
    
    NSLog(@"Width of cell: %f",cellWidth);
    NSLog(@"Height of cell: %f",cellHeight);
    
    return CGSizeMake(cellWidth,cellHeight);
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //1
    static NSString *identifier = @"CollectionViewCell";
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    GuideObject *userLocal=[aryGuides objectAtIndex:indexPath.row];
    
    [cell.lblName setText:userLocal.strFirstName];
    [cell.lblReviews setText:[NSString stringWithFormat:@"%@ Review",userLocal.strTotalReviews]]; 
    
    if (userLocal.priceObject.strPrice!=nil && userLocal.priceObject.strPrice!=(id)[NSNull null])
    {
        [cell.lblRatePerHour setText:[NSString stringWithFormat:@"$%d/hr",userLocal.priceObject.strPrice.intValue]];
    }
    else
    {
        [cell.lblRatePerHour setText:[NSString stringWithFormat:@"$0/hr"]];
    }
    
    //[cell.lblRatePerHour setTextColor:KAppColor];
    
    [cell.imgviewGuide sd_setImageWithURL:[NSURL URLWithString:userLocal.strProfileImageURL] placeholderImage:[UIImage imageNamed:@"guidePlaceHolder.png"]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CollectionViewCell *cell =(CollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    
    grayView=[[GrayTouchView alloc] init];
    [grayView addToView:cell.imgviewGuide shouldRemoveAutomatically:NO];
    
    GuideObject *userLocal=[aryGuides objectAtIndex:indexPath.row];
    
    App_Delegate.selectedGuide=userLocal;
    
    [self callGuideDetailAPIWithID:userLocal.strId];
    
}

@end
