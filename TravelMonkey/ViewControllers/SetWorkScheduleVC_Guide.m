//
//  SetWorkScheduleVC_Guide.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/24/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "SetWorkScheduleVC_Guide.h"
#import "DaysCell.h"

@interface SetWorkScheduleVC_Guide ()

@end

@implementation SetWorkScheduleVC_Guide

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    
    [self setupPickerColor];
    [self setTimePickersWithDate:[NSDate date]];
    
    
    aryDays=[[NSMutableArray alloc]initWithObjects:@"Every Sunday",@"Every Monday",@"Every Tuesday",@"Every Wednesday",@"Every Thursday",@"Every Friday",@"Every Saturday", nil];
    
    //[self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    arySelectedDays=[[NSMutableArray alloc]init];
    [self performSelector:@selector(setupView) withObject:nil afterDelay:1.0];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadView) name:K_Notification_ReloadView object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


-(void)viewDidAppear:(BOOL)animated
{
    
    
    [self.view addSubview:scrlView];
    [scrlView setFrame:CGRectMake(0,115,[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-115)];
    [self.view bringSubviewToFront:lblChargePerHour];
    
    
}

-(void)viewDidLayoutSubviews
{
    [scrlView setContentSize:CGSizeMake(0,tblDays.frame.origin.y+tblDays.frame.size.height+10)];
    [constraintViewSubViewScrollView setConstant:tblDays.frame.origin.y+tblDays.frame.size.height+10];
}

#pragma mark- Methods

-(void)reloadView
{
    arySelectedDays=[[NSMutableArray alloc]init];
    [self setupView];
}

-(void)setupView
{
    if (App_Delegate.userObject.locatDetailObject.priceObject.strPrice!=nil && App_Delegate.userObject.locatDetailObject.priceObject.strPrice!=(id)[NSNull null])
    {
        [txtFieldPrice setText:[NSString stringWithFormat:@"%d",App_Delegate.userObject.locatDetailObject.priceObject.strPrice.intValue]];
    }
    else
    {
        [App_Delegate showAlertWithMessage:@"Please set your work schedule so\nTourists find you in their search results" inViewController:self];
        return;
    }
    
    NSDateFormatter *formatter=[NSDateFormatter new];
    [formatter setDateFormat:@"HH:mm:ss"];
    
    
    if (App_Delegate.userObject.locatDetailObject.priceObject.strDays!=nil && App_Delegate.userObject.locatDetailObject.priceObject.strDays!=(id)[NSNull null])
    {
        NSArray *aryDaysSelected=[App_Delegate.userObject.locatDetailObject.priceObject.strDays componentsSeparatedByString:@","];
        
        if (aryDaysSelected!=nil && aryDaysSelected!=(id)[NSNull null] && aryDaysSelected.count>0)
        {
            for (NSString *strDay in aryDaysSelected)
            {
                
                int indexPathRow=[strDay intValue]-1;
                NSLog(@"IndexPath : %d",indexPathRow);
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexPathRow inSection:0];
                [tblDays selectRowAtIndexPath:indexPath animated:NO scrollPosition: UITableViewScrollPositionNone];
                DaysCell *cell=(DaysCell*)[tblDays cellForRowAtIndexPath:indexPath];
                [cell.btnSelect setSelected:YES];
            }
            
            arySelectedDays=[aryDaysSelected mutableCopy];
        }
    }
    
    if (App_Delegate.userObject.locatDetailObject.priceObject.strStartTime!=nil && App_Delegate.userObject.locatDetailObject.priceObject.strStartTime!=(id)[NSNull null] && App_Delegate.userObject.locatDetailObject.priceObject.strStartTime.length>0)
    {
        NSLog(@"StartDate : %@",App_Delegate.userObject.locatDetailObject.priceObject.strStartTime);
        
        NSDate *startDate=[formatter dateFromString:App_Delegate.userObject.locatDetailObject.priceObject.strStartTime];
        [startTimePicker setDate:startDate];
    }
    
    if (App_Delegate.userObject.locatDetailObject.priceObject.strEndTime!=nil && App_Delegate.userObject.locatDetailObject.priceObject.strEndTime!=(id)[NSNull null] && App_Delegate.userObject.locatDetailObject.priceObject.strEndTime.length>0)
    {
        NSDate *endDate=[formatter dateFromString:App_Delegate.userObject.locatDetailObject.priceObject.strEndTime];
        [endTimePicker setDate:endDate];
    }

}

-(IBAction)timeChanged:(UIDatePicker*)timePicker
{
    if (timePicker.tag==101)
    {
        //Start Time
        NSTimeInterval seconds_End = 1 * 60 * 60; //hh*mm*sec
        NSDate *dateHoursAhead_End=[timePicker.date dateByAddingTimeInterval:seconds_End];
        [endTimePicker setMinimumDate:dateHoursAhead_End];
    }
    else
    {
        //End Time
    }
}

-(void)setupPickerColor
{
    [startTimePicker setValue:KAppColor_Blue forKeyPath:@"textColor"];
    [endTimePicker setValue:KAppColor_Blue forKeyPath:@"textColor"];
    SEL selector = NSSelectorFromString( @"setHighlightsToday:" );
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature :
                                [UIDatePicker
                                 instanceMethodSignatureForSelector:selector]];
    BOOL no = NO;
    [invocation setSelector:selector];
    [invocation setArgument:&no atIndex:2];
    [invocation invokeWithTarget:startTimePicker];
    [invocation invokeWithTarget:endTimePicker];
}

-(void)setTimePickersWithDate:(NSDate*)inputDate
{
    NSTimeInterval seconds = 1 * 60 * 60; //hh*mm*sec
    NSDate *dateHoursAhead=[inputDate dateByAddingTimeInterval:seconds];
    //[startTimePicker setMinimumDate:dateHoursAhead];
    [startTimePicker setDate:dateHoursAhead];
    
    
    NSTimeInterval seconds_End = 2 * 60 * 60; //hh*mm*sec
    NSDate *dateHoursAhead_End=[inputDate dateByAddingTimeInterval:seconds_End];
    [endTimePicker setMinimumDate:dateHoursAhead_End];
    [endTimePicker setDate:dateHoursAhead_End];
}

-(void)callSaveScheduleAPI
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KSaveWorkSchedule];
    
    NSDateFormatter *formatter=[NSDateFormatter new];
    [formatter setDateFormat:@"HH:mm"];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:[formatter stringFromDate:startTimePicker.date] forKey:@"start_time"];
    [dicParameter setObject:[formatter stringFromDate:endTimePicker.date] forKey:@"end_time"];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:txtFieldPrice.text forKey:@"price"];
    [dicParameter setObject:@"USD" forKey:@"currency"]; //temp
    
    [dicParameter setObject:[arySelectedDays componentsJoinedByString:@","] forKey:@"days"];

    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(getSaveScheduleAPIResponse:) WithCallBackObject:self];

}

-(void)getSaveScheduleAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        [App_Delegate showAlertWithMessage:@"Work schedule updated, Thank you !" inViewController:self];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Work schedule cannot be update now, Please try again later" inViewController:self];
        }
    }

}

-(BOOL)isValidationCorrect
{
    if (txtFieldPrice.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter your fee per hour" inViewController:self];
        return NO;
    }
    
    if (arySelectedDays.count==0)
    {
        [App_Delegate showAlertWithMessage:@"Please select atleast one day from week" inViewController:self];
        return NO;
    }
    
    return YES;
    
}

#pragma mark- Button Actions

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnSavePressed:(id)sender
{
    if ([self isValidationCorrect])
    {
        NSLog(@"Go ahead");
        [self callSaveScheduleAPI];
    }
}


#pragma mark- TextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    int length =(int)[currentString length];
    
    if (textField.tag==50 && length > 4) //Month
    {
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    if (textField.tag==50 && [textField.text intValue]<10) //Month
    {
        [App_Delegate showAlertWithMessage:@"Min price is $10" inViewController:self];
        
        return NO;
    }
    
    return YES;
}


#pragma mark- Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return aryDays.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strCellIdentifier=@"DaysCell";
    
    DaysCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
    
    if (cell==nil)
    {
        cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    
    [cell.lblName setText:[aryDays objectAtIndex:indexPath.row]];
    
    
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DaysCell *cell=(DaysCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.btnSelect.selected)
    {
        [cell.btnSelect setSelected:NO];
    }
    else
    {
        [cell.btnSelect setSelected:YES];
    }
    
    int number=(int)indexPath.row+1;
    
    if ([arySelectedDays containsObject:[NSString stringWithFormat:@"%d",number]])
    {
        [arySelectedDays removeObject:[NSString stringWithFormat:@"%d",number]];
    }
    else
    {
        [arySelectedDays addObject:[NSString stringWithFormat:@"%d",number]];
    }
}


@end
