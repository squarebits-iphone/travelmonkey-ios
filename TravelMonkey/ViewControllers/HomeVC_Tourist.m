//
//  HomeVC_Tourist.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "HomeVC_Tourist.h"
#import "SelectDateVC_Tourist.h"


#import <AddressBook/AddressBook.h>
#import <CoreLocation/CoreLocation.h>


@interface HomeVC_Tourist ()

@end

@implementation HomeVC_Tourist

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewSubNavigationBar setHidden:YES];
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    //[App_Delegate GetcurrentLocation];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

-(void)viewDidAppear:(BOOL)animated
{
   //[App_Delegate showTMAlertWithMessage:@"Message" andTitle:@"Title" inViewController:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Methods

-(void)callSearchGuideAPI
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KCheckGuideInCity];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:[NSString stringWithFormat:@"%f",touristGMSPlace.coordinate.latitude] forKey:@"latitude"];
    [dicParameter setObject:[NSString stringWithFormat:@"%f",touristGMSPlace.coordinate.longitude] forKey:@"longitude"];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:txtFieldPlace.text forKey:@"city"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    NSLog(@"Search Guide API : %@/n Auth token: %@/n & Parameters : %@",strURL,dicHeader,dicParameter);
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(searchGuideAPIResponse:) WithCallBackObject:self];
}

-(void)searchGuideAPIResponse:(NSDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        
        isSuccess=YES;
        SelectDateVC_Tourist *selectDate=[[SelectDateVC_Tourist alloc]init];
        selectDate.touristGMSPlace=touristGMSPlace;
        [self.navigationController pushViewController:selectDate animated:YES];
    }
    
    if (!isSuccess)
    {
        //[App_Delegate showAlertWithMessage:KNoGuideFound inViewController:self];
        [App_Delegate showTMAlertWithMessage:KNoGuideInCity andTitle:@"Oops!" doneButtonTitle:@"Continue" inViewController:self];
    }
}


#pragma mark- Button Action

-(IBAction)btnCityPressed:(id)sender
{
    [txtFieldPlace setText:@""];
    
    //Present the Autocomplete view controller when the button is pressed.
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    [acController setTintColor:KAppColor];
    [acController setPrimaryTextColor:KAppColor_GrayBlue];
    [acController setPrimaryTextHighlightColor:KAppColor];
    GMSAutocompleteFilter *filter=[GMSAutocompleteFilter new];
    filter.type=kGMSPlacesAutocompleteTypeFilterCity;
    
    acController.autocompleteFilter=filter;
    acController.delegate = (id)self;
    [self presentViewController:acController animated:YES completion:^{
        
    }];
    
    [App_Delegate setStatusBarBackgroundColor:KAppColor];
    
}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnNextPressed:(id)sender
{
    if (touristGMSPlace==nil)
    {
        [App_Delegate showAlertWithMessage:@"Please select city first" inViewController:self];
    }
    else
    {
        [self callSearchGuideAPI];
    }
}

#pragma mark- GMSAutoComplete Delegate

- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place
{
    
    [App_Delegate setStatusBarBackgroundColor:[UIColor clearColor]];
    
    // The user has selected a place.
    [self dismissViewControllerAnimated:YES completion:^{
        
        //http://stackoverflow.com/questions/15036007/how-to-obtain-country-state-city-from-reversegeocodecoordinate
        
        touristGMSPlace=place;
        
        [txtFieldPlace setText:place.name];
        
        //NSString *strLat=[NSString stringWithFormat:@"%f",place.coordinate.latitude];
        //NSString *strLong=[NSString stringWithFormat:@"%f",place.coordinate.longitude];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        CLLocation *location=[[CLLocation alloc]initWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude];
        
        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            NSLog(@"Finding address");
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if (error)
            {
                NSLog(@"Error %@", error.description);
            }
            else
            {
                CLPlacemark *placemark = [placemarks lastObject];
                App_Delegate.currentPlacemark=placemark;
                
                //NSString *city = [[placemark addressDictionary] objectForKey:(NSString *)kABPersonAddressCityKey];
                //NSString *countryLocal = [[placemark addressDictionary] objectForKey:(NSString *)kABPersonAddressCountryKey];
                //NSString *countryISO = [[placemark addressDictionary] objectForKey:(NSString *)kABPersonAddressCountryCodeKey];
                
                NSLog(@"Time Zome : %ld",placemark.timeZone.secondsFromGMT);
                NSLog(@"Time Zome Day light saving : %ld",placemark.timeZone.secondsFromGMT);
        
            }
        }];
        
    }];
}

-(NSString*)getTimeZoneStringForAbbriviation:(NSString*)abbr{
    NSTimeZone *atimezone=[NSTimeZone timeZoneWithAbbreviation:abbr];
    
    NSString *strSecond=[NSString stringWithFormat:@"%ld",(long)atimezone.secondsFromGMT];
    
    if (atimezone.secondsFromGMT<0)
    {
        strSecond=[strSecond stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }
    
    int sec=[strSecond intValue];
    
    int minutes = (sec / 60) % 60;
    int hours = sec/3600;
    NSString *aStrOffset=[NSString stringWithFormat:@"%02d:%02d",hours, minutes];
    
    if (atimezone.secondsFromGMT<0)
    {
        return [NSString stringWithFormat:@"GMT-%@",aStrOffset];
    }
    else
    {
        return [NSString stringWithFormat:@"GMT+%@",aStrOffset];
    }
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithError:(NSError *)error
{
    [App_Delegate setStatusBarBackgroundColor:[UIColor clearColor]];
    [self dismissViewControllerAnimated:YES completion:nil];
}

// User pressed cancel button.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController
{
    [App_Delegate setStatusBarBackgroundColor:[UIColor clearColor]];
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
