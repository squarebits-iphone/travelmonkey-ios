//
//  AddPhotosVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/1/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "AddPhotosVC.h"
#import "CellPhoto.h"
#import "PhotoObject.h"
#import "Reachability.h"
#import "UNIRest.h"
#import "UIImageView+WebCache.h"
#import "Parser.h"


@interface AddPhotosVC ()

@end

@implementation AddPhotosVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [collectionViewTable registerNib:[UINib nibWithNibName:@"CellPhoto" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"CellPhoto"];
    
    [self.view addSubview:viewAddImage];
   
    [viewAddImage setHidden:YES];
    
    arySelectedPhotoId=[[NSMutableArray alloc]init];
    
    [self refreshTable];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshTable) name:@"refreshData" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
     [viewAddImage setFrame:self.view.bounds];
}

#pragma mark- Methods

-(void)refreshTable
{
    [collectionViewTable reloadData];
    
    if (arySelectedPhotoId.count>0)
    {
        [btnAddDeletePhotos setSelected:YES];
    }
    else
    {
        [btnAddDeletePhotos setSelected:NO];
    }
}

-(void)uploadImageUnirest //This will upload image to the
{
    
    //Image path
    NSString *fileImagePath=[NSString stringWithFormat:@"%@/Documents/profilepic.jpeg",NSHomeDirectory()];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    //Url
    NSString *url=[NSString stringWithFormat:@"%@%@",KBaseURL,KAddPhoto];
    url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //Check network
    if (networkStatus==NotReachable)
    {
        [App_Delegate showAlertWithMessage:@"No internet connection." inViewController:self];
    }
    else
    {
        
        //Show loading
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        __block NSDictionary *dicResponse;
        
        //Content-Type: application/json
        NSMutableDictionary *headers =[[NSMutableDictionary alloc]init];
        [headers setObject:@"application/json" forKey:@"Content-Type"];
        [headers setObject:@"utf-8" forKey:@"Accept-Encoding"];
        
        NSData *data = [txtViewCaption.text dataUsingEncoding:NSNonLossyASCIIStringEncoding];
        NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSDictionary* parameters = @{@"role" :App_Delegate.userObject.strUserType,@"user_id" :App_Delegate.userObject.strId,@"caption" :goodValue, @"image": [NSURL fileURLWithPath:fileImagePath]};
        
        //Initializing Async request
        [[UNIRest post:^(UNISimpleRequest* request)
          {
              [request setUrl:url];
              [request setParameters:parameters];
              
          }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
         {
             
             //Fetching resposne
             NSData *rawBody = response.rawBody;
             NSString *strResponse = [[NSString alloc] initWithData:rawBody encoding:NSUTF8StringEncoding];
             NSLog(@"Response %@",strResponse);
             
             dicResponse=response.body.object;
             
             if (dicResponse==nil)
             {
                 dicResponse=[response.body.array objectAtIndex:0];
             }
             
             [self uploadProfilePicAPIResponse:dicResponse];
             
         }];
    }
}

-(void)uploadProfilePicAPIResponse:(NSDictionary*)response
{
    //Calling main thread
    dispatch_async(dispatch_get_main_queue(), ^()
                   {
                       //All UI related change can be done here.
                       [MBProgressHUD hideHUDForView:self.view animated:YES];
                       
                       NSLog(@"Upload Profile pic : %@",response);
                       
                       if (response!=nil && [response objectForKey:@"status"] && [[response objectForKey:@"status"] intValue]==1)
                       {
                           Parser *parser=[[Parser alloc]init];
                           App_Delegate.userObject.aryPhotos=[parser getPhotoArray:[response objectForKey:@"images"]];
                           [collectionViewTable reloadData];
                           [self clearSubviewData];
                       }
                       else
                       {
                           [App_Delegate showAlertWithMessage:@"Photo cannot be uploaded at this time" inViewController:self];
                       }
                       
                   });
}

-(void)callDeleteAPI
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KDeletePhotosAPI];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    NSString *strImageIDs=[arySelectedPhotoId componentsJoinedByString:@","];
    NSLog(@"Image ids selected for deletion is : %@",strImageIDs);
    [dicParameter setObject:strImageIDs forKey:@"image_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strUrl andHeader:dicHeader andParameters:dicParameter Selector:@selector(deleteAPIResponse:) WithCallBackObject:self];
}

-(void)deleteAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        [App_Delegate internalLogin];
        arySelectedPhotoId=[[NSMutableArray alloc]init];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Password cannot be update now, Please try again later" inViewController:self];
        }
    }
    
    
    [collectionViewTable reloadData];
    
}



#pragma mark- Button Action

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnBackPressed:(id)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCollectionView" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnAddPhotoPressed:(id)sender
{
    if (btnAddDeletePhotos.selected)
    {
        
        NSString *strMessage=@"Are you sure you want to delete the photo?";
        
        if (arySelectedPhotoId.count>1)
        {
            strMessage=@"Are you sure you want to delete photos?";
        }
        
        SCLAlertView *alert =[App_Delegate getAlertInstance];
        
        [alert addButton:@"No" actionBlock:^(void) {
            
        }];
        
        [alert addButton:@"Yes" actionBlock:^(void) {
            
            [self callDeleteAPI];
            
        }];
        
        [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:strMessage closeButtonTitle:nil duration:0.0];
    }
    else
    {
        UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:@"Select Picture" delegate:(id)self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Gallery",@"Camera", nil];
        
        [actionSheet showInView:self.view];
    }
}

-(void)clearSubviewData
{
    [imgSelectedImage setImage:nil];
    [txtViewCaption setText:@""];
    [viewAddImage setHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

-(void)addSubView
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [imgSelectedImage setImage:chosenImage];
    [viewAddImage setHidden:NO];
    [self.view bringSubviewToFront:viewAddImage];
}

-(IBAction)btnCloseCurrentSelectedImagePressed:(id)sender
{
    [self clearSubviewData];
}

-(IBAction)btnAddCurrentSelectedImagePressed:(id)sender
{
    [self uploadImageUnirest];
}

#pragma mark- AlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1012)
    {
        if (buttonIndex==1)
        {
            [self callDeleteAPI];
        }
        else
        {
             arySelectedPhotoId=[[NSMutableArray alloc]init];
        }
    }
}


#pragma mark- Actionsheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        //Gallery
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = (id)self;
        picker.view.tag=101;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    else if (buttonIndex==1)
    {
        //Camera
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate =(id)self;
        picker.view.tag=101;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
}

#pragma mark- UIImagePicker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    if (picker.view.tag==101)
    {
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        //Select image from gallery
        chosenImage = info[UIImagePickerControllerEditedImage];
        
        NSString *fileImagePath=[NSString stringWithFormat:@"%@/Documents/profilepic.jpeg",NSHomeDirectory()];
        
        NSData *data = UIImageJPEGRepresentation(chosenImage, 0.8);
        
        NSError *error;
        
        [data writeToFile:fileImagePath options:NSDataWritingAtomic error:&error];
        
        if(error != nil)
            NSLog(@"write error %@", error);
        
        //Do something here
        [self addSubView];
    }
}

#pragma mark - collection view delegates
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return App_Delegate.userObject.aryPhotos.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Adjust cell size
    float widthOfView=[UIScreen mainScreen].bounds.size.width;
    widthOfView=widthOfView-5-5-5; //10+10+10
    
    float cellWidth=widthOfView/2;
    float cellHeight=cellWidth;
    
    NSLog(@"Width of cell: %f",cellWidth);
    NSLog(@"Height of cell: %f",cellHeight);
    
    return CGSizeMake(cellWidth,cellHeight);
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //1
    static NSString *identifier = @"CellPhoto";
    CellPhoto *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    PhotoObject *photoObject=[App_Delegate.userObject.aryPhotos objectAtIndex:indexPath.row];
    [cell.lblCaption setText:photoObject.strCaption];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:photoObject.strImageURL]];
    
    if ([arySelectedPhotoId containsObject:photoObject.strId])
    {
        [cell.imgViewSelected setHidden:NO];
    }
    else
    {
        [cell.imgViewSelected setHidden:YES];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoObject *photoObject=[App_Delegate.userObject.aryPhotos objectAtIndex:indexPath.row];
    
    if (photoObject.strId==nil || photoObject.strId==(id)[NSNull null] || photoObject.strId.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Server error id of photo not coming." inViewController:self];
        return;
    }
    
    if ([arySelectedPhotoId containsObject:photoObject.strId])
    {
        [arySelectedPhotoId removeObject:photoObject.strId];
    }
    else
    {
        [arySelectedPhotoId addObject:photoObject.strId];
    }
    
    [self refreshTable];
    
}

@end
