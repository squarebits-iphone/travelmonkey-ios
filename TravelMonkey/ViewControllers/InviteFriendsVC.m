//
//  InviteFriendsVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/12/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "InviteFriendsVC.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "AboutUsVC.h"
#import "AboutUsCell.h"
#import <TwitterKit/TwitterKit.h>
#import <Crashlytics/Crashlytics.h>
#import <PinterestSDK/PinterestSDK.h>
#import <PinterestSDK/PDKPin.h>




#define KName @"name"
#define KImage @"image"
#define KSelectedImage @"imageSelected"

@interface InviteFriendsVC ()

@end

@implementation InviteFriendsVC

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewStatusBar setBackgroundColor:KAppColor];
    [viewTitle setBackgroundColor:KAppColor];
    
    aryOptions=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Pinterest" forKey:KName];
    [dic setObject:@"icn_pintrest" forKey:KImage];
    [dic setObject:@"icn_pintrest_touch" forKey:KSelectedImage];
    [aryOptions addObject:dic];
    
    dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Facebook" forKey:KName];
    [dic setObject:@"icn_facebook" forKey:KImage];
    [dic setObject:@"icn_facebook_touch" forKey:KSelectedImage];
    [aryOptions addObject:dic];
    
    dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Instagram" forKey:KName];
    [dic setObject:@"icn_instagram" forKey:KImage];
    [dic setObject:@"icn_instagram_touch" forKey:KSelectedImage];
    [aryOptions addObject:dic];
    
    dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Twitter" forKey:KName];
    [dic setObject:@"icn_twitter" forKey:KImage];
    [dic setObject:@"icn_twitter_touch" forKey:KSelectedImage];
    [aryOptions addObject:dic];
    
    dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Email" forKey:KName];
    [dic setObject:@"icn_email_Invite" forKey:KImage];
    [dic setObject:@"icn_email_Invite_touch" forKey:KSelectedImage];
    [aryOptions addObject:dic];
    
    dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Text" forKey:KName];
    [dic setObject:@"icn_text" forKey:KImage];
    [dic setObject:@"icn_text_touch" forKey:KSelectedImage];
    [aryOptions addObject:dic];
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- Methods

-(void)ShareOnTwitter
{
    
    // Objective-C
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    
    [composer setText:[NSString stringWithFormat:@"%@%@",KSharingText,KAppStoreURL]];
    
    // Called from a UIViewController
    [composer showFromViewController:self completion:^(TWTRComposerResult result)
     {
         if (result == TWTRComposerResultCancelled) {
             NSLog(@"Tweet composition cancelled");
         }
         else {
             NSLog(@"Sending Tweet!");
         }
     }];
    
}

-(void)ShareOnFacebook
{
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:KAppStoreURL];
    content.contentTitle=KAppName;
    content.contentDescription=KSharingText;
    [FBSDKShareDialog showFromViewController:self
                                 withContent:content
                                    delegate:nil];
}

-(void)shareOnEmail
{
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = (id)self;
    [mailComposer setSubject:KAppName];
    [mailComposer setMessageBody:[NSString stringWithFormat:@"%@%@",KSharingText,KAppStoreURL] isHTML:NO];
    [self presentViewController:mailComposer animated:YES completion:nil];

}


-(void)ShareOnSMS
{
    MFMessageComposeViewController *controller =[[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body =[NSString stringWithFormat:@"%@%@",KSharingText,KAppStoreURL];
        controller.messageComposeDelegate =(id)self;
        [self presentViewController:controller animated:YES completion:nil];
        
    }
}

-(void)shareOnPinterest
{
    //__weak InviteFriendsVC *weakSelf = self;
    
    [PDKPin pinWithImageURL:[NSURL URLWithString:KSharingImageURL] link:[NSURL URLWithString:KAppStoreURL] suggestedBoardName:KSharingText note:@"Travel Monkey" withSuccess:^{
        
        [App_Delegate showCustomAlert:@"Successfully Pinned." isSucess:YES];
        
        //weakSelf.resultLabel.text = [NSString stringWithFormat:@"successfully pinned pin"];
        
    } andFailure:^(NSError *error) {
        //weakSelf.resultLabel.text = @"pin it failed";
        
        [App_Delegate showTMAlertWithMessage:@"Pin Failed!" andTitle:@"Oops!" doneButtonTitle:@"Okay" inViewController:self];
        
    }];

}

-(void)shareOnInstagram
{
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
    {
        NSData *imageData = UIImagePNGRepresentation([UIImage imageNamed:@"ShareImage"]); //convert image into .png format.
        NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
        NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
        NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"insta.igo"]]; //add our image to the path
        [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
        NSLog(@"image saved");
        
        CGRect rect = CGRectMake(0 ,0 , 0, 0);
        UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIGraphicsEndImageContext();
        NSString *fileNameToSave = [NSString stringWithFormat:@"Documents/insta.igo"];
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:fileNameToSave];
        NSLog(@"jpg path %@",jpgPath);
        NSString *newJpgPath = [NSString stringWithFormat:@"file://%@",jpgPath];
        NSLog(@"with File path %@",newJpgPath);
        NSURL *igImageHookFile = [[NSURL alloc]initFileURLWithPath:newJpgPath];
        NSLog(@"url Path %@",igImageHookFile);
        
        documentController.UTI = @"com.instagram.exclusivegram";
        documentController = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
        documentController=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
        NSString *caption = KSharingText; //settext as Default Caption
        documentController.annotation=[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",caption],@"InstagramCaption", nil];
        [documentController presentOpenInMenuFromRect:rect inView: self.view animated:YES];
    }
    else
    {
        NSLog (@"Instagram not found");
        [App_Delegate showAlertWithMessage:@"Please install instagram in oder to invite your instagram friends" inViewController:self];
    }
}

- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate
{
    NSLog(@"file url %@",fileURL);
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    
    return interactionController;
}

#pragma mark- Mail Delegate

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (result)
    {
        NSLog(@"Result : %d",result);
    }
    
    if (error)
    {
        NSLog(@"Error : %@",error);
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark- Message Composer delegates

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
            NSLog(@"Cancelled");
            break;
        case MessageComposeResultFailed:
            NSLog(@"Failed");
            break;
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



#pragma mark- Button Action

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

#pragma mark- Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return aryOptions.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strCellIdentifier=@"AboutUsCell";
    
    AboutUsCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
    
    if (cell==nil)
    {
        cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NSMutableDictionary *dic=[aryOptions objectAtIndex:indexPath.row];
    
    [cell.lblTitle setText:[dic objectForKey:KName]];
    [cell.imgIcon setImage:[UIImage imageNamed:[dic objectForKey:KImage]]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AboutUsCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    [cell.lblTitle setTextColor:[UIColor whiteColor]];
    [cell.imgViewBorder setImage:[UIImage imageNamed:@"cell_contactus_touch.png"]];
    [cell.imgViewArrow setImage:[UIImage imageNamed:@"icn_arrow_contactus_touch.png"]];
    NSMutableDictionary *dic=[aryOptions objectAtIndex:indexPath.row];
    [cell.imgIcon setImage:[UIImage imageNamed:[dic objectForKey:KSelectedImage]]];
    
    switch (indexPath.row)
    {
        case 0:
        {
            //Pinterest
            [self shareOnPinterest];
            break;
        }
            
        case 1:
        {
            //Facebook
            [self ShareOnFacebook];
            break;
        }
            
        case 2:
        {
            //Instagram
            [self shareOnInstagram];
            break;
        }
            
        case 3:
        {
            //Twitter
            [self ShareOnTwitter];
            break;
        }
            
        case 4:
        {
            //Email
            [self shareOnEmail];
            break;
        }
        
        case 5:
        {
            //Text
            [self ShareOnSMS];
            break;
        }
            
        default:
            break;
    }
    
    [tableView performSelector:@selector(reloadData) withObject:nil afterDelay:1.0];
}
































@end
