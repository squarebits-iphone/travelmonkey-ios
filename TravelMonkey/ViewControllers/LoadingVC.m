//
//  LoadingVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/23/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "LoadingVC.h"
#import "Parser.h"
#import "LoginVC.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>


@interface LoadingVC ()

@end

@implementation LoadingVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    counter=0;
}

-(void)viewDidAppear:(BOOL)animated
{
    
    // grab a local URL to our video
    NSURL *videoURL = [[NSBundle mainBundle]URLForResource:@"Splash" withExtension:@"mp4"];
    
    // create an AVPlayer
    AVPlayer *player = [AVPlayer playerWithURL:videoURL];
    
    // create a player view controller
    AVPlayerViewController *controller = [[AVPlayerViewController alloc]init];
    controller.player = player;
    controller.showsPlaybackControls=NO;
    player.muted=YES;
    [player play];
    
    // show the view controller
    [self addChildViewController:controller];
    [self.view addSubview:controller.view];
    controller.view.frame = self.view.frame;
    [controller.view setBackgroundColor:[UIColor whiteColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying) name:AVPlayerItemDidPlayToEndTimeNotification object:player.currentItem];
    
    NSDictionary *userDetail=[[NSUserDefaults standardUserDefaults]objectForKey:Default_SaveUserObject];
    
    Parser *parser=[[Parser alloc]init];
    App_Delegate.userObject=[parser getUserDetail:userDetail];
    
    NSLog(@"User login from : %@",App_Delegate.userObject.strSite);
    
    if ([App_Delegate.userObject.strSite isEqualToString:@"NATIVE"])
    {
        [self callLogin];
    }
    else
    {
        [self callSocialLogin];
    }
    
    timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(checkAndGo) userInfo:nil repeats:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated
{
    [timer invalidate];
    timer=nil;
}

#pragma mark- Methods

-(void)itemDidFinishPlaying
{
    counter++;
}

-(void)checkAndGo
{
    NSLog(@"Value of Counter : %d",counter);
    if (counter==2)
    {
        [timer invalidate];
        timer=nil;
        [self goToHomePage];
    }
}

-(void)callLogin
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KNativeLogin];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strEmail forKey:@"email"];
    [dicParameter setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"password"] forKey:@"password"];
    
    [APIsList postAPIWithURL:strUrl andParameters:dicParameter Selector:@selector(loginAPIResponse:) WithCallBackObject:self];
}

-(void)callSocialLogin
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KSocialLogin];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strEmail forKey:@"email"];
    [dicParameter setObject:App_Delegate.userObject.strSite forKey:@"site"];
    
    [APIsList postAPIWithURL:strUrl andParameters:dicParameter Selector:@selector(loginAPIResponse:) WithCallBackObject:self];
}

-(void)loginAPIResponse:(NSDictionary*)dictionary
{
        
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        Parser *parser=[[Parser alloc]init];
        App_Delegate.userObject=[parser getUserDetail:dictionary];
        
        if ([App_Delegate.userObject.strUserType isEqualToString:@"TRAVELER"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserTourist forKey:Default_UserType];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserGuide forKey:Default_UserType];
        }
        
        [[NSUserDefaults standardUserDefaults]setObject:dictionary forKey:Default_SaveUserObject];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        counter++;
    }
    
    if (!isSuccess)
    {
        [self LogOutUser];
    }
}

-(void)goToHomePage
{
    [[App_Delegate window]setRootViewController:[App_Delegate setupDeckViewController]];
}

-(void)LogOutUser
{
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    LoginVC *viewController=[[LoginVC alloc]init];
    App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
    [App_Delegate.navigationController setNavigationBarHidden:YES];
    [App_Delegate.window setRootViewController:App_Delegate.navigationController];
}

@end
