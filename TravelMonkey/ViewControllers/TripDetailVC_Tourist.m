//
//  TripDetailVC_Tourist.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/19/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "TripDetailVC_Tourist.h"
#import "UIImageView+WebCache.h"
#import "Parser.h"

@interface TripDetailVC_Tourist ()

@end

@implementation TripDetailVC_Tourist
@synthesize strTripID;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    [self.view addSubview:scrlView];
    [scrlView setHidden:YES];
    [btnNegative setSelected:NO];
    [btnPositive setSelected:NO];
    [self callTripDetailByTraveller];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    
    if (shouldHideReviewArea)
    {
        [scrlView setFrame:CGRectMake(0,140,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-140-btnSubmitReview.frame.size.height)];
        
        [constraintHeight setConstant:lblReviewPlaceHolder.frame.origin.y+lblReviewPlaceHolder.frame.size.height+50];
        [scrlView setContentSize:CGSizeMake(0,lblReviewPlaceHolder.frame.origin.y+lblReviewPlaceHolder.frame.size.height+50)];
    }
    else
    {
        [scrlView setFrame:CGRectMake(0,140,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-140-btnSubmitReview.frame.size.height)];
        [scrlView setContentSize:CGSizeMake(0,txtViewComment.frame.origin.y+txtViewComment.frame.size.height+34)];
        [constraintHeight setConstant:txtViewComment.frame.origin.y+txtViewComment.frame.size.height+34];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

#pragma mark- Methods

-(void)setupView
{
    
    //Setting prfile image
    [imgViewProfile setClipsToBounds:YES];
    [imgViewProfile.layer setCornerRadius:125/2];
    [imgViewProfile sd_setImageWithURL:[NSURL URLWithString:objectTripDetail.strGuideImageURL]];
    
    NSString *strName=objectTripDetail.strGuideName;
    NSArray *aryName=[objectTripDetail.strGuideName componentsSeparatedByString:@" "];
    if (aryName && aryName.count>0)
    {
        strName=[aryName firstObject];
    }
    
    [lblName setText:strName];
    
    [lblPlace setText:[NSString stringWithFormat:@"%@, %@",objectTripDetail.strGuideCity,objectTripDetail.strGuideCountry]];
    
    //Setting dates
    NSDateFormatter *formatter=[NSDateFormatter new];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];

    NSDate *startTime=[formatter dateFromString:objectTripDetail.strStartTime];
    NSDate *endTime=[formatter dateFromString:objectTripDetail.strEndTime];
    
    startTime=[startTime dateByAddingTimeInterval:timeZoneSeconds];
    endTime=[endTime dateByAddingTimeInterval:timeZoneSeconds];
    
    [formatter setDateFormat:@"MMM dd, yyyy"];
    [lblTripDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
    
    [formatter setDateFormat:@"hh:mm a"];
    [lblTripStartTime setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
    [lblTripEndTime setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:endTime]]];
    
    
    NSString *strTotalPrice=[NSString stringWithFormat:@"Total Cost $%@",objectTripDetail.strPrice];
    
    //Attributed Text
    if ([lblTotalCost respondsToSelector:@selector(setAttributedText:)]) {
        
        // Define general attributes for the entire text
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName: KAppColor_Blue,
                                  NSFontAttributeName: lblTotalCost.font
                                  };
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:strTotalPrice
                                               attributes:attribs];
        
        //Blue text attributes
        NSRange redTextRange = [strTotalPrice rangeOfString:@"Total Cost"];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
        [attributedText setAttributes:@{NSForegroundColorAttributeName:KAppColor_GrayBlue}
                                range:redTextRange];
        
        lblTotalCost.attributedText = attributedText;
    }
    else
    {
        lblTotalCost.text =strTotalPrice;
    }
    
    [lblPaidWith setText:[NSString stringWithFormat:@"Payed with card ending in %@",objectTripDetail.strCardDetails]];
    
    
    //Total Duration
    float totalMinutes=[App_Delegate getMinutesDifferenceBetweenStartDate:objectTripDetail.strStartTime endDate:objectTripDetail.strEndTime withFormaterStyle:@"YYYY-MM-dd HH:mm:ss"];
    
        
    int hour=totalMinutes/60;
    int minute=(int)totalMinutes%60;
    
    if (minute>0)
    {
        if (hour==1)
        {
            [lblTripTotalTime setText:[NSString stringWithFormat:@"%d hour %d minutes of booking",hour,minute]];
        }
        else
        {
            [lblTripTotalTime setText:[NSString stringWithFormat:@"%d hours %d minutes of booking",hour,minute]];
        }
    }
    else
    {
        if (hour==1)
        {
            [lblTripTotalTime setText:[NSString stringWithFormat:@"%d hour of booking",hour]];
        }
        else
        {
            [lblTripTotalTime setText:[NSString stringWithFormat:@"%d hours of booking",hour]];
        }
    }
    
    
    shouldHideReviewArea=objectTripDetail.isReviewedByTraveller;
    
    if (shouldHideReviewArea)
    {
        [self hideReviewOption];
    }
    
}

-(void)hideReviewOption
{
    
    [lblReviewPlaceHolder setText:@"Thank you for the review."];
    [lblNegativePlaceHolder setHidden:YES];
    [lblPositivePlaceHolder setHidden:YES];
    [lblOptionalCommentPlaceHolder setHidden:YES];
    [imgViewCommentBackground setHidden:YES];
    [btnPositive setHidden:YES];
    [btnNegative setHidden:YES];
    [btnSubmitReview setHidden:YES];
    [txtViewComment setHidden:YES];
}

-(void)callTripDetailByTraveller
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KGetTripDetail];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strTripID forKey:@"booking_id"];
    [dicParameter setObject:@"TRAVELER" forKey:@"role"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(tripDetailResponse:) WithCallBackObject:self];
}

-(void)tripDetailResponse:(NSDictionary*)dictionary
{
    [scrlView setHidden:NO];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        
        if ([dictionary objectForKey:@"booking"])
        {
            isSuccess=YES;
            Parser *parser=[[Parser alloc]init];
            objectTripDetail=[parser getTripDetails:[dictionary objectForKey:@"booking"]];
            [self setupView];
        }
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Trip details not found. Please try again later" inViewController:self];
        }
    }
}

-(void)callReviewGuideAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KPostReview];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strTripID forKey:@"booking_id"];
    [dicParameter setObject:@"TRAVELER" forKey:@"role"];
    
    if (btnPositive.selected)
    {
      [dicParameter setObject:@"LIKE" forKey:@"review"];
    }
    else
    {
      [dicParameter setObject:@"UNLIKE" forKey:@"review"];
    }
    
    if (txtViewComment.text.length>0)
    {
        [dicParameter setObject:txtViewComment.text forKey:@"comment"];
    }
    else
    {
        [dicParameter setObject:@"" forKey:@"comment"];
    }
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(reviewResponse:) WithCallBackObject:self];
}

-(void)reviewResponse:(NSMutableDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        [App_Delegate showAlertWithMessage:@"Thank you for your feedback" inViewController:self];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Please try again later" inViewController:self];
        }
    }
    
    [self callTripDetailByTraveller];
}

#pragma mark- Button Action

-(IBAction)btnSubmitReviewForGuide:(id)sender
{
    if (btnPositive.selected || btnNegative.selected)
    {
        [self callReviewGuideAPI];
    }
    else
    {
        [App_Delegate showAlertWithMessage:@"Please select Like or Dislike button above with some feedback text to review. Thank you" inViewController:self];
    }
    
}

-(IBAction)btnPositiveNegativePressed:(UIButton *)senderButton
{
    [btnNegative setSelected:NO];
    [btnPositive setSelected:NO];
    [senderButton setSelected:YES];
}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
