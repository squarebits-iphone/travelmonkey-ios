//
//  PageView_PhotosVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/12/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageView_PhotosVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
{
    IBOutlet UICollectionView *collectionViewTable;
    UserObject *userObject;
    
}

@property (nonatomic,strong) UserObject *guideObject;
@property BOOL isForViewProfile;

@end
