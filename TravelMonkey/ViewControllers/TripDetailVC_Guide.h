//
//  TripDetailVC_Guide.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/20/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripDetails.h"

@interface TripDetailVC_Guide : UIViewController
{
    
    BOOL shouldHideReviewArea;
    
    //Outlets
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    
    IBOutlet UIScrollView *scrlView;
    IBOutlet UIView *viewContent;
    
    
    IBOutlet UIImageView *imgViewProfile;
    IBOutlet UIImageView *imgViewCommentBackground;
    IBOutlet UILabel *lblName;
    IBOutlet UILabel *lblPlace;
    IBOutlet UILabel *lblTripDate;
    IBOutlet UILabel *lblTripStartTime;
    IBOutlet UILabel *lblTripEndTime;
    IBOutlet UILabel *lblTotalCost;
    IBOutlet UILabel *lblTripDeduction;
    IBOutlet UILabel *lblTripEarning;
    IBOutlet UIButton *btnNegative;
    IBOutlet UIButton *btnPositive;
    IBOutlet UITextView *txtViewComment;
    IBOutlet UIButton *btnSubmitReview;
    
    IBOutlet UILabel *lblReviewPlaceHolder;
    IBOutlet UILabel *lblNegativePlaceHolder;
    IBOutlet UILabel *lblPositivePlaceHolder;
    IBOutlet UILabel *lblOptionalCommentPlaceHolder;
    
    
    
    __weak IBOutlet NSLayoutConstraint *constraintHeight;
    TripDetails *objectTripDetail;
}

@property (nonatomic,strong) NSString *strTripID;

@end
