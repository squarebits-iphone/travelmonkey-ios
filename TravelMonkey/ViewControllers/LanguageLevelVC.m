//
//  LanguageLevelVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/16/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "LanguageLevelVC.h"
#import "PSelectionCell.h"
#import "PSelectionView.h"
#import "LanguageObject.h"


#define  KBasic @"Basic"
#define  KIntermediate @"Intermediate"
#define  KFluent @"Fluent"


@interface LanguageLevelVC ()

@end

@implementation LanguageLevelVC
@synthesize aryLanguageData;


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //aryLanguageLevel=[[NSMutableArray alloc]initWithObjects:KFluent,KIntermediate,KBasic, nil];
    aryLanguageLevel=[[NSMutableArray alloc]initWithObjects:KFluent,nil];
    
    [viewStatusBar setBackgroundColor:KAppColor];
    [viewTitle setBackgroundColor:KAppColor];
    
    for (LanguageObject *object in App_Delegate.userObject.locatDetailObject.aryLanguage)
    {
        NSLog(@"Selected Language %@ and level %@",object.strLanguageName,object.strLanguageLevel);
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- PSelectionDelegate

-(void)userSelectedArray:(NSMutableArray*)arySelectedData
{
    
    NSMutableArray *aryTemp=[[NSMutableArray alloc]init];
    
    //Remove the already added language in the current categaory
    for (LanguageObject *object in App_Delegate.userObject.locatDetailObject.aryLanguage)
    {
        if ([object.strLanguageLevel isEqualToString:strCurrentLevel])
        {
            //Leave them
            NSLog(@"Selected Language %@ and level %@",object.strLanguageName,object.strLanguageLevel);
        }
        else
        {
            [aryTemp addObject:object];
        }
    }
    
    App_Delegate.userObject.locatDetailObject.aryLanguage=aryTemp;
    
    
    for (NSString *strLanguage in arySelectedData)
    {
        LanguageObject *langObject=[[LanguageObject alloc]init];
        langObject.strLanguageName=[NSString stringWithString:strLanguage];
        langObject.strLanguageLevel=strCurrentLevel;
        [App_Delegate.userObject.locatDetailObject.aryLanguage addObject:langObject];
    }
    
    for (LanguageObject *object in App_Delegate.userObject.locatDetailObject.aryLanguage)
    {
        NSLog(@"Selected Language %@ and level %@",object.strLanguageName,object.strLanguageLevel);
    }
}

#pragma mark- Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return aryLanguageLevel.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *strCellIdentifier=@"PSelectionCell";
    
    PSelectionCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
    
    if (cell==nil)
    {
        cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    [cell.imgSelection setHidden:NO];
    [cell.imgSelection setImage:[UIImage imageNamed:@"btn_arrow1.png"]];
    cell.constraint_ImgSelection_width.constant=20;
    cell.constraint_ImgSelection_height.constant=11;
    
    if ([[aryLanguageLevel objectAtIndex:indexPath.row] isEqualToString:KFluent])
    {
        [cell.labelName setText:@"Languages I am fluent"];
    }
    else if ([[aryLanguageLevel objectAtIndex:indexPath.row] isEqualToString:KIntermediate])
    {
        [cell.labelName setText:@"Languages I am Intermediate"];
    }
    else
    {
        [cell.labelName setText:@"Languages I am basic"];
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    strCurrentLevel=[aryLanguageLevel objectAtIndex:indexPath.row];
    
    PSelectionView *selectionView=[[PSelectionView alloc]init];
    selectionView.pageTitle=@"Select Language";
    selectionView.arrayData=aryLanguageData;
    
    NSMutableArray *aryFilteredLanguage=[[NSMutableArray alloc]init];
    
    for (LanguageObject *object in App_Delegate.userObject.locatDetailObject.aryLanguage)
    {
        if ([object.strLanguageLevel isEqualToString:[aryLanguageLevel objectAtIndex:indexPath.row]])
        {
            [aryFilteredLanguage addObject:object.strLanguageName];
        }
    }
    
    selectionView.arraySelectedData=aryFilteredLanguage;
    selectionView.viewColor=KAppColor;
    selectionView.delegate=(id)self;
    [self presentViewController:selectionView animated:YES completion:nil];
}


@end
