//
//  HomeVC_Guide.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"
#import "UserObject.h"
#import "PCustomScrollView.h"



@interface HomeVC_Guide : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UIView *imageContainer;

    IBOutlet UIScrollView *scrollViewMain;
    
    IBOutlet UILabel *lblLocalName;
    IBOutlet UILabel *lblLocalLocation;
    IBOutlet UILabel *lblPrice;
    IBOutlet UILabel *lblGender;
    IBOutlet UILabel *lblAge;
    IBOutlet UILabel *lblPerHour;
    
    
    IBOutlet UIButton *btnBack;
    IBOutlet UIButton *btnReportUser;
    
    IBOutlet UIImageView *imgUserProfie;
    CGRect frame;
    
    IBOutlet UIView *viewButtonBackground;
    IBOutlet UIButton *btnBookMe;
    IBOutlet NSLayoutConstraint *constScrollBottomSpace;
    
}

@property (nonatomic,strong) UserObject *guideObject;
@property BOOL isForViewProfile;

@end
