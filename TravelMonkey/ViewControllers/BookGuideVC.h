//
//  BookGuideVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/26/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserObject.h"
#import "CreditCard.h"


@import Stripe;


@interface BookGuideVC : UIViewController
{
    
    //Outlets
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UILabel *lblTitle;
    IBOutlet UIScrollView *scrollviewMain;
    IBOutlet UIButton *btnSendBookingRequest;
    IBOutlet UILabel *lblLocalLocation;
    
    IBOutlet UILabel *lblLastTemp; 
    
    __weak IBOutlet UILabel *lblDate;
    __weak IBOutlet UILabel *lblStartTime;
    __weak IBOutlet UILabel *lblEndTime;
    __weak IBOutlet UILabel *lblNumberOfPerson;
    __weak IBOutlet UILabel *lblTotalCost;
    __weak IBOutlet UILabel *lblTotalBookingTime;
    __weak IBOutlet UILabel *lblVisaCardInfo;
    __weak IBOutlet UIButton *btnMinus;
    __weak IBOutlet UIButton *btnPlus;
    
    //Constraints
     __weak IBOutlet NSLayoutConstraint *constraintTableHeight;
    
    float totalPrice;
    
    CreditCard *currentCard;
    
    
}

@property (nonatomic,strong) UserObject *guideObject;
@property (nonatomic,strong) NSString *guidePrice;

- (IBAction)btnMinusPressed:(id)sender;
- (IBAction)btnPlusPressed:(id)sender;
- (IBAction)btnSendBookingRequestPressed:(id)sender;


@end
