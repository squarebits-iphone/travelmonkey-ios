//
//  GuideBankDetailVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/14/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "GuideBankDetailVC.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "UNIRest.h"
#import "Reachability.h"


@interface GuideBankDetailVC ()

@end

@implementation GuideBankDetailVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    strIP=[self getIPAddress];
    
    if (strIP==nil || strIP==(id)[NSNull null] || strIP.length==0)
    {
        strIP=@"Please connect wifi as IP-Adress is not detected.";
    }
    
    [lblIPAddress setText:[NSString stringWithFormat:@"IP Address : %@",strIP]];
    
    [viewVerify setHidden:YES];
    [scrollviewMain setHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

-(void)viewDidLayoutSubviews
{
    [scrollviewMain setContentSize:CGSizeMake(0,txtFieldBirthDate.frame.origin.y+txtFieldBirthDate.frame.size.height+40)];
    [constraintHeight setConstant:txtFieldBirthDate.frame.origin.y+txtFieldBirthDate.frame.size.height+40];
    
    //Need to put condition here ---- //Temp
    if (App_Delegate.userObject.locatDetailObject.guideBankDetailObject!=nil && App_Delegate.userObject.locatDetailObject.guideBankDetailObject!=(id)[NSNull null] && App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus!=nil && App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus!=(id)[NSNull null] && App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus.length>0)
    {
        
        if ([App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus isEqualToString:@"verified"])
        {
            
        }
        else if ([App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus isEqualToString:@"success"])
        {
            //Bank detail verification
            [viewVerify setHidden:NO];
            [scrollviewMain setHidden:YES];
            [txtFieldAccountNumberVerify setText:[NSString stringWithFormat:@"********%@",App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strLastDigit]];
            [btnSave setHidden:YES];
            [txtFieldNameVerify setText:App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strBankName];
             [lblHeaderTitle setText:@"Please upload your bank document photo to verify."];
            
        }
        else
        {
            [lblHeaderTitle setText:@"Where do you want your earnings deposit?"];
        }
    }
    else
    {
        
    }

    
}

#pragma mark- Methods

- (NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                
                 address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                
                // Check if interface is en0 which is the wifi connection on the iPhone
//                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
//                    // Get NSString from C String
//                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
//                    
//                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

-(BOOL)isValidationCorrect
{
    
    for (UITextField *txtField in viewScrollContent.subviews)
    {
        if ([txtField isKindOfClass:[UITextField class]])
        {
            if (txtField.text.length==0)
            {
                [App_Delegate showAlertWithMessage:[NSString stringWithFormat:@"Please enter %@",txtField.placeholder] inViewController:self];
                return NO;
            }
        }
    }
    
    return YES;
}

-(void)saveBankDetailAPI
{
    
    NSString *timestamp =[NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KAddGuideBankDetail];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:txtFieldAccountNumber.text forKey:@"account_number"];
    [dicParameter setObject:txtFieldAddress.text forKey:@"address"];
    [dicParameter setObject:txtFieldCity.text forKey:@"city"];
    [dicParameter setObject:txtFieldState.text forKey:@"state"];
    [dicParameter setObject:timestamp forKey:@"date"];
    [dicParameter setObject:txtFieldBirthDate.text forKey:@"day"];
    [dicParameter setObject:txtFieldFirstName.text forKey:@"first_name"];
    [dicParameter setObject:strIP forKey:@"ip"];
    [dicParameter setObject:txtFieldLastName.text forKey:@"last_name"];
    [dicParameter setObject:txtFieldBirthMonth.text forKey:@"month"];
    [dicParameter setObject:txtFieldPostalCode.text forKey:@"postal_code"];
    [dicParameter setObject:txtFieldRouting.text forKey:@"routing_number"];
    [dicParameter setObject:txtFieldSSN.text forKey:@"ssn"];
    [dicParameter setObject:@"individual" forKey:@"type"];
    [dicParameter setObject:txtFieldBirthYear.text forKey:@"year"];
    [dicParameter setObject:txtFieldPersonalID.text forKey:@"personal_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(getResponse:) WithCallBackObject:self];
}

-(void)getResponse:(NSDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self performSelector:@selector(loadAppAgain) withObject:nil afterDelay:10.0];
        //[App_Delegate internalLogin];
        //[App_Delegate showAlertWithMessage:@"Bank details added successfully" inViewController:self];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Please try again, Server not responding" inViewController:self];
        }
    }

}

-(void)loadAppAgain
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [App_Delegate application:[UIApplication sharedApplication] didFinishLaunchingWithOptions:nil];
}

-(void)openPhotoLibrary
{
    BOOL op=1;
    if(op)
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.allowsEditing=NO;
        
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self presentViewController:imagePicker animated:YES completion:nil];
        });
    }
}

-(void)openCamera
{
    BOOL op=1;
    
    if(op)
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing=NO;
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self presentViewController:imagePicker animated:YES completion:nil];
        });
    }
}

-(void)uploadImageUnirest //This will upload image to the
{
    
    //Image path
    NSString *fileImagePath=[NSString stringWithFormat:@"%@/Documents/profilepic.jpeg",NSHomeDirectory()];
    
    if ([[NSFileManager defaultManager]fileExistsAtPath:fileImagePath]==NO)
    {
        NSLog(@"File not found");
        return;
    }
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    //Url
    NSString *url=[NSString stringWithFormat:@"%@%@",KBaseURL,KGuideBankUploadDoc];
    url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //Check network
    if (networkStatus==NotReachable)
    {
        [App_Delegate showAlertWithMessage:@"No internet connection." inViewController:self];
    }
    else
    {
        
        //Show loading
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        __block NSDictionary *dicResponse;
        
        //Content-Type: application/json
        NSMutableDictionary *headers =[[NSMutableDictionary alloc]init];
        [headers setObject:@"application/json" forKey:@"Content-Type"];
        [headers setObject:@"utf-8" forKey:@"Accept-Encoding"];
        
        NSDictionary* parameters = @{@"user_id" :App_Delegate.userObject.strId, @"doc": [NSURL fileURLWithPath:fileImagePath]};
        
        //Initializing Async request
        [[UNIRest post:^(UNISimpleRequest* request)
          {
              [request setUrl:url];
              [request setParameters:parameters];
              
          }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
         {
             
             //Fetching resposne
             NSData *rawBody = response.rawBody;
             NSString *strResponse = [[NSString alloc] initWithData:rawBody encoding:NSUTF8StringEncoding];
             NSLog(@"Response %@",strResponse);
             
             dicResponse=response.body.object;
             
             if (dicResponse==nil)
             {
                 dicResponse=[response.body.array objectAtIndex:0];
             }
             
             [self uploadProfilePicAPIResponse:dicResponse];
             
         }];
    }
}

-(void)uploadProfilePicAPIResponse:(NSDictionary*)response
{
    //Calling main thread
    dispatch_async(dispatch_get_main_queue(), ^()
                   {
                       //All UI related change can be done here.
                       [MBProgressHUD hideHUDForView:self.view animated:YES];
                       
                       NSLog(@"Upload Profile pic : %@",response);
                       
                       if (response!=nil && [response objectForKey:@"status"] && [[response objectForKey:@"status"] intValue]==1)
                       {
                           
                           strFileID=[response objectForKey:@"file_id"];
                           [self callVerifyAPI:strFileID];
                           //[App_Delegate internalLogin];
                           //[App_Delegate showAlertWithMessage:@"Bank document uploaded successfully" inViewController:self];
                       }
                       else
                       {
                           [App_Delegate showAlertWithMessage:@"Photo cannot be uploaded at this time" inViewController:self];
                       }
                       
                   });
    
}

-(void)callVerifyAPI:(NSString*)strFileID
{
    NSString *timestamp =[NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KGuideBankAccountVerify];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strFileID forKey:@"file_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(getResponse:) WithCallBackObject:self];
}

-(void)responseOfVerify:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self performSelector:@selector(loadAppAgain) withObject:nil afterDelay:10.0];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Please try again, Server not responding" inViewController:self];
        }
    }
}

#pragma mark- Image Picker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    //Select image from gallery
    chosenImage = image;
    
    
    NSString *fileImagePath=[NSString stringWithFormat:@"%@/Documents/profilepic.jpeg",NSHomeDirectory()];
    [[NSFileManager defaultManager]removeItemAtPath:fileImagePath error:nil];
    
    NSData *data = UIImageJPEGRepresentation(chosenImage, 0.8);
    
    NSError *error;
    
    [data writeToFile:fileImagePath atomically:YES];
    
    if(error != nil)
    {
        NSLog(@"write error %@", error);
    }
    else
    {
        [self uploadImageUnirest];
    }
}

#pragma mark- TextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    int length =(int)[currentString length];
    
    if (textField.tag==50 && length > 2) //Month
    {
        return NO;
    }
    
    if (textField.tag==51 && length > 2) //Date
    {
        return NO;
    }
    
    if (textField.tag==55 && length > 4) //Year
    {
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.tag==50 && [textField.text intValue]>12) //Month
    {
        [App_Delegate showAlertWithMessage:@"Birth month must be between 1-12" inViewController:self];
        return NO;
    }
    
    if (textField.tag==51 && [textField.text intValue]>31) //Date
    {
        [App_Delegate showAlertWithMessage:@"Birth date must be between 1-31" inViewController:self];
        return NO;
    }
    
    return YES;
}

#pragma mark - UIActionSheet Delegates
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==101) //For Camera
    {
        if (buttonIndex==1) //Gallery
        {
            [self openPhotoLibrary];
        }
        else if (buttonIndex==0) //Camera
        {
            [self openCamera];
        }
    }
}

#pragma mark- Button Action

- (IBAction)btnUploadedDoc:(id)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery", nil];
    sheet.tag=101;
    [sheet showInView:self.view];
}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnSavePressed:(id)sender
{
    if ([self isValidationCorrect])
    {
        [self saveBankDetailAPI];
    }
}


@end
