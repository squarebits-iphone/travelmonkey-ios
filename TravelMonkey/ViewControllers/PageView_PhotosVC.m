//
//  PageView_PhotosVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/12/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "PageView_PhotosVC.h"
#import "CellPhoto.h"
#import "PhotoObject.h"
#import "UIImageView+WebCache.h"
#import "BookMeCell_CollectionView.h"
#import "ViewPhotosVC.h"
#import "AddPhotosVC.h"
#import "PhotosHeaderView.h"
#import "LoginVC.h"




@interface PageView_PhotosVC ()

@end

@implementation PageView_PhotosVC
@synthesize guideObject;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (!guideObject)
    {
        userObject=App_Delegate.userObject;
    }
    else
    {
        userObject=guideObject;
    }
    
    [collectionViewTable registerNib:[UINib nibWithNibName:@"BookMeCell_CollectionView" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"BookMeCell_CollectionView"];
    [collectionViewTable registerNib:[UINib nibWithNibName:@"CellPhoto" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"CellPhoto"];
    [collectionViewTable registerNib:[UINib nibWithNibName:@"PhotosHeaderView" bundle:[NSBundle mainBundle]]  forSupplementaryViewOfKind: UICollectionElementKindSectionHeader withReuseIdentifier:@"PhotosHeaderView"];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(enableScrollview) name:@"enableTableScrolling" object:nil];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:collectionViewTable selector:@selector(reloadData) name:@"refreshCollectionView" object:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (App_Delegate.shouldDisableScrolling==NO)
    {
        [collectionViewTable setScrollEnabled:NO];
    }
    else
    {
        [collectionViewTable setScrollEnabled:YES];
    }
    
}

-(void)showGuestAlert
{
    
    NSString *strMessage=@"Please sign up or login to book local guides.";
    
    SCLAlertView *alert =[App_Delegate getAlertInstance];
    
    [alert addButton:@"No" actionBlock:^(void) {
        
    }];
    
    [alert addButton:@"Yes" actionBlock:^(void) {
        
        [App_Delegate.xmppManager disconnect];
        
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        
        LoginVC *viewController=[[LoginVC alloc]init];
        App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
        [App_Delegate.navigationController setNavigationBarHidden:YES];
        [App_Delegate.window setRootViewController:App_Delegate.navigationController];
        
    }];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:strMessage closeButtonTitle:nil duration:0.0];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==7800)
    {
        //Guest Alert
        if (buttonIndex==1)
        {
            [App_Delegate.xmppManager disconnect];
            
            NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
            [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
            
            LoginVC *viewController=[[LoginVC alloc]init];
            App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
            [App_Delegate.navigationController setNavigationBarHidden:YES];
            [App_Delegate.window setRootViewController:App_Delegate.navigationController];
        }
    }
}

#pragma mark- Button Actions

- (IBAction)btnAddPhotosPressed:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"openAddPhotosPage" object:nil];
}

#pragma mark- ScrollView Delegates

-(void)enableScrollview
{
    [collectionViewTable setScrollEnabled:YES];
    //[collectionViewTable setContentOffset:CGPointMake(0,1)];
}

-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollOffset = scrollView.contentOffset.y;
    if (scrollOffset <=0)
    {
        [collectionViewTable setScrollEnabled:NO];
    }
}

#pragma mark - collection view delegates

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    //return CGSizeMake([UIScreen mainScreen].bounds.size.width,100); //temp
    
    if (userObject.aryPhotos.count==0)
    {
        return CGSizeMake([UIScreen mainScreen].bounds.size.width,100);
    }
    
    return CGSizeZero;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader)
    {
        PhotosHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"PhotosHeaderView" forIndexPath:indexPath];
        
        
        if (guideObject && self.isForViewProfile==NO)
        {
            //Hide button
            headerView.lblTitle.text=@"This user has no photos yet.";
            [headerView.btnAddPhotos setHidden:YES];
        }
        else
        {
            //Show button
            headerView.lblTitle.text=@"You do not have any photos yet";
            [headerView.btnAddPhotos setHidden:NO];
        }
        
        [headerView.btnAddPhotos addTarget:self action:@selector(btnAddPhotosPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        reusableview = headerView;
    }
    
    return reusableview;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (guideObject && self.isForViewProfile==NO)
    {
        return userObject.aryPhotos.count;
    }
    else
    {
        return userObject.aryPhotos.count;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==userObject.aryPhotos.count)
    {
        return CGSizeMake([UIScreen mainScreen].bounds.size.width-10,55);
    }
    else
    {
        // Adjust cell size
        float widthOfView=[UIScreen mainScreen].bounds.size.width;
        widthOfView=widthOfView-5-5-5-5-5; //10+10+10
        
        float cellWidth=widthOfView/3;
        float cellHeight=cellWidth;
        
        NSLog(@"Width of cell: %f",cellWidth);
        NSLog(@"Height of cell: %f",cellHeight);
        
        return CGSizeMake(cellWidth,cellHeight);
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==userObject.aryPhotos.count)
    {
        //1
        static NSString *identifier = @"BookMeCell_CollectionView";
        BookMeCell_CollectionView *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        return cell;
    }
    else
    {
        //1
        static NSString *identifier = @"CellPhoto";
        CellPhoto *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        PhotoObject *photoObject=[userObject.aryPhotos objectAtIndex:indexPath.row];
        [cell.lblCaption setText:photoObject.strCaption];
        NSLog(@"INDEX : %ld URL : %@",(long)indexPath.row,photoObject.strImageURL);
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:photoObject.strImageURL]];
        
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==userObject.aryPhotos.count)
    {
        if ([App_Delegate getCurrentUser]==Guest)
        {
            [self showGuestAlert];
            return;
        }
        
       [[NSNotificationCenter defaultCenter]postNotificationName:@"goToBookGuidePage" object:nil];
    }
    else
    {
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        [dic setObject:[NSNumber numberWithInt:(int)indexPath.row] forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openPhotosPage" object:dic];
    }
}

@end
