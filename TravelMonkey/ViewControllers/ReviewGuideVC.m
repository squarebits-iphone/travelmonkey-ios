//
//  ReviewView_AboutVCViewController.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/9/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "ReviewGuideVC.h"
#import "ReviewCell.h"
#import "TotalReviewsCell.h"
#import "BookMeCell.h"
#import "ReviewObject.h"
#import "UIImageView+WebCache.h"
#import "LoginVC.h"



#define KNotAvailable @"Not Available";


@implementation ReviewGuideVC
@synthesize guideObject,aryInformation;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    tblView.rowHeight = UITableViewAutomaticDimension;
    tblView.estimatedRowHeight = 80.0;
    
    [tblView registerNib:[UINib nibWithNibName:@"ReviewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ReviewCell"];
    [tblView registerNib:[UINib nibWithNibName:@"TotalReviewsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TotalReviewsCell"];
    [tblView registerNib:[UINib nibWithNibName:@"BookMeCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BookMeCell"];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(enableScrollview) name:@"enableTableScrolling" object:nil];
    
    aryInformation=[self getFilterArray:aryInformation];
    
    self.automaticallyAdjustsScrollViewInsets=NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    if (App_Delegate.shouldDisableScrolling==NO)
    {
        [tblView setScrollEnabled:NO];
    }
    else
    {
        [tblView setScrollEnabled:YES];
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [tblView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Methods

-(void)showGuestAlert
{
    NSString *strMessage=@"Please sign up or login to book local guides.";
    
    SCLAlertView *alert =[App_Delegate getAlertInstance];
    
    [alert addButton:@"No" actionBlock:^(void) {
        
    }];
    
    [alert addButton:@"Yes" actionBlock:^(void) {
        
        [App_Delegate.xmppManager disconnect];
        
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        
        LoginVC *viewController=[[LoginVC alloc]init];
        App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
        [App_Delegate.navigationController setNavigationBarHidden:YES];
        [App_Delegate.window setRootViewController:App_Delegate.navigationController];
        
    }];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:strMessage closeButtonTitle:nil duration:0.0];

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==7800)
    {
        //Guest Alert
        if (buttonIndex==1)
        {
            [App_Delegate.xmppManager disconnect];
            
            NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
            [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
            
            LoginVC *viewController=[[LoginVC alloc]init];
            App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
            [App_Delegate.navigationController setNavigationBarHidden:YES];
            [App_Delegate.window setRootViewController:App_Delegate.navigationController];
        }
    }
}

-(NSMutableArray*)getFilterArray:(NSMutableArray*)inputArray
{
    NSMutableArray *ary=[[NSMutableArray alloc]init];
    totalNegative=0;
    totalPositive=0;
    
    for (ReviewObject *reviewObj in inputArray)
    {
        
        if (reviewObj.isReviewedByTraveller)
        {
            if (reviewObj.isPositiveByTraveller)
            {
                totalPositive++;
            }
            else
            {
                totalNegative++;
            }
        }
        
        if (reviewObj.isReviewedByTraveller==YES)
        {
            [ary addObject:reviewObj];
        }
    }
    
    return ary;
}

#pragma mark- ScrollView Delegates

-(void)enableScrollview
{
    [tblView setScrollEnabled:YES];
    //[tblView setContentOffset:CGPointMake(0,1)];
}

-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollOffset = scrollView.contentOffset.y;
    if (scrollOffset <=0)
    {
        [tblView setScrollEnabled:NO];
    }
}

-(IBAction)btnBookMePressed:(id)sender
{
    if ([App_Delegate getCurrentUser]==Guest)
    {
        [self showGuestAlert];
        return;
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"goToBookGuidePage" object:nil];
}

#pragma mark- Tableview Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (aryInformation.count==0)
    {
        return 100;
    }
    
    return 0;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (guideObject)
    {
        //Hide button
        lblHeaderTitle.text=@"This local guide has no reviews yet.";
        [btnHeaderAdd setHidden:YES];
    }
    else
    {
        //Show button
        lblHeaderTitle.text=@"You do not have any reviews yet.";
        [btnHeaderAdd setHidden:YES];
    }
    
    //[btnHeaderAdd addTarget:self action:@selector(btnAddPhotosPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return viewHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    if (guideObject && self.isForViewProfile==NO)
    {
        return 0;
    }
    else
    {
        return 0;
    }
    
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIButton *btnBook=[[UIButton alloc]init];
    [btnBook addTarget:self action:@selector(btnBookMePressed:) forControlEvents:UIControlEventTouchUpInside];
    [btnBook setFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,55)];
    [btnBook setBackgroundColor:[UIColor clearColor]];
    [viewFooter addSubview:btnBook];
    return viewFooter;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (aryInformation.count==0)
    {
        return 0;
    }
    
    if (guideObject && self.isForViewProfile==NO)
    {
        //return aryInformation.count+2;
        return aryInformation.count+1;
    }
    else
    {
        return aryInformation.count+1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        NSString *strCellIdentifier=@"TotalReviewsCell";
        
        TotalReviewsCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        [cell.lblPositiveCount setText:[NSString stringWithFormat:@"%d Positive",totalPositive]];
        [cell.lblNegativeCount setText:[NSString stringWithFormat:@"%d Negative",totalNegative]];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else if (indexPath.row==aryInformation.count+1)
    {
        NSString *strCellIdentifier=@"BookMeCell";
        
        BookMeCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        NSString *strCellIdentifier=@"ReviewCell";
        ReviewCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        return 100;
    }
    else if (indexPath.row==aryInformation.count+1)
    {
        return 55;
    }
    else
    {
        NSString *strCellIdentifier=@"ReviewCell";
        
        static ReviewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            cell = [tblView dequeueReusableCellWithIdentifier:strCellIdentifier];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        return [self calculateHeightForConfiguredSizingCell:cell];
    }
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell
{
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

- (void)setUpCell:(ReviewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    ReviewObject *revObject=[aryInformation objectAtIndex:indexPath.row-1];
    
    [cell.imgProfile setClipsToBounds:YES];
    [cell.imgLikeDislike setClipsToBounds:YES];
    [cell.imgProfile.layer setCornerRadius:45/2];
    [cell.imgLikeDislike.layer setCornerRadius:20/2];
    
    [cell.imgProfile_Opposite setClipsToBounds:YES];
    [cell.imgLikeDislike_Opposite setClipsToBounds:YES];
    [cell.imgProfile_Opposite.layer setCornerRadius:45/2];
    [cell.imgLikeDislike_Opposite.layer setCornerRadius:20/2];
    
    [cell.viewBackground setClipsToBounds:YES];
    [cell.viewBackground.layer setCornerRadius:2];
    
    //temp
    //revObject.strTravellerComment=@"An article review is both a summary and an evaluation of another writer's article. Teachers often assign article reviews to introduce students to the work of experts in the field. Experts also are often asked to review the work of other professionals. Understanding the main points and arguments of the article is essential for an accurate summation. Logical evaluation of the article's main theme, supporting arguments, and implications for further research is an important element of a review. Here are a few guidelines for writing an article review. End";
    
    //revObject.strGuideComment=@"Before you even begin reading the article you will review, you need to understand how your article review will be set up. This will help you understand how to read the article so that you can write an effective review. Your review will be set up in the following parts. End";

    
        //Opposite
        if (revObject.isReviewedByGuide)
        {
            cell.lblReviewerName_Opposite.text=revObject.strGuideName;
            cell.lblReview_Opposite.text=revObject.strGuideComment;
            //cell.lblReview_Opposite.text=@"A ture story is the thing that will not react to any of the matter of the things and now they will not try.";
            [cell.imgProfile_Opposite sd_setImageWithURL:[NSURL URLWithString:revObject.strGuideProfilePicURL]];
            [cell.imgProfile_Opposite setHidden:NO];
            [cell.lblReviewerAddress_Opposite setText:[NSString stringWithFormat:@"%@, %@",revObject.strGuideCity,revObject.strGuideCountry]];
            [cell.lblDate_Opposite setText:revObject.strGuideDate];
            
            //temp
            //revObject.isPositiveByGuide=NO;
            
            if (revObject.isPositiveByGuide)
            {
                [cell.imgLikeDislike_Opposite setImage:[UIImage imageNamed:@"icn_like.png"]];
            }
            else
            {
                [cell.imgLikeDislike_Opposite setImage:[UIImage imageNamed:@"icn_dislike.png"]];
            }
            
        }
        else
        {
            cell.lblReviewerName_Opposite.hidden=cell.lblReview_Opposite.hidden=cell.lblReviewerAddress_Opposite.hidden=cell.imgProfile_Opposite.hidden=cell.imgProfile_Opposite.hidden=cell.lblReviewerAddress_Opposite.hidden=cell.lblDate_Opposite.hidden=cell.imgLikeDislike_Opposite.hidden=cell.viewBackground.hidden=YES;
        }
        
        //Reviewer
        if (revObject.isReviewedByTraveller)
        {
            cell.lblReviewerName.text=revObject.strTravellerName;
            cell.lblReview.text=revObject.strTravellerComment;
            [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:revObject.strTravellerProfilePicURL]];
            [cell.imgProfile setHidden:NO];
            [cell.lblReviewerAddress setText:[NSString stringWithFormat:@"%@, %@",revObject.strTravellerCity,revObject.strTravellerCountry]];
            [cell.lblDate setText:revObject.strTravellerDate];
            
            if (revObject.isPositiveByTraveller)
            {
                [cell.imgLikeDislike setImage:[UIImage imageNamed:@"icn_like.png"]];
            }
            else
            {
                [cell.imgLikeDislike setImage:[UIImage imageNamed:@"icn_dislike.png"]];
            }
        }
        else
        {
            cell.lblReviewerName.text=KNotAvailable;
            cell.lblReviewerAddress.text=KNotAvailable;
            cell.lblReview.text=KNotAvailable;
            [cell.imgProfile setHidden:YES];
        }
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==aryInformation.count+1)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"goToBookGuidePage" object:nil];
    }
}


@end
