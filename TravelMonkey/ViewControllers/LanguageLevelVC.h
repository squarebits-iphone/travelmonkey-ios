//
//  LanguageLevelVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/16/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>



@interface LanguageLevelVC : UIViewController
{
    IBOutlet UIView *viewStatusBar;
    IBOutlet UIView *viewTitle;
    IBOutlet UITableView *tblViewLanguage;
    NSMutableArray *aryLanguageLevel;
    NSString *strCurrentLevel;
    
}

@property (nonatomic,strong) NSMutableArray *aryLanguageData;
//@property (nonatomic,strong) NSMutableArray *arySelectedLanguageData;


@end
