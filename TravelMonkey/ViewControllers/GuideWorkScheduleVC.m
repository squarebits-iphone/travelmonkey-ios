//
//  GuideWorkScheduleVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 12/29/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "GuideWorkScheduleVC.h"
#import "WorkScheduleCell.h"
#import "ScheduleObject.h"
#import "WorkSchedulePlaceHolderCell.h"
#import "PriceObject.h"


#define KValue 40

@interface GuideWorkScheduleVC ()

@end

@implementation GuideWorkScheduleVC

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    CGFloat dummyViewHeight = 80;
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tblWorkSchedule.bounds.size.width, dummyViewHeight)];
    tblWorkSchedule.tableHeaderView = dummyView;
    tblWorkSchedule.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0);
    
    [btnEdit setTitleColor:KAppColor_Blue forState:UIControlStateNormal];
    [self setupPickerColor];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //Adding observer to the done button of keyboard
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receivedNotification:) name:K_Notification_DoneButtonPressed object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    //tblWorkSchedule.tableFooterView=viewSaveSchedule;
    isEdit=NO;
}


-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Methods

-(void)receivedNotification:(NSNotification*)notification
{
    NSLog(@"Received notification from done button");
    [self textFieldShouldEndEditing:[notification object]];
}

-(void)setupPickerColor
{
    [startTimePicker setValue:KAppColor_Blue forKeyPath:@"textColor"];
    SEL selector = NSSelectorFromString( @"setHighlightsToday:" );
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature :
                                [UIDatePicker
                                 instanceMethodSignatureForSelector:selector]];
    BOOL no = NO;
    [invocation setSelector:selector];
    [invocation setArgument:&no atIndex:2];
    [invocation invokeWithTarget:startTimePicker];
}

-(NSString*)getDayName:(NSString*)strDay
{
    
    int value=[strDay intValue];
    NSString *strDayName=@"";
    
    switch (value)
    {
        case 1:
            strDayName=@"Monday";
            break;
            
        case 2:
            strDayName=@"Tuesday";
            break;
            
        case 3:
            strDayName=@"Wednesday";
            break;
            
        case 4:
            strDayName=@"Thursday";
            break;
            
        case 5:
            strDayName=@"Friday";
            break;
            
        case 6:
            strDayName=@"Saturday";
            break;
            
        case 7:
            strDayName=@"Sunday";
            break;
            
        default:
            break;
    }
    
    return strDayName;
}

-(void)callSaveScheduleAPI
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KSaveWorkSchedule];
    
    NSDateFormatter *formatter=[NSDateFormatter new];
    [formatter setDateFormat:@"HH:mm"];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:App_Delegate.userObject.strOffset forKey:@"offset"];
    [dicParameter setObject:@"USD" forKey:@"currency"]; //temp
    
    //Need to set the schedule
    NSMutableDictionary *mainScheduleDictionary=[[NSMutableDictionary alloc]init];
    
    for (ScheduleObject *scheduleObject in App_Delegate.userObject.locatDetailObject.scheduleObjects)
    {
        NSMutableArray *aryPriceObject=[[NSMutableArray alloc]init];
        
        for (PriceObject *priceObject in scheduleObject.aryPriceObject)
        {
            
            if ([priceObject.strPrice isEqualToString:@"Per Hour"] || [priceObject.strPrice isEqualToString:@""] || [priceObject.strStartTime isEqualToString:@"Start"] || [priceObject.strEndTime isEqualToString:@"End"] )
            {
                [App_Delegate showAlertWithMessage:@"Please enter correct Start Time, End Time or Price Per Hour" inViewController:self];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                return;
            }
            
            if ([priceObject.strPrice intValue]<10)
            {
                [App_Delegate showAlertWithMessage:@"Please enter min $10 price" inViewController:self];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                return;
            }
            
            NSMutableDictionary *priceDictionary=[[NSMutableDictionary alloc]init];
            [priceDictionary setObject:priceObject.strPrice forKey:@"price"];
            
            NSLog(@"Local StartTime : %@ EndTime : %@",priceObject.strStartTime,priceObject.strEndTime);
            NSLog(@"GMT StartTime : %@ EndTime %@",[App_Delegate getGMTFromLocalTime:priceObject.strStartTime andDateFormat:@"HH:mm:ss"],[App_Delegate getGMTFromLocalTime:priceObject.strEndTime andDateFormat:@"HH:mm:ss"]);
            
            
            //[priceDictionary setObject:[App_Delegate getGMTFromLocalTime:priceObject.strStartTime andDateFormat:@"HH:mm:ss"] forKey:@"start_time"];
            //[priceDictionary setObject:[App_Delegate getGMTFromLocalTime:priceObject.strEndTime andDateFormat:@"HH:mm:ss"] forKey:@"end_time"];
            
            [priceDictionary setObject:[App_Delegate getConvertedDateStringFromDateString:priceObject.strStartTime inputDateString:@"HH:mm:ss" andOutputDateFormat:@"HH:mm:ss"] forKey:@"start_time"];
            [priceDictionary setObject:[App_Delegate getConvertedDateStringFromDateString:priceObject.strEndTime inputDateString:@"HH:mm:ss" andOutputDateFormat:@"HH:mm:ss"] forKey:@"end_time"];
            [priceDictionary setObject:priceObject.strIsOpen forKey:@"is_open"];
            
            [aryPriceObject addObject:priceDictionary];
        }
        
        [mainScheduleDictionary setObject:aryPriceObject forKey:scheduleObject.strDay];
        
    }
    
    [dicParameter setObject:mainScheduleDictionary forKey:@"schedule"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(getSaveScheduleAPIResponse:) WithCallBackObject:self];
}

-(void)getSaveScheduleAPIResponse:(NSDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        //[App_Delegate showAlertWithMessage:@"Work schedule updated, Thank you !" inViewController:self];
        [App_Delegate showTMAlertWithMessage:@"Your ""Work Schedule"" has been updated" andTitle:@"Done" doneButtonTitle:@"Okay" inViewController:self];
    }
    
    if (!isSuccess)
    {
        
        [App_Delegate showAlertWithMessage:@"Work schedule cannot be update now (Please add atleast 1 schedule in case if you have not added)" inViewController:self];
        
        return;
        
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Work schedule cannot be update now, Please try again later" inViewController:self];
        }
    }
}

-(PriceObject*)getPriceObjectFromButton:(UIButton*)btn
{
    UIView *contentView = (UIView *)[[btn superview] superview];
    UITableViewCell *cell = (UITableViewCell *)[contentView superview];
    NSIndexPath *indexPath = [tblWorkSchedule indexPathForCell:cell];
    
    int currentDay=(int)indexPath.section+1;
    
    for (int i=0;i<App_Delegate.userObject.locatDetailObject.scheduleObjects.count;i++)
    {
        ScheduleObject *scheduleObject=[App_Delegate.userObject.locatDetailObject.scheduleObjects objectAtIndex:i];
        
        if ([scheduleObject.strDay intValue]==currentDay && scheduleObject.aryPriceObject.count>0)
        {
            PriceObject *priceObject=[scheduleObject.aryPriceObject objectAtIndex:indexPath.row-1];
            
            currentPriceObject=priceObject;
            return priceObject;
            break;
        }
    }
    
    return nil;
}

-(void)closeDatePicker
{
    [pickerBottam setConstant:-viewPicker.frame.size.height];
}

-(void)openDatePicker
{
    [pickerBottam setConstant:0];
}

#pragma mark- Button Actions

-(IBAction)btnSaveSchedulePressed:(id)sender
{
    [self callSaveScheduleAPI];
}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnEditPressed:(id)sender
{
    
    if (isEdit)
    {
        isEdit=NO;
        [btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
    }
    else
    {
        isEdit=YES;
        [btnEdit setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    
    [tblWorkSchedule reloadData];
}

-(void)btnPlusPressed:(UIButton *)btnPlus
{
    
    isEdit=NO;
    [btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
    
    NSLog(@"Button Plus Pressed - %ld",btnPlus.tag);
    
    BOOL isScheduleObjectFound=NO;
    
    int currentDay=(int)btnPlus.tag;
    
    for (int i=0;i<App_Delegate.userObject.locatDetailObject.scheduleObjects.count;i++)
    {
        ScheduleObject *scheduleObject=[App_Delegate.userObject.locatDetailObject.scheduleObjects objectAtIndex:i];
        
        NSLog(@"Day - %@",scheduleObject.strDay);
        
        if ([scheduleObject.strDay intValue]==currentDay)
        {
            isScheduleObjectFound=YES;
            PriceObject *priceObject=[[PriceObject alloc]init];
            [priceObject setStrPrice:@"Per Hour"]; //temp
            [priceObject setStrIsOpen:@"0"];
            [priceObject setStrStartTime:@"Start"]; //temp
            [priceObject setStrEndTime:@"End"]; //temp
            
            if (scheduleObject.aryPriceObject && scheduleObject.aryPriceObject.count>2)
            {
                [App_Delegate showAlertWithMessage:@"You can set maximum 3 schedules for a day, You can either edit or delete schedule" inViewController:self];
                return;
            }
            
            if (scheduleObject.aryPriceObject && scheduleObject.aryPriceObject.count>0)
            {
                [scheduleObject.aryPriceObject addObject:priceObject];
            }
            else
            {
                scheduleObject.aryPriceObject=[[NSMutableArray alloc]init];
                [scheduleObject.aryPriceObject addObject:priceObject];
            }
            
            break;
        }
    }
    
    if (isScheduleObjectFound==NO)
    {
        ScheduleObject *newObject=[[ScheduleObject alloc]init];
        newObject.strDay=[NSString stringWithFormat:@"%d",currentDay];
        newObject.aryPriceObject=[[NSMutableArray alloc]init];
        
        PriceObject *priceObject=[[PriceObject alloc]init];
        [priceObject setStrPrice:@"50.00"]; //temp
        [priceObject setStrIsOpen:@"0"];
        [priceObject setStrStartTime:@"07:00:00"]; //temp
        [priceObject setStrEndTime:@"23:00:00"]; //temp
        [newObject.aryPriceObject addObject:priceObject];
        
        [App_Delegate.userObject.locatDetailObject.scheduleObjects addObject:newObject];
    }
    
    [tblWorkSchedule reloadData];
}

-(void)btnFromPressed:(UIButton *)btnFrom
{
    isFrom=YES;
    [self getPriceObjectFromButton:btnFrom];
    [self openDatePicker];
}

-(void)btnDeletePressed:(UIButton *)btnFrom
{
    UIView *contentView = (UIView *)[btnFrom superview];
    UITableViewCell *cell = (UITableViewCell *)[contentView superview];
    NSIndexPath *indexPath = [tblWorkSchedule indexPathForCell:cell];
    
    int currentDay=(int)indexPath.section+1;
    
    for (int i=0;i<App_Delegate.userObject.locatDetailObject.scheduleObjects.count;i++)
    {
        ScheduleObject *scheduleObject=[App_Delegate.userObject.locatDetailObject.scheduleObjects objectAtIndex:i];
        
        if ([scheduleObject.strDay intValue]==currentDay && scheduleObject.aryPriceObject.count>0)
        {
            PriceObject *priceObject=[scheduleObject.aryPriceObject objectAtIndex:indexPath.row-1];
            [scheduleObject.aryPriceObject removeObject:priceObject];
            break;
        }
        
    }
    
    [tblWorkSchedule reloadData];
}

-(void)btnToPressed:(UIButton *)btnTo
{
    isFrom=NO;
    [self getPriceObjectFromButton:btnTo];
    [self openDatePicker];
}

-(IBAction)btnClosePressedOnPicker:(id)sender
{
    [self closeDatePicker];
}

-(IBAction)btnSavePressedOnPicker:(id)sender
{
    
    NSDateFormatter *formatter=[NSDateFormatter new];
    [formatter setDateFormat:@"HH:mm:ss"];
    
    if (isFrom)
    {
        currentPriceObject.strStartTime=[formatter stringFromDate:[App_Delegate getDateWithZeroSecond:startTimePicker.date]];
    }
    else
    {
        currentPriceObject.strEndTime=[formatter stringFromDate:[App_Delegate getDateWithZeroSecond:startTimePicker.date]];
    }
    
    [self closeDatePicker];
    [tblWorkSchedule reloadData];
}

-(void)btnOnOffPressed:(UIButton*)btnOnOff
{
    PriceObject *priceObject=[self getPriceObjectFromButton:btnOnOff];
    
    if (priceObject==nil)
    {
        return;
    }
    
    if ([priceObject.strIsOpen boolValue])
    {
        priceObject.strIsOpen=@"0";
    }
    else
    {
        priceObject.strIsOpen=@"1";
    }
    
    
    [tblWorkSchedule reloadData];
    
}


#pragma mark- UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [textField setText:@""];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    PriceObject *priceObject;
    
    UIView *contentView = (UIView *)[[textField superview]superview];
    UITableViewCell *cell = (UITableViewCell *)[contentView superview];
    NSIndexPath *indexPath = [tblWorkSchedule indexPathForCell:cell];
    
    int currentDay=(int)indexPath.section+1;
    
    for (int i=0;i<App_Delegate.userObject.locatDetailObject.scheduleObjects.count;i++)
    {
        ScheduleObject *scheduleObject=[App_Delegate.userObject.locatDetailObject.scheduleObjects objectAtIndex:i];
        
        if ([scheduleObject.strDay intValue]==currentDay && scheduleObject.aryPriceObject.count>0)
        {
            priceObject=[scheduleObject.aryPriceObject objectAtIndex:indexPath.row-1];
            currentPriceObject=priceObject;
            break;
        }
        
    }
    
    if (priceObject==nil)
    {
        return YES;
    }
    
    if (textField.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter some price" inViewController:self];
        //return NO;
    }
    
    NSString *strPrice=textField.text;
    
    strPrice=[strPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    if (strPrice.length>0 && [strPrice intValue]<10)
    {
        [App_Delegate showAlertWithMessage:@"Please enter min $10 price" inViewController:self];
        //return NO;
    }
    
    priceObject.strPrice=strPrice;
    
    [tblWorkSchedule reloadData];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    PriceObject *priceObject;
    
    UIView *contentView = (UIView *)[[textField superview]superview];
    UITableViewCell *cell = (UITableViewCell *)[contentView superview];
    NSIndexPath *indexPath = [tblWorkSchedule indexPathForCell:cell];
    
    int currentDay=(int)indexPath.section+1;
    
    for (int i=0;i<App_Delegate.userObject.locatDetailObject.scheduleObjects.count;i++)
    {
        ScheduleObject *scheduleObject=[App_Delegate.userObject.locatDetailObject.scheduleObjects objectAtIndex:i];
        
        if ([scheduleObject.strDay intValue]==currentDay && scheduleObject.aryPriceObject.count>0)
        {
            priceObject=[scheduleObject.aryPriceObject objectAtIndex:indexPath.row-1];
            currentPriceObject=priceObject;
            break;
        }
        
    }
    
    if (priceObject==nil)
    {
        return YES;
    }
    
    if (textField.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter some price" inViewController:self];
        return NO;
    }
    
    if ([textField.text intValue]<10)
    {
        [App_Delegate showAlertWithMessage:@"Please enter min $10 price" inViewController:self];
        return NO;
    }
    
    priceObject.strPrice=textField.text;
    
    [tblWorkSchedule reloadData];
    
    return YES;
}

#pragma mark- Tableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return App_Delegate.userObject.locatDetailObject.scheduleObjects.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (section==0)
    {
        return KValue;
    }
    
    return 80.0;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[UIView new];
    
    if (section==0)
    {
        [view setFrame:CGRectMake(0,0,tblWorkSchedule.frame.size.width,KValue)];
    }
    else
    {
        [view setFrame:CGRectMake(0,0,tblWorkSchedule.frame.size.width,80)];
    }
    
    
    [view setBackgroundColor:[UIColor clearColor]];
    //[view.layer setBorderColor:KAppColor.CGColor];
    //[view.layer setBorderWidth:1.0];
    
    //Adding name
    UILabel *lblDay=[[UILabel alloc]initWithFrame:CGRectMake(0,view.frame.size.height-KValue,view.frame.size.width,KValue)];
    [lblDay setTextColor:KAppColor_Blue];
    [lblDay setText:[self getDayName:[NSString stringWithFormat:@"%ld",section+1]]];
    [lblDay setFont:[UIFont fontWithName:@"Source Sans Pro" size:22]];
    [view addSubview:lblDay];
    
    //Adding button
    UIButton *btnPlus=[[UIButton alloc]initWithFrame:CGRectMake(view.frame.size.width-KValue+8,(view.frame.size.height-KValue+8),KValue-8,KValue-8)];
    [btnPlus setBackgroundImage:[UIImage imageNamed:@"icn_add.png"] forState:UIControlStateNormal];
    [btnPlus setBackgroundColor:[UIColor clearColor]];
    [btnPlus setTag:section+1];
    [btnPlus addTarget:self action:@selector(btnPlusPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnPlus];
    
    UIButton *btnAdditionalPlus=[[UIButton alloc]initWithFrame:CGRectMake(btnPlus.frame.origin.x-100,btnPlus.frame.origin.y,100,btnPlus.frame.size.height)];
    [btnAdditionalPlus setBackgroundColor:[UIColor clearColor]];
    [btnAdditionalPlus setTag:section+1];
    [btnAdditionalPlus addTarget:self action:@selector(btnPlusPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnAdditionalPlus];
    
    //[view setBackgroundColor:KAppColor_GrayBlue];
    
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    int currentDay=(int)section+1;
    
    for (int i=0;i<App_Delegate.userObject.locatDetailObject.scheduleObjects.count;i++)
    {
        ScheduleObject *scheduleObject=[App_Delegate.userObject.locatDetailObject.scheduleObjects objectAtIndex:i];
        
        if ([scheduleObject.strDay intValue]==currentDay && scheduleObject.aryPriceObject.count>0)
        {
            return scheduleObject.aryPriceObject.count+1;
        }
        
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        NSString *strCellIdentifier=@"WorkSchedulePlaceHolderCell";
        
        WorkSchedulePlaceHolderCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell setBackgroundColor:[UIColor clearColor]];
        }
        
        return cell;
    }
    else
    {
        NSString *strCellIdentifier=@"WorkScheduleCell";
        
        WorkScheduleCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell setBackgroundColor:[UIColor clearColor]];
        }
        
        int currentDay=(int)indexPath.section+1;
        
        for (int i=0;i<App_Delegate.userObject.locatDetailObject.scheduleObjects.count;i++)
        {
            ScheduleObject *scheduleObject=[App_Delegate.userObject.locatDetailObject.scheduleObjects objectAtIndex:i];
            
            if ([scheduleObject.strDay intValue]==currentDay && scheduleObject.aryPriceObject.count>0)
            {
                PriceObject *priceObject=[scheduleObject.aryPriceObject objectAtIndex:indexPath.row-1];
                
                
                //NSLog(@"Start Time : %@",priceObject.strStartTime);
                
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"HH:mm:ss"];
                
                NSDate *startTime=[dateFormatter dateFromString:priceObject.strStartTime];
                NSDate *endTime=[dateFormatter dateFromString:priceObject.strEndTime];
                [dateFormatter setDateFormat:@"hh:mm a"];
                
                //Setting tags
                cell.btnPrice.tag=cell.btnTo.tag=cell.btnFrom.tag=cell.btnONOFF.tag=cell.btnDelete.tag=indexPath.row-1;
                
                //Setting Titles
                [cell.btnONOFF setSelected:[priceObject.strIsOpen boolValue]];
                [cell.btnFrom setTitle:[dateFormatter stringFromDate:startTime] forState:UIControlStateNormal];
                [cell.btnTo setTitle:[dateFormatter stringFromDate:endTime] forState:UIControlStateNormal];
                [cell.btnPrice setText:[NSString stringWithFormat:@"$ %@",priceObject.strPrice]];
                [cell.btnPrice setDelegate:self];
                
                //Setting Target
                [cell.btnONOFF addTarget:self action:@selector(btnOnOffPressed:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnFrom addTarget:self action:@selector(btnFromPressed:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnTo addTarget:self action:@selector(btnToPressed:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnDelete addTarget:self action:@selector(btnDeletePressed:) forControlEvents:UIControlEventTouchUpInside];
                
                if ([priceObject.strPrice isEqualToString:@"Per Hour"])
                {
                    [cell.btnPrice setTextColor:[UIColor lightGrayColor]];
                    //[cell.btnPrice setText:priceObject.strPrice];
                }
                else
                {
                    [cell.btnPrice setTextColor:[UIColor blackColor]];
                }
                
                if ([priceObject.strStartTime isEqualToString:@"Start"])
                {
                    [cell.btnFrom setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                    [cell.btnFrom setTitle:priceObject.strStartTime forState:UIControlStateNormal];
                }
                else
                {
                    [cell.btnFrom setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                }
                
                if ([priceObject.strEndTime isEqualToString:@"End"])
                {
                    [cell.btnTo setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                    [cell.btnTo setTitle:priceObject.strEndTime forState:UIControlStateNormal];
                }
                else
                {
                    [cell.btnTo setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                }
                
                break;
            }
            
        }
        
        if (isEdit)
        {
            [cell.btnDeleteLeading setConstant:73];
        }
        else
        {
            [cell.btnDeleteLeading setConstant:-8];
        }
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
