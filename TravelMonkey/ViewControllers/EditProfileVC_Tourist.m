//
//  EditProfileVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/12/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "EditProfileVC_Tourist.h"
#import "TravellerDetailObject.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "UNIRest.h"
#import "Parser.h"
#import "UITextView+Placeholder.h"
#import "ChangePasswordVC.h"
#import "AddPhotosVC.h"
#import "AddCountryCell.h"
#import "CountryCell.h"
#import "PSelectionView.h"



#import <AddressBook/AddressBook.h>
#import <CoreLocation/CoreLocation.h>


@interface EditProfileVC_Tourist ()

@end

@implementation EditProfileVC_Tourist

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [txtViewAboutMe setPlaceholder:@"Type here.."];
    [txtViewAboutMe setPlaceholderColor:[UIColor lightGrayColor]];
    [txtViewTravelInterested setPlaceholder:@"Type here.."];
    [txtViewTravelInterested setPlaceholderColor:[UIColor lightGrayColor]];
    
    [imgUserProfile.layer setCornerRadius:110/2];
    //[imgUserProfile.layer setBorderColor:KAppColor.CGColor];
    //[imgUserProfile.layer setBorderWidth:0.2];
    [imgUserProfile setClipsToBounds:YES];
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    [txtFieldEmail setUserInteractionEnabled:NO];
    
    //Add tap gesture to image
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(editUserImage)];
    [imgUserProfile addGestureRecognizer:tapGesture];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    
    //Preparing the country option to show user country options
    NSString *strPlistPath=[[NSBundle mainBundle] pathForResource:@"DiallingCodes" ofType:@"json"];
    
    aryCountriesOption=[[NSMutableArray alloc]init];
    if ([[NSFileManager defaultManager] fileExistsAtPath:strPlistPath])
    {
        NSString *string=[NSString stringWithContentsOfFile:strPlistPath encoding:NSUTF8StringEncoding error:nil];
        
        NSData* data = [string dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *values = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        if (values && values.count>0)
        {
            for (NSDictionary *dicCountry in values)
            {
                NSString *strCountryName=[dicCountry objectForKey:@"name"];
                strCountryName=[strCountryName stringByReplacingOccurrencesOfString:@"," withString:@" "];
                [aryCountriesOption addObject:strCountryName];
            }
        }
    }
    
    //Fetching the user visited countries
    if (App_Delegate.userObject.travellerDetailObject.strVisitedPlaces && App_Delegate.userObject.travellerDetailObject.strVisitedPlaces!=(id)[NSNull null] && App_Delegate.userObject.travellerDetailObject.strVisitedPlaces.length>0)
    {
        aryVisitedCounties=[[App_Delegate.userObject.travellerDetailObject.strVisitedPlaces componentsSeparatedByString:@","] mutableCopy];
    }
    else
    {
        aryVisitedCounties=[[NSMutableArray alloc] init];
    }

    [self setUpViewForTourist];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadView) name:K_Notification_ReloadView object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.view addSubview:scrlView];
    [scrlView setFrame:CGRectMake(0,140,[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-140)];
    
}

-(void)viewDidLayoutSubviews
{
    //[constraintTableHeight setConstant:(aryVisitedCounties.count*56)+60];
    [constraintTableHeight setConstant:60];
    [scrlView setContentSize:CGSizeMake(0,btnChangePassword.frame.origin.y+80)];
    [constraintViewSubViewScrollView setConstant:btnChangePassword.frame.origin.y+80];
}

#pragma mark- Methods

-(void)reloadView
{
    [self setUpViewForTourist];
}

-(void)editUserImage
{
    UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:@"Edit Picture" delegate:(id)self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Gallery",@"Camera", nil];
    
    [actionSheet showInView:self.view];
}

-(void)setUpViewForTourist
{
    if (App_Delegate.userObject==nil)
    {
        return;
    }
    
    [btnSave setTitleColor:KAppColor_Blue forState:UIControlStateNormal];
    
    if (App_Delegate.userObject.strProfileImageURL!=nil && App_Delegate.userObject.strProfileImageURL!=(id)[NSNull null] && chosenImage==nil)
    {
        [imgUserProfile sd_setImageWithURL:[NSURL URLWithString:App_Delegate.userObject.strProfileImageURL]];
    }
    
    if ([App_Delegate.userObject.strUserType isEqualToString:@"TRAVELER"])
    {
        [btnTourist setSelected:YES];
        [btnLcoal setSelected:NO];
    }
    else
    {
        [btnTourist setSelected:NO];
        [btnLcoal setSelected:YES];
    }
    
    if (App_Delegate.userObject.strFirstName!=nil && App_Delegate.userObject.strFirstName!=(id)[NSNull null] && App_Delegate.userObject.strFirstName.length>0)
    {
        [txtFieldFirstName setText:App_Delegate.userObject.strFirstName];
    }
    
    if (App_Delegate.userObject.strLastName!=nil && App_Delegate.userObject.strLastName!=(id)[NSNull null] && App_Delegate.userObject.strLastName.length>0)
    {
        [txtFieldLastName setText:App_Delegate.userObject.strLastName];
    }
    
    if (App_Delegate.userObject.strEmail!=nil && App_Delegate.userObject.strEmail!=(id)[NSNull null] && App_Delegate.userObject.strEmail.length>0)
    {
        NSLog(@"TextField Message : %@",App_Delegate.userObject.strEmail);
        [txtFieldEmail setText:App_Delegate.userObject.strEmail];
    }
    
    if (App_Delegate.userObject.strAddress!=nil && App_Delegate.userObject.strAddress!=(id)[NSNull null] && App_Delegate.userObject.strAddress.length>0)
    {
        //strAddress=App_Delegate.userObject.strAddress;
        //[btnAddress setTitle:App_Delegate.userObject.strAddress forState:UIControlStateNormal];
    }
    
    if (App_Delegate.userObject.strCity!=nil && App_Delegate.userObject.strCity!=(id)[NSNull null] && App_Delegate.userObject.strCity.length>0)
    {
        [txtFieldCity setText:App_Delegate.userObject.strCity];
    }
    
    if (App_Delegate.userObject.strCountry!=nil && App_Delegate.userObject.strCountry!=(id)[NSNull null] && App_Delegate.userObject.strCountry.length>0)
    {
        strCountry=App_Delegate.userObject.strCountry;
        [btnCountry setTitle:App_Delegate.userObject.strCountry forState:UIControlStateNormal];
        [btnCountry setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    
    if (App_Delegate.userObject.strLat!=nil && App_Delegate.userObject.strLat!=(id)[NSNull null] && App_Delegate.userObject.strLat.length>0)
    {
        strLat=App_Delegate.userObject.strLat;
    }
    
    if (App_Delegate.userObject.strLong!=nil && App_Delegate.userObject.strLong!=(id)[NSNull null] && App_Delegate.userObject.strLong.length>0)
    {
        strLong=App_Delegate.userObject.strLong;
    }
    
    NSLog(@"Birth Date : %@",App_Delegate.userObject.strBirthDate);
    
    if (App_Delegate.userObject.strBirthDate!=nil && App_Delegate.userObject.strBirthDate!=(id)[NSNull null] && App_Delegate.userObject.strBirthDate.length>0 && ![App_Delegate.userObject.strBirthDate isEqualToString:@"0000-00-00"])
    {
        NSArray *array=[App_Delegate.userObject.strBirthDate componentsSeparatedByString:@"-"];
        
        if (array.count==3)
        {
            [txtFieldYear setText:[array objectAtIndex:0]];
            [txtFieldMonth setText:[array objectAtIndex:1]];
            [txtFieldDate setText:[array objectAtIndex:2]];
        }
    }
    
    if (App_Delegate.userObject.isMale)
    {
        [btnMale setSelected:YES];
        [btnFemale setSelected:NO];
    }
    else
    {
        [btnMale setSelected:NO];
        [btnFemale setSelected:YES];
    }
    
    TravellerDetailObject *localObject=App_Delegate.userObject.travellerDetailObject;
    
    if (localObject.strAbout!=nil && localObject.strAbout!=(id)[NSNull null] && localObject.strAbout.length>0)
    {
        [txtViewAboutMe setText:localObject.strAbout];
    }
    
    if (localObject.strTravelIntrested!=nil && localObject.strTravelIntrested!=(id)[NSNull null] && localObject.strTravelIntrested.length>0)
    {
        [txtViewTravelInterested setText:localObject.strTravelIntrested];
    }
    
    
    if ([App_Delegate.userObject.strSite isEqualToString:@"NATIVE"])
    {
        [btnChangePassword setEnabled:YES];
        [btnChangePassword setHidden:NO];
        [imgPasswordArrow setHidden:NO];
    }
    else
    {
        [btnChangePassword setEnabled:NO];
        [btnChangePassword setHidden:YES];
        [imgPasswordArrow setHidden:YES];
    }
    
    
}

-(BOOL)isValidationCorrect //Check if any field is empty and show user respective message
{
    
    if (txtFieldFirstName.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter First name" inViewController:self];
        return NO;
    }
    
    if (txtFieldLastName.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter Last name" inViewController:self];
        return NO;
    }
    
    if (txtFieldEmail.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter Email" inViewController:self];
        return NO;
    }
    
    if (![App_Delegate NSStringIsValidEmail:txtFieldEmail.text])
    {
        [App_Delegate showAlertWithMessage:@"Please enter a valid Email" inViewController:self];
        return NO;
    }
    
//    if (strAddress==nil || strAddress.length==0)
//    {
//        [App_Delegate showAlertWithMessage:@"Please select address" inViewController:self];
//        return NO;
//    }
    
    if (txtFieldCity.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter city" inViewController:self];
        return NO;
    }
    
    if (strCountry.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter country" inViewController:self];
        return NO;
    }
    
    return YES;
}


-(void)uploadImageUnirest //This will upload image to the
{
    
    //Image path
    NSString *fileImagePath=[NSString stringWithFormat:@"%@/Documents/profilepic.jpeg",NSHomeDirectory()];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    //Url
    NSString *url=[NSString stringWithFormat:@"%@%@",KBaseURL,KUploadProfilePic];
    url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //Check network
    if (networkStatus==NotReachable)
    {
        [App_Delegate showAlertWithMessage:@"No internet connection." inViewController:self];
    }
    else
    {
        
        //Show loading
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        __block NSDictionary *dicResponse;
        
        //Content-Type: application/json
        NSMutableDictionary *headers =[[NSMutableDictionary alloc]init];
        [headers setObject:@"application/json" forKey:@"Content-Type"];
        [headers setObject:@"utf-8" forKey:@"Accept-Encoding"];
        
        NSDictionary* parameters = @{@"type" : @"Profile", @"file": [NSURL fileURLWithPath:fileImagePath]};
        
        //Initializing Async request
        [[UNIRest post:^(UNISimpleRequest* request)
          {
              [request setUrl:url];
              [request setParameters:parameters];
              
          }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
         {
             
             //Fetching resposne
             NSData *rawBody = response.rawBody;
             NSString *strResponse = [[NSString alloc] initWithData:rawBody encoding:NSUTF8StringEncoding];
             NSLog(@"Response %@",strResponse);
             
             dicResponse=response.body.object;
             
             if (dicResponse==nil)
             {
                 dicResponse=[response.body.array objectAtIndex:0];
             }
             
             [self uploadProfilePicAPIResponse:dicResponse];
             
         }];
    }
    
}


-(void)uploadProfilePicAPIResponse:(NSDictionary*)response
{
    //Calling main thread
    dispatch_async(dispatch_get_main_queue(), ^()
                   {
                       //All UI related change can be done here.
                       [MBProgressHUD hideHUDForView:self.view animated:YES];
                       
                       NSLog(@"Upload Profile pic : %@",response);
                       
                       if (response!=nil && [response objectForKey:@"image_id"])
                       {
                           strUserImageId=[response objectForKey:@"image_id"];
                           
                           [self callSaveLocalAPI];
                       }
                       else
                       {
                           [App_Delegate showAlertWithMessage:@"Profile pic cannot be uploaded at this time" inViewController:self];
                       }
                       
                   });
    
}

-(void)callSaveLocalAPI
{
    
    //Guide
    /*
     
     {"about":"about guide","role":"GUIDE","language":"[{\"Language\":\"Arabic\",\"type\":\"Basic\"},{\"Language\":\"French\",\"type\":\"flunt\"}]","transport":"car,van","user_id":"17","offer_tourist":"gfdgfd","dob":"1991-01-01","city":"jodhpur","latitude":"26.00256","longitude":"73.02554","location":"jodhpur","gender":"F","first_name":"pp","last_name":"ss","country":"india",}
     
     if (role.equalsIgnoreCase(Utils.GUIDE)) {
     jsonParams.put("transport", transportation.getText().toString());
     jsonParams.put("offer_tourist", offerprovidetotourist.getText().toString());
     jsonParams.put("language", "[{\"Language\":\"Arabic\",\"type\":\"Basic\"},{\"Language\":\"French\",\"type\":\"flunt\"}]");
     
     } else {
     jsonParams.put("interests", trabelInterest.getText().toString());
     jsonParams.put("visited_country", "");
     }

     Common Parameter
     city
     location
     dob
     latitude
     longitude
     first_name
     last_name
     gender
     country
     
     Role:- TRAVELER/GUIDE
     
    */
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KUpdateProfile];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:txtViewAboutMe.text forKey:@"about"];
    [dicParameter setObject:App_Delegate.userObject.strOffset forKey:@"offset"];
    [dicParameter setObject:strUserType forKey:@"role"];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:txtViewTravelInterested.text forKey:@"interests"];
    [dicParameter setObject:[aryVisitedCounties componentsJoinedByString:@","] forKey:@"visited_country"];
    [dicParameter setObject:txtFieldCity.text forKey:@"city"];
    [dicParameter setObject:txtFieldCity.text forKey:@"location"];
    [dicParameter setObject:[NSString stringWithFormat:@"%@:%@:%@",txtFieldYear.text,txtFieldMonth.text,txtFieldDate.text] forKey:@"dob"];
    [dicParameter setObject:strLat forKey:@"latitude"];
    [dicParameter setObject:strLong forKey:@"longitude"];
    [dicParameter setObject:txtFieldFirstName.text forKey:@"first_name"];
    [dicParameter setObject:txtFieldLastName.text forKey:@"last_name"];
    [dicParameter setObject:strCountry forKey:@"country"];
    
    if (strUserImageId!=nil && chosenImage!=nil)
    {
        [dicParameter setObject:strUserImageId forKey:@"profile_image"];
    }
    
    if (btnMale.selected)
    {
        [dicParameter setObject:@"M" forKey:@"gender"];
    }
    else
    {
        [dicParameter setObject:@"F" forKey:@"gender"];
    }
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc]init];
    [header setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList putAPIWithURL:strUrl andHeader:header andParameters:dicParameter Selector:@selector(saveLocalAPIResponse:) WithCallBackObject:self];
    
}

-(void)saveLocalAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        Parser *parser=[[Parser alloc]init];
        App_Delegate.userObject=[parser getUserDetail:dictionary];
        
        if ([App_Delegate.userObject.strUserType isEqualToString:@"TRAVELER"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserTourist forKey:Default_UserType];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserGuide forKey:Default_UserType];
        }
        
        [[NSUserDefaults standardUserDefaults]setObject:dictionary forKey:Default_SaveUserObject];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //[App_Delegate application:[UIApplication sharedApplication] didFinishLaunchingWithOptions:nil];
        
        [App_Delegate internalLogin];
        
        [App_Delegate showAlertWithMessage:[NSString stringWithFormat:@"Thank you %@ for updating your profile.",App_Delegate.userObject.strFirstName] inViewController:self];
        
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Profile cannot be update now, Please try again later" inViewController:self];
        }
    }
    
}

-(void)switchRole
{
    
    NSString *strMessage=@"Are you sure you want to become a local guide?";
    
   SCLAlertView *alert =[App_Delegate getAlertInstance];
    
    [alert addButton:@"No" actionBlock:^(void) {
        
    }];
    
    [alert addButton:@"Yes" actionBlock:^(void) {
        
        [self callSwitchRoleApi];
        
    }];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:strMessage closeButtonTitle:nil duration:0.0];
}

-(void)callSwitchRoleApi
{
    
    /*
     {"user_id":"17","role":"GUIDE"}

     */
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KSwitchRole];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:@"GUIDE" forKey:@"role"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(switchRoleAPIResponse:) WithCallBackObject:self];
    
    
}

-(void)switchRoleAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        [App_Delegate application:[UIApplication sharedApplication] didFinishLaunchingWithOptions:nil];
        
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Profile cannot be update now, Please try again later" inViewController:self];
        }
    }

}

#pragma mark- Button Action

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnSavePressed:(id)sender
{
    
    if ([self isValidationCorrect])
    {
        
        if (btnTourist.selected)
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserTourist forKey:Default_UserType];
            strUserType=@"TRAVELER";
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserGuide forKey:Default_UserType];
            strUserType=@"GUIDE";
        }
        
        
        if (chosenImage!=nil)
        {
            [self uploadImageUnirest];
        }
        else
        {
            [self callSaveLocalAPI];
        }
    }
}

-(IBAction)btnAdressPressed:(id)sender
{
    
    //[btnAddress setTitle:@"Address" forState:UIControlStateNormal];
    //[btnAddress setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btnCountry setTitle:@"Country" forState:UIControlStateNormal];
    [btnCountry setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [txtFieldCity setText:@""];
    strLat=nil;
    strLong=nil;
    strAddress=@"";
    strCountry=@"";
    
    // Present the Autocomplete view controller when the button is pressed.
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    [acController setTintColor:KAppColor];
    [acController setPrimaryTextColor:KAppColor_GrayBlue];
    [acController setPrimaryTextHighlightColor:KAppColor];
    GMSAutocompleteFilter *filter=[GMSAutocompleteFilter new];
    filter.type=kGMSPlacesAutocompleteTypeFilterCity;
    
    acController.autocompleteFilter=filter;
    acController.delegate = (id)self;
    [self presentViewController:acController animated:YES completion:nil];
    [App_Delegate setStatusBarBackgroundColor:KAppColor];
    
}

- (IBAction)btnBackPressed:(UIButton *)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
    //[App_Delegate showAlertWithMessage:@"Under Construction" inViewController:self];
}


- (IBAction)btnTouristPressed:(UIButton *)sender
{
    
}

- (IBAction)btnGuidePressed:(UIButton *)sender
{
    [self switchRole];
}

- (IBAction)btnMalePressed:(UIButton *)sender
{
    
    [btnMale setSelected:YES];
    [btnFemale setSelected:NO];
}

- (IBAction)btnFemalePressed:(UIButton *)sender
{
    [btnMale setSelected:NO];
    [btnFemale setSelected:YES];
}

- (void)btnChangeCountryPressed:(NSIndexPath *)indexPath
{
    isChangeCountry=YES;
    currentIndex=(int)indexPath.row;
    
    PSelectionView *selectionView=[[PSelectionView alloc]init];
    selectionView.pageTitle=@"Select Country";
    selectionView.arrayData=aryCountriesOption;
    selectionView.viewColor=KAppColor;
    selectionView.delegate=(id)self;
    [self presentViewController:selectionView animated:YES completion:nil];
}

- (IBAction)btnAddcountiesPressed:(UIButton *)sender
{
    isChangeCountry=NO;
    
    PSelectionView *selectionView=[[PSelectionView alloc]init];
    selectionView.pageTitle=@"Select Country";
    selectionView.arrayData=aryCountriesOption;
    selectionView.arraySelectedData=aryVisitedCounties;
    selectionView.viewColor=KAppColor;
    selectionView.delegate=(id)self;
    [self presentViewController:selectionView animated:YES completion:nil];
}

- (IBAction)btnChangePasswordPressed:(UIButton *)sender
{
    ChangePasswordVC *changePasswordVC=[[ChangePasswordVC alloc]init];
    [self.navigationController pushViewController:changePasswordVC animated:YES];
}

- (IBAction)btnAddPhotosPressed:(UIButton *)sender
{
    AddPhotosVC *changePasswordVC=[[AddPhotosVC alloc]init];
    [self.navigationController pushViewController:changePasswordVC animated:YES];
}

#pragma mark- AlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1012)
    {
        NSLog(@"Logout Alert");
        
        NSLog(@"Button index : %d",(int)buttonIndex);
        
        if (buttonIndex==1)
        {
            [self callSwitchRoleApi];
        }
    }
}

#pragma mark- TextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    int length =(int)[currentString length];
    
    if (textField.tag==50 && length > 2) //Month
    {
        return NO;
    }
    
    if (textField.tag==51 && length > 2) //Date
    {
        return NO;
    }
    
    if (textField.tag==55 && length > 4) //Year
    {
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.tag==50 && [textField.text intValue]>12) //Month
    {
        [App_Delegate showAlertWithMessage:@"Birth month must be between 1-12" inViewController:self];
        return NO;
    }
    
    if (textField.tag==51 && [textField.text intValue]>31) //Date
    {
        [App_Delegate showAlertWithMessage:@"Birth date must be between 1-31" inViewController:self];
        return NO;
    }
    
    return YES;
}

#pragma mark- GMSAutoComplete Delegate

- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    
    [App_Delegate setStatusBarBackgroundColor:[UIColor clearColor]];
    // The user has selected a place.
    [self dismissViewControllerAnimated:YES completion:^{
        
        //http://stackoverflow.com/questions/15036007/how-to-obtain-country-state-city-from-reversegeocodecoordinate
        
        strAddress=place.name;
        //[btnAddress setTitle:place.name forState:UIControlStateNormal];
        //[btnAddress setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [txtFieldCity setText:place.name];
        strLat=[NSString stringWithFormat:@"%f",place.coordinate.latitude];
        strLong=[NSString stringWithFormat:@"%f",place.coordinate.longitude];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        CLLocation *location=[[CLLocation alloc]initWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude];
        
        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            NSLog(@"Finding address");
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if (error)
            {
                NSLog(@"Error %@", error.description);
            }
            else
            {
                
                CLPlacemark *placemark = [placemarks lastObject];
                NSString *city = [[placemark addressDictionary] objectForKey:(NSString *)kABPersonAddressCityKey];
                NSString *countryLocal = [[placemark addressDictionary] objectForKey:(NSString *)kABPersonAddressCountryKey];
                NSString *countryISO = [[placemark addressDictionary] objectForKey:(NSString *)kABPersonAddressCountryCodeKey];
                
                if (city!=nil && city!=(id)[NSNull null])
                {
                    txtFieldCity.text=city;
                }
                
                if (countryLocal!=nil && countryLocal!=(id)[NSNull null])
                {
                    strCountry=countryLocal;
                    [btnCountry setTitle:countryLocal forState:UIControlStateNormal];
                    [btnCountry setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                }
                
                if (countryISO!=nil && countryISO!=(id)[NSNull null])
                {
                    strCountryISO=countryISO;
                }
                
                App_Delegate.userObject.strOffset=[NSString stringWithFormat:@"%ld",placemark.timeZone.secondsFromGMT];
                
            }
            
        }];
        
        
//        [[GMSGeocoder geocoder] reverseGeocodeCoordinate:place.coordinate completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
//            NSLog(@"reverse geocoding results:");
//            
//            if (response.results!=nil && response.results!=(id)[NSNull null] && response.results.count>0)
//            {
//                GMSAddress *address=[[response results] objectAtIndex:0];
//                
//                if (address.locality!=nil && address.locality!=(id)[NSNull null])
//                {
//                    txtFieldCity.text=address.locality;
//                }
//                
//                if (address.country!=nil && address.country!=(id)[NSNull null])
//                {
//                    txtFieldCity.text=address.country;
//                }
//            }
//        }];
        
    }];
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithError:(NSError *)error
{
    [App_Delegate setStatusBarBackgroundColor:[UIColor clearColor]];
    [self dismissViewControllerAnimated:YES completion:nil];
}

// User pressed cancel button.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController
{
    [App_Delegate setStatusBarBackgroundColor:[UIColor clearColor]];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark- PSelectionViewDelegate

-(void)userSelectedArray:(NSMutableArray*)arySelectedData
{
    aryVisitedCounties=[[NSMutableArray alloc]init];
    aryVisitedCounties=arySelectedData;
}

#pragma mark- Actionsheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        //Gallery
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = (id)self;
        picker.view.tag=101;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    else if (buttonIndex==1)
    {
        //Camera
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate =(id)self;
        picker.view.tag=101;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
}

#pragma mark- UIImagePicker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    if (picker.view.tag==101)
    {
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        //Select image from gallery
        chosenImage = info[UIImagePickerControllerEditedImage];
        
        NSString *fileImagePath=[NSString stringWithFormat:@"%@/Documents/profilepic.jpeg",NSHomeDirectory()];
        
        NSData *data = UIImageJPEGRepresentation(chosenImage, 0.8);
        
        NSError *error;
        
        [data writeToFile:fileImagePath options:NSDataWritingAtomic error:&error];
        
        if(error != nil)
            NSLog(@"write error %@", error);
        
        [imgUserProfile setImage:chosenImage];
        
    }
}

#pragma mark- Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return aryVisitedCounties.count+1;
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if (indexPath.row==App_Delegate.userObject.locatDetailObject.aryLanguage.count)
    if (indexPath.row==0)
    {
        return 60;
    }
    else
    {
        return 76;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if (indexPath.row==aryVisitedCounties.count)
    if (indexPath.row==0)
    {
        NSString *strCellIdentifier=@"AddCountryCell";
        
        AddCountryCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        [cell.btnAddCountry setTitleColor:KAppColor_Blue forState:UIControlStateNormal];
        
        return cell;
    }
    else
    {
        NSString *strCellIdentifier=@"CountryCell";
        
        CountryCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        
        NSString *object=[aryVisitedCounties objectAtIndex:indexPath.row];
        [cell.lblName setText:object];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if (indexPath.row==aryVisitedCounties.count)
    if (indexPath.row==0)
    {
        [self btnAddcountiesPressed:nil];
    }
    else
    {
        [self btnChangeCountryPressed:indexPath];
    }
}


@end
