//
//  AlertVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 1/27/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertVC : UIViewController
{
    IBOutlet UILabel *lblTitle;
    IBOutlet UITextView *txtViewMessage;
    IBOutlet UIButton *btnDone;
    
}

@property (nonatomic,strong) NSString *strTitle;
@property (nonatomic,strong) NSString *strDoneBtnTitle;
@property (nonatomic,strong) NSString *strMessage;



@end
