//
//  LeftVC_Local.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/9/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.

#import "LeftVC_Local.h"
#import "DeckCustomCell.h"
#import "HomeVC_Guide.h"
#import "LoginVC.h"
#import "EditProfileVC.h"
#import "GuideWorkScheduleVC.h"
#import "UIView+MWParallax.h"
#import "TripsVC_Guide.h"
#import "AboutUsVC.h"
#import "InviteFriendsVC.h"
#import "HelpVC.h"
#import "GuideBankDetailVC.h"


#import "UIImageView+WebCache.h"

@interface LeftVC_Local ()

@end

@implementation LeftVC_Local

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Setting Image
    [imgProfile.layer setCornerRadius:130/2];
    [imgProfile.layer setBorderColor:[UIColor clearColor].CGColor];
    [viewSideBar setBackgroundColor:RGBCOLOR(40, 56, 79)];
    [lblViewProfile setTextColor:KAppColor_GrayBlue];
    [imgProfile.layer setBorderWidth:0.2];
    [imgProfile setClipsToBounds:YES];
    
    //Add tap gesture to image
    UITapGestureRecognizer *tapGesture1=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewProfile)];
    UITapGestureRecognizer *tapGesture2=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewProfile)];
    UITapGestureRecognizer *tapGesture3=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewProfile)];
    [imgProfile addGestureRecognizer:tapGesture1];
    [lblUserName addGestureRecognizer:tapGesture2];
    [lblViewProfile addGestureRecognizer:tapGesture3];
    
    if (App_Delegate.userObject.strProfileImageURL!=nil && App_Delegate.userObject.strProfileImageURL!=(id)[NSNull null])
    {
        [imgProfile sd_setImageWithURL:[NSURL URLWithString:App_Delegate.userObject.strProfileImageURL]];
    }
    
    //Setting User Name
    NSString *strUserName=@"";
    
    if (App_Delegate.userObject.strFirstName!=nil && App_Delegate.userObject.strFirstName!=(id)[NSNull null])
    {
        strUserName=[strUserName stringByAppendingString:App_Delegate.userObject.strFirstName];
        [lblUserName setText:strUserName];
    }
    
    if (App_Delegate.userObject.strLastName!=nil && App_Delegate.userObject.strLastName!=(id)[NSNull null])
    {
        //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",App_Delegate.userObject.strLastName]];
        //[lblUserName setText:strUserName];
    }
    
    //Registering Cell for reuse
    [tblOption registerNib:[UINib nibWithNibName:@"DeckCustomCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"DeckCustomCell"];
 
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openTripPage) name:@"openTripPage" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshTable) name:@"refreshData" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(openEditProfilePage) name:@"openEditProfile" object:nil];
    
    currentIndex=[NSIndexPath indexPathForRow:1 inSection:0];
    [tblOption selectRowAtIndexPath:currentIndex animated:YES scrollPosition:UITableViewScrollPositionNone];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if (App_Delegate.userObject.strProfileImageURL!=nil && App_Delegate.userObject.strProfileImageURL!=(id)[NSNull null])
    {
        [imgProfile sd_setImageWithURL:[NSURL URLWithString:App_Delegate.userObject.strProfileImageURL]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Methods

-(void)openEditProfilePage
{
    EditProfileVC *viewCont = [[EditProfileVC alloc]init];
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
    [navController setNavigationBarHidden:YES];
    App_Delegate.deckController.centerController=navController;
    [App_Delegate.dictRootController setObject:navController forKey:@"1"];
}

-(void)openTripPage
{
    TripsVC_Guide *viewCont = [[TripsVC_Guide alloc]init];
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
    [navController setNavigationBarHidden:YES];
    App_Delegate.deckController.centerController=navController;
    [App_Delegate.dictRootController setObject:navController forKey:@"2"];
}

-(void)refreshTable
{
    [tblOption reloadData];
}

-(void)callLogoutAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KLogoutUser];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(logoutAPIResponse:) WithCallBackObject:self];
}

-(void)logoutAPIResponse:(NSDictionary*)dictionary
{
    [self LogOutUser];
}

-(void)viewProfile
{
    [lblViewProfile setTextColor:[UIColor whiteColor]];
    [imgProfile.layer setBorderColor:[UIColor clearColor].CGColor];
    [imgProfile.layer setBorderWidth:1];
    currentIndex=[NSIndexPath indexPathForRow:0 inSection:1];
    [tblOption reloadData];
    
    HomeVC_Guide *viewCont = [[HomeVC_Guide alloc]init];
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
    [navController setNavigationBarHidden:YES];
    App_Delegate.deckController.centerController=navController;
    [App_Delegate.dictRootController setObject:navController forKey:@"0"];
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(void)LogOutUser
{
    [App_Delegate.xmppManager disconnect];
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    LoginVC *viewController=[[LoginVC alloc]init];
    App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
    [App_Delegate.navigationController setNavigationBarHidden:YES];
    [App_Delegate.window setRootViewController:App_Delegate.navigationController];
}

-(void)logoutAlert
{
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:KAppName message:@"Are you sure to logout?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
    [alertView setTag:1012];
    
    [alertView show];
    
}

#pragma mark- Button Action


#pragma mark- AlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1012)
    {
        NSLog(@"Logout Alert");
        
        NSLog(@"Button index : %d",(int)buttonIndex);
        
        if (buttonIndex==1)
        {
            [self callLogoutAPI];
        }
        
    }
}


#pragma mark- Table View Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 7;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    DeckCustomCell *cell = (DeckCustomCell *)[tableView dequeueReusableCellWithIdentifier:@"deckCustomCell"];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DeckCustomCell" owner:self options:nil];
        cell = (DeckCustomCell *)[nib objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    [cell.imgStatus setHidden:YES];
    
    switch (indexPath.row)
    {
    
        case 0:
        {
            cell.lblOptionName.text = @"Edit Profile";
            cell.imgView.image=[UIImage imageNamed:@"icn_edit_profile.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_edit_profile_hover.png"];
            
            break;
        }
        case 1:
        {
            cell.lblOptionName.text = @"Bookings";
            cell.imgView.image=[UIImage imageNamed:@"icn_trips.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_trips_hover.png"];
            
            break;
        }
        case 2:
        {
            cell.lblOptionName.text = @"Work Schedule";
            cell.imgView.image=[UIImage imageNamed:@"icn_work.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_work_hover.png"];
            
            break;
        }
        case 3:
        {
            
            cell.lblOptionName.text = @"Bank Account";
            cell.imgView.image=[UIImage imageNamed:@"icn_payments.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_payments_hover.png"];
            
            //App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus=@"success"; //temp
            
            //Need to put condition here
            if (App_Delegate.userObject.locatDetailObject.guideBankDetailObject!=nil && App_Delegate.userObject.locatDetailObject.guideBankDetailObject!=(id)[NSNull null] && App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus!=nil && App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus!=(id)[NSNull null] && App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus.length>0)
            {
            
                if ([App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus isEqualToString:@"verified"])
                {
                    //Bank detail not filled
                    [cell.imgStatus setImage:[UIImage imageNamed:@"btn_StatusGreen"]];
                    [cell.imgStatus setHidden:NO];
                }
                else
                {
                    //Bank detail not filled
                    [cell.imgStatus setImage:[UIImage imageNamed:@"btn_StatusYellow"]];
                    [cell.imgStatus setHidden:NO];
                }
            }
            else
            {
                //Bank detail not filled
                [cell.imgStatus setImage:[UIImage imageNamed:@"btn_StatusRed"]];
                [cell.imgStatus setHidden:NO];
            }
            
            
            break;
        }
        case 4:
        {
            cell.lblOptionName.text = @"Help";
            cell.imgView.image=[UIImage imageNamed:@"icn_help.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_help_hover.png"];
            break;
        }
        case 5:
        {
            cell.lblOptionName.text = @"Invite Friends";
            cell.imgView.image=[UIImage imageNamed:@"icn_invite_friends.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_invite_friends_hover.png"];
            break;
        }
        case 6:
        {
            cell.lblOptionName.text = @"About Us";
            cell.imgView.image=[UIImage imageNamed:@"icn_about_us.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_about_us_hover.png"];
            break;
        }
        case 7:
        {
            cell.lblOptionName.text = @"Logout";
            cell.imgView.image=[UIImage imageNamed:@"icn_logout_menu.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_logout_menu_hover.png"];
            break;
        }
            
        default:
            break;
            
    }
    
    if (currentIndex==indexPath)
    {
        [cell.lblOptionName setTextColor:[UIColor whiteColor]];
        [cell.imgView setHidden:YES];
        [cell.imgViewHighLight setHidden:NO];
    }
    else
    {
        [cell.lblOptionName setTextColor:KAppColor_GrayBlue];
        [cell.imgView setHidden:NO];
        [cell.imgViewHighLight setHidden:YES];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //DeckCustomCell *cell = (DeckCustomCell *)[tableView cellForRowAtIndexPath:indexPath];
    //[AppDelegate rippleWithView:cell.viewRipple center:cell.viewRipple.center colorFrom:KAppColor_Blue colorTo:KAppColor_Blue];
    
    [lblViewProfile setTextColor:KAppColor_GrayBlue];
    
    if (indexPath.row==0 || indexPath.row==1 || indexPath.row==3 || indexPath.row==2 || indexPath.row==7 || indexPath.row==6 || indexPath.row==5 || indexPath.row==4)
    {
        
        currentIndex=indexPath;
        [imgProfile.layer setBorderColor:[UIColor clearColor].CGColor];
        [imgProfile.layer setBorderWidth:0];
        [tblOption reloadData];
        
        if (indexPath.row==3)
        {
            //Need to put condition here ---- //Temp
            if (App_Delegate.userObject.locatDetailObject.guideBankDetailObject!=nil && App_Delegate.userObject.locatDetailObject.guideBankDetailObject!=(id)[NSNull null] && App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus!=nil && App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus!=(id)[NSNull null] && App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus.length>0)
            {
                
                
                if ([App_Delegate.userObject.locatDetailObject.guideBankDetailObject.strStatus isEqualToString:@"verified"])
                {
                    //Bank detail not filled
                    [App_Delegate showAlertWithMessage:@"Your account is verifed successfully." inViewController:self];
                    return;
                }
                else
                {
                    //Bank detail not filled
                    [App_Delegate showAlertWithMessage:@"Please upload the doc so that Stripe can verfify you account." inViewController:self];
                }
                
                
            }
            else
            {
                //Bank detail not filled
                [App_Delegate showAlertWithMessage:@"Please fill your bank details in order to get the earning deposit." inViewController:self];
            }
        }
    }
    else
    {
        [App_Delegate showAlertWithMessage:@"Under Construction" inViewController:self];
        return;
    }
    
    if ([App_Delegate.dictRootController objectForKey:[NSString stringWithFormat:@"%d",(int)indexPath.row]])
    {
        App_Delegate.deckController.centerController=[App_Delegate.dictRootController objectForKey:[NSString stringWithFormat:@"%d",(int)indexPath.row]];
    }
    else
    {
        switch (indexPath.row)
        {
            
            //Home
            case 0:
            {
                EditProfileVC *viewCont = [[EditProfileVC alloc]init];
                
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                [navController setNavigationBarHidden:YES];
                App_Delegate.deckController.centerController=navController;
                [App_Delegate.dictRootController setObject:navController forKey:@"1"];
                
                break;
            }
            case 1:
            {
                TripsVC_Guide *viewCont = [[TripsVC_Guide alloc]init];
                
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                [navController setNavigationBarHidden:YES];
                App_Delegate.deckController.centerController=navController;
                [App_Delegate.dictRootController setObject:navController forKey:@"2"];
                
                break;
            }
            case 2:
            {
                if (App_Delegate.userObject.strOffset==nil || App_Delegate.userObject.strOffset.length==0 || [App_Delegate.userObject.strOffset isEqualToString:@""] || [App_Delegate.userObject.strOffset isEqualToString:@"0"])
                {
                    [App_Delegate showAlertWithMessage:@"Please enter your city first and complete your profile in EDIT PROFILE" inViewController:self];
                    return;
                    
                }
                else
                {
                    GuideWorkScheduleVC *viewCont = [[GuideWorkScheduleVC alloc]init];
                    
                    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                    [navController setNavigationBarHidden:YES];
                    App_Delegate.deckController.centerController=navController;
                    [App_Delegate.dictRootController setObject:navController forKey:@"3"];
                }
                
                break;
                
            }
            case 3:
            {
                GuideBankDetailVC *viewCont = [[GuideBankDetailVC alloc]init];
                
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                [navController setNavigationBarHidden:YES];
                App_Delegate.deckController.centerController=navController;
                [App_Delegate.dictRootController setObject:navController forKey:@"4"];
                
                break;
            }
            case 4:
            {
                HelpVC *viewCont = [[HelpVC alloc]init];
                viewCont.isGuide=YES;
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                [navController setNavigationBarHidden:YES];
                App_Delegate.deckController.centerController=navController;
                [App_Delegate.dictRootController setObject:navController forKey:@"5"];
                
                break;
            }
            case 5:
            {
                
                InviteFriendsVC *viewCont = [[InviteFriendsVC alloc]init];
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                [navController setNavigationBarHidden:YES];
                App_Delegate.deckController.centerController=navController;
                [App_Delegate.dictRootController setObject:navController forKey:@"6"];
                
                break;
            }
            case 6:
            {
                AboutUsVC *viewCont = [[AboutUsVC alloc]init];
                
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                [navController setNavigationBarHidden:YES];
                App_Delegate.deckController.centerController=navController;
                [App_Delegate.dictRootController setObject:navController forKey:@"7"];
                
                break;
            }
            case 7:
            {
                [self logoutAlert];
                return;
                break;
            }
                
            default:
                break;
                
        }
        
    }
    
    //Purushottam sain
    [[NSNotificationCenter defaultCenter]postNotificationName:K_Notification_ReloadView object:nil];
    [self.viewDeckController performSelector:@selector(toggleLeftViewAnimated:) withObject:[NSNumber numberWithBool:YES] afterDelay:0.3];
    
}


@end
