//
//  LeftVC_Local.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/9/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftVC_Local : UIViewController
{
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblViewProfile;
    IBOutlet UIImageView *imgProfile;
    IBOutlet UITableView *tblOption;
    IBOutlet UIView *viewSideBar;
    NSIndexPath *currentIndex;
    
}
@end
