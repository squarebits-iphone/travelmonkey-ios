//
//  GuideWorkScheduleVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 12/29/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PriceObject.h"
#import "ScheduleObject.h"


@interface GuideWorkScheduleVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    IBOutlet UITableView *tblWorkSchedule;
    IBOutlet UIView *viewSaveSchedule;
    IBOutlet UIDatePicker *startTimePicker;
    IBOutlet UIView *viewPicker;
    IBOutlet NSLayoutConstraint *pickerBottam;
    IBOutlet UIButton *btnEdit;
    IBOutlet UIButton *btnSave;
    
    
    PriceObject *currentPriceObject;
    ScheduleObject *currentScheduleObject;
    
    BOOL isFrom;
    BOOL isEdit;
    
}

@end
