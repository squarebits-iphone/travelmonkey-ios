//
//  VerifyEmailVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/16/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "VerifyEmailVC.h"
#import "Parser.h"


@interface VerifyEmailVC ()

@end

@implementation VerifyEmailVC
@synthesize strEmail,strPassword;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    NSString *str=[NSString stringWithFormat:@"We have sent you a verification email to:\n%@\n\nPlease click on the verification link inside your mail and verify your email address, then return to this page and click on the 'Continue' button below.\n\nIf you don't found it in your inbox please check you spam box.",strEmail];
    
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:str];
    
    NSRange totalRange = [str rangeOfString:str]; // 4 characters, starting at index 22
    NSRange selectedRange = [str rangeOfString:strEmail]; // 4 characters, starting at index 22
    
    [string beginEditing];
    
    
    [string addAttribute:NSFontAttributeName
                   value:[UIFont fontWithName:@"SourceSansPro-Regular" size:15.0]
                   range:totalRange];
    
    [string addAttribute:NSForegroundColorAttributeName
                   value:[UIColor darkGrayColor]
                   range:totalRange];

    
    [string addAttribute:NSFontAttributeName
                   value:[UIFont fontWithName:@"SourceSansPro-Bold" size:15.0]
                   range:selectedRange];
    
    [string addAttribute:NSForegroundColorAttributeName
                   value:[UIColor darkGrayColor]
                   range:selectedRange];
    
    
    [string endEditing];
    
    
    [lblMessage setAttributedText:string];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Methods

-(void)callLogin
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KNativeLogin];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:strEmail forKey:@"email"];
    [dicParameter setObject:strPassword forKey:@"password"];
    
    [APIsList postAPIWithURL:strUrl andParameters:dicParameter Selector:@selector(loginAPIResponse:) WithCallBackObject:self];
}

-(void)goToHomePage
{
    [[App_Delegate window]setRootViewController:[App_Delegate setupDeckViewController]];
}


-(void)loginAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        
        Parser *parser=[[Parser alloc]init];
        App_Delegate.userObject=[parser getUserDetail:dictionary];
        
        if ([App_Delegate.userObject.strUserType isEqualToString:@"TRAVELER"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserTourist forKey:Default_UserType];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserGuide forKey:Default_UserType];
        }
        
        [[NSUserDefaults standardUserDefaults]setObject:dictionary forKey:Default_SaveUserObject];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self goToHomePage];
        
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Email not verified Yet" inViewController:self];
        }
    }
}

#pragma mark- Button Action 

-(IBAction)btnContinuePressed:(id)sender
{
    [self callLogin];
}

-(IBAction)btnBackPressedPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}




@end
