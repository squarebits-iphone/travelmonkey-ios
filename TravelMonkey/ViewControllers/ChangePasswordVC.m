//
//  ChangePasswordVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/1/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "ChangePasswordVC.h"

@interface ChangePasswordVC ()

@end

@implementation ChangePasswordVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Mehtods

-(BOOL)isValidationCorrect //Check if any field is empty and show user respective message
{
    
    if (txtfieldCurrentPassword.hidden==NO && txtfieldCurrentPassword.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter your current password" inViewController:self];
        return NO;
    }
    
    if (txtfieldNewPassword.hidden==NO && txtfieldNewPassword.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter your new password" inViewController:self];
        return NO;
    }
    
    if (txtfieldNewPassword.text.length<4)
    {
        [App_Delegate showAlertWithMessage:@"Please use atleast minimum 4 characters password." inViewController:self];
        return NO;
    }
    
    
    if (txtfieldConfirmPassword.hidden==NO && txtfieldConfirmPassword.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter your confirm password" inViewController:self];
        return NO;
    }

    
    if (![txtfieldConfirmPassword.text isEqualToString:txtfieldNewPassword.text])
    {
        [App_Delegate showAlertWithMessage:@"Password entered doesn't match, Please make sure you enter same password." inViewController:self];
        return NO;
    }
    
    return YES;
}

-(void)callChangePasswordAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KChangePassword];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:txtfieldCurrentPassword.text forKey:@"old_password"];
    [dicParameter setObject:txtfieldNewPassword.text forKey:@"new_password"];
    
    [APIsList postAPIWithURL:strUrl andParameters:dicParameter Selector:@selector(changePasswordAPIResponse:) WithCallBackObject:self];
}

-(void)changePasswordAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        
        isSuccess=YES;
        [[NSUserDefaults standardUserDefaults]setObject:txtfieldNewPassword.text forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [App_Delegate showAlertWithMessage:@"Your password is changed successfully." inViewController:self];
        
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Password cannot be update now, Please try again later" inViewController:self];
        }
    }

}

#pragma mark- Button Action

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnSavePressed:(id)sender
{
    if ([self isValidationCorrect])
    {
        [self callChangePasswordAPI];
    }
}

@end
