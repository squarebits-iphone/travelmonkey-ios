//
//  ThankYouVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 1/27/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "ThankYouVC.h"

@interface ThankYouVC ()

@end

@implementation ThankYouVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [txtViewMessage setUserInteractionEnabled:NO];
    [lblTitle setText:self.strTitle];
    [lblConfirmationTitle setText:self.strConfirmationTitle];
    [txtViewMessage setText:self.strMessage];
    
    [self setupButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self performSelector:@selector(selectCheckBox) withObject:nil afterDelay:0.5];
}

#pragma mark- Methods

-(void)selectCheckBox
{
    [checkBox setHideBox:NO];
    [checkBox setOn:YES animated:YES];
}

-(void)setupButton
{
    [checkBox setUserInteractionEnabled:NO];
    checkBox.onAnimationType = BEMAnimationTypeBounce;
    checkBox.offAnimationType = BEMAnimationTypeBounce;
    checkBox.animationDuration=1;
    checkBox.hideBox=YES;
    
}

#pragma mark- Button Action

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnClosePressed:(id)sender
{
    NSLog(@"Close button pressed");
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    if (self.isForBooking)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"bookingConfirmed" object:nil];
    }
}

@end
