//
//  TripsVC_Traveller.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/30/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripObject.h"


@interface TripsVC_Traveller : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    //Outlets
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UITableView *tblTrips;
    IBOutlet UIButton *btnFutureTrips;
    IBOutlet UIButton *btnPastTrips;
    
    BOOL showFutureTrips;
    
    NSMutableArray *aryFutureTrips;
    NSMutableArray *aryPastTrips;
    
    NSTimer *refreshTimer;
    TripObject *currentObject;
}

@end
