//
//  FilterScreenVC_Tourist.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/29/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NMRangeSlider.h"


@interface FilterScreenVC_Tourist : UIViewController
{
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UILabel *lblAge;
    IBOutlet UIScrollView *scrollviewMain;
    IBOutlet UITableView *tblLanguage;
    
    //Buttons
    IBOutlet UIButton *btnMale;
    IBOutlet UIButton *btnFemale;
    IBOutlet UIButton *btnAny;
    IBOutlet UIButton *btnWithLocalYes;
    IBOutlet UIButton *btnWithLocalNo;
    IBOutlet UIButton *btnH2L;
    IBOutlet UIButton *btnL2H;
    
    //Range slider
    IBOutlet NMRangeSlider *labelSlider;
    
    //Distance slider
    IBOutlet UISlider *distanceSlider;
    
    
    //Add Language
    NSMutableArray *arySelectedLanguage;
    NSMutableArray *aryLanguageLevel;
    NSMutableArray *aryLanguage;
    BOOL isForChangeSelection;
    BOOL isChangeLanguage;
    int currentIndex;
     __weak IBOutlet NSLayoutConstraint *constraintTableHeight;
}

@property (weak, nonatomic) IBOutlet UILabel *lowerLabel;
@property (weak, nonatomic) IBOutlet UILabel *upperLabel;

@end
