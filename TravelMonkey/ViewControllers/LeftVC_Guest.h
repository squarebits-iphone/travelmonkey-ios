//
//  LeftVC_Guest.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 1/30/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftVC_Guest : UIViewController
{
    IBOutlet UILabel *lblUserName;
    IBOutlet UIImageView *imgProfile;
    IBOutlet UITableView *tblOption;
    IBOutlet UILabel *lblViewProfile;
    IBOutlet UIView *viewSideBar;
    NSIndexPath *currentIndex;
    
}

@end
