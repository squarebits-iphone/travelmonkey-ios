//
//  LoginVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/11/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SocialInformation.h"


@interface LoginVC : UIViewController<GIDSignInUIDelegate>
{
    SocialInformation *object;
    IBOutlet UIButton *btnSkipLogin;
    IBOutlet UIButton *btnLogin;
    IBOutlet NSLayoutConstraint *constraintBottamClearView;
    IBOutlet NSLayoutConstraint *constraintLogoTopDistance;
    IBOutlet UIImageView *imgLogo;
    IBOutlet UILabel *lblTerms;
    
}

@end
