//
//  ContactUsVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "ContactUsVC.h"
#import "ThankYouVC.h"
#import <PinterestSDK/PinterestSDK.h>


@interface ContactUsVC ()

@end

@implementation ContactUsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [scrollviewMain setContentSize:CGSizeMake(0, txtViewMessage.frame.origin.y+txtViewMessage.frame.size.height+80)];
}

#pragma mark- Methods

-(NSString*)getUserType:(NSString*)inputUser
{
    NSString *strReturn=@"OTHER";
    
    if ([inputUser isEqualToString:@"Travel Monkey Tourist"])
    {
        strReturn=@"TOURIST";
    }
    else if ([inputUser isEqualToString:@"Travel Monkey Local Guide"])
    {
        strReturn=@"GUIDE";
    }
    else
    {
        strReturn=@"OTHER";
    }
    
    return strReturn;
}

-(NSString*)getDepartmentType:(NSString*)inputDepartment
{
    NSString *strReturn=@"other";
    
    if ([inputDepartment isEqualToString:@"Customer Service"])
    {
        strReturn=@"customer";
    }
    else if ([inputDepartment isEqualToString:@"Technical Support"])
    {
        strReturn=@"technical";
    }
    else if ([inputDepartment isEqualToString:@"Careers"])
    {
        strReturn=@"careers";
    }
    else if ([inputDepartment isEqualToString:@"Investor Relations"])
    {
        strReturn=@"investor_relations";
    }
    else if ([inputDepartment isEqualToString:@"Business Partnerships"])
    {
        strReturn=@"business_partnerships";
    }
    else if ([inputDepartment isEqualToString:@"Press and Media"])
    {
        strReturn=@"press_media";
    }
    else if ([inputDepartment isEqualToString:@"Legal Affairs"])
    {
        strReturn=@"legal";
    }
    else if ([inputDepartment isEqualToString:@"Other, not listed"])
    {
        strReturn=@"other";
    }
    
    return strReturn;
}

-(void)callContactUSAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KContactUsAPI];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:txtFieldEmail.text forKey:@"email"]; //Email
    [dicParameter setObject:txtFieldName.text forKey:@"username"]; //Name
    [dicParameter setObject:[self getUserType:btnIam.titleLabel.text] forKey:@"type"]; //UserType
    [dicParameter setObject:[self getDepartmentType:btnDepartment.titleLabel.text] forKey:@"support_type"]; //Suppot Type
    [dicParameter setObject:txtViewMessage.text forKey:@"message"]; //Message
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(ContactUsAPIResponse:) WithCallBackObject:self];
}

-(void)ContactUsAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        ThankYouVC *view=[[ThankYouVC alloc]init];
        view.strTitle=@"Contact Us";
        view.strConfirmationTitle=@"Done!";
        view.strMessage=@"Thank you for contacting us, we try our best to respond to all questions within 24 hours.";
        [self.navigationController pushViewController:view animated:NO];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Please try again later" inViewController:self];
        }
    }
    
}

-(BOOL)isValidation
{
    if ([btnDepartment.titleLabel.text isEqualToString:@"Department"])
    {
        [App_Delegate showAlertWithMessage:@"Please select a department." inViewController:self];
        return NO;
    }
    
    if ([btnIam.titleLabel.text isEqualToString:@"I am"])
    {
        [App_Delegate showAlertWithMessage:@"Please select your user type." inViewController:self];
        return NO;
    }
    
    if (txtFieldName.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter your name" inViewController:self];
        return NO;
    }
    
    if (txtFieldEmail.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter your email address" inViewController:self];
        return NO;
    }
    
    if (txtViewMessage.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter message" inViewController:self];
        return NO;
    }
    
    return YES;
}

#pragma mark- DropDown delegate

-(void)niDropDownDelegateMethod:(NIDropDown *)sender selected:(NSString *)strText
{
    if (sender.tag==101)
    {
        //Department
        [btnDepartment setTitle:strText forState:UIControlStateNormal];
    }
    else
    {
        //Iam
        [btnIam setTitle:strText forState:UIControlStateNormal];
    }
}

#pragma mark- Button Action

-(IBAction)btnSubmitPressed:(id)sender
{
    if ([self isValidation])
    {
        [self callContactUSAPI];
    }
}

-(IBAction)btnDepartmentPressed:(id)sender
{
    CGRect rect=btnDepartment.frame;
    
    NSArray *arrayName=[NSArray arrayWithObjects:@"Customer Service",@"Technical Support",@"Careers",@"Investor Relations",@"Business Partnerships",@"Press and Media",@"Legal Affairs",@"Other, not listed", nil];
    
    if (dropDown)
    {
        [dropDown removeFromSuperview];
    }
    
    CGFloat f = 40*arrayName.count;
    
    if(dropDown == nil)
    {
        dropDown = [[NIDropDown alloc]showDropDown:sender height:&f dataArray:arrayName imgArr:nil direction:@"down" inView:subViewScroll frame:rect];
        dropDown.tag=101;
        dropDown.delegate=self;
    }
    else
    {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }

}

-(IBAction)btnIAmPressed:(id)sender
{
    CGRect rect=btnIam.frame;
    
    NSArray *arrayName=[NSArray arrayWithObjects:@"Tourist",@"Local Guide",@"Other, not listed", nil];
    
    if (dropDown)
    {
        [dropDown removeFromSuperview];
    }
    
    CGFloat f = 120;
    
    if(dropDown == nil)
    {
        dropDown = [[NIDropDown alloc]showDropDown:sender height:&f dataArray:arrayName imgArr:nil direction:@"down" inView:subViewScroll frame:rect];
        dropDown.tag=102;
        dropDown.delegate=self;
    }
    else
    {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }

}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
