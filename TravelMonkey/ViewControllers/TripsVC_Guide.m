//
//  TripsVC_Guide.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/5/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "TripsVC_Guide.h"
#import "Parser.h"
#import "TripObject.h"
#import "UIImageView+WebCache.h"

#import "TripGuideNewRequestCell.h"
#import "TripGuideConfirmedRequestCell.h"
#import "SVPullToRefresh.h"
#import "NSDate+TimeAgo.h"

#import "PastTripNormalCell.h"
#import "PastTripDetailCell.h"
#import "TripDetailVC_Guide.h"
#import "ChatViewControllerVC_Guide.h"
#import "HomeVC_TouristProfile.h"


@interface TripsVC_Guide ()

@end

@implementation TripsVC_Guide

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    tblTrips.delegate=self;
    tblTrips.dataSource=self;
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    
    __weak TripsVC_Guide *weakSelf = self;
    
    // setup pull-to-refresh
    [tblTrips addPullToRefreshWithActionHandler:^{
        [weakSelf refreshTripTableView];
    }];
    
    // setup infinite scrolling
    //    [tblTrips addInfiniteScrollingWithActionHandler:^{
    //        [weakSelf refreshTripTableView];
    //    }];
    
    [tblTrips setHidden:YES];
    
    [self btnFutureTripsPressed:btnFutureTrips];
    
    [[NSNotificationCenter defaultCenter]addObserver:tblTrips selector:@selector(reloadData) name:kMessageUnreadReceivedNotification object:nil];
    
    
    
    //[App_Delegate showCustomAlert:@"Error" isSucess:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [refreshTimer invalidate];
}

#pragma mark- Methods

-(void)callStartTrip:(NSString*)strBookingID
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KStartTrip];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strBookingID forKey:@"booking_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(bookingRequestResponse:) WithCallBackObject:self];
}

-(void)callEndTrip:(NSString*)strBookingID
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KEndTrip];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strBookingID forKey:@"booking_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(bookingRequestResponse:) WithCallBackObject:self];
}

-(void)refreshTripTableView
{
    if (btnFutureTrips.selected)
    {
        [self fetchFutureTripsAPI:YES];
    }
    else
    {
        [self fetchPastTripsAPI:YES];
    }
}

-(void)refreshSilently
{
    if (btnFutureTrips.selected)
    {
        [self fetchFutureTripsAPI:NO];
    }
    else
    {
        [self fetchPastTripsAPI:NO];
    }
}

-(void)fetchPastTripsAPI:(BOOL)shouldShowHUD
{
    
    [refreshTimer invalidate];
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KGetPastBooking];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:App_Delegate.userObject.strUserType forKey:@"role"];
    [dicParameter setObject:@"1" forKey:@"page"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    if (shouldShowHUD)
    {
        [tblTrips setHidden:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(tripAPIResponse:) WithCallBackObject:self];
    }
    else
    {
        [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(tripAPIResponseBackground:) WithCallBackObject:self];
    }
}

-(void)fetchFutureTripsAPI:(BOOL)shouldShowHUD
{
    [refreshTimer invalidate];
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KGetFutureBooking];
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:App_Delegate.userObject.strUserType forKey:@"role"];
    [dicParameter setObject:@"1" forKey:@"page"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    if (shouldShowHUD)
    {
        [tblTrips setHidden:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(tripAPIResponse:) WithCallBackObject:self];
    }
    else
    {
        [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(tripAPIResponseBackground:) WithCallBackObject:self];
    }
}

-(void)tripAPIResponse:(NSDictionary *)dictionary
{
    
    aryFutureTrips=[[NSMutableArray alloc]init];
    aryPastTrips=[[NSMutableArray alloc]init];
    
    [tblTrips setHidden:NO];
    [tblTrips reloadData];
    refreshTimer=[NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(refreshSilently) userInfo:nil repeats:YES];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [tblTrips.pullToRefreshView stopAnimating];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        
        if ([dictionary objectForKey:@"booking_detail"] && [[dictionary objectForKey:@"booking_detail"] isKindOfClass:[NSArray class]])
        {
            NSArray *ary=[dictionary objectForKey:@"booking_detail"];
            
            if (ary.count>0)
            {
                isSuccess=YES;
                
                if (showFutureTrips)
                {
                    Parser *parser=[[Parser alloc]init];
                    aryFutureTrips=[parser getTripArray:ary];
                }
                else
                {
                    
                    Parser *parser=[[Parser alloc]init];
                    aryPastTrips=[parser getTripArray:ary];
                    
                    NSMutableArray *aryTemp=[[NSMutableArray alloc]init];
                    
                    for (TripObject *object in aryPastTrips)
                    {
                        if (![object.strStatus isEqualToString:@"NEW_REQUEST"])
                        {
                            [aryTemp addObject:object];
                        }
                    }
                    
                    aryPastTrips=aryTemp;
                    
                }
            }
        }
        
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate dropDownalertWithMessage:[dictionary objectForKey:@"message"]];
        }
        else
        {
            [App_Delegate dropDownalertWithMessage:@"You have no new booking requests"];
        }
    }
    
    //temp
    //    NSMutableArray *aryTemp=[[NSMutableArray alloc]init];
    //    TripObject *tripObj=[[TripObject alloc]init];
    //    [tripObj setStrStatus:@"ACCEPTED"];
    //    [aryTemp addObject:tripObj];
    //
    //    tripObj=[[TripObject alloc]init];
    //    [tripObj setStrStatus:@"GUIDING"];
    //    [aryTemp addObject:tripObj];
    //
    //
    //    tripObj=[[TripObject alloc]init];
    //    [tripObj setStrStatus:@"NEW_REQUEST"];
    //    [aryTemp addObject:tripObj];
    //
    //    tripObj=[[TripObject alloc]init];
    //    [tripObj setStrStatus:@""];
    //    [aryTemp addObject:tripObj];
    //
    //    aryFutureTrips=aryTemp;
    
    //        NSMutableArray *aryTemp=[[NSMutableArray alloc]init];
    //        TripObject *tripObj=[[TripObject alloc]init];
    //        [tripObj setStrStatus:@"TIMEOUT"];
    //        [aryTemp addObject:tripObj];
    //
    //        tripObj=[[TripObject alloc]init];
    //        [tripObj setStrStatus:@"DECLINED"];
    //        [aryTemp addObject:tripObj];
    //
    //
    //        tripObj=[[TripObject alloc]init];
    //        [tripObj setStrStatus:@"CANCELLED"];
    //        [aryTemp addObject:tripObj];
    //
    //        tripObj=[[TripObject alloc]init];
    //        [tripObj setStrStatus:@"COMPLETED"];
    //        [aryTemp addObject:tripObj];
    //
    //        tripObj=[[TripObject alloc]init];
    //        [tripObj setStrStatus:@""];
    //        [aryTemp addObject:tripObj];
    //    
    //        aryPastTrips=aryTemp;
    
    //-------------

    
    [tblTrips reloadData];
}


-(void)tripAPIResponseBackground:(NSDictionary *)dictionary
{
    
    aryFutureTrips=[[NSMutableArray alloc]init];
    aryPastTrips=[[NSMutableArray alloc]init];
    
    [tblTrips setHidden:NO];
    [tblTrips reloadData];
    refreshTimer=[NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(refreshSilently) userInfo:nil repeats:YES];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [tblTrips.pullToRefreshView stopAnimating];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        
        if ([dictionary objectForKey:@"booking_detail"] && [[dictionary objectForKey:@"booking_detail"] isKindOfClass:[NSArray class]])
        {
            
            NSArray *ary=[dictionary objectForKey:@"booking_detail"];
            
            if (ary.count>0)
            {
                isSuccess=YES;
                
                if (showFutureTrips)
                {
                    Parser *parser=[[Parser alloc]init];
                    aryFutureTrips=[parser getTripArray:ary];
                }
                else
                {
                    
                    Parser *parser=[[Parser alloc]init];
                    aryPastTrips=[parser getTripArray:ary];
                    
                    NSMutableArray *aryTemp=[[NSMutableArray alloc]init];
                    
                    for (TripObject *object in aryPastTrips)
                    {
                        if (![object.strStatus isEqualToString:@"NEW_REQUEST"])
                        {
                            [aryTemp addObject:object];
                        }
                    }
                    
                    aryPastTrips=aryTemp;
                    
                }
            }
        }
    }
    
    [tblTrips reloadData];
}

-(void)calltouristDetailAPIWithID:(NSString*)strTouristID
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KGetUserDetail];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strTouristID forKey:@"otheruser_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(userDetailAPIResponse:) WithCallBackObject:self];
    
}

-(void)userDetailAPIResponse:(NSDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        Parser *parser=[[Parser alloc]init];
        UserObject *guideObject=[parser getUserDetail:dictionary];
        
        HomeVC_TouristProfile *filter=[[HomeVC_TouristProfile alloc]init];
        filter.touristObject=guideObject;
        filter.isForViewProfile=YES;
        [self.navigationController pushViewController:filter animated:YES];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"User details not found" inViewController:self];
        }
    }
    
}

-(void)callAcceptBookingRequest:(NSString*)strBookingID
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KAcceptBookingRequest];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strBookingID forKey:@"booking_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(bookingRequestResponse:) WithCallBackObject:self];
}

-(void)callRejectBookingRequest:(NSString*)strBookingID
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KRejectBookingRequest];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strBookingID forKey:@"booking_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(bookingRequestResponse:) WithCallBackObject:self];
}

-(void)callCancelBookedBookingbyGuide:(NSString*)strBookingID
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KCancelBookedRequest];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strBookingID forKey:@"booking_id"];
    [dicParameter setObject:@"GUIDE" forKey:@"role"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(bookingRequestResponse:) WithCallBackObject:self];
}

-(void)bookingRequestResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        
        isSuccess=YES;
        [self refreshTripTableView];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Please try again, Server not responding" inViewController:self];
        }
    }
}



-(UILabel *)getCustomBackgroundLabel
{
    UILabel *messageLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,tblTrips.bounds.size.width,tblTrips.bounds.size.height)];
    messageLbl.text = KNoBookingDetailFound;
    messageLbl.textAlignment = NSTextAlignmentCenter;
    messageLbl.numberOfLines=0;
    messageLbl.font=[UIFont fontWithName:@"SourceSansPro-Regular" size:15.0];
    messageLbl.textColor=KAppColor_Blue;
    [messageLbl sizeToFit];
    
    return messageLbl;
}

-(void)addTapGuestureToImageView:(UIImageView*)imageView
{
    UITapGestureRecognizer *gesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewProfileButtonPressed:)];
    [imageView addGestureRecognizer:gesture];
}


#pragma mark- Actions

-(void)viewProfileButtonPressed:(UITapGestureRecognizer*)tapGesture
{
    UIImageView *imgView=(UIImageView*)[tapGesture view];
    
    TripObject *object=[aryFutureTrips objectAtIndex:imgView.tag];
    [self calltouristDetailAPIWithID:object.strTouristID];
}

-(void)acceptNewRequestButtonPressed:(UIButton*)button
{
    TripObject *object=[aryFutureTrips objectAtIndex:button.tag];
    [self callAcceptBookingRequest:object.strBookingId];
}

-(void)rejectNewRequestButtonPressed:(UIButton*)button
{
    TripObject *object=[aryFutureTrips objectAtIndex:button.tag];
    [self callRejectBookingRequest:object.strBookingId];
}

-(void)btnCancelBookedBookingPressed:(UIButton*)button
{
    
    TripObject *object=[aryFutureTrips objectAtIndex:button.tag];
    //[self callCancelBookedBookingbyGuide:object.strBookingId];
    currentObject=object;
    
    NSString *strMessage=[NSString stringWithFormat:@"Are you sure you want to cancel this booking?"];
    
    SCLAlertView *alert =[App_Delegate getAlertInstance];
    
    [alert addButton:@"No" actionBlock:^(void) {
        
    }];
    
    [alert addButton:@"Yes" actionBlock:^(void) {
        
        [self callCancelBookedBookingbyGuide:currentObject.strBookingId];
        
    }];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:strMessage closeButtonTitle:nil duration:0.0];
    
}

-(void)startTripByGuideButton:(UIButton*)button
{
    TripObject *object=[aryFutureTrips objectAtIndex:button.tag];
    [self callStartTrip:object.strBookingId];
}

-(void)endTripByGuide:(UIButton*)button
{
    TripObject *object=[aryFutureTrips objectAtIndex:button.tag];
    [self callEndTrip:object.strBookingId];
}

-(void)btnChatWithTouristPressed:(UIButton*)button
{
    TripObject *object=[aryFutureTrips objectAtIndex:button.tag];
    ChatViewControllerVC_Guide *chatVC=[[ChatViewControllerVC_Guide alloc]init];
    chatVC.strUserChatId=object.strUserChatID;
    chatVC.strBookingID=object.strBookingId;
    chatVC.strOtherName=[NSString stringWithFormat:@"%@",object.strFirstName];
    chatVC.strOtherImageUrl=object.strImageURL;
    chatVC.strOtherPlace=[NSString stringWithFormat:@"%@, %@",object.strCity,object.strCountry];
    [self.navigationController pushViewController:chatVC animated:YES];
    
    if ([App_Delegate.aryUnreadChat containsObject:object.strBookingId])
    {
        [App_Delegate.aryUnreadChat removeObject:object.strBookingId];
        [tblTrips reloadData];
    }
}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnFutureTripsPressed:(id)sender
{
    aryFutureTrips=[[NSMutableArray alloc]init];
    aryPastTrips=[[NSMutableArray alloc]init];
    [tblTrips reloadData];
    [self fetchFutureTripsAPI:YES];
    showFutureTrips=YES;
    [btnPastTrips setSelected:NO];
    [btnFutureTrips setSelected:YES];
}

-(IBAction)btnPastTripsPressed:(id)sender
{
    aryFutureTrips=[[NSMutableArray alloc]init];
    aryPastTrips=[[NSMutableArray alloc]init];
    [tblTrips reloadData];
    [self fetchPastTripsAPI:YES];
    showFutureTrips=NO;
    [btnPastTrips setSelected:YES];
    [btnFutureTrips setSelected:NO];
}

-(void)btnViewDetailsPressed:(UIButton*)button
{
    TripObject *object=[aryPastTrips objectAtIndex:button.tag];
    
    TripDetailVC_Guide *view=[[TripDetailVC_Guide alloc]init];
    view.strTripID=object.strBookingId;
    [self.navigationController pushViewController:view animated:YES];
}

#pragma mark- Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (showFutureTrips)
    {
        
        if (aryFutureTrips==nil || aryFutureTrips==(id)[NSNull null] || aryFutureTrips.count==0)
        {
            tblTrips.backgroundView = [self getCustomBackgroundLabel];
            return 0;
        }
        else
        {
            tblTrips.backgroundView=nil;
            return aryFutureTrips.count;
        }
    }
    else
    {
        
        if (aryPastTrips==nil || aryPastTrips==(id)[NSNull null] || aryPastTrips.count==0)
        {
            tblTrips.backgroundView = [self getCustomBackgroundLabel];
            return 0;
        }
        else
        {
            tblTrips.backgroundView=nil;
            return aryPastTrips.count;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (showFutureTrips)
    {
        return 140;
    }
    else
    {
        TripObject *object=[aryPastTrips objectAtIndex:indexPath.row];
        
        if ([object.strStatus isEqualToString:@"TIMEOUT"] || [object.strStatus isEqualToString:@"DECLINED"] || [object.strStatus isEqualToString:@"CANCELLED"]) //Rejected
        {
            return 85;
        }
        else if ([object.strStatus isEqualToString:@"COMPLETED"]) //Comepleted
        {
            return 140;
        }
        else //Cancelled
        {
            return 140;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (showFutureTrips)
    {
        TripObject *object=[aryFutureTrips objectAtIndex:indexPath.row];
        
        if ([object.strStatus isEqualToString:@"ACCEPTED"]) //Trip accepted
        {
            NSString *strCellIdentifier=@"TripGuideConfirmedRequestCell";
            
            TripGuideConfirmedRequestCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
            
            if (cell==nil)
            {
                cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            //Image
            [cell.imgUserProfile setClipsToBounds:YES];
            [cell.imgUserProfile.layer setCornerRadius:20];
            [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:object.strImageURL]];
            
            [cell.imgCurrentStatus setImage:[UIImage imageNamed:@"icn_accepted.png"]];
            
            //Name
            NSString *strUserName=@"";
            
            if (object.strFirstName!=nil && object.strFirstName!=(id)[NSNull null])
            {
                strUserName=[strUserName stringByAppendingString:object.strFirstName];
                [cell.lblName setText:strUserName];
            }
            
            if (object.strLastName!=nil && object.strLastName!=(id)[NSNull null])
            {
                //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",object.strLastName]];
                //[cell.lblName setText:strUserName];
            }
            
            //Location
            NSString *strLocation=@"";
            
            if(object.strCity!=nil && object.strCity!=(id)[NSNull null] && object.strCity.length>0)
            {
                strLocation=[strLocation stringByAppendingString:object.strCity];
                [cell.lblPlace setText:strLocation];
            }
            
            if(object.strCountry!=nil && object.strCountry!=(id)[NSNull null] && object.strCountry.length>0)
            {
                strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",object.strCountry]];
                [cell.lblPlace setText:strLocation];
            }
            
            //Date
            NSDateFormatter *formatter=[NSDateFormatter new];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            
            //If startTime is less than 5 mins then only enable the Start trip button -- CONDITION
            if ([App_Delegate getMinutesDifferenceBetweenStartDate:[formatter stringFromDate:[NSDate date]] endDate:object.strStartTime withFormaterStyle:formatter.dateFormat]<2)
            {
                [cell.btnStartTrip setUserInteractionEnabled:YES];
                [cell.btnStartTrip setTitleColor:KAppColor_Blue forState:UIControlStateNormal];
                [cell.lblMessage setHidden:YES];
            }
            else
            {
                [cell.btnStartTrip setUserInteractionEnabled:NO];
                [cell.btnStartTrip setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                [cell.lblMessage setHidden:NO];
                
                //herePuru
                int remianingMinutes=[App_Delegate getMinutesDifferenceBetweenStartDate:[formatter stringFromDate:[NSDate date]] endDate:object.strStartTime withFormaterStyle:formatter.dateFormat];
                
                if (remianingMinutes<60)
                {
                    [cell.lblMessage setText:[NSString stringWithFormat:@"START TRIP button will get automatically enable 2 mins before Trip start time. (remaining %d minutes)",remianingMinutes]];
                }
                else
                {
                    
                    int hour=remianingMinutes/60;
                    int min=remianingMinutes%60;
                    
                    if (hour<2)
                    {
                        [cell.lblMessage setText:[NSString stringWithFormat:@"START TRIP button will get automatically enable 2 mins before Trip start time. (remaining %d hour %d minutes)",hour,min]];
                    }
                    else
                    {
                        [cell.lblMessage setText:[NSString stringWithFormat:@"START TRIP button will get automatically enable 2 mins before Trip start time. (remaining %d hours %d minutes)",hour,min]];
                    }
                    
                }
            }
            
            NSDate *startTime=[formatter dateFromString:object.strStartTime];
            NSDate *endTime=[formatter dateFromString:object.strEndTime];
            [formatter setDateFormat:@"MMM dd, yyyy"];
            [cell.lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
            
            //Time
            [formatter setDateFormat:@"hh:mm a"];
            NSString *strTime=[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:startTime],[formatter stringFromDate:endTime]];
            [cell.lblTime setText:strTime];
            
            //Status
            [cell.lblCurrentStatus setText:object.strStatus];
            
            cell.btnStartTrip.tag=cell.btnRejectRequest.tag=cell.btnChat.tag=indexPath.row; //Assiging tag to buttons
            
            [cell.btnStartTrip addTarget:self action:@selector(startTripByGuideButton:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnRejectRequest addTarget:self action:@selector(btnCancelBookedBookingPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnChat addTarget:self action:@selector(btnChatWithTouristPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            //UnreadMessage
            if ([App_Delegate.aryUnreadChat containsObject:object.strBookingId])
            {
                [cell.imgNewChatMessage setHidden:NO];
            }
            else
            {
                [cell.imgNewChatMessage setHidden:YES];
            }
            
            //TapToViewProfile
            [cell.lblViewProfile setHidden:NO];
            cell.imgUserProfile.tag=indexPath.row;
            [cell.imgUserProfile setUserInteractionEnabled:YES];
            [self addTapGuestureToImageView:cell.imgUserProfile];
            
            return cell;
            
        }
        else if ([object.strStatus isEqualToString:@"GUIDING"])
        {
            
            //Guiding
            NSString *strCellIdentifier=@"TripGuideNewRequestCell";
            
            TripGuideNewRequestCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
            
            if (cell==nil)
            {
                cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            //Image
            [cell.imgUserProfile setClipsToBounds:YES];
            [cell.imgUserProfile.layer setCornerRadius:20];
            [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:object.strImageURL]];
            
            //Set image
            [cell.imgCurrentStatus setImage:[UIImage imageNamed:@"icn_guiding"]];
            
            //Name
            NSString *strUserName=@"";
            
            if (object.strFirstName!=nil && object.strFirstName!=(id)[NSNull null])
            {
                strUserName=[strUserName stringByAppendingString:object.strFirstName];
                [cell.lblName setText:strUserName];
            }
            
            if (object.strLastName!=nil && object.strLastName!=(id)[NSNull null])
            {
                //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",object.strLastName]];
                //[cell.lblName setText:strUserName];
            }
            
            //Location
            NSString *strLocation=@"";
            
            if(object.strCity!=nil && object.strCity!=(id)[NSNull null] && object.strCity.length>0)
            {
                strLocation=[strLocation stringByAppendingString:object.strCity];
                [cell.lblPlace setText:strLocation];
            }
            
            if(object.strCountry!=nil && object.strCountry!=(id)[NSNull null] && object.strCountry.length>0)
            {
                strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",object.strCountry]];
                [cell.lblPlace setText:strLocation];
            }
            
            //Date
            NSDateFormatter *formatter=[NSDateFormatter new];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            
            NSDate *startTime=[formatter dateFromString:object.strStartTime];
            NSDate *endTime=[formatter dateFromString:object.strEndTime];
            [formatter setDateFormat:@"MMM dd, yyyy"];
            [cell.lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
            
            //Time
            [formatter setDateFormat:@"hh:mm a"];
            NSString *strTime=[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:startTime],[formatter stringFromDate:endTime]];
            [cell.lblTime setText:strTime];
            
            //Status
            [cell.lblCurrentStatus setText:object.strStatus];
            
            //Time ago
            formatter=[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            NSDate *date=[formatter dateFromString:object.strStartTime];
            
            if (date!=nil)
            {
                strTime=[date timeAgo];
            }
            
            NSString *strCompletedTime=@"";
            
            float completedMinutes=[App_Delegate getMinutesDifferenceBetweenStartDate:object.strStartTime endDate:[formatter stringFromDate:[NSDate date]] withFormaterStyle:@"YYYY-MM-dd HH:mm:ss"];
            
            strCompletedTime=[NSString stringWithFormat:@"%d",(int)completedMinutes];
            
            if (completedMinutes<0)
            {
                [cell.btnAcceptRequest setTitle:@"Starting" forState:UIControlStateNormal];
            }
            else
            {
                [cell.btnAcceptRequest setTitle:[NSString stringWithFormat:@"%@:00",strCompletedTime] forState:UIControlStateNormal];
            }
            
            [cell.btnAcceptRequest setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
            [cell.btnRejectRequest setTitle:@"End Trip" forState:UIControlStateNormal];
            
            [cell.lblRemainingTime setHidden:YES];
            
            cell.btnAcceptRequest.tag=cell.btnRejectRequest.tag=indexPath.row; //Assiging tag to buttons
            //[cell.btnAcceptRequest addTarget:self action:@selector(acceptNewRequestButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnRejectRequest addTarget:self action:@selector(endTripByGuide:) forControlEvents:UIControlEventTouchUpInside];
            
            //TapToViewProfile
            [cell.lblViewProfile setHidden:NO];
            cell.imgUserProfile.tag=indexPath.row;
            [cell.imgUserProfile setUserInteractionEnabled:YES];
            [self addTapGuestureToImageView:cell.imgUserProfile];
            
            return cell;
        }
        else
        {
            
            //New Request
            NSString *strCellIdentifier=@"TripGuideNewRequestCell";
            
            TripGuideNewRequestCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
            
            if (cell==nil)
            {
                cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            //Image
            [cell.imgUserProfile setClipsToBounds:YES];
            [cell.imgUserProfile.layer setCornerRadius:20];
            [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:object.strImageURL]];
            
            //Name
            NSString *strUserName=@"";
            
            if (object.strFirstName!=nil && object.strFirstName!=(id)[NSNull null])
            {
                strUserName=[strUserName stringByAppendingString:object.strFirstName];
                [cell.lblName setText:strUserName];
            }
            
            if (object.strLastName!=nil && object.strLastName!=(id)[NSNull null])
            {
                //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",object.strLastName]];
                //[cell.lblName setText:strUserName];
            }
            
            //Location
            NSString *strLocation=@"";
            
            if(object.strCity!=nil && object.strCity!=(id)[NSNull null] && object.strCity.length>0)
            {
                strLocation=[strLocation stringByAppendingString:object.strCity];
                [cell.lblPlace setText:strLocation];
            }
            
            if(object.strCountry!=nil && object.strCountry!=(id)[NSNull null] && object.strCountry.length>0)
            {
                strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",object.strCountry]];
                [cell.lblPlace setText:strLocation];
            }
            
            //Date
            NSDateFormatter *formatter=[NSDateFormatter new];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            
            NSDate *startTime=[formatter dateFromString:object.strStartTime];
            NSDate *endTime=[formatter dateFromString:object.strEndTime];
            [formatter setDateFormat:@"MMM dd, yyyy"];
            [cell.lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
            
            //Time
            [formatter setDateFormat:@"hh:mm a"];
            NSString *strTime=[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:startTime],[formatter stringFromDate:endTime]];
            [cell.lblTime setText:strTime];
            
            //Status
            [cell.lblCurrentStatus setText:object.strStatus];
            
            //Time ago
            formatter=[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            NSDate *date=[formatter dateFromString:object.strStartTime];
            
            if (date!=nil)
            {
                strTime=[date timeAgo];
            }
            
            float totalMinutes=[App_Delegate getMinutesDifferenceBetweenStartDate:object.strBookingTime endDate:object.strStartTime withFormaterStyle:@"YYYY-MM-dd HH:mm:ss"];
            
            
            if (totalMinutes<60*24)
            {
                
                //Same day booking
                float remainingMinutes=[App_Delegate getMinutesDifferenceBetweenStartDate:object.strBookingTime endDate:[formatter stringFromDate:[NSDate date]] withFormaterStyle:@"YYYY-MM-dd HH:mm:ss"];
                
                remainingMinutes=20-remainingMinutes;
                
                if (remainingMinutes<2)
                {
                    [cell.lblRemainingTime setText:[NSString stringWithFormat:@"Remaining time to respond request %d Minute",(int)remainingMinutes]];
                }
                else
                {
                    [cell.lblRemainingTime setText:[NSString stringWithFormat:@"Remaining time to respond request %d Minutes",(int)remainingMinutes]];
                }
                
            }
            else
            {
                
                //Booking before 12 hours
                float remainingMinutes=[App_Delegate getMinutesDifferenceBetweenStartDate:object.strBookingTime endDate:[formatter stringFromDate:[NSDate date]] withFormaterStyle:@"YYYY-MM-dd HH:mm:ss"];
                
                remainingMinutes=12*60-remainingMinutes;
                
                int hour=remainingMinutes/60;
                int minute=(int)remainingMinutes%60;
                
                
                if (minute>0)
                {
                    if (hour==1)
                    {
                        [cell.lblRemainingTime setText:[NSString stringWithFormat:@"Remaining time to respond request %d hour %d minutes",hour,minute]];
                    }
                    else
                    {
                        [cell.lblRemainingTime setText:[NSString stringWithFormat:@"Remaining time to respond request %d hours %d minutes",hour,minute]];
                    }
                }
                else
                {
                    if (hour==1)
                    {
                        [cell.lblRemainingTime setText:[NSString stringWithFormat:@"Remaining time to respond request %d hour",hour]];
                    }
                    else
                    {
                        [cell.lblRemainingTime setText:[NSString stringWithFormat:@"Remaining time to respond request  %d hours",hour]];
                    }
                }
            }
            
            cell.btnAcceptRequest.tag=cell.btnRejectRequest.tag=cell.imgUserProfile.tag=indexPath.row; //Assiging tag to buttons
            [cell.btnAcceptRequest addTarget:self action:@selector(acceptNewRequestButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnRejectRequest addTarget:self action:@selector(rejectNewRequestButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            //TapToViewProfile
            [cell.lblViewProfile setHidden:NO];
            cell.imgUserProfile.tag=indexPath.row;
            [cell.imgUserProfile setUserInteractionEnabled:YES];
            [self addTapGuestureToImageView:cell.imgUserProfile];
            
            return cell;
            
        }
        
    }
    else
    {
        
        TripObject *object=[aryPastTrips objectAtIndex:indexPath.row];
        
        if ([object.strStatus isEqualToString:@"TIMEOUT"] || [object.strStatus isEqualToString:@"DECLINED"] || [object.strStatus isEqualToString:@"CANCELLED"]) //Rejected
        {
            
            NSString *strCellIdentifier=@"PastTripNormalCell";
            
            PastTripNormalCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
            
            if (cell==nil)
            {
                cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            //Image
            [cell.imgUserProfile setClipsToBounds:YES];
            [cell.imgUserProfile.layer setCornerRadius:20];
            [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:object.strImageURL]];
            
            //Name
            NSString *strUserName=@"";
            
            if (object.strFirstName!=nil && object.strFirstName!=(id)[NSNull null])
            {
                strUserName=[strUserName stringByAppendingString:object.strFirstName];
                [cell.lblName setText:strUserName];
            }
            
            if (object.strLastName!=nil && object.strLastName!=(id)[NSNull null])
            {
                //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",object.strLastName]];
                //[cell.lblName setText:strUserName];
            }
            
            //Location
            NSString *strLocation=@"";
            
            if(object.strCity!=nil && object.strCity!=(id)[NSNull null] && object.strCity.length>0)
            {
                strLocation=[strLocation stringByAppendingString:object.strCity];
                [cell.lblPlace setText:strLocation];
            }
            
            if(object.strCountry!=nil && object.strCountry!=(id)[NSNull null] && object.strCountry.length>0)
            {
                strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",object.strCountry]];
                [cell.lblPlace setText:strLocation];
            }
            
            //Date
            NSDateFormatter *formatter=[NSDateFormatter new];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            
            NSDate *startTime=[formatter dateFromString:object.strStartTime];
            NSDate *endTime=[formatter dateFromString:object.strEndTime];
            [formatter setDateFormat:@"MMM dd, yyyy"];
            [cell.lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
            
            
            //Time
            [formatter setDateFormat:@"hh:mm a"];
            NSString *strTime=[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:startTime],[formatter stringFromDate:endTime]];
            [cell.lblTime setText:strTime];
            
            if ([object.strStatus isEqualToString:@"TIMEOUT"])
            {
                [cell.lblCurrentStatus setText:@"NOT\nRESPONDED"];
                [cell.imgCurrentStatus setImage:[UIImage imageNamed:@"icn_pending.png"]];
            }
            else if ([object.strStatus isEqualToString:@"CANCELLED"])
            {
                [cell.lblCurrentStatus setText:object.strStatus];
                [cell.imgCurrentStatus setImage:[UIImage imageNamed:@"icn_canceled.png"]];
            }
            else
            {
                [cell.lblCurrentStatus setText:object.strStatus];
                [cell.imgCurrentStatus setImage:[UIImage imageNamed:@"icn_declined.png"]];
            }
            
            return cell;
            
        }
        else if ([object.strStatus isEqualToString:@"COMPLETED"]) //Comepleted
        {
            
            NSString *strCellIdentifier=@"PastTripDetailCell";
            
            PastTripDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
            
            if (cell==nil)
            {
                cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            //Image
            [cell.imgUserProfile setClipsToBounds:YES];
            [cell.imgUserProfile.layer setCornerRadius:20];
            [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:object.strImageURL]];
            
            //Name
            NSString *strUserName=@"";
            
            if (object.strFirstName!=nil && object.strFirstName!=(id)[NSNull null])
            {
                strUserName=[strUserName stringByAppendingString:object.strFirstName];
                [cell.lblName setText:strUserName];
            }
            
            if (object.strLastName!=nil && object.strLastName!=(id)[NSNull null])
            {
                //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",object.strLastName]];
               //[cell.lblName setText:strUserName];
            }
        
            //Location
            NSString *strLocation=@"";
            
            if(object.strCity!=nil && object.strCity!=(id)[NSNull null] && object.strCity.length>0)
            {
                strLocation=[strLocation stringByAppendingString:object.strCity];
                [cell.lblPlace setText:strLocation];
            }
            
            if(object.strCountry!=nil && object.strCountry!=(id)[NSNull null] && object.strCountry.length>0)
            {
                strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",object.strCountry]];
                [cell.lblPlace setText:strLocation];
            }
            
            //Date
            NSDateFormatter *formatter=[NSDateFormatter new];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            
            NSDate *startTime=[formatter dateFromString:object.strStartTime];
            NSDate *endTime=[formatter dateFromString:object.strEndTime];
            [formatter setDateFormat:@"MMM dd, yyyy"];
            [cell.lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
            
            //Time
            [formatter setDateFormat:@"hh:mm a"];
            NSString *strTime=[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:startTime],[formatter stringFromDate:endTime]];
            [cell.lblTime setText:strTime];
            
            //Status
            [cell.lblCurrentStatus setText:object.strStatus];
            [cell.imgCurrentStatus setImage:[UIImage imageNamed:@"icn_completed.png"]];
            cell.btnViewTripDetail.tag=indexPath.row;
            [cell.btnViewTripDetail addTarget:self action:@selector(btnViewDetailsPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            
        }
        else //Cancelled
        {
            
            NSString *strCellIdentifier=@"PastTripDetailCell";
            
            PastTripDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
            
            if (cell==nil)
            {
                cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            //Image
            [cell.imgUserProfile setClipsToBounds:YES];
            [cell.imgUserProfile.layer setCornerRadius:20];
            [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:object.strImageURL]];
            
            //Name
            NSString *strUserName=@"";
            
            if (object.strFirstName!=nil && object.strFirstName!=(id)[NSNull null])
            {
                strUserName=[strUserName stringByAppendingString:object.strFirstName];
                [cell.lblName setText:strUserName];
            }
            
            if (object.strLastName!=nil && object.strLastName!=(id)[NSNull null])
            {
                //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",object.strLastName]];
                //[cell.lblName setText:strUserName];
            }
            
            //Location
            NSString *strLocation=@"";
            
            if(object.strCity!=nil && object.strCity!=(id)[NSNull null] && object.strCity.length>0)
            {
                strLocation=[strLocation stringByAppendingString:object.strCity];
                [cell.lblPlace setText:strLocation];
            }
            
            if(object.strCountry!=nil && object.strCountry!=(id)[NSNull null] && object.strCountry.length>0)
            {
                strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",object.strCountry]];
                [cell.lblPlace setText:strLocation];
            }
            
            //Date
            NSDateFormatter *formatter=[NSDateFormatter new];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            
            NSDate *startTime=[formatter dateFromString:object.strStartTime];
            NSDate *endTime=[formatter dateFromString:object.strEndTime];
            [formatter setDateFormat:@"MMM dd, yyyy"];
            [cell.lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
            
            
            //Time
            [formatter setDateFormat:@"hh:mm a"];
            NSString *strTime=[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:startTime],[formatter stringFromDate:endTime]];
            [cell.lblTime setText:strTime];
            
            //Status
            [cell.lblCurrentStatus setText:object.strStatus];
            
            [cell.btnViewTripDetail setHidden:YES]; //temp
            
            return cell;
        }
    
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark- AlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==7800)
    {
        //Guest Alert
        if (buttonIndex==1)
        {
            [self callCancelBookedBookingbyGuide:currentObject.strBookingId];
        }
    }
}



@end

