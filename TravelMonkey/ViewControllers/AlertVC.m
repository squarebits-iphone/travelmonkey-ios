//
//  AlertVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 1/27/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "AlertVC.h"

@interface AlertVC ()

@end

@implementation AlertVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [lblTitle setText:self.strTitle];
    [txtViewMessage setText:self.strMessage];
    [btnDone.layer setCornerRadius:4.0];
    [btnDone setClipsToBounds:YES];
    
    if (self.strDoneBtnTitle)
    {
        [btnDone setTitle:self.strDoneBtnTitle forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self adjustContentSize:txtViewMessage];
}

#pragma mark- Methods

-(void)adjustContentSize:(UITextView*)tv
{
    CGFloat deadSpace = ([tv bounds].size.height - [tv contentSize].height);
    CGFloat inset = MAX(0, deadSpace/2.0);
    tv.contentInset = UIEdgeInsetsMake(inset, tv.contentInset.left, inset, tv.contentInset.right);
}

#pragma mark- Button Action

-(IBAction)btnClosePressed:(id)sender
{
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

@end
