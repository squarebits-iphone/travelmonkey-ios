//
//  InviteFriendsVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/12/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>


@interface InviteFriendsVC : UIViewController
{
    IBOutlet UIView *viewStatusBar;
    IBOutlet UIView *viewTitle;
    
    NSMutableArray *aryOptions;
    MFMailComposeViewController *mailComposer;
    UIDocumentInteractionController *documentController;
    
}

@end
