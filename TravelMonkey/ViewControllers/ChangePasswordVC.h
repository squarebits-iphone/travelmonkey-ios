//
//  ChangePasswordVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/1/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordVC : UIViewController
{
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    
    
    IBOutlet UITextField *txtfieldCurrentPassword;
    IBOutlet UITextField *txtfieldNewPassword;
    IBOutlet UITextField *txtfieldConfirmPassword;
    
}

@end
