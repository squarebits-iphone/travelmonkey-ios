//
//  AddPhotosVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/1/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddPhotosVC : UIViewController
{
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UICollectionView *collectionViewTable;
    UIImage *chosenImage;
    
    //SubView
    IBOutlet UIView *viewAddImage;
    IBOutlet UIImageView *imgSelectedImage;
    IBOutlet UITextView *txtViewCaption;
    
    NSMutableArray *arySelectedPhotoId;
    IBOutlet UIButton *btnAddDeletePhotos;
    
}

@end
