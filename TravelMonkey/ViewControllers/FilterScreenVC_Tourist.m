//
//  FilterScreenVC_Tourist.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/29/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "FilterScreenVC_Tourist.h"
#import "AddLanguageCell.h"
#import "LanguageCell.h"
#import "LanguageObject.h"
#import "PSelectionView.h"



@interface FilterScreenVC_Tourist ()

@end

@implementation FilterScreenVC_Tourist

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Language level
    aryLanguageLevel=[[NSMutableArray alloc]initWithObjects:@"Basic",@"Intermediate",@"Fluent", nil];
    
    //Language options
    aryLanguage=[[NSMutableArray alloc]init];
    arySelectedLanguage=[[NSMutableArray alloc]init];
    
    [lblAge setTextColor:KAppColor_Blue];
    
    NSString *strPlistPath=[[NSBundle mainBundle] pathForResource:@"LanguageList" ofType:@"plist"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:strPlistPath])
    {
        aryLanguage= [[NSArray arrayWithContentsOfFile:strPlistPath] mutableCopy];
    }
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    [self configureLabelSlider];
    [self updateSliderLabels];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.viewDeckController setPanningMode:IIViewDeckNoPanning];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.view addSubview:scrollviewMain];
    [scrollviewMain setFrame:CGRectMake(0, 80,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-80)];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.viewDeckController setPanningMode:IIViewDeckFullViewPanning];
}

-(void)viewDidLayoutSubviews
{
    [labelSlider layoutSubviews];
    [constraintTableHeight setConstant:(arySelectedLanguage.count*56)+60+20];
    [scrollviewMain setContentSize:CGSizeMake(0,tblLanguage.frame.origin.y+constraintTableHeight.constant)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Methods

- (void) configureLabelSlider
{
    
    labelSlider.minimumValue = 0;
    labelSlider.maximumValue = 37;
    
    labelSlider.lowerValue = 0;
    labelSlider.upperValue = 37;
    
    labelSlider.minimumRange = 0;
    
    
    UIImage* image = nil;
    
    image = [UIImage imageNamed:@"img_slider.png"];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 5.0)];
    labelSlider.trackBackgroundImage = image;
    [distanceSlider setMaximumTrackImage:image forState:UIControlStateNormal];
    
    image = [UIImage imageNamed:@"img_slider2.png"];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 7.0, 0.0, 7.0)];
    labelSlider.trackImage = image;
    [distanceSlider setMinimumTrackImage:image forState:UIControlStateNormal];
    
    image = [UIImage imageNamed:@"circle_slider.png"];
    image = [image imageWithAlignmentRectInsets:UIEdgeInsetsMake(-1, 0, 1, 0)];
    labelSlider.lowerHandleImageNormal = image;
    labelSlider.lowerHandleImageHighlighted = image;
    
    image = [UIImage imageNamed:@"circle_slider.png"];
    image = [image imageWithAlignmentRectInsets:UIEdgeInsetsMake(-1,0, 1, 0)];
    labelSlider.upperHandleImageNormal = image;
    labelSlider.upperHandleImageHighlighted = image;
    
    [distanceSlider setThumbImage:image forState:UIControlStateNormal];
    [distanceSlider setThumbImage:image forState:UIControlStateHighlighted];
    
}

- (void) updateSliderLabels
{
    
    // You get the center point of the slider handles and use this to arrange other subviews
    CGPoint lowerCenter;
    lowerCenter.x = (labelSlider.lowerCenter.x + labelSlider.frame.origin.x);
    lowerCenter.y = (labelSlider.center.y - 30.0f);
    self.lowerLabel.center = lowerCenter;
    
    CGPoint upperCenter;
    upperCenter.x = (labelSlider.upperCenter.x + labelSlider.frame.origin.x);
    upperCenter.y = (labelSlider.center.y - 30.0f);
    self.upperLabel.center = upperCenter;
    
    [lblAge setText:[NSString stringWithFormat:@"%d - %d",(int)labelSlider.lowerValue+18,(int)labelSlider.upperValue+18]];
    
    [self.view setNeedsLayout];
    
}

// Handle control value changed events just like a normal slider
- (IBAction)labelSliderChanged:(NMRangeSlider*)sender
{
    [self updateSliderLabels];
}

#pragma mark- Button Action

-(IBAction)btnSavePressed
{
    NSMutableDictionary *dicOptions=[[NSMutableDictionary alloc]init];
    
    //Gender
    if (btnAny.selected==NO)
    {
        if (btnMale.selected==YES)
        {
            [dicOptions setObject:@"M" forKey:@"gender"];
        }
        else
        {
            [dicOptions setObject:@"F" forKey:@"gender"];
        }
    }
    
    //Age
    [dicOptions setObject:[NSString stringWithFormat:@"%d-%d",(int)labelSlider.lowerValue+18,(int)labelSlider.upperValue+18] forKey:@"age"];
    
    //Distance
    [dicOptions setObject:[NSString stringWithFormat:@"%f",distanceSlider.value] forKey:@"distance"];
    
    //Language
    if (arySelectedLanguage.count>0)
    {
        NSMutableArray *arrayLanguage=[[NSMutableArray alloc]init];
        
        for (NSString *strLanguage in arySelectedLanguage)
        {
            NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
            [dictionary setValue:[NSString stringWithFormat:@"%@",strLanguage] forKey:@"Language"];
            [dictionary setValue:[NSString stringWithFormat:@"%@",strLanguage] forKey:@"type"];
            [arrayLanguage addObject:dictionary];
        }
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrayLanguage options:NSJSONWritingPrettyPrinted error:nil];
        
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        [dicOptions setObject:jsonString forKey:@"language"];
    }
    
    //Review
    if (btnWithLocalYes.selected==YES)
    {
        [dicOptions setObject:@"Y" forKey:@"review"];
    }
    else
    {
        [dicOptions setObject:@"N" forKey:@"review"];
    }
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"filterSearch" object:dicOptions];
    [self btnBackPressed:nil];
    
}

-(IBAction)btnSelectGenderPressed:(UIButton*)sender
{
    btnAny.selected=btnMale.selected=btnFemale.selected=NO;
    [sender setSelected:YES];
    
}

-(IBAction)btnSelectLocalWithReviewPressed:(UIButton*)sender
{
    btnWithLocalNo.selected=btnWithLocalYes.selected=NO;
    [sender setSelected:YES];
}

-(IBAction)btnSortingPressed:(UIButton*)sender
{
    btnH2L.selected=btnL2H.selected=NO;
    [sender setSelected:YES];
}

- (IBAction)btnAddLanguagePressed:(UIButton *)sender
{
    PSelectionView *selectionView=[[PSelectionView alloc]init];
    selectionView.pageTitle=@"Select Language";
    selectionView.arrayData=aryLanguage;
    selectionView.arraySelectedData=arySelectedLanguage;
    selectionView.viewColor=KAppColor;
    selectionView.delegate=(id)self;
    [self presentViewController:selectionView animated:YES completion:nil];
    
}

-(void)btnChangeLanguagePressed:(UIButton*)button
{
    isForChangeSelection=YES;
    isChangeLanguage=YES;
    currentIndex=(int)button.tag;
    
    PSelectionView *selectionView=[[PSelectionView alloc]init];
    selectionView.pageTitle=@"Select Language";
    selectionView.arrayData=aryLanguage;
    selectionView.viewColor=KAppColor;
    selectionView.delegate=(id)self;
    [self presentViewController:selectionView animated:YES completion:nil];
}

-(void)btnChangeLanguageLevelPressed:(UIButton*)button
{
    isForChangeSelection=YES;
    isChangeLanguage=NO;
    currentIndex=(int)button.tag;
    
    PSelectionView *selectionView=[[PSelectionView alloc]init];
    selectionView.pageTitle=@"Select Level";
    selectionView.arrayData=aryLanguageLevel;
    selectionView.viewColor=KAppColor;
    selectionView.delegate=(id)self;
    [self presentViewController:selectionView animated:YES completion:nil];
}


-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- PSelectionViewDelegate



-(void)userSelectedArray:(NSMutableArray*)arySelected
{
    arySelectedLanguage=arySelected;
}


#pragma mark- Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *strCellIdentifier=@"AddLanguageCell";
    
    AddLanguageCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
    
    if (cell==nil)
    {
        cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    [cell.lblLabel setTitle:@"Select languages" forState:UIControlStateNormal];
    [cell.lblLabel setTitleColor:KAppColor_Blue forState:UIControlStateNormal];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self btnAddLanguagePressed:nil];
}



@end
