//
//  SignUpVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "SignUpVC.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "UNIRest.h"
#import "Parser.h"
#import "UserObject.h"
#import "VerifyEmailVC.h"


@interface SignUpVC ()

@end

@implementation SignUpVC
@synthesize btnGuide,btnSignUp,btnTourist,txtFieldEmail,txtFieldLastName,txtFieldPassword,txtFieldFirstName,txtFieldConfirmPassword,socialObject,imgtxtFieldPassword,imgtxtFieldConfirmPass;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    //Setup View if the page is pushed from social login
    if (socialObject)
    {
        [self setupPageForSocialSignUp];
    }
    
    //Add tap gesture to image
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(editUserImage)];
    [imgUserProfile addGestureRecognizer:tapGesture];
    
    [imgUserProfile.layer setCornerRadius:30];
    [imgUserProfile setClipsToBounds:YES];
    strUserImageId=@"";
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    //[self btnTouristPressed:btnTourist];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [constScrollViewBottomSpace setConstant:[UIScreen mainScreen].bounds.size.height-140];
}

-(void)viewDidAppear:(BOOL)animated
{
    [scrollviewMain setContentSize:CGSizeMake(0, txtFieldConfirmPassword.frame.origin.y+txtFieldConfirmPassword.frame.size.height+50+btnSignUp.frame.size.height)];
    
    [self startWaterfallAnimation];
}

#pragma mark- Method

-(void)startWaterfallAnimation
{
    [constScrollViewBottomSpace setConstant:0];
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded];
                         
                     }];
}


-(void)uploadImageUnirest //This will upload image to the
{
    
    //Image path
    NSString *fileImagePath=[NSString stringWithFormat:@"%@/Documents/profilepic.jpeg",NSHomeDirectory()];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    //Url
    NSString *url=[NSString stringWithFormat:@"%@%@",KBaseURL,KUploadProfilePic];
    url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //Check network
    if (networkStatus==NotReachable)
    {
        [App_Delegate showAlertWithMessage:@"No internet connection." inViewController:self];
    }
    else
    {
        
        //Show loading
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        __block NSDictionary *dicResponse;
        
        //Content-Type: application/json
        NSMutableDictionary *headers =[[NSMutableDictionary alloc]init];
        [headers setObject:@"application/json" forKey:@"Content-Type"];
        [headers setObject:@"utf-8" forKey:@"Accept-Encoding"];
        
        NSDictionary* parameters = @{@"type" : @"Profile", @"file": [NSURL fileURLWithPath:fileImagePath]};
        
        //Initializing Async request
        [[UNIRest post:^(UNISimpleRequest* request)
          {
              [request setUrl:url];
              [request setParameters:parameters];
              
          }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
         {
             
             //Fetching resposne
             NSData *rawBody = response.rawBody;
             NSString *strResponse = [[NSString alloc] initWithData:rawBody encoding:NSUTF8StringEncoding];
             NSLog(@"Response %@",strResponse);
             
             dicResponse=response.body.object;
             
             if (dicResponse==nil)
             {
                 dicResponse=[response.body.array objectAtIndex:0];
             }
             
             [self uploadProfilePicAPIResponse:dicResponse];
             
         }];
    }
    
}


-(void)uploadProfilePicAPIResponse:(NSDictionary*)response
{
    //Calling main thread
    dispatch_async(dispatch_get_main_queue(), ^()
                   {
                       //All UI related change can be done here.
                       [MBProgressHUD hideHUDForView:self.view animated:YES];
                       
                       NSLog(@"Upload Profile pic : %@",response);
                       
                       if (response!=nil && [response objectForKey:@"image_id"])
                       {
                           strUserImageId=[response objectForKey:@"image_id"];
                           
                           if (socialObject)
                           {
                               socialObject.strImageURL=[response objectForKey:@"image_id"];
                               [self callSignUpAPI_Social];
                           }
                           else
                           {
                               [self callSignUpAPI];
                           }
                           
                       }
                       else
                       {
                           [App_Delegate showAlertWithMessage:@"Profile pic cannot be uploaded at this time" inViewController:self];
                       }
                       
                   });
    
}

-(void)callSignUpAPI_Social
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KSocialRegister];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:txtFieldFirstName.text forKey:@"first_name"];
    [dicParameter setObject:txtFieldLastName.text forKey:@"last_name"];
    [dicParameter setObject:txtFieldEmail.text forKey:@"email"];
    [dicParameter setObject:@"" forKey:@"password"];
    [dicParameter setObject:strUserType forKey:@"role"];
    [dicParameter setObject:socialObject.strImageURL forKey:@"profile_image"];
    [dicParameter setObject:socialObject.strSite forKey:@"site"];
    [dicParameter setObject:socialObject.strId forKey:@"social_id"];
    
    [APIsList postAPIWithURL:strUrl andParameters:dicParameter Selector:@selector(signUpAPIResponse:) WithCallBackObject:self];
    
}

-(void)callSignUpAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KNativRegister];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:txtFieldFirstName.text forKey:@"first_name"];
    [dicParameter setObject:txtFieldLastName.text forKey:@"last_name"];
    [dicParameter setObject:txtFieldEmail.text forKey:@"email"];
    [dicParameter setObject:txtFieldPassword.text forKey:@"password"];
    [dicParameter setObject:strUserType forKey:@"role"];
    [dicParameter setObject:strUserImageId forKey:@"image_id"];
    
    [APIsList postAPIWithURL:strUrl andParameters:dicParameter Selector:@selector(signUpAPIResponse:) WithCallBackObject:self];
    
}

-(void)signUpAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        
        //Native Signup Response
        if (socialObject==nil)
        {
            isSuccess=YES;
            
            Parser *parser=[[Parser alloc]init];
            App_Delegate.userObject=[parser getUserDetail:dictionary];
            
            if ([App_Delegate.userObject.strUserType isEqualToString:@"TRAVELER"])
            {
                [[NSUserDefaults standardUserDefaults]setObject:KUserTourist forKey:Default_UserType];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults]setObject:KUserGuide forKey:Default_UserType];
            }
            
            [[NSUserDefaults standardUserDefaults]setObject:txtFieldPassword.text forKey:@"password"];
            
            [[NSUserDefaults standardUserDefaults]setObject:dictionary forKey:Default_SaveUserObject];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            VerifyEmailVC *verifyEmailVC=[[VerifyEmailVC alloc]init];
            verifyEmailVC.strEmail=txtFieldEmail.text;
            verifyEmailVC.strPassword=txtFieldPassword.text;
            [self.navigationController pushViewController:verifyEmailVC animated:YES];
            
            [Answers logSignUpWithMethod:App_Delegate.userObject.strSite
                                 success:@YES
                        customAttributes:@{}];
            
        }
        else
        {
            isSuccess=YES;
            
            Parser *parser=[[Parser alloc]init];
            App_Delegate.userObject=[parser getUserDetail:dictionary];
            
            if ([App_Delegate.userObject.strUserType isEqualToString:@"TRAVELER"])
            {
                [[NSUserDefaults standardUserDefaults]setObject:KUserTourist forKey:Default_UserType];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults]setObject:KUserGuide forKey:Default_UserType];
            }
            
            [[NSUserDefaults standardUserDefaults]setObject:dictionary forKey:Default_SaveUserObject];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            [self goToHomePage];
        }
        
    }
    
    if (!isSuccess)
    {
        
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Registration failed" inViewController:self];
        }
    }
}

-(void)setupPageForSocialSignUp
{
    [txtFieldPassword setHidden:YES];
    [txtFieldConfirmPassword setHidden:YES];
    [imgtxtFieldConfirmPass setHidden:YES];
    [imgtxtFieldPassword setHidden:YES];
    
    [txtFieldEmail setText:socialObject.strEmail];
    [txtFieldEmail setEnabled:NO];
    
    [txtFieldFirstName setText:socialObject.strFirstName];
    [txtFieldLastName setText:socialObject.strLastName];
    
    if (socialObject.strImageURL!=nil)
    {
        [imgUserProfile sd_setImageWithURL:[NSURL URLWithString:socialObject.strImageURL]];
    }
    
}

-(void)editUserImage
{
    UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:@"Select Picture" delegate:(id)self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Gallery",@"Camera", nil];
    
    [actionSheet showInView:self.view];
}

-(void)goToHomePage
{
    [[App_Delegate window]setRootViewController:[App_Delegate setupDeckViewController]];
}

-(BOOL)isValidationCorrect //Check if any field is empty and show user respective message
{
    
    if (btnGuide.selected==NO && btnTourist.selected==NO)
    {
        [App_Delegate showAlertWithMessage:@"Please select user type" inViewController:self];
        return NO;
    }
    
    if (txtFieldFirstName.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter First name" inViewController:self];
        return NO;
    }
    
    if (txtFieldLastName.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter Last name" inViewController:self];
        return NO;
    }
    
    if (txtFieldEmail.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter Email" inViewController:self];
        return NO;
    }
    
    if (![App_Delegate NSStringIsValidEmail:txtFieldEmail.text])
    {
        [App_Delegate showAlertWithMessage:@"Please enter a valid Email" inViewController:self];
        return NO;
    }
    
    if (txtFieldPassword.hidden==NO && txtFieldPassword.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter Password" inViewController:self];
        return NO;
    }
    
    if (txtFieldPassword.hidden==NO && txtFieldConfirmPassword.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter password in confirm password field also." inViewController:self];
        return NO;
    }
    
    if (txtFieldPassword.hidden==NO && txtFieldPassword.text.length<4)
    {
        [App_Delegate showAlertWithMessage:@"Please use atleast minimum 4 characters password." inViewController:self];
        return NO;
    }
    
    if (![txtFieldConfirmPassword.text isEqualToString:txtFieldPassword.text] && txtFieldPassword.hidden==NO)
    {
        [App_Delegate showAlertWithMessage:@"Password entered doesn't match, Please make sure you enter same password." inViewController:self];
        return NO;
    }
    
    return YES;
}


#pragma mark- Button Actions

- (IBAction)btnBackPressed:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnTouristPressed:(UIButton *)sender
{
    [sender setSelected:YES];
    [btnGuide setSelected:NO];
    
    if (!socialObject)
    {
        chosenImage=[UIImage imageNamed:@"user_noimage.png"];
        
        NSString *fileImagePath=[NSString stringWithFormat:@"%@/Documents/profilepic.jpeg",NSHomeDirectory()];
        
        NSData *data = UIImageJPEGRepresentation(chosenImage, 0.8);
        
        NSError *error;
        
        [data writeToFile:fileImagePath options:NSDataWritingAtomic error:&error];
        
        if(error != nil)
            NSLog(@"write error %@", error);
        
        [imgUserProfile setImage:chosenImage];
    }
    
}

- (IBAction)btnGuidePressed:(UIButton *)sender
{
    [sender setSelected:YES];
    [btnTourist setSelected:NO];
    
    if (!socialObject)
    {
        chosenImage=[UIImage imageNamed:@"user_noimage.png"];
        
        NSString *fileImagePath=[NSString stringWithFormat:@"%@/Documents/profilepic.jpeg",NSHomeDirectory()];
        
        NSData *data = UIImageJPEGRepresentation(chosenImage, 0.8);
        
        NSError *error;
        
        [data writeToFile:fileImagePath options:NSDataWritingAtomic error:&error];
        
        if(error != nil)
            NSLog(@"write error %@", error);
        
        [imgUserProfile setImage:chosenImage];
    }
}

- (IBAction)btnSignUpPressed:(UIButton *)sender
{
    if ([self isValidationCorrect])
    {
        if (btnTourist.selected)
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserTourist forKey:Default_UserType];
            strUserType=@"TRAVELER";
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserGuide forKey:Default_UserType];
            strUserType=@"GUIDE";
        }
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        if (socialObject)
        {
            if (chosenImage==nil)
            {
                [self callSignUpAPI_Social];
            }
            else
            {
                [self uploadImageUnirest];
            }
        }
        else
        {
            
            if (chosenImage==nil)
            {
                [self callSignUpAPI];
            }
            else
            {
                [self uploadImageUnirest];
            }
        }
    }
}

#pragma mark- Actionsheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        //Gallery
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = (id)self;
        picker.view.tag=101;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    else if (buttonIndex==1)
    {
        //Camera
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate =(id)self;
        picker.view.tag=101;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
}

#pragma mark- UIImagePicker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    if (picker.view.tag==101)
    {
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        //Select image from gallery
        chosenImage = info[UIImagePickerControllerEditedImage];
        
        
        NSString *fileImagePath=[NSString stringWithFormat:@"%@/Documents/profilepic.jpeg",NSHomeDirectory()];
        
        NSData *data = UIImageJPEGRepresentation(chosenImage, 0.8);
        
        NSError *error;
        
        [data writeToFile:fileImagePath options:NSDataWritingAtomic error:&error];
        
        if(error != nil)
            NSLog(@"write error %@", error);
        
        
        [imgUserProfile setImage:chosenImage];
        
    }
}



@end
