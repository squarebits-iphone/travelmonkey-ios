//
//  BookGuideVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/26/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "BookGuideVC.h"
#import "ThankYouVC.h"
#import "LoginVC.h"
#import "CreditCard.h"




@interface BookGuideVC ()

@end

@implementation BookGuideVC
@synthesize guideObject;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [self setUpUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.view addSubview:scrollviewMain];
}

-(void)viewDidLayoutSubviews
{
    [scrollviewMain setFrame:CGRectMake(0, 140,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-btnSendBookingRequest.frame.size.height-140)];
    [constraintTableHeight setConstant:lblLastTemp.frame.origin.y+lblLastTemp.frame.size.height+10];
    [scrollviewMain setContentSize:CGSizeMake(0,lblLastTemp.frame.origin.y+lblLastTemp.frame.size.height+10)];
    [self.view bringSubviewToFront:btnSendBookingRequest];
}

#pragma mark- AlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag==7854)
    {
        
        if (buttonIndex==1)
        {
            if (App_Delegate.userObject.travellerDetailObject.aryCards && App_Delegate.userObject.travellerDetailObject.aryCards.count>0)
            {
                
                BOOL isDefaultCard=NO;
                
                for (CreditCard *card in App_Delegate.userObject.travellerDetailObject.aryCards)
                {
                    
                    if ([card.strActive isEqualToString:@"1"])
                    {
                        currentCard=card;
                        
                        [lblVisaCardInfo setText:[NSString stringWithFormat:@"Paying with %@ card ending in %@",currentCard.strBrand,currentCard.strLastdigits]];
                        
                        isDefaultCard=YES;
                        break;
                    }
                }
                
                
                if (isDefaultCard)
                {
                    [self callBookGuideAPI];
                }
                else
                {
                    [App_Delegate showAlertWithMessage:@"Please set default card in Payment option." inViewController:self];
                }
                
            }
            else
            {
                STPAddCardViewController *addCardViewController = [[STPAddCardViewController alloc] init];
                addCardViewController.delegate = (id)self;
                
                // STPAddCardViewController must be shown inside a UINavigationController.
                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:addCardViewController];
                [self presentViewController:navigationController animated:YES completion:nil];
            }
        }
    }
    
    if (alertView.tag==7800)
    {
        //Guest Alert
        if (buttonIndex==1)
        {
            [App_Delegate.xmppManager disconnect];
            
            NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
            [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
            
            LoginVC *viewController=[[LoginVC alloc]init];
            App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
            [App_Delegate.navigationController setNavigationBarHidden:YES];
            [App_Delegate.window setRootViewController:App_Delegate.navigationController];
        }
    }
}

#pragma mark- Methods

-(void)showGuestAlert
{

    NSString *strMessage=@"Please sign up or login to book local guides.";
    
    SCLAlertView *alert =[App_Delegate getAlertInstance];
    
    [alert addButton:@"No" actionBlock:^(void) {
        
    }];
    
    [alert addButton:@"Yes" actionBlock:^(void) {
        
        [App_Delegate.xmppManager disconnect];
        
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        
        LoginVC *viewController=[[LoginVC alloc]init];
        App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
        [App_Delegate.navigationController setNavigationBarHidden:YES];
        [App_Delegate.window setRootViewController:App_Delegate.navigationController];
        
    }];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:strMessage closeButtonTitle:nil duration:0.0];
    
    
    
}

-(void)showBookGuideAlert
{
    
    NSString *strMessage=[NSString stringWithFormat:@"Are you ready to send your booking request to %@?",lblTitle.text];
    
    SCLAlertView *alert =[App_Delegate getAlertInstance];
    
    [alert addButton:@"No" actionBlock:^(void) {
        
    }];
    
    [alert addButton:@"Yes" actionBlock:^(void) {
        
        if (App_Delegate.userObject.travellerDetailObject.aryCards && App_Delegate.userObject.travellerDetailObject.aryCards.count>0)
        {
            
            BOOL isDefaultCard=NO;
            
            for (CreditCard *card in App_Delegate.userObject.travellerDetailObject.aryCards)
            {
                
                if ([card.strActive isEqualToString:@"1"])
                {
                    currentCard=card;
                    
                    [lblVisaCardInfo setText:[NSString stringWithFormat:@"Paying with %@ card ending in %@",currentCard.strBrand,currentCard.strLastdigits]];
                    
                    isDefaultCard=YES;
                    break;
                }
            }
            
            
            if (isDefaultCard)
            {
                [self callBookGuideAPI];
            }
            else
            {
                [App_Delegate showAlertWithMessage:@"Please set default card in Payment option." inViewController:self];
            }
            
        }
        else
        {
            STPAddCardViewController *addCardViewController = [[STPAddCardViewController alloc] init];
            addCardViewController.delegate = (id)self;
            
            // STPAddCardViewController must be shown inside a UINavigationController.
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:addCardViewController];
            [self presentViewController:navigationController animated:YES completion:nil];
        }

     
    }];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:strMessage closeButtonTitle:nil duration:0.0];
}

-(void)callTokenAPI:(NSString*)strToken
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KSendStripeToken];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strToken forKey:@"token"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(tokenAPIResponse:) WithCallBackObject:self];
}

-(void)tokenAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        [self callBookGuideAPI];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Guide cannot be book at this time, Please try again" inViewController:self];
        }
    }
}

-(void)callBookGuideAPI
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KBookGuide];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:guideObject.strId forKey:@"guide_id"];
    //[dicParameter setObject:App_Delegate.strStartTime forKey:@"start_time"];
    //[dicParameter setObject:App_Delegate.strEndTime forKey:@"end_time"];
    
    [dicParameter setObject:[App_Delegate getGMTFromLocalTime:App_Delegate.strStartTime andDateFormat:@"YYYY-MM-dd HH:mm:ss"] forKey:@"start_time"];
    [dicParameter setObject:[App_Delegate getGMTFromLocalTime:App_Delegate.strEndTime andDateFormat:@"YYYY-MM-dd HH:mm:ss"] forKey:@"end_time"];
    [dicParameter setObject:lblNumberOfPerson.text forKey:@"numberofperson"];
    [dicParameter setObject:[NSNumber numberWithFloat:totalPrice] forKey:@"price"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(bookGuideAPIResponse:) WithCallBackObject:self];
}

-(void)bookGuideAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        NSLog(@"Booking confirmed");
        //[self.navigationController popToRootViewControllerAnimated:NO];
        //[[NSNotificationCenter defaultCenter]postNotificationName:@"bookingConfirmed" object:nil];
        //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:KAppName message:@"Booking request sent to guide." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        ThankYouVC *view=[[ThankYouVC alloc]init];
        view.strTitle=@"Request Sent";
        view.strConfirmationTitle=@"Thank you!";
        view.isForBooking=YES;
        
        NSDateFormatter *formatter=[NSDateFormatter new];
        [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        
        float minutes=[App_Delegate getMinutesDifferenceBetweenStartDate:[formatter stringFromDate:[NSDate date]] endDate:App_Delegate.strStartTime withFormaterStyle:@"YYYY-MM-dd HH:mm:ss"];
        
        
        if (minutes<1440) //Less than 24 hours
        {
            view.strMessage=@"Your booking request is within the next 24 hours, so your local guide has 20 minutes to accept or decline your request. If your local guide does not respond, then your request will be automatically cancelled, at which point you can try to rebook this same local guide or send a booking request to a different local guide. Please take into consideration the time zone where the local guide works, they might be sleeping and cannot respond.";
        }
        else
        {
            view.strMessage=@"Your booking request is more than 24 hours away, so your local guide has 12 hours to accept or decline your booking request. If your local guide does not respond, then your request will be automatically cancelled, at which point you can try to rebook this same local guide or send a booking request to a different local guide. Please take into consideration the time zone where the local guide works, they might be sleeping and cannot respond.";
        }
        
        [self.navigationController pushViewController:view animated:NO];
        
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Guide cannot be book at this time, Please try again" inViewController:self];
        }
    }
    
}

-(void)setUpUI
{
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    //Setting User Name
    NSString *strUserName=@"";
    
    if (guideObject.strFirstName!=nil && guideObject.strFirstName!=(id)[NSNull null])
    {
        strUserName=[strUserName stringByAppendingString:guideObject.strFirstName];
        [lblTitle setText:strUserName];
    }
    
    if (guideObject.strLastName!=nil && guideObject.strLastName!=(id)[NSNull null])
    {
        //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",guideObject.strLastName]];
        //[lblTitle setText:strUserName];
    }
    
    //Location
    NSString *strLocation=@"";
    
    if(guideObject.strCity!=nil && guideObject.strCity!=(id)[NSNull null] && guideObject.strCity.length>0)
    {
        strLocation=[strLocation stringByAppendingString:guideObject.strCity];
        [lblLocalLocation setText:strLocation];
    }
    
    if(guideObject.strCountry!=nil && guideObject.strCountry!=(id)[NSNull null] && guideObject.strCountry.length>0)
    {
        strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",guideObject.strCountry]];
        [lblLocalLocation setText:strLocation];
    }
    
    //Setting dates
    NSDateFormatter *formatter=[NSDateFormatter new];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    //LocalTime
    //NSTimeZone *locatTimeZone = [NSTimeZone systemTimeZone];
    //[formatter setTimeZone:locatTimeZone];
    
    //NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    
    //NSDate *startTime=[formatter dateFromString:App_Delegate.strStartTime];
    //NSDate *endTime=[formatter dateFromString:App_Delegate.strEndTime];
    NSDate *startTime=[formatter dateFromString:App_Delegate.strStartTime];
    NSDate *endTime=[formatter dateFromString:App_Delegate.strEndTime];
    
    //startTime=[startTime dateByAddingTimeInterval:timeZoneSeconds];
    //endTime=[endTime dateByAddingTimeInterval:timeZoneSeconds];
    
    [formatter setDateFormat:@"MMM dd, yyyy"];
    [lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
    [lblDate setTextColor:KAppColor_Blue];
    
    [formatter setDateFormat:@"hh:mm a"];
    [lblStartTime setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
    [lblEndTime setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:endTime]]];
    [lblStartTime setTextColor:KAppColor_Blue];
    [lblEndTime setTextColor:KAppColor_Blue];
    [lblNumberOfPerson setTextColor:KAppColor_Blue];
    
    [self calculatePrice];
    [self checkNumberOfPerson];
    
    if (App_Delegate.userObject.travellerDetailObject.aryCards && App_Delegate.userObject.travellerDetailObject.aryCards.count>0)
    {
        for (CreditCard *card in App_Delegate.userObject.travellerDetailObject.aryCards)
        {
            if ([card.strActive isEqualToString:@"1"])
            {
                currentCard=card;
                
                [lblVisaCardInfo setText:[NSString stringWithFormat:@"Paying with %@ card ending in %@",currentCard.strBrand,currentCard.strLastdigits]];
                [lblVisaCardInfo setTextColor:KAppColor_GrayBlue];
                
                break;
            }
            
            [lblVisaCardInfo setText:@"Please set default card in Payment option."];
            [lblVisaCardInfo setTextColor:KAppColor];
            
        }
        
    }
    else
    {
        [lblVisaCardInfo setText:@"No card added yet"];
        [lblVisaCardInfo setTextColor:KAppColor];
    }
}

-(void)calculatePrice
{
    
    //Price formula
    //Get price from 1 person
    //Add 25% percent * N-1
    
    
    //PricePuru
    float costPerHour=[App_Delegate.selectedGuide.priceObject.strPrice floatValue];
    int numberOfPerson=[lblNumberOfPerson.text intValue];
    int numberOfPersonExcludingMainPerson=numberOfPerson-1;
    
    float costPerMinute=costPerHour/60.0;
    float totalMinutes=[App_Delegate getMinutesDifferenceBetweenStartDate:App_Delegate.strStartTime endDate:App_Delegate.strEndTime withFormaterStyle:@"YYYY-MM-dd HH:mm:ss"];
    
    float price=costPerMinute*totalMinutes;
    totalPrice=price;
    
    if (numberOfPersonExcludingMainPerson>0)
    {
        totalPrice=price+(price*0.25*numberOfPersonExcludingMainPerson);
    }
    
    
    int hour=totalMinutes/60;
    int minute=(int)totalMinutes%60;
    
    if (minute>0)
    {
        if (hour==1)
        {
            [lblTotalBookingTime setText:[NSString stringWithFormat:@"%d hour %d minutes of booking",hour,minute]];
        }
        else
        {
            [lblTotalBookingTime setText:[NSString stringWithFormat:@"%d hours %d minutes of booking",hour,minute]];
        }
    }
    else
    {
        if (hour==1)
        {
            [lblTotalBookingTime setText:[NSString stringWithFormat:@"%d hour of booking",hour]];
        }
        else
        {
            [lblTotalBookingTime setText:[NSString stringWithFormat:@"%d hours of booking",hour]];
        }
    }
    
    NSString *strTotalPrice=[NSString stringWithFormat:@"Totat Cost: $%.02f",totalPrice];
    
    
    // If attributed text is supported (iOS6+)
    if ([lblTotalCost respondsToSelector:@selector(setAttributedText:)]) {
        
        // Define general attributes for the entire text
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName: KAppColor_Blue,
                                  NSFontAttributeName: lblTotalCost.font
                                  };
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:strTotalPrice
                                               attributes:attribs];
        
        //Blue text attributes
        NSRange redTextRange = [strTotalPrice rangeOfString:@"Totat Cost:"];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
        [attributedText setAttributes:@{NSForegroundColorAttributeName:KAppColor_GrayBlue}
                                range:redTextRange];
        
        lblTotalCost.attributedText = attributedText;
    }
    else
    {
        lblTotalCost.text =strTotalPrice;
    }
    
    
    //[lblTotalCost setText:[NSString stringWithFormat:@"Totat Cost: $%.02f",totalPrice]];
    
}

-(void)checkNumberOfPerson
{
    if ([lblNumberOfPerson.text intValue]==1)
    {
        [btnMinus setEnabled:NO];
    }
    else
    {
        [btnMinus setEnabled:YES];
    }
    
    [self calculatePrice];
}

#pragma mark- Button Action

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

- (IBAction)btnMinusPressed:(id)sender
{
    int value=[lblNumberOfPerson.text intValue];
    value--;
    [lblNumberOfPerson setText:[NSString stringWithFormat:@"%d",value]];
    [self checkNumberOfPerson];
}

- (IBAction)btnPlusPressed:(id)sender
{
    int value=[lblNumberOfPerson.text intValue];
    value++;
    [lblNumberOfPerson setText:[NSString stringWithFormat:@"%d",value]];
    [self checkNumberOfPerson];
}

- (IBAction)btnSendBookingRequestPressed:(id)sender
{
    if ([App_Delegate getCurrentUser]==Guest)
    {
        [self showGuestAlert];
    }
    else
    {
       [self showBookGuideAlert];
    }
    
}

#pragma mark- STPAddCardViewControllerDelegate

-(void)addCardViewControllerDidCancel:(STPAddCardViewController *)addCardViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)addCardViewController:(STPAddCardViewController *)addCardViewController
              didCreateToken:(STPToken *)token
                  completion:(STPErrorBlock)completion
{
    
    [self hideAddCardScreen];
    NSLog(@"Token id : %@",token.tokenId);
    [self callTokenAPI:token.tokenId];
}

-(void)hideAddCardScreen
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
