//
//  SelectDateVC_Tourist.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/22/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTCalendar.h"



@interface SelectDateVC_Tourist : UIViewController<JTCalendarDelegate>
{
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UIScrollView *scrollviewMain;
    IBOutlet UIDatePicker *startTimePicker;
    IBOutlet UIDatePicker *endTimePicker;
    
    
    NSMutableDictionary *_eventsByDate;
    
    NSDate *_dateSelected;
}


@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;
@property (strong, nonatomic) JTCalendarManager *calendarManager;

@property (nonatomic,strong) GMSPlace *touristGMSPlace;



@end
