//
//  SetWorkScheduleVC_Guide.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/24/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetWorkScheduleVC_Guide : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UILabel *lblChargePerHour;
    
    IBOutlet UITextField *txtFieldPrice;
    IBOutlet UIScrollView *scrlView;
    
    IBOutlet UITableView *tblDays;
    
    IBOutlet UIDatePicker *startTimePicker;
    IBOutlet UIDatePicker *endTimePicker;
    
    
    __weak IBOutlet NSLayoutConstraint *constraintViewSubViewScrollView;
    
    NSMutableArray *aryDays;
    NSMutableArray *arySelectedDays;

}

@end
