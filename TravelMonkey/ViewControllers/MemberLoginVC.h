//
//  MemberLoginVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/11/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MemberLoginVC : UIViewController
{
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UILabel *lblTitle;
    __weak IBOutlet NSLayoutConstraint *constraintBottamSpace;
    
    IBOutlet NSLayoutConstraint *constraintTxtPasswordBottamSpace;
    
    NSMutableArray *aryAnimationImage;
    IBOutlet UIImageView *imgViewBtnSignIn;
    
    
}

@property (strong, nonatomic) IBOutlet UITextField *txtFieldEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;

- (IBAction)btnForgetPasswordPressed:(UIButton *)sender;
- (IBAction)btnLoginPressed:(id)sender;

@end
