//
//  MemberLoginVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/11/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "MemberLoginVC.h"
#import "Parser.h"
#import "IQKeyboardManager.h"
#import "ForgetPasswordVC.h"



@interface MemberLoginVC ()

@end

@implementation MemberLoginVC
@synthesize txtFieldEmail,txtFieldPassword,btnLogin;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    [lblTitle setTextColor:KAppColor];
    
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:220];
    
    aryAnimationImage=[[NSMutableArray alloc]init];
    
    for (int i=1; i<43; i++)
    {
        [aryAnimationImage addObject:[UIImage imageNamed:[NSString stringWithFormat:@"btn_login%d.png",i]]];
    }
    
    [self resetAnimation];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:40];
}

-(void)viewDidLayoutSubviews
{
    if (IS_IPHONE_4)
    {
        constraintBottamSpace.constant=0;
        constraintTxtPasswordBottamSpace.constant=10;
    }
    
    if (IS_IPHONE_5)
    {
        constraintBottamSpace.constant=10;
    }
    
    if (IS_IPHONE_6_PLUS)
    {
        constraintBottamSpace.constant=60;
    }
}

#pragma mark- Methods

-(void)resetAnimation
{
    [imgViewBtnSignIn setImage:aryAnimationImage.lastObject];
    [imgViewBtnSignIn setHidden:YES];
    [btnLogin setHidden:NO];
    //[btnLogin setBackgroundImage:[UIImage imageNamed:@"btn_login.png"] forState:UIControlStateNormal];
    //[btnLogin setTitle:@"Log in" forState:UIControlStateNormal];
}

-(void)startAnimation
{
    
    float animationDuration=1;
    
    [btnLogin setHidden:YES];
    [imgViewBtnSignIn setHidden:NO];
    
    [imgViewBtnSignIn setBackgroundColor:[UIColor clearColor]];
    imgViewBtnSignIn.animationImages = aryAnimationImage;
    imgViewBtnSignIn.animationDuration =animationDuration;
    imgViewBtnSignIn.animationRepeatCount =1;
    [imgViewBtnSignIn startAnimating];
    
    [self.view setUserInteractionEnabled:NO];
    [self performSelector:@selector(stopAnimation) withObject:nil afterDelay:animationDuration];
}

-(void)stopAnimation
{
    [imgViewBtnSignIn setImage:aryAnimationImage.lastObject];
    [self.view setUserInteractionEnabled:YES];
    [self callLogin];
}

-(void)btnNextShouldEnable
{
    if (txtFieldPassword.text.length>0 && txtFieldEmail.text.length>0)
    {
        [btnLogin setEnabled:YES];
    }
    else
    {
        [btnLogin setEnabled:NO];
    }
    
}

-(void)callLogin
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KNativeLogin];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:txtFieldEmail.text forKey:@"email"];
    [dicParameter setObject:txtFieldPassword.text forKey:@"password"];
    
    [APIsList postAPIWithURL:strUrl andParameters:dicParameter Selector:@selector(loginAPIResponse:) WithCallBackObject:self];
}

-(void)goToHomePage
{
    [[App_Delegate window]setRootViewController:[App_Delegate setupDeckViewController]];
}

-(void)loginAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        
        Parser *parser=[[Parser alloc]init];
        App_Delegate.userObject=[parser getUserDetail:dictionary];
        
        if ([App_Delegate.userObject.strUserType isEqualToString:@"TRAVELER"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserTourist forKey:Default_UserType];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserGuide forKey:Default_UserType];
        }
        
        [[NSUserDefaults standardUserDefaults]setObject:txtFieldPassword.text forKey:@"password"];
        
        [[NSUserDefaults standardUserDefaults]setObject:dictionary forKey:Default_SaveUserObject];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self goToHomePage];
        
        [Answers logLoginWithMethod:@"Native"
                            success:@YES
                   customAttributes:@{}];

    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Login failed" inViewController:self];
        }
        
        [self resetAnimation];
    }
}

-(BOOL)isValidationCorrect //Check if any field is empty and show user respective message
{

    if (txtFieldEmail.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter Email" inViewController:self];
        return NO;
    }
    
    if (![App_Delegate NSStringIsValidEmail:txtFieldEmail.text])
    {
        [App_Delegate showAlertWithMessage:@"Please enter a valid Email" inViewController:self];
        return NO;
    }
    
    if (txtFieldPassword.hidden==NO && txtFieldPassword.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter Password" inViewController:self];
        return NO;
    }
    
    return YES;
    
}

#pragma mark- Actions

-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnForgetPasswordPressed:(UIButton *)sender
{
    ForgetPasswordVC *view=[[ForgetPasswordVC alloc]init];
    [self.navigationController pushViewController:view animated:YES];
}

- (IBAction)btnLoginPressed:(id)sender
{
    
    
    if ([self isValidationCorrect])
    {
        [self startAnimation];
    }
}

@end
