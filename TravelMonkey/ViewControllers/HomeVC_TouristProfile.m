//
//  HomeVC_Guide.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "HomeVC_TouristProfile.h"
#import "UIImageView+WebCache.h"
#import "CAPSPageMenu.h"
#import "PageView_AboutVCViewController.h"
#import "ReviewTouristVC.h"
#import "PageView_PhotosVC.h"
#import "GuideObject.h"
#import "UIView+MWParallax.h"
#import "ViewPhotosVC.h"
#import "AddPhotosVC.h"


#define floatParallex 20


@interface HomeVC_TouristProfile ()
@property (nonatomic) CAPSPageMenu *pageMenu;
@end

@implementation HomeVC_TouristProfile
@synthesize touristObject;


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    //imgUserProfie.iOS6ParallaxIntensity=floatParallex;
    
    if (self.isForViewProfile)
    {
        [btnReportUser setHidden:NO];
    }
    else
    {
        [btnReportUser setHidden:YES];
    }
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    if (touristObject)
    {
        [self setupViewForOtherTourist];
        [btnBack setHidden:NO];
        [btnReportUser setHidden:NO];
    }
    else
    {
        [self setupViewForSelfTourist];
        [btnBack setHidden:YES];
        [btnReportUser setHidden:YES];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotToAddPhotos:) name:@"openAddPhotosPage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToViewPhotosPage:) name:@"openPhotosPage" object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

-(void)viewDidLayoutSubviews
{
    [self addPageView];
    frame = imgUserProfie.frame;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark- Methods

-(void)goToViewPhotosPage:(NSNotification*)notificationObject
{
    
    int index=[[notificationObject.object objectForKey:@"index"] intValue];
    
    ViewPhotosVC *viewPhotos=[[ViewPhotosVC alloc]init];
    
    if (touristObject)
    {
        viewPhotos.aryPhotos=touristObject.aryPhotos;
    }
    else
    {
        viewPhotos.aryPhotos=App_Delegate.userObject.aryPhotos;
    }
    
    viewPhotos.currentIndex=index;
    [self.navigationController pushViewController:viewPhotos animated:YES];
    
    
}

-(void)gotToAddPhotos:(NSNotification*)notificationObject
{
    AddPhotosVC *changePasswordVC=[[AddPhotosVC alloc]init];
    [self.navigationController pushViewController:changePasswordVC animated:YES];
}


-(void)addPageView
{
    
    for (UIView *view in scrollViewMain.subviews)
    {
        if (view.tag==101)
        {
            [view removeFromSuperview];
        }
    }
    
    PageView_AboutVCViewController *view1=[[PageView_AboutVCViewController alloc]init];
    
    view1.title=@"About Me";
    
    ReviewTouristVC *view2=[[ReviewTouristVC alloc]init];
    view2.title=@"Reviews";
    
    PageView_PhotosVC *view3=[[PageView_PhotosVC alloc]init];
    view3.title=@"Photos";
    
    view1.isForViewProfile=view2.isForViewProfile=view3.isForViewProfile=self.isForViewProfile;
    
    if (touristObject)
    {
        view1.guideObject=touristObject;
        view2.guideObject=touristObject;
        view3.guideObject=touristObject;
        view2.aryInformation=touristObject.aryReviews;
    }
    else
    {
        view2.aryInformation=App_Delegate.userObject.aryReviews;
    }
    
    NSArray *controllerArray=@[view1,view2,view3];
    
    
    NSDictionary *parameters = @{
                                 
                                 CAPSPageMenuOptionMenuItemSeparatorWidth:@(10.0),
                                 CAPSPageMenuOptionMenuItemSeparatorColor:KAppColor_Blue,
                                 CAPSPageMenuOptionMenuItemSeparatorPercentageHeight:@(80),
                                 CAPSPageMenuOptionMenuItemSeparatorRoundEdges:@(YES),
                                 CAPSPageMenuOptionScrollMenuBackgroundColor:[UIColor clearColor],
                                 CAPSPageMenuOptionViewBackgroundColor:[UIColor whiteColor],
                                 CAPSPageMenuOptionSelectionIndicatorColor: KAppColor_Blue,
                                 //CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor darkGrayColor],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"SourceSansPro-Regular" size:16.0],
                                 CAPSPageMenuOptionMenuHeight: @(40.0),
                                 CAPSPageMenuOptionMenuItemWidth: @([UIScreen mainScreen].bounds.size.width/3),
                                 CAPSPageMenuOptionCenterMenuItems: @(NO),
                                 CAPSPageMenuOptionMenuMargin:@(0),
                                 //CAPSPageMenuOptionSelectedMenuItemLabelColor:KAppColor_Blue
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor:[UIColor blackColor],
                                 };
    
    CGRect framePage=CGRectMake(0.0,imageContainer.frame.origin.y+imageContainer.frame.size.height,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-scrollViewMain.frame.origin.y-imageContainer.frame.origin.y);
    
    if (!_pageMenu)
    {
        _pageMenu = [[CAPSPageMenu alloc]initWithViewControllers:controllerArray frame:framePage options:parameters];
        [_pageMenu.view setTag:1011];
        
        [scrollViewMain addSubview:_pageMenu.view];
    }
    
    [_pageMenu.view setFrame:framePage];
    
    [scrollViewMain setContentSize:CGSizeMake(0,_pageMenu.view.frame.size.height+_pageMenu.view.frame.origin.y)];
    
}

-(void)setupViewForSelfTourist
{
    
    //Setting User Name
    NSString *strUserName=@"";
    
    if (App_Delegate.userObject.strFirstName!=nil && App_Delegate.userObject.strFirstName!=(id)[NSNull null])
    {
        strUserName=[strUserName stringByAppendingString:App_Delegate.userObject.strFirstName];
        [lblLocalName setText:strUserName];
    }
    
    if (App_Delegate.userObject.strLastName!=nil && App_Delegate.userObject.strLastName!=(id)[NSNull null])
    {
        //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",App_Delegate.userObject.strLastName]];
        //[lblLocalName setText:strUserName];
    }
    
    if (App_Delegate.userObject.isMale)
    {
        [lblGender setText:@"Male"];
    }
    else
    {
        [lblGender setText:@"Female"];
    }
    
    if (App_Delegate.userObject.strBirthDate!=nil && App_Delegate.userObject.strBirthDate!=(id)[NSNull null])
    {
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        
        [lblAge setText:[NSString stringWithFormat:@"%d Years old",[App_Delegate getAgeFromBirthdate:[formatter dateFromString:App_Delegate.userObject.strBirthDate]]]];
    }
    
    NSString *strLocation=@"";
    
    if(App_Delegate.userObject.strCity!=nil && App_Delegate.userObject.strCity!=(id)[NSNull null] && App_Delegate.userObject.strCity.length>0)
    {
        strLocation=[strLocation stringByAppendingString:App_Delegate.userObject.strCity];
        [lblLocalLocation setText:strLocation];
    }
    
    if(App_Delegate.userObject.strCountry!=nil && App_Delegate.userObject.strCountry!=(id)[NSNull null] && App_Delegate.userObject.strCountry.length>0)
    {
        strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",App_Delegate.userObject.strCountry]];
        [lblLocalLocation setText:strLocation];
    }
    
    if (App_Delegate.userObject.strProfileImageURL!=nil && App_Delegate.userObject.strProfileImageURL!=(id)[NSNull null])
    {
        [imgUserProfie sd_setImageWithURL:[NSURL URLWithString:App_Delegate.userObject.strProfileImageURL]];
    }
    
//    if (App_Delegate.userObject.locatDetailObject.priceObject.strPrice!=nil && App_Delegate.userObject.locatDetailObject.priceObject.strPrice!=(id)[NSNull null])
//    {
//        [lblPrice setText:[NSString stringWithFormat:@"$%@",App_Delegate.userObject.locatDetailObject.priceObject.strPrice]];
//    }

}

-(void)setupViewForOtherTourist
{
    //Setting User Name
    NSString *strUserName=@"";
    
    if (touristObject.strFirstName!=nil && touristObject.strFirstName!=(id)[NSNull null])
    {
        strUserName=[strUserName stringByAppendingString:touristObject.strFirstName];
        [lblLocalName setText:strUserName];
    }
    
    if (touristObject.strLastName!=nil && touristObject.strLastName!=(id)[NSNull null])
    {
        //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",touristObject.strLastName]];
        //[lblLocalName setText:strUserName];
    }
    
    if (touristObject.isMale)
    {
        [lblGender setText:@"Male"];
    }
    else
    {
        [lblGender setText:@"Female"];
    }
    
    if (touristObject.strBirthDate!=nil && touristObject.strBirthDate!=(id)[NSNull null])
    {
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        
        [lblAge setText:[NSString stringWithFormat:@"%d Years old",[App_Delegate getAgeFromBirthdate:[formatter dateFromString:touristObject.strBirthDate]]]];
    }
    
    NSString *strLocation=@"";
    
    if(touristObject.strCity!=nil && touristObject.strCity!=(id)[NSNull null] && touristObject.strCity.length>0)
    {
        strLocation=[strLocation stringByAppendingString:touristObject.strCity];
        [lblLocalLocation setText:strLocation];
    }
    
    if(touristObject.strCountry!=nil && touristObject.strCountry!=(id)[NSNull null] && touristObject.strCountry.length>0)
    {
        strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",touristObject.strCountry]];
        [lblLocalLocation setText:strLocation];
    }
    
    if (touristObject.strProfileImageURL!=nil && touristObject.strProfileImageURL!=(id)[NSNull null])
    {
        [imgUserProfie sd_setImageWithURL:[NSURL URLWithString:touristObject.strProfileImageURL]];
    }
    
//    if (touristObject.locatDetailObject.priceObject.strPrice!=nil && touristObject.locatDetailObject.priceObject.strPrice!=(id)[NSNull null])
//    {
//        [lblPrice setText:[NSString stringWithFormat:@"$%@",touristObject.locatDetailObject.priceObject.strPrice]];
//    }
    
}

-(void)callReportUserProfileAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSLog(@"Reporting : %@ %@",App_Delegate.userObject.strId,App_Delegate.userObject.strEmail);
    NSLog(@"Report This user : %@ %@",touristObject.strId,touristObject.strEmail);
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KReportUser];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"reporting_user"];
    [dicParameter setObject:touristObject.strId forKey:@"report_user"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(reportUserResponse:) WithCallBackObject:self];
}

-(void)reportUserResponse:(NSDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        [App_Delegate showAlertWithMessage:@"Thank you for reporting user" inViewController:self];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"User details not found" inViewController:self];
        }
    }
    
}

#pragma mark- ScrollView Delegate

-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    if (scrollView.contentOffset.y>=frame.size.height-floatParallex-floatParallex)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"enableTableScrolling" object:nil];
        App_Delegate.shouldDisableScrolling=YES;
        NSLog(@"Y : %f",frame.size.height-floatParallex-floatParallex);
        NSLog(@"Offset %f",scrollView.contentOffset.y);
    }
    
    if (scrollView.contentOffset.y<=0)
    {
        App_Delegate.shouldDisableScrolling=NO;
    }
}

#pragma mark- AlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1012)
    {
        NSLog(@"Logout Alert");
        
        NSLog(@"Button index : %d",(int)buttonIndex);
        
        if (buttonIndex==1)
        {
            [self callReportUserProfileAPI];
        }
        
    }
}

#pragma mark- Button Actions

-(IBAction)btnReportUser:(id)sender
{
    
    NSString *strMessage=[NSString stringWithFormat:@"Report this user for spam or other violation?",touristObject.strFirstName];
    
    SCLAlertView *alert =[App_Delegate getAlertInstance];
    
    [alert addButton:@"No" actionBlock:^(void) {
        
    }];
    
    [alert addButton:@"Yes" actionBlock:^(void) {
        
        [self callReportUserProfileAPI];
        
    }];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:strMessage closeButtonTitle:nil duration:0.0];
}

-(IBAction)btnBackPressed:(id)sender
{   

    if (self.isForViewProfile )
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self btnTogglePressed:nil];
    }
}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}



@end
