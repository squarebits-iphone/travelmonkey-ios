//
//  LoginVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/11/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "LoginVC.h"
#import "MemberLoginVC.h"
#import "SignUpVC.h"
#import "UserObject.h"
#import "Parser.h"
#import "WebViewVC.h"




//Facebook
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

//Google
#import <Google/SignIn.h>


@interface LoginVC ()

@end

@implementation LoginVC

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[GIDSignIn sharedInstance] setAllowsSignInWithWebView:YES];
    //[[GIDSignIn sharedInstance] setAllowsSignInWithBrowser:NO];
    

    [GIDSignIn sharedInstance].uiDelegate = self;
    
    [btnLogin setTitleColor:KAppColor forState:UIControlStateNormal];
    [btnSkipLogin setTitleColor:KAppColor forState:UIControlStateNormal];
    
    if (IS_IPHONE_4 )
    {
        constraintBottamClearView.constant=0;
        constraintLogoTopDistance.constant=22;
        
        //        float height=(168/265)*150;
        //        [imgLogo setBounds:CGRectMake(0, 0, 150, height)];
    }
    
    if (IS_IPHONE_5)
    {
        constraintBottamClearView.constant=10;
    }
    
    if (IS_IPHONE_6_PLUS)
    {
        constraintBottamClearView.constant=40;
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(googleSignInCompletedWithUser:) name:@"googleSign" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Methods

-(IBAction)termsAndCondition:(id)sender
{
    WebViewVC *webView=[[WebViewVC alloc]init];
    webView.strUrl=KTermsAndConditionPage;
    webView.strTitle=@"Terms and Conditions";
    webView.shouldHideToggle=YES;
    [self.navigationController pushViewController:webView animated:YES];
}

-(IBAction)privacyPolicy:(id)sender
{
    WebViewVC *webView=[[WebViewVC alloc]init];
    webView.strUrl=KPrivacyPolicyPage;
    webView.strTitle=@"Privacy Policy";
    webView.shouldHideToggle=YES;
    [self.navigationController pushViewController:webView animated:YES];
}


-(void)callLoginThroughSocialSites
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KSocialLogin];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:object.strEmail forKey:@"email"];
    [dicParameter setObject:object.strSite forKey:@"site"];
    
    [APIsList postAPIWithURL:strUrl andParameters:dicParameter Selector:@selector(loginAPIResponse:) WithCallBackObject:self];
}

-(void)loginAPIResponse:(NSDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        Parser *parser=[[Parser alloc]init];
        App_Delegate.userObject=[parser getUserDetail:dictionary];
        
        if ([App_Delegate.userObject.strUserType isEqualToString:@"TRAVELER"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserTourist forKey:Default_UserType];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserGuide forKey:Default_UserType];
        }
        
        [[NSUserDefaults standardUserDefaults]setObject:dictionary forKey:Default_SaveUserObject];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [Answers logLoginWithMethod:App_Delegate.userObject.strSite
                            success:@YES
                   customAttributes:@{}];
        
        [self goToHomePage];
    }
    
    if (!isSuccess)
    {
        [self goToSignupPageWithObject:object];
    }
    
}

-(void)googleSignInCompletedWithUser:(NSNotification*)notification
{
    GIDGoogleUser *user=notification.object;
    
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    NSString *givenName = user.profile.givenName;
    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;
    
    NSLog(@"User : %@",user);
    
    object=[[SocialInformation alloc]init];
    
    if (email!=nil && email!=(id)[NSNull null])
    {
        object.strEmail=email;
    }
    
    if (fullName!=nil && fullName!=(id)[NSNull null])
    {
        
        NSArray *aryName=[fullName componentsSeparatedByString:@" "];
        object.strFirstName=[aryName firstObject];
        object.strLastName=[aryName lastObject];
        
    }
    
    object.strId=userId;
    if (user.profile.hasImage)
    {
        object.strImageURL=[[user.profile imageURLWithDimension:500] path];
    }
    object.strSite=@"gmail";
    
    [self callLoginThroughSocialSites];
}

-(void)goToSignupPageWithObject:(SocialInformation*)object1
{
    SignUpVC *signupVC=[[SignUpVC alloc]init];
    signupVC.socialObject=object1;
    [self.navigationController pushViewController:signupVC animated:YES];
}

-(void)goToHomePage
{
    [[App_Delegate window]setRootViewController:[App_Delegate setupDeckViewController]];
}

#pragma mark- Actions

-(IBAction)btnCreateAccountPressed:(id)sender
{
    SignUpVC *signUpVC=[[SignUpVC alloc]init];
    [self.navigationController pushViewController:signUpVC animated:YES];
}

-(IBAction)btnGuestPressed:(id)sender
{
    [[App_Delegate window]setRootViewController:[App_Delegate setupDeckViewController]];
}

-(IBAction)btnFacebookPressed:(id)sender
{
    [FBSDKAccessToken setCurrentAccessToken:nil];
    
    if ([FBSDKAccessToken currentAccessToken])
    {
        // User is logged in, do work such as go to next view controller.
        NSLog(@"User is logged in already");
    }
    else
    {
        NSLog(@"User is NOT autenticated");
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        login.loginBehavior = FBSDKLoginBehaviorWeb;
        
        [login logInWithReadPermissions:@[@"email",@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error)
            {
                NSLog(@"Process error");
            }
            else if (result.isCancelled)
            {
                NSLog(@"Cancelled");
                //[App_Delegate showAlertWithMessage:@"Registration cancelled." inViewController:self];
            }
            else
            {
                NSLog(@"Logged in");
                
                if ([result.grantedPermissions containsObject:@"public_profile"])
                {
                    if ([FBSDKAccessToken currentAccessToken]) {
                        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                        [parameters setValue:@"name,email,picture.type(large)" forKey:@"fields"];
                        
                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
                         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                             if (!error)
                             {
                                 NSLog(@"fetched user:%@", result);
                                 
                                 
                                 NSString *name = [result valueForKey:@"name"];
                                 //NSString *username = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                                 NSArray *picture_arr = [result objectForKey:@"picture"];
                                 NSArray *data_arr = [picture_arr valueForKey:@"data"];
                                 NSString *profile_pic = [data_arr valueForKey:@"url"];
                                 
                                 object=[[SocialInformation alloc]init];
                                 
                                 if ([result objectForKey:@"email"]!=nil && [result objectForKey:@"email"]!=(id)[NSNull null])
                                 {
                                     object.strEmail=[result objectForKey:@"email"];
                                 }
                                 
                                 if (name!=nil && name!=(id)[NSNull null])
                                 {
                                     NSArray *aryName=[name componentsSeparatedByString:@" "];
                                     object.strFirstName=[aryName firstObject];
                                     object.strLastName=[aryName lastObject];
                                 }
                                 
                                 if (profile_pic!=nil && profile_pic!=(id)[NSNull null])
                                 {
                                     object.strImageURL=profile_pic;
                                 }
                                 
                                 object.strId=[result objectForKey:@"id"];
                                 object.strSite=@"fb";
                                 
                                 [self callLoginThroughSocialSites];
                             }
                             
                         }];
                    }
                }
            }
        }];
    }
}

-(IBAction)btnGmailPressed:(id)sender
{
    [[GIDSignIn sharedInstance] signOut];
    [[GIDSignIn sharedInstance] signIn];
}

-(IBAction)btnMemberLoginPressed:(id)sender
{
    MemberLoginVC *memberVC=[[MemberLoginVC alloc]init];
    [self.navigationController pushViewController:memberVC animated:YES];
}

#pragma mark- Google Sign in

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error
{
    //[myActivityIndicator stopAnimating];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController
{
    viewController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
