//
//  TripDetailVC_Tourist.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/19/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripDetails.h"

@interface TripDetailVC_Tourist : UIViewController
{
    //Outlets
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    
    IBOutlet UIScrollView *scrlView;
    IBOutlet UIView *viewContent;
    IBOutlet NSLayoutConstraint *constraintHeight;
    
    
    IBOutlet UIImageView *imgViewProfile;
    IBOutlet UILabel *lblName;
    IBOutlet UILabel *lblPlace;
    IBOutlet UILabel *lblTripDate;
    IBOutlet UILabel *lblTripStartTime;
    IBOutlet UILabel *lblTripEndTime;
    IBOutlet UILabel *lblTotalCost;
    IBOutlet UILabel *lblTripTotalTime;
    IBOutlet UILabel *lblPaidWith;
    IBOutlet UIButton *btnNegative;
    IBOutlet UIButton *btnPositive;
    IBOutlet UITextView *txtViewComment;
    IBOutlet UIButton *btnSubmitReview;
    TripDetails *objectTripDetail;
    
    BOOL shouldHideReviewArea;
    IBOutlet UILabel *lblReviewPlaceHolder;
    IBOutlet UILabel *lblNegativePlaceHolder;
    IBOutlet UILabel *lblPositivePlaceHolder;
    IBOutlet UILabel *lblOptionalCommentPlaceHolder;
    IBOutlet UIImageView *imgViewCommentBackground;
}

@property (nonatomic,strong) NSString *strTripID;


@end
