//
//  HomeVC_Guide.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "HomeVC_Guide.h"
#import "UIImageView+WebCache.h"
#import "CAPSPageMenu.h"
#import "PageView_AboutVCViewController.h"
#import "PageView_PhotosVC.h"
#import "GuideObject.h"
#import "UIView+MWParallax.h"
#import "BookGuideVC.h"
#import "ViewPhotosVC.h"
#import "ReviewGuideVC.h"
#import "LoginVC.h"
#import "AddPhotosVC.h"




#define floatParallex 20


@interface HomeVC_Guide ()
@property (nonatomic) CAPSPageMenu *pageMenu;
@end

@implementation HomeVC_Guide
@synthesize guideObject;


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    //imgUserProfie.iOS6ParallaxIntensity=floatParallex;
    
    
    //[lblPrice setTextColor:KAppColor];
    //[lblPerHour setTextColor:KAppColor];
    
    if (self.isForViewProfile)
    {
        [btnReportUser setHidden:NO];
    }
    else
    {
        [btnReportUser setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (guideObject)
    {
        [self setupViewForOtherGuide];
        [btnBack setHidden:NO];
        [btnReportUser setHidden:NO];
    }
    else
    {
        [self setupViewForTraveller];
        [btnBack setHidden:YES];
        [btnReportUser setHidden:YES];
    }
    
    //Notification for making the booking of guide.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(bookGuide) name:@"goToBookGuidePage" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToViewPhotosPage:) name:@"openPhotosPage" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotToAddPhotos:) name:@"openAddPhotosPage" object:nil];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

-(void)viewDidLayoutSubviews
{
    [self addPageView];
    frame = imgUserProfie.frame;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark- Methods

-(void)goToViewPhotosPage:(NSNotification*)notificationObject
{
    int index=[[notificationObject.object objectForKey:@"index"] intValue];
    
    ViewPhotosVC *viewPhotos=[[ViewPhotosVC alloc]init];
    
    if (guideObject)
    {
        viewPhotos.aryPhotos=guideObject.aryPhotos;
    }
    else
    {
        viewPhotos.aryPhotos=App_Delegate.userObject.aryPhotos;
    }
    
    viewPhotos.currentIndex=index;
    [self.navigationController pushViewController:viewPhotos animated:YES];
    
}

-(void)gotToAddPhotos:(NSNotification*)notificationObject
{
    AddPhotosVC *changePasswordVC=[[AddPhotosVC alloc]init];
    [self.navigationController pushViewController:changePasswordVC animated:YES];
}


-(void)bookGuide
{
    if (guideObject)
    {
        BookGuideVC *bookGuideVC=[[BookGuideVC alloc]init];
        bookGuideVC.guideObject=guideObject;
        [self.navigationController pushViewController:bookGuideVC animated:YES];
    }
    else
    {
        [App_Delegate showAlertWithMessage:@"Guide details not found contact developers" inViewController:self];
    }
}

-(void)addPageView
{
    
    for (UIView *view in scrollViewMain.subviews)
    {
        if (view.tag==101)
        {
            [view removeFromSuperview];
        }
    }
    
    PageView_AboutVCViewController *view1=[[PageView_AboutVCViewController alloc]init];
    
    view1.title=@"About me";
    
    ReviewGuideVC *view2=[[ReviewGuideVC alloc]init];
    view2.title=@"Reviews";
    
    PageView_PhotosVC *view3=[[PageView_PhotosVC alloc]init];
    view3.title=@"Photos";
    
    view1.isForViewProfile=view2.isForViewProfile=view3.isForViewProfile=self.isForViewProfile;
    
    if (guideObject)
    {
        view1.guideObject=guideObject;
        view2.guideObject=guideObject;
        view3.guideObject=guideObject;
        view2.aryInformation=guideObject.aryReviews;
    }
    else
    {
        view2.aryInformation=App_Delegate.userObject.aryReviews;
    }
    
    NSArray *controllerArray=@[view1,view2,view3];
    
    
    NSDictionary *parameters = @{
                                 
                                 CAPSPageMenuOptionMenuItemSeparatorWidth:@(10.0),
                                 CAPSPageMenuOptionMenuItemSeparatorColor:KAppColor_Blue,
                                 CAPSPageMenuOptionMenuItemSeparatorPercentageHeight:@(80),
                                 CAPSPageMenuOptionMenuItemSeparatorRoundEdges:@(YES),
                                 CAPSPageMenuOptionScrollMenuBackgroundColor:[UIColor clearColor],
                                 CAPSPageMenuOptionViewBackgroundColor:[UIColor whiteColor],
                                 CAPSPageMenuOptionSelectionIndicatorColor: KAppColor_Blue,
                                 //CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor darkGrayColor],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"SourceSansPro-Regular" size:16.0],
                                 CAPSPageMenuOptionMenuHeight: @(40.0),
                                 CAPSPageMenuOptionMenuItemWidth: @([UIScreen mainScreen].bounds.size.width/3),
                                 CAPSPageMenuOptionCenterMenuItems: @(NO),
                                 CAPSPageMenuOptionMenuMargin:@(0),
                                 //CAPSPageMenuOptionSelectedMenuItemLabelColor:KAppColor_Blue
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor:[UIColor blackColor],
                                 };
    
    CGRect framePage=CGRectMake(0.0,imageContainer.frame.origin.y+imageContainer.frame.size.height,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-constScrollBottomSpace.constant-scrollViewMain.frame.origin.y-imageContainer.frame.origin.y);
    
    if (!_pageMenu)
    {
        _pageMenu = [[CAPSPageMenu alloc]initWithViewControllers:controllerArray frame:framePage options:parameters];
        [_pageMenu.view setTag:1011];
        [scrollViewMain addSubview:_pageMenu.view];
    }
    
    [_pageMenu.view setFrame:framePage];
    
    [scrollViewMain setContentSize:CGSizeMake(0,_pageMenu.view.frame.size.height+_pageMenu.view.frame.origin.y)];
    
}

-(void)setupViewForTraveller
{
    
    //Setting User Name
    NSString *strUserName=@"";
    
    if (App_Delegate.userObject.strFirstName!=nil && App_Delegate.userObject.strFirstName!=(id)[NSNull null])
    {
        strUserName=[strUserName stringByAppendingString:App_Delegate.userObject.strFirstName];
        [lblLocalName setText:strUserName];
    }
    
    if (App_Delegate.userObject.strLastName!=nil && App_Delegate.userObject.strLastName!=(id)[NSNull null])
    {
        //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",App_Delegate.userObject.strLastName]];
        //[lblLocalName setText:strUserName];
    }
    
    if (App_Delegate.userObject.isMale)
    {
        [lblGender setText:@"Male"];
    }
    else
    {
        [lblGender setText:@"Female"];
    }
    
    if (App_Delegate.userObject.strBirthDate!=nil && App_Delegate.userObject.strBirthDate!=(id)[NSNull null])
    {
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        
        [lblAge setText:[NSString stringWithFormat:@"%d Years old",[App_Delegate getAgeFromBirthdate:[formatter dateFromString:App_Delegate.userObject.strBirthDate]]]];
    }
    
    NSString *strLocation=@"";
    
    if(App_Delegate.userObject.strCity!=nil && App_Delegate.userObject.strCity!=(id)[NSNull null] && App_Delegate.userObject.strCity.length>0)
    {
        strLocation=[strLocation stringByAppendingString:App_Delegate.userObject.strCity];
        [lblLocalLocation setText:strLocation];
    }
    
    if(App_Delegate.userObject.strCountry!=nil && App_Delegate.userObject.strCountry!=(id)[NSNull null] && App_Delegate.userObject.strCountry.length>0)
    {
        strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",App_Delegate.userObject.strCountry]];
        [lblLocalLocation setText:strLocation];
    }
    
    if (App_Delegate.userObject.strProfileImageURL!=nil && App_Delegate.userObject.strProfileImageURL!=(id)[NSNull null])
    {
        [imgUserProfie sd_setImageWithURL:[NSURL URLWithString:App_Delegate.userObject.strProfileImageURL]];
    }
    
    //PricePuru
    //    if (App_Delegate.userObject.locatDetailObject.priceObject.strPrice!=nil && App_Delegate.userObject.locatDetailObject.priceObject.strPrice!=(id)[NSNull null])
    //    {
    //        [lblPrice setText:[NSString stringWithFormat:@"$%@",App_Delegate.userObject.locatDetailObject.priceObject.strPrice]];
    //    }
    
    [lblPrice setHidden:YES];
    [lblPerHour setHidden:YES];
    
    //Hide Book Me Tab
    [viewButtonBackground setHidden:YES];
    [btnBookMe setHidden:YES];
    constScrollBottomSpace.constant=0;
    
}

-(void)setupViewForOtherGuide
{
    
    //Setting User Name
    NSString *strUserName=@"";
    
    if (guideObject.strFirstName!=nil && guideObject.strFirstName!=(id)[NSNull null])
    {
        strUserName=[strUserName stringByAppendingString:guideObject.strFirstName];
        [lblLocalName setText:strUserName];
    }
    
    if (guideObject.strLastName!=nil && guideObject.strLastName!=(id)[NSNull null])
    {
        //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",guideObject.strLastName]];
        //[lblLocalName setText:strUserName];
    }
    
    if (guideObject.isMale)
    {
        [lblGender setText:@"Male"];
    }
    else
    {
        [lblGender setText:@"Female"];
    }
    
    if (guideObject.strBirthDate!=nil && guideObject.strBirthDate!=(id)[NSNull null])
    {
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        
        [lblAge setText:[NSString stringWithFormat:@"%d Years old",[App_Delegate getAgeFromBirthdate:[formatter dateFromString:guideObject.strBirthDate]]]];
    }
    
    NSString *strLocation=@"";
    
    
    if(guideObject.strCity!=nil && guideObject.strCity!=(id)[NSNull null] && guideObject.strCity.length>0)
    {
        strLocation=[strLocation stringByAppendingString:guideObject.strCity];
        [lblLocalLocation setText:strLocation];
    }
    
    if(guideObject.strCountry!=nil && guideObject.strCountry!=(id)[NSNull null] && guideObject.strCountry.length>0)
    {
        strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",guideObject.strCountry]];
        [lblLocalLocation setText:strLocation];
    }
    
    if (guideObject.strProfileImageURL!=nil && guideObject.strProfileImageURL!=(id)[NSNull null])
    {
        [imgUserProfie sd_setImageWithURL:[NSURL URLWithString:guideObject.strProfileImageURL]];
    }
    
    //PricePuru
    //    if (guideObject.locatDetailObject.priceObject.strPrice!=nil && guideObject.locatDetailObject.priceObject.strPrice!=(id)[NSNull null])
    //    {
    //        [lblPrice setText:[NSString stringWithFormat:@"$%@",guideObject.locatDetailObject.priceObject.strPrice]];
    //    }
    
    if (App_Delegate.selectedGuide.priceObject.strPrice!=nil && App_Delegate.selectedGuide.priceObject.strPrice!=(id)[NSNull null])
    {
        [lblPrice setText:[NSString stringWithFormat:@"$%@",App_Delegate.selectedGuide.priceObject.strPrice]];
    }
    else
    {
        [lblPrice setText:[NSString stringWithFormat:@"$ Not Detected"]];
    }
    
    
    if (guideObject && self.isForViewProfile==NO)
    {
        constScrollBottomSpace.constant=55;
    }
    else
    {
        //Hide Book Me Tab
        [viewButtonBackground setHidden:YES];
        [btnBookMe setHidden:YES];
        constScrollBottomSpace.constant=0;
    }
}

-(void)callReportUserProfileAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSLog(@"Reporting : %@ %@",App_Delegate.userObject.strId,App_Delegate.userObject.strEmail);
    NSLog(@"Report this user for spam or other violation?",guideObject.strId,guideObject.strEmail);
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KReportUser];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"reporting_user"];
    [dicParameter setObject:guideObject.strId forKey:@"report_user"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(reportUserResponse:) WithCallBackObject:self];
}

-(void)reportUserResponse:(NSDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        [App_Delegate showAlertWithMessage:@"Thank you for reporting user" inViewController:self];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"User details not found" inViewController:self];
        }
    }

}

-(void)showGuestAlert
{
    
    NSString *strMessage=@"Please sign up or login to report a lcoal guide.";
    
    SCLAlertView *alert =[App_Delegate getAlertInstance];
    
    [alert addButton:@"No" actionBlock:^(void) {
        
    }];
    
    [alert addButton:@"Yes" actionBlock:^(void) {
        
        [App_Delegate.xmppManager disconnect];
        
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        
        LoginVC *viewController=[[LoginVC alloc]init];
        App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
        [App_Delegate.navigationController setNavigationBarHidden:YES];
        [App_Delegate.window setRootViewController:App_Delegate.navigationController];
        
    }];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:strMessage closeButtonTitle:nil duration:0.0];
}

#pragma mark- ScrollView Delegate

-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    
    if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height)
    {
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, scrollView.contentSize.height - scrollView.frame.size.height)];
        
        //NSLog(@"Enable- Table Scrolling");
        App_Delegate.shouldDisableScrolling=YES;
        [[NSNotificationCenter defaultCenter]postNotificationName:@"enableTableScrolling" object:nil];
    }
    else
    {
        //NSLog(@"Disable- Table Scrolling");
        [[NSNotificationCenter defaultCenter]postNotificationName:@"disableScrolling" object:nil];
        App_Delegate.shouldDisableScrolling=NO;
    }
    
//    if (scrollView.contentOffset.y>=frame.size.height-floatParallex-floatParallex)
//    {
//        App_Delegate.shouldDisableScrolling=YES;
//        [[NSNotificationCenter defaultCenter]postNotificationName:@"enableTableScrolling" object:nil];
//    }
//    
//    if (scrollView.contentOffset.y<=0)
//    {
//        App_Delegate.shouldDisableScrolling=NO;
//        
//    }
}

#pragma mark- AlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1012)
    {
        NSLog(@"Logout Alert");
        
        NSLog(@"Button index : %d",(int)buttonIndex);
        
        if (buttonIndex==1)
        {
            if ([App_Delegate getCurrentUser]==Guest)
            {
                [self showGuestAlert];
                return;
            }
            
            [self callReportUserProfileAPI];
        }
    }
    
    
    if (alertView.tag==7800)
    {
        //Guest Alert
        if (buttonIndex==1)
        {
            [App_Delegate.xmppManager disconnect];
            
            NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
            [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
            
            LoginVC *viewController=[[LoginVC alloc]init];
            App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
            [App_Delegate.navigationController setNavigationBarHidden:YES];
            [App_Delegate.window setRootViewController:App_Delegate.navigationController];
        }
    }
}

#pragma mark- Button Actions

-(IBAction)btnReportUser:(id)sender
{
    
    NSString *strMessage=[NSString stringWithFormat:@"Report this user for spam or other violation?",guideObject.strFirstName];
    
    SCLAlertView *alert =[App_Delegate getAlertInstance];
    
    [alert addButton:@"No" actionBlock:^(void) {
        
    }];
    
    [alert addButton:@"Yes" actionBlock:^(void) {
        
        if ([App_Delegate getCurrentUser]==Guest)
        {
            [self showGuestAlert];
            return;
        }
        
        [self callReportUserProfileAPI];
        
    }];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:strMessage closeButtonTitle:nil duration:0.0];
}

-(IBAction)btnBackPressed:(id)sender
{
    NSArray *viewControlles =self.navigationController.viewControllers;
    
    if (self.isForViewProfile==NO || viewControlles.count>0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self btnTogglePressed:nil];
    }
}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}


-(IBAction)btnBookMeButtonPressed:(id)sender
{
    [self bookGuide];
}

@end
