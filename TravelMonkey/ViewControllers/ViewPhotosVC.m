//
//  ViewPhotosVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/6/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "ViewPhotosVC.h"
#import "PhotoObject.h"
#import "UIImageView+WebCache.h"


@interface ViewPhotosVC ()

@end

@implementation ViewPhotosVC

@synthesize aryPhotos,currentIndex;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    [scrl setHidden:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self setupScrollView];
}

#pragma mark- Button Action

-(IBAction)btnGoBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- Scroll View delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrl.tag!=1020)
    {
        //Load visible page
        [self loadVisiblePages];
        
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page = scrl.contentOffset.x/scrl.frame.size.width;
    
    //Get object
    PhotoObject *photoObject=[aryPhotos objectAtIndex:page];
    
    [lblCaption setText:photoObject.strCaption];
    [lblCaption layoutIfNeeded];
    
    //set page number
    [lblPageNumber setText:[NSString stringWithFormat:@"%d/%lu",page+1,(unsigned long)aryPhotos.count]];
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    UIView *view = [scrollView.subviews objectAtIndex:0];
    return view;
}

#pragma mark- Methods

-(void)setupScrollView
{
    //Removing views for the first time
    for (UIView *v in scrl.subviews)
    {
        [v removeFromSuperview];
    }
    
    scrl.pagingEnabled = YES;
    scrl.delegate = self;
    
    scrl.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width*aryPhotos.count,0);
    
    pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < aryPhotos.count; ++i)
    {
        [pageViews addObject:[NSNull null]];
    }
    
    //Label Page Number
    float lblHeight=30;
    
    lblPageNumber=[[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-lblHeight,self.view.frame.size.width,lblHeight)];
    [lblPageNumber setTextAlignment:NSTextAlignmentCenter];
    [lblPageNumber setTextColor:[UIColor whiteColor]];
    [lblPageNumber setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:25]];
    //[lblPageNumber setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:lblPageNumber];
    
    [self loadVisiblePages];
    
    //Get object
    PhotoObject *photoObject=[aryPhotos objectAtIndex:self.currentIndex];
    
    [lblCaption setText:photoObject.strCaption];
    [lblCaption layoutIfNeeded];
    
    //set page number
    [lblPageNumber setText:[NSString stringWithFormat:@"%d/%lu",self.currentIndex+1,(unsigned long)aryPhotos.count]];
    
    [self performSelector:@selector(moveToIndex) withObject:nil afterDelay:0.2];
}

-(void)moveToIndex
{
    //Move to the index
    NSLog(@"Move to index : %d",currentIndex);
    [scrl setContentOffset:CGPointMake(currentIndex*self.view.frame.size.width,0)];
    [scrl setHidden:NO];
}

- (void)loadVisiblePages
{
    // First, determine which page is currently visible
    CGFloat pageWidth = scrl.frame.size.width;
    
    NSInteger page = (NSInteger)floor((scrl.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    // Update the page control
    //self.pageControl.currentPage = page;
    
    // Work out which pages you want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++)
    {
        [self purgePage:i];
    }
    
    // Load pages in our range
    for (NSInteger i=firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    
    // Purge anything after the last page
    for (NSInteger i=lastPage+1; i<pageViews.count; i++) {
        [self purgePage:i];
    }
}

- (void)purgePage:(NSInteger)page
{
    if (page < 0 || page >=aryPhotos.count)
    {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [pageViews objectAtIndex:page];
    
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (void)loadPage:(NSInteger)page
{
    if (page < 0 || page >=aryPhotos.count)
    {
        //If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    
    //Get object
    PhotoObject *photoObject=[aryPhotos objectAtIndex:page];

    
    // 1
    UIView *pageView = [pageViews objectAtIndex:page];
    
    if ((NSNull*)pageView == [NSNull null])
    {
        
        //Scrollview
        UIScrollView *mainView=[[UIScrollView alloc]initWithFrame:CGRectMake(page*scrl.frame.size.width,0,scrl.frame.size.width,scrl.frame.size.height)];
        
        [mainView setMinimumZoomScale:1];
        [mainView setMaximumZoomScale:4];
        [mainView setDelegate:self];
        [mainView setTag:1020];
        
        //Image view inside scrollview
        UIImageView *imgView=[[UIImageView alloc]init];
        [imgView setFrame:mainView.bounds];
        [imgView setClipsToBounds:YES];
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
        [imgView setUserInteractionEnabled:YES];
        
        //Activity indicator
        UIActivityIndicatorView *indicatorView=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [indicatorView setCenter:imgView.center];
        [indicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        [indicatorView startAnimating];
        
        [imgView sd_setImageWithURL:[NSURL URLWithString:photoObject.strImageURL] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            [indicatorView removeFromSuperview];
            
            
            // what object is going to handle the gesture when it gets recognised ?
            // the argument for tap is the gesture that caused this message to be sent
            UITapGestureRecognizer *tapTwice = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTwice:)];
            
            // set number of taps required
            tapTwice.numberOfTapsRequired = 2;
            
            // stops tapOnce from overriding tapTwice
            
            // now add the gesture recogniser to a view
            // this will be the view that recognises the gesture
            [mainView addGestureRecognizer:tapTwice];
            
            
            
        }];
        
        
    
        [mainView addSubview:imgView];
        [scrl addSubview:mainView];
        
        //temp
        //[imgView.layer setCornerRadius:2];
        //[imgView.layer setBorderColor:[UIColor blueColor].CGColor];
        //[imgView setBackgroundColor:[UIColor greenColor]];
        
        [pageViews replaceObjectAtIndex:page withObject:mainView];
    }
}

- (CGRect)zoomRectForScrollView:(UIScrollView *)scrollView withScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    
    // The zoom rect is in the content view's coordinates.
    // At a zoom scale of 1.0, it would be the size of the
    // imageScrollView's bounds.
    // As the zoom scale decreases, so more content is visible,
    // the size of the rect grows.
    zoomRect.size.height = scrollView.frame.size.height / scale;
    zoomRect.size.width  = scrollView.frame.size.width  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
}

- (void)tapTwice:(UIGestureRecognizer *)gesture
{
   
   UIScrollView *scrlView=(UIScrollView*)[gesture view];
    
   if(scrlView.zoomScale==1)
   {
       //CGRect rect = [[scrlView.subviews objectAtIndex:0] bounds];
       //[scrlView zoomToRect:rect animated:YES];
       //[scrlView.layer setAnchorPoint:CGPointMake(scrlView.frame.size.width/2, scrlView.frame.size.height/2)];
       [scrlView setZoomScale:2];
        isZoomed = YES;
    }
    else
    {
        //CGRect zrect = [self zoomRectForScrollView:scrlView withScale:0.6 withCenter:gesture.view.center];
        //[scrlView zoomToRect:zrect animated:YES];
        //[scrlView.layer setAnchorPoint:CGPointMake(scrlView.frame.size.width/2, scrlView.frame.size.height/2)];
        [scrlView setZoomScale:1];
        isZoomed = NO;
    }
}

@end
