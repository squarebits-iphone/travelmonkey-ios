//
//  SelectGuideVC_Tourist.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/22/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectGuideVC_Tourist : UIViewController
{
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UICollectionView *collectionViewTable;
    
    NSMutableArray *aryGuides;
    NSMutableDictionary *dicFilter;
    GrayTouchView *grayView;
    
}


@property (nonatomic,strong) NSString *strStartTime;
@property (nonatomic,strong) NSString *strEndTime;

@property (nonatomic,strong) GMSPlace *touristGMSPlace;



@end
