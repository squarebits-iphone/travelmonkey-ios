//
//  TripsVC_Guide.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/5/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripObject.h"

@interface TripsVC_Guide : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    //Outlets
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UITableView *tblTrips;
    IBOutlet UIButton *btnFutureTrips;
    IBOutlet UIButton *btnPastTrips;
    
    BOOL showFutureTrips;
    
    NSMutableArray *aryFutureTrips;
    NSMutableArray *aryPastTrips;
    
    NSTimer *refreshTimer;
    
    TripObject *currentObject;
    
}

@end
