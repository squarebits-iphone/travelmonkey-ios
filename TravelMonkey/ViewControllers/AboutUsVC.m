//
//  AboutUsVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/11/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "AboutUsVC.h"
#import "AboutUsCell.h"
#import "WebViewVC.h"
#import "LoginVC.h"



#define KName @"name"
#define KImage @"image"
#define KSelectedImage @"imageSelected"


@interface AboutUsVC ()

@end

@implementation AboutUsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewStatusBar setBackgroundColor:KAppColor];
    [viewTitle setBackgroundColor:KAppColor];
    
    aryOptions=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Rate us in the App Store" forKey:KName];
    [dic setObject:@"icn_rateus" forKey:KImage];
    [dic setObject:@"icn_rateus_touch" forKey:KSelectedImage];
    [aryOptions addObject:dic];
    
    dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Like us on Facebook" forKey:KName];
    [dic setObject:@"icn_like_AboutUs" forKey:KImage];
    [dic setObject:@"icn_like_touch" forKey:KSelectedImage];
    [aryOptions addObject:dic];
    
    dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Follow us on Instagram" forKey:KName];
    [dic setObject:@"icn_instagram" forKey:KImage];
    [dic setObject:@"icn_instagram_touch" forKey:KSelectedImage];
    [aryOptions addObject:dic];
    
    dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Follow us on Twitter" forKey:KName];
    [dic setObject:@"icn_twitter" forKey:KImage];
    [dic setObject:@"icn_twitter_touch" forKey:KSelectedImage];
    [aryOptions addObject:dic];
    
    dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Follow us on Pinterest" forKey:KName];
    [dic setObject:@"icn_pintrest" forKey:KImage];
    [dic setObject:@"icn_pintrest_touch" forKey:KSelectedImage];
    [aryOptions addObject:dic];

    dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Terms and Conditions" forKey:KName];
    [dic setObject:@"icn_terms_condition" forKey:KImage];
    [dic setObject:@"icn_terms_condition_touch" forKey:KSelectedImage];
    [aryOptions addObject:dic];
    
    dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Privacy Policy" forKey:KName];
    [dic setObject:@"icn_privacy" forKey:KImage];
    [dic setObject:@"icn_privacy_policy_touch" forKey:KSelectedImage];
    [aryOptions addObject:dic];
    
    if ([App_Delegate getCurrentUser]!=Guest)
    {
        dic=[[NSMutableDictionary alloc]init];
        [dic setObject:@"Logout" forKey:KName];
        [dic setObject:@"icn_logout_AU" forKey:KImage];
        [dic setObject:@"icn_logout_touch" forKey:KSelectedImage];
        [aryOptions addObject:dic];

    }
    
    //Setting build Information
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    NSString *strInformation=[NSString stringWithFormat:@"Version : %@",version];
    [lblVersionInfo setText:strInformation];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Methods

-(void)rateUsOnAppStore
{
    
    NSString *str;
    float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    if (ver >= 7.0 && ver < 7.1)
    {
        str = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@",KApplicationId];
    }
    else if (ver >= 8.0)
    {
        str = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software",KApplicationId];
    }
    else
    {
        str = [NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@",KApplicationId];
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

-(void)likeUsOnFacebook
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@",KFacebookPageID]]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@",KFacebookPageID]]];
    }
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://facebook.com/%@",KFacebookPageName]]];
    }
}

-(void)followOnInstagram
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:KInstagramPageURL]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:KInstagramPageURL]];
    }
}

-(void)followOnTwitter
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:KTwitterPageURL]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:KTwitterPageURL]];
    }
}

-(void)followOnPinterest
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:KPinterestPageURL]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:KPinterestPageURL]];
    }
}

-(void)termsAndCondition
{
    WebViewVC *webView=[[WebViewVC alloc]init];
    webView.strUrl=KTermsAndConditionPage;
    webView.strTitle=@"Terms and Conditions";
    [self.navigationController pushViewController:webView animated:YES];
}

-(void)privacyPolicy
{
    WebViewVC *webView=[[WebViewVC alloc]init];
    webView.strUrl=KPrivacyPolicyPage;
    webView.strTitle=@"Privacy Policy";
    [self.navigationController pushViewController:webView animated:YES];
}

-(void)callLogoutAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KLogoutUser];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(logoutAPIResponse:) WithCallBackObject:self];
    
}

-(void)logoutAPIResponse:(NSDictionary*)dictionary
{
    [self LogOutUser];
}


-(void)LogOutUser
{
    [App_Delegate.xmppManager disconnect];
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    LoginVC *viewController=[[LoginVC alloc]init];
    App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
    [App_Delegate.navigationController setNavigationBarHidden:YES];
    [App_Delegate.window setRootViewController:App_Delegate.navigationController];
}

-(void)logoutAlert
{
    
    SCLAlertView *alert=[App_Delegate getAlertInstance];
    
    [alert addButton:@"No" actionBlock:^(void) {
        
    }];
    
    [alert addButton:@"Yes" actionBlock:^(void) {
        
        [self LogOutUser];
        
    }];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:@"Are you sure you want to log out?" closeButtonTitle:nil duration:0.0];

    
}


#pragma mark- Button Action

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

#pragma mark- AlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1012)
    {
        
        NSLog(@"Logout Alert");
        
        NSLog(@"Button index : %d",(int)buttonIndex);
        
        if (buttonIndex==1)
        {
            [self callLogoutAPI];
        }
    }
}


#pragma mark- Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return aryOptions.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strCellIdentifier=@"AboutUsCell";
    
    AboutUsCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
    
    if (cell==nil)
    {
        cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NSMutableDictionary *dic=[aryOptions objectAtIndex:indexPath.row];
    
    [cell.lblTitle setText:[dic objectForKey:KName]];
    NSLog(@"Title : %@",[dic objectForKey:KName]);
    
    [cell.imgIcon setImage:[UIImage imageNamed:[dic objectForKey:KImage]]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AboutUsCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    
    [cell.lblTitle setTextColor:[UIColor whiteColor]];
    [cell.imgViewBorder setImage:[UIImage imageNamed:@"cell_contactus_touch.png"]];
    [cell.imgViewArrow setImage:[UIImage imageNamed:@"icn_arrow_contactus_touch.png"]];
    
    NSMutableDictionary *dic=[aryOptions objectAtIndex:indexPath.row];
    [cell.imgIcon setImage:[UIImage imageNamed:[dic objectForKey:KSelectedImage]]];
    
    switch (indexPath.row)
    {
        case 0:
        {
            [self rateUsOnAppStore];
            break;
        }
            
        case 1:
        {
            [self likeUsOnFacebook];
            break;
        }
            
        case 2:
        {
            [self followOnInstagram];
            break;
        }
            
        case 3:
        {
            [self followOnTwitter];
            break;
        }
            
        case 4:
        {
            [self followOnPinterest];
            break;
        }
            
        case 5:
        {
            [self termsAndCondition];
            break;
        }
            
        case 6:
        {
            [self privacyPolicy];
            break;
        }
            
        case 7:
        {
            [self logoutAlert];
            break;
        }
            
        default:
            break;
    }
    
    [tableView performSelector:@selector(reloadData) withObject:nil afterDelay:1.0];
}


@end
