//
//  HomeVC_Guide.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"
#import "UserObject.h"



@interface HomeVC_TouristProfile : UIViewController
{
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UIView *imageContainer;

    IBOutlet UIScrollView *scrollViewMain;
    
    IBOutlet UILabel *lblLocalName;
    IBOutlet UILabel *lblLocalLocation;
    IBOutlet UILabel *lblPrice;
    IBOutlet UILabel *lblGender;
    IBOutlet UILabel *lblAge;
    
    
    IBOutlet UIButton *btnBack;
    IBOutlet UIButton *btnReportUser;
    IBOutlet UIImageView *imgUserProfie;
    CGRect frame;
    
}

@property (nonatomic,strong) UserObject *touristObject;
@property BOOL isForViewProfile;


@end
