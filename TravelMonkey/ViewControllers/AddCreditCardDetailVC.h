//
//  AddCreditCardDetailVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/20/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreditCard.h"


@import Stripe;


@interface AddCreditCardDetailVC : UIViewController<STPPaymentContextDelegate>
{
    IBOutlet UITableView *tblCardView;
    NSMutableArray *aryCard;
    //Outlets
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    
    CreditCard *currentCard;
    
}

@property (nonatomic,strong) STPPaymentContext *paymentContext;


@end
