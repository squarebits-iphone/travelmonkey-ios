//
//  LeftVC_Tourist.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/9/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "LeftVC_Tourist.h"
#import "DeckCustomCell.h"
#import "HomeVC_Tourist.h"
#import "LoginVC.h"
#import "EditProfileVC_Tourist.h"
#import "HomeVC_TouristProfile.h"
#import "AddCreditCardDetailVC.h"
#import "TripsVC_Traveller.h"
#import "AboutUsVC.h"
#import "InviteFriendsVC.h"
#import "HelpVC.h"


#import "UIImageView+WebCache.h"


@interface LeftVC_Tourist ()

@end

@implementation LeftVC_Tourist

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Setting Image
    [imgProfile.layer setCornerRadius:130/2];
    [imgProfile.layer setBorderColor:[UIColor clearColor].CGColor];
    [viewSideBar setBackgroundColor:RGBCOLOR(40, 56, 79)];
    [lblViewProfile setTextColor:KAppColor_GrayBlue];
    [imgProfile.layer setBorderWidth:0.2];
    [imgProfile setClipsToBounds:YES];
    
    //Add tap gesture to image
    UITapGestureRecognizer *tapGesture1=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewProfile)];
    UITapGestureRecognizer *tapGesture2=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewProfile)];
    UITapGestureRecognizer *tapGesture3=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewProfile)];
    [imgProfile addGestureRecognizer:tapGesture1];
    [lblUserName addGestureRecognizer:tapGesture2];
    [lblViewProfile addGestureRecognizer:tapGesture3];
    
    
    //Setting User Name
    NSString *strUserName=@"";
    
    if (App_Delegate.userObject.strFirstName!=nil && App_Delegate.userObject.strFirstName!=(id)[NSNull null])
    {
        strUserName=[strUserName stringByAppendingString:App_Delegate.userObject.strFirstName];
        [lblUserName setText:strUserName];
    }
    
    if (App_Delegate.userObject.strLastName!=nil && App_Delegate.userObject.strLastName!=(id)[NSNull null])
    {
        //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",App_Delegate.userObject.strLastName]];
        //[lblUserName setText:strUserName];
    }

    //Registering Cell for reuse
    [tblOption registerNib:[UINib nibWithNibName:@"DeckCustomCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"DeckCustomCell"];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(openTripPage) name:@"bookingConfirmed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openTripPage) name:@"openTripPage" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(openEditProfilePage) name:@"openEditProfile" object:nil];
    
    currentIndex=[NSIndexPath indexPathForRow:0 inSection:0];
    [tblOption selectRowAtIndexPath:currentIndex animated:YES scrollPosition:UITableViewScrollPositionNone];
}

-(void)viewDidAppear:(BOOL)animated
{
    if (App_Delegate.userObject.strProfileImageURL!=nil && App_Delegate.userObject.strProfileImageURL!=(id)[NSNull null])
    {
        [imgProfile sd_setImageWithURL:[NSURL URLWithString:App_Delegate.userObject.strProfileImageURL]];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Methods

-(void)openEditProfilePage
{
    EditProfileVC_Tourist *viewCont = [[EditProfileVC_Tourist alloc]init];
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
    [navController setNavigationBarHidden:YES];
    App_Delegate.deckController.centerController=navController;
    [App_Delegate.dictRootController setObject:navController forKey:@"1"];
}

-(void)callLogoutAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KLogoutUser];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(logoutAPIResponse:) WithCallBackObject:self];
    
}

-(void)logoutAPIResponse:(NSDictionary*)dictionary
{
    [self LogOutUser];
}


-(void)LogOutUser
{
    [App_Delegate.xmppManager disconnect];
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    LoginVC *viewController=[[LoginVC alloc]init];
    App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
    [App_Delegate.navigationController setNavigationBarHidden:YES];
    [App_Delegate.window setRootViewController:App_Delegate.navigationController];
}

-(void)logoutAlert
{
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:KAppName message:@"Are you sure to logout?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
    [alertView setTag:1012];
    
    [alertView show];
}

-(void)openTripPage
{
    TripsVC_Traveller *viewCont = [[TripsVC_Traveller alloc]init];
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
    [navController setNavigationBarHidden:YES];
    App_Delegate.deckController.centerController=navController;
    [App_Delegate.dictRootController setObject:navController forKey:@"2"];
}

-(void)viewProfile
{
    [lblViewProfile setTextColor:[UIColor whiteColor]];
    [imgProfile.layer setBorderColor:[UIColor clearColor].CGColor];
    [imgProfile.layer setBorderWidth:1];
    currentIndex=[NSIndexPath indexPathForRow:0 inSection:1];
    [tblOption reloadData];
    
    HomeVC_TouristProfile *viewCont = [[HomeVC_TouristProfile alloc]init];
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
    [navController setNavigationBarHidden:YES];
    App_Delegate.deckController.centerController=navController;
    [App_Delegate.dictRootController setObject:navController forKey:@"0"];
    
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

#pragma mark- AlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1012)
    {
        
        NSLog(@"Logout Alert");
        
        NSLog(@"Button index : %d",(int)buttonIndex);
        
        if (buttonIndex==1)
        {
            [self callLogoutAPI];
        }
    }
}

#pragma mark- Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DeckCustomCell *cell = (DeckCustomCell *)[tableView dequeueReusableCellWithIdentifier:@"deckCustomCell"];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DeckCustomCell" owner:self options:nil];
        cell = (DeckCustomCell *)[nib objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    [cell.imgStatus setHidden:YES];
    
    switch (indexPath.row)
    {
        case 0:
        {
            cell.lblOptionName.text = @"Home";
            cell.imgView.image=[UIImage imageNamed:@"icn_home.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_home_hover.png"];
            
            break;
        }
        case 1:
        {
            cell.lblOptionName.text = @"Edit Profile";
            cell.imgView.image=[UIImage imageNamed:@"icn_edit_profile.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_edit_profile_hover.png"];
            break;
        }
        case 2:
        {
            cell.lblOptionName.text = @"Bookings";
            cell.imgView.image=[UIImage imageNamed:@"icn_trips.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_trips_hover.png"];
            
            break;
        }
        case 3:
        {
            cell.lblOptionName.text = @"Payments";
            cell.imgView.image=[UIImage imageNamed:@"icn_payments.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_payments_hover.png"];
            break;
        }
        case 4:
        {
            cell.lblOptionName.text = @"Help";
            cell.imgView.image=[UIImage imageNamed:@"icn_help.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_help_hover.png"];
            break;
        }
        case 5:
        {
            cell.lblOptionName.text = @"Invite Friends";
            cell.imgView.image=[UIImage imageNamed:@"icn_invite_friends.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_invite_friends_hover.png"];
            break;
        }
        case 6:
        {
            cell.lblOptionName.text = @"About Us";
            cell.imgView.image=[UIImage imageNamed:@"icn_about_us.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_about_us_hover.png"];
            break;
        }
        case 7:
        {
            cell.lblOptionName.text = @"Logout";
            cell.imgView.image=[UIImage imageNamed:@"icn_logout_menu.png"];
            cell.imgViewHighLight.image=[UIImage imageNamed:@"icn_logout_menu_hover.png"];
            break;
        }
            
        default:
            break;
    }
    
    if (currentIndex==indexPath)
    {
        [cell.lblOptionName setTextColor:[UIColor whiteColor]];
        [cell.imgView setHidden:YES];
        [cell.imgViewHighLight setHidden:NO];
    }
    else
    {
       [cell.lblOptionName setTextColor:KAppColor_GrayBlue];
        [cell.imgView setHidden:NO];
        [cell.imgViewHighLight setHidden:YES];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [lblViewProfile setTextColor:KAppColor_GrayBlue];
    DeckCustomCell *cell = (DeckCustomCell *)[tableView cellForRowAtIndexPath:indexPath];
    [AppDelegate rippleWithView:cell.viewRipple center:cell.viewRipple.center colorFrom:KAppColor_Blue colorTo:KAppColor_Blue];
    
    if (indexPath.row==0 || indexPath.row==1 || indexPath.row==7 || indexPath.row==3 || indexPath.row==2 || indexPath.row==6 || indexPath.row==5 || indexPath.row==4)
    {
        currentIndex=indexPath;
        [imgProfile.layer setBorderWidth:0];
        [tblOption reloadData];
    }
    else
    {
        [App_Delegate showAlertWithMessage:@"Under Construction" inViewController:self];
        return;
    }
    
    if ([App_Delegate.dictRootController objectForKey:[NSString stringWithFormat:@"%d",(int)indexPath.row]])
    {
        App_Delegate.deckController.centerController=[App_Delegate.dictRootController objectForKey:[NSString stringWithFormat:@"%d",(int)indexPath.row]];
    }
    else
    {
        switch (indexPath.row)
        {
            //Home
            case 0:
            {
                HomeVC_Tourist *viewCont = [[HomeVC_Tourist alloc]init];
                
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                [navController setNavigationBarHidden:YES];
                App_Delegate.deckController.centerController=navController;
                [App_Delegate.dictRootController setObject:navController forKey:@"0"];
                
                break;
            }
            case 1:
            {
                EditProfileVC_Tourist *viewCont = [[EditProfileVC_Tourist alloc]init];
                
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                [navController setNavigationBarHidden:YES];
                App_Delegate.deckController.centerController=navController;
                [App_Delegate.dictRootController setObject:navController forKey:@"1"];
                
                break;
            }
            case 2:
            {
                TripsVC_Traveller *viewCont = [[TripsVC_Traveller alloc]init];
                
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                [navController setNavigationBarHidden:YES];
                App_Delegate.deckController.centerController=navController;
                [App_Delegate.dictRootController setObject:navController forKey:@"2"];
                
                break;
            }
            case 3:
            {
                AddCreditCardDetailVC *viewCont = [[AddCreditCardDetailVC alloc]init];
                
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                [navController setNavigationBarHidden:YES];
                App_Delegate.deckController.centerController=navController;
                [App_Delegate.dictRootController setObject:navController forKey:@"3"];
                
                break;
            }
            case 4:
            {
                HelpVC *viewCont = [[HelpVC alloc]init];
                viewCont.isGuide=NO;
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                [navController setNavigationBarHidden:YES];
                App_Delegate.deckController.centerController=navController;
                [App_Delegate.dictRootController setObject:navController forKey:@"4"];
                
                break;
            }
            case 5:
            {
                
                InviteFriendsVC *viewCont = [[InviteFriendsVC alloc]init];
                
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                [navController setNavigationBarHidden:YES];
                App_Delegate.deckController.centerController=navController;
                [App_Delegate.dictRootController setObject:navController forKey:@"5"];
                
                break;
            }
            case 6:
            {
                AboutUsVC *viewCont = [[AboutUsVC alloc]init];
                
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:viewCont];
                [navController setNavigationBarHidden:YES];
                App_Delegate.deckController.centerController=navController;
                [App_Delegate.dictRootController setObject:navController forKey:@"6"];
                
                break;
            }
            case 7:
            {
                [self logoutAlert];
                return;
                break;
            }

            default:
                break;
                
        }
        
    }
    
    //Purushottam sain
    [[NSNotificationCenter defaultCenter]postNotificationName:K_Notification_ReloadView object:nil];
    [self.viewDeckController performSelector:@selector(toggleLeftViewAnimated:) withObject:[NSNumber numberWithBool:YES] afterDelay:0.3];
    
}


@end
