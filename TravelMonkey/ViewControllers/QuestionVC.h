//
//  QuestionVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *tbl;
    IBOutlet UIView *viewStatusBar;
    IBOutlet UIView *viewTitle;
    
    NSMutableArray *aryMain;
    NSIndexPath *currentIndexPath;
}

@end
