//
//  EditProfileVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/12/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "EditProfileVC.h"
#import "LocalDetailObject.h"
#import "TravellerDetailObject.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "UNIRest.h"
#import "Parser.h"
#import "UITextView+Placeholder.h"
#import "ChangePasswordVC.h"
#import "AddPhotosVC.h"
#import "AddLanguageCell.h"
#import "LanguageCell.h"
#import "LanguageObject.h"
#import "PSelectionView.h"
#import "LanguageLevelVC.h"


#import <AddressBook/AddressBook.h>
#import <CoreLocation/CoreLocation.h>


@interface EditProfileVC ()

@end

@implementation EditProfileVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [txtViewAboutMe setPlaceholder:@"Type here.."];
    [txtViewAboutMe setPlaceholderColor:[UIColor lightGrayColor]];
    [txtViewOfferTourist setPlaceholder:@"Type here.."];
    [txtViewOfferTourist setPlaceholderColor:[UIColor lightGrayColor]];
    [txtViewTransportation setPlaceholder:@"Type here.."];
    [txtViewTransportation setPlaceholderColor:[UIColor lightGrayColor]];
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    [txtFieldEmail setUserInteractionEnabled:NO];
    
    [imgUserProfile.layer setCornerRadius:110/2];
    //[imgUserProfile.layer setBorderColor:KAppColor.CGColor];
    //[imgUserProfile.layer setBorderWidth:0.2];
    [imgUserProfile setClipsToBounds:YES];
    
    //Add tap gesture to image
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(editUserImage)];
    [imgUserProfile addGestureRecognizer:tapGesture];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    //Language level
    aryLanguageLevel=[[NSMutableArray alloc]initWithObjects:@"Basic",@"Intermediate",@"Fluent", nil];
    
    //Language options
    aryLanguage=[[NSMutableArray alloc]init];
    
    NSString *strPlistPath=[[NSBundle mainBundle] pathForResource:@"LanguageList" ofType:@"plist"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:strPlistPath])
    {
        aryLanguage= [[NSArray arrayWithContentsOfFile:strPlistPath] mutableCopy];
    }
    
    [self setUpViewForLocal];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadView) name:K_Notification_ReloadView object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.view addSubview:scrlView];
    [scrlView setFrame:CGRectMake(0,140,[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-140)];
}

-(void)viewDidLayoutSubviews
{
    //[constraintTableHeight setConstant:(App_Delegate.userObject.locatDetailObject.aryLanguage.count*56)+60];
    [constraintTableHeight setConstant:60];
    [scrlView setContentSize:CGSizeMake(0,btnChangePassword.frame.origin.y+80)];
    [constraintViewSubViewScrollView setConstant:btnChangePassword.frame.origin.y+80];
}

#pragma mark- Methods

-(void)reloadView
{
    [self setUpViewForLocal];
}

-(void)editUserImage
{
    UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:@"Edit Picture" delegate:(id)self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Gallery",@"Camera", nil];
    
    [actionSheet showInView:self.view];
}


-(void)setUpViewForLocal
{
    if (App_Delegate.userObject==nil)
    {
        return;
    }
    
    [btnChangePassword setTitleColor:KAppColor_Blue forState:UIControlStateNormal];
    [btnAddPhoto setTitleColor:KAppColor_Blue forState:UIControlStateNormal];
    [btnSave setTitleColor:KAppColor_Blue forState:UIControlStateNormal];
    
    NSLog(@"Image Url : %@",App_Delegate.userObject.strProfileImageURL);
    
    if (App_Delegate.userObject.strProfileImageURL!=nil && App_Delegate.userObject.strProfileImageURL!=(id)[NSNull null] && chosenImage==nil)
    {
        [imgUserProfile sd_setImageWithURL:[NSURL URLWithString:App_Delegate.userObject.strProfileImageURL]];
    }
    
    if ([App_Delegate.userObject.strUserType isEqualToString:@"TRAVELER"])
    {
        [btnTourist setSelected:YES];
        [btnLcoal setSelected:NO];
    }
    else
    {
        [btnTourist setSelected:NO];
        [btnLcoal setSelected:YES];
    }
    
    if (App_Delegate.userObject.strFirstName!=nil && App_Delegate.userObject.strFirstName!=(id)[NSNull null] && App_Delegate.userObject.strFirstName.length>0)
    {
        [txtFieldFirstName setText:App_Delegate.userObject.strFirstName];
    }
    
    if (App_Delegate.userObject.strLastName!=nil && App_Delegate.userObject.strLastName!=(id)[NSNull null] && App_Delegate.userObject.strLastName.length>0)
    {
        [txtFieldLastName setText:App_Delegate.userObject.strLastName];
    }
    
    if (App_Delegate.userObject.strEmail!=nil && App_Delegate.userObject.strEmail!=(id)[NSNull null] && App_Delegate.userObject.strEmail.length>0)
    {
        [txtFieldEmail setText:App_Delegate.userObject.strEmail];
    }
    
    if (App_Delegate.userObject.strAddress!=nil && App_Delegate.userObject.strAddress!=(id)[NSNull null] && App_Delegate.userObject.strAddress.length>0)
    {
        //strAddress=App_Delegate.userObject.strAddress;
        //[btnAddress setTitle:App_Delegate.userObject.strAddress forState:UIControlStateNormal];
    }
    
    if (App_Delegate.userObject.strCity!=nil && App_Delegate.userObject.strCity!=(id)[NSNull null] && App_Delegate.userObject.strCity.length>0)
    {
        [txtFieldCity setText:App_Delegate.userObject.strCity];
    }
    
    if (App_Delegate.userObject.strCountry!=nil && App_Delegate.userObject.strCountry!=(id)[NSNull null] && App_Delegate.userObject.strCountry.length>0)
    {
        strCountry=App_Delegate.userObject.strCountry;
        [btnCountry setTitle:App_Delegate.userObject.strCountry forState:UIControlStateNormal];
        [btnCountry setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    
    if (App_Delegate.userObject.strLat!=nil && App_Delegate.userObject.strLat!=(id)[NSNull null] && App_Delegate.userObject.strLat.length>0)
    {
        strLat=App_Delegate.userObject.strLat;
    }
    
    if (App_Delegate.userObject.strLong!=nil && App_Delegate.userObject.strLong!=(id)[NSNull null] && App_Delegate.userObject.strLong.length>0)
    {
        strLong=App_Delegate.userObject.strLong;
    }

    
    NSLog(@"Birth Date : %@",App_Delegate.userObject.strBirthDate);
    
    if (App_Delegate.userObject.strBirthDate!=nil && App_Delegate.userObject.strBirthDate!=(id)[NSNull null] && App_Delegate.userObject.strBirthDate.length>0 && ![App_Delegate.userObject.strBirthDate isEqualToString:@"0000-00-00"])
    {
        NSArray *array=[App_Delegate.userObject.strBirthDate componentsSeparatedByString:@"-"];
        
        if (array.count==3)
        {
            [txtFieldYear setText:[array objectAtIndex:0]];
            [txtFieldMonth setText:[array objectAtIndex:1]];
            [txtFieldDate setText:[array objectAtIndex:2]];
        }
    }
    
    if (App_Delegate.userObject.isMale)
    {
        [btnMale setSelected:YES];
        [btnFemale setSelected:NO];
    }
    else
    {
        [btnMale setSelected:NO];
        [btnFemale setSelected:YES];
    }
    
    LocalDetailObject *localObject=App_Delegate.userObject.locatDetailObject;
    
    if (localObject.strAbout!=nil && localObject.strAbout!=(id)[NSNull null] && localObject.strAbout.length>0)
    {
        [txtViewAboutMe setText:localObject.strAbout];
    }
    
    if (localObject.strOfferTourist!=nil && localObject.strOfferTourist!=(id)[NSNull null] && localObject.strOfferTourist.length>0)
    {
        [txtViewOfferTourist setText:localObject.strOfferTourist];
    }
    
    if (localObject.strTransport!=nil && localObject.strTransport!=(id)[NSNull null] && localObject.strTransport.length>0)
    {
        [txtViewTransportation setText:localObject.strTransport];
    }
    
    if ([App_Delegate.userObject.strSite isEqualToString:@"NATIVE"])
    {
        [btnChangePassword setEnabled:YES];
        [btnChangePassword setHidden:NO];
        [imgPasswordArrow setHidden:NO];
    }
    else
    {
        [btnChangePassword setEnabled:NO];
        [btnChangePassword setHidden:YES];
        [imgPasswordArrow setHidden:YES];
    }
    
}

-(BOOL)isValidationCorrect //Check if any field is empty and show user respective message
{
    
    if (txtFieldFirstName.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter First name" inViewController:self];
        return NO;
    }
    
    if (txtFieldLastName.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter Last name" inViewController:self];
        return NO;
    }
    
    if (txtFieldEmail.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter Email" inViewController:self];
        return NO;
    }
    
    if (![App_Delegate NSStringIsValidEmail:txtFieldEmail.text])
    {
        [App_Delegate showAlertWithMessage:@"Please enter a valid Email" inViewController:self];
        return NO;
    }
    
    
//    if (strAddress==nil || strAddress.length==0)
//    {
//        [App_Delegate showAlertWithMessage:@"Please select address" inViewController:self];
//        return NO;
//    }
    
    
    if (txtFieldCity.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter city" inViewController:self];
        return NO;
    }
    
    if (strCountry.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter country" inViewController:self];
        return NO;
    }
    
    if (txtFieldDate.text.length==0 || txtFieldMonth.text.length==0 || txtFieldYear.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter date of birth" inViewController:self];
        return NO;
    }
    else
    {
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        int age=[App_Delegate getAgeFromBirthdate:[formatter dateFromString:[NSString stringWithFormat:@"%@-%@-%@",txtFieldYear.text,txtFieldMonth.text,txtFieldDate.text]]];
        
        if (age<18)
        {
            [App_Delegate showAlertWithMessage:@"Min age 18 Years is required" inViewController:self];
            return NO;
        }
    }
    
    return YES;
}


-(void)uploadImageUnirest //This will upload image to the
{
    
    //Image path
    NSString *fileImagePath=[NSString stringWithFormat:@"%@/Documents/profilepic.jpeg",NSHomeDirectory()];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    //Url
    NSString *url=[NSString stringWithFormat:@"%@%@",KBaseURL,KUploadProfilePic];
    url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //Check network
    if (networkStatus==NotReachable)
    {
        [App_Delegate showAlertWithMessage:@"No internet connection." inViewController:self];
    }
    else
    {
        
        //Show loading
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        __block NSDictionary *dicResponse;
        
        //Content-Type: application/json
        NSMutableDictionary *headers =[[NSMutableDictionary alloc]init];
        [headers setObject:@"application/json" forKey:@"Content-Type"];
        [headers setObject:@"utf-8" forKey:@"Accept-Encoding"];
        
        NSDictionary* parameters = @{@"type" : @"Profile", @"file": [NSURL fileURLWithPath:fileImagePath]};
        
        //Initializing Async request
        [[UNIRest post:^(UNISimpleRequest* request)
          {
              [request setUrl:url];
              [request setParameters:parameters];
              
          }] asJsonAsync:^(UNIHTTPJsonResponse *response, NSError *error)
         {
             
             //Fetching resposne
             NSData *rawBody = response.rawBody;
             NSString *strResponse = [[NSString alloc] initWithData:rawBody encoding:NSUTF8StringEncoding];
             NSLog(@"Response %@",strResponse);
             
             dicResponse=response.body.object;
             
             if (dicResponse==nil)
             {
                 dicResponse=[response.body.array objectAtIndex:0];
             }
             
             [self uploadProfilePicAPIResponse:dicResponse];
             
         }];
    }
    
}


-(void)uploadProfilePicAPIResponse:(NSDictionary*)response
{
    //Calling main thread
    dispatch_async(dispatch_get_main_queue(), ^()
                   {
                       //All UI related change can be done here.
                       [MBProgressHUD hideHUDForView:self.view animated:YES];
                       
                       NSLog(@"Upload Profile pic : %@",response);
                       
                       if (response!=nil && [response objectForKey:@"image_id"])
                       {
                           strUserImageId=[response objectForKey:@"image_id"];
                           
                           [self callSaveLocalAPI];
                       }
                       else
                       {
                           [App_Delegate showAlertWithMessage:@"Profile pic cannot be uploaded at this time" inViewController:self];
                       }
                       
                   });
    
}

-(void)callSaveLocalAPI
{
    
    //Guide
    /*
     
     {"about":"about guide","role":"GUIDE","language":"[{\"Language\":\"Arabic\",\"type\":\"Basic\"},{\"Language\":\"French\",\"type\":\"flunt\"}]","transport":"car,van","user_id":"1","offer_tourist":"gfdgfd"}
     
     Common Parameter
     city
     location
     dob
     latitude
     longitude
     first_name
     last_name
     gender
     country
     
     Role:- TRAVELER/GUIDE
     
    */
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KUpdateProfile];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:txtViewAboutMe.text forKey:@"about"];
    [dicParameter setObject:App_Delegate.userObject.strOffset forKey:@"offset"];
    [dicParameter setObject:strUserType forKey:@"role"];
    [dicParameter setObject:txtViewTransportation.text forKey:@"transport"];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:txtViewOfferTourist.text forKey:@"offer_tourist"];
    [dicParameter setObject:txtFieldCity.text forKey:@"city"];
    [dicParameter setObject:txtFieldCity.text forKey:@"location"];
    [dicParameter setObject:[NSString stringWithFormat:@"%@:%@:%@",txtFieldYear.text,txtFieldMonth.text,txtFieldDate.text] forKey:@"dob"];
    [dicParameter setObject:strLat forKey:@"latitude"];
    [dicParameter setObject:strLong forKey:@"longitude"];
    [dicParameter setObject:txtFieldFirstName.text forKey:@"first_name"];
    [dicParameter setObject:txtFieldLastName.text forKey:@"last_name"];
    [dicParameter setObject:strCountry forKey:@"country"];
    
    //[{\"Language\":\"Arabic\",\"type\":\"Basic\"},{\"Language\":\"French\",\"type\":\"flunt\"}]
//    NSString *strLanguage=[NSString stringWithFormat:@"["];
//    
//    for (LanguageObject *object in App_Delegate.userObject.locatDetailObject.aryLanguage)
//    {
//        NSDictionary *dictionary=[[NSDictionary alloc]init];
//        [dictionary setValue:object.strLanguageName forKey:@"Language"];
//        [dictionary setValue:object.strLanguageLevel forKey:@"type"];
//        
//        strLanguage=[strLanguage stringByAppendingString:dictionary];
//        
//    }
    
    
    NSMutableArray *arrayLanguage=[[NSMutableArray alloc]init];
    
    for (LanguageObject *object in App_Delegate.userObject.locatDetailObject.aryLanguage)
    {
        NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
        [dictionary setValue:[NSString stringWithFormat:@"%@",object.strLanguageName] forKey:@"Language"];
        [dictionary setValue:[NSString stringWithFormat:@"%@",object.strLanguageLevel] forKey:@"type"];
        [arrayLanguage addObject:dictionary];
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrayLanguage options:NSJSONWritingPrettyPrinted error:nil];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    [dicParameter setObject:jsonString forKey:@"language"];
    
    if (strUserImageId!=nil && chosenImage!=nil)
    {
        [dicParameter setObject:strUserImageId forKey:@"profile_image"];
    }
    
    if (btnMale.selected)
    {
        [dicParameter setObject:@"M" forKey:@"gender"];
    }
    else
    {
        [dicParameter setObject:@"F" forKey:@"gender"];
    }
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc]init];
    [header setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    NSLog(@"Parmaeters : %@",dicParameter);
    NSLog(@"Headers : %@",header);
    NSLog(@"URL : %@",strUrl);
    
    
    [APIsList putAPIWithURL:strUrl andHeader:header andParameters:dicParameter Selector:@selector(saveLocalAPIResponse:) WithCallBackObject:self];
    
}

-(void)saveLocalAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        Parser *parser=[[Parser alloc]init];
        App_Delegate.userObject=[parser getUserDetail:dictionary];
        
        if ([App_Delegate.userObject.strUserType isEqualToString:@"TRAVELER"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserTourist forKey:Default_UserType];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserGuide forKey:Default_UserType];
        }
        
        [[NSUserDefaults standardUserDefaults]setObject:dictionary forKey:Default_SaveUserObject];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [App_Delegate internalLogin];
        
        [App_Delegate showAlertWithMessage:[NSString stringWithFormat:@"Thank you %@ for updating your profile.",App_Delegate.userObject.strFirstName] inViewController:self];
        
        //[App_Delegate application:[UIApplication sharedApplication] didFinishLaunchingWithOptions:nil];
        
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Profile cannot be update now, Please try again later" inViewController:self];
        }
    }
    
}

-(void)switchRole
{
    
    NSString *strMessage=@"Are you sure you want to become a tourist?";
    
    SCLAlertView *alert =[App_Delegate getAlertInstance];
    
    [alert addButton:@"No" actionBlock:^(void) {
        
    }];
    
    [alert addButton:@"Yes" actionBlock:^(void) {
        
         [self callSwitchRoleApi];
        
    }];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:strMessage closeButtonTitle:nil duration:0.0];
    
    
}

-(void)callSwitchRoleApi
{
    
    /*
     {"user_id":"17","role":"GUIDE"}
     
     */
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KSwitchRole];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:@"TRAVELER" forKey:@"role"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(switchRoleAPIResponse:) WithCallBackObject:self];
    
    
}

-(void)switchRoleAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        [App_Delegate application:[UIApplication sharedApplication] didFinishLaunchingWithOptions:nil];
        
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Profile role cannot be update now, Please try again later" inViewController:self];
        }
    }
    
}



#pragma mark- Button Action

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnSavePressed:(id)sender
{
    
    if ([self isValidationCorrect])
    {
        
        if (btnTourist.selected)
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserTourist forKey:Default_UserType];
            strUserType=@"TRAVELER";
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:KUserGuide forKey:Default_UserType];
            strUserType=@"GUIDE";
        }
        
        
        if (chosenImage!=nil)
        {
            [self uploadImageUnirest];
        }
        else
        {
            [self callSaveLocalAPI];
        }
    }
}

-(IBAction)btnAdressPressed:(id)sender
{
    
    //[btnAddress setTitle:@"Address" forState:UIControlStateNormal];
    //[btnAddress setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btnCountry setTitle:@"Country" forState:UIControlStateNormal];
    [btnCountry setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [txtFieldCity setText:@""];
    strLat=nil;
    strLong=nil;
    strAddress=@"";
    strCountry=@"";
    
    // Present the Autocomplete view controller when the button is pressed.
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    [acController setTintColor:KAppColor];
    [acController setPrimaryTextColor:KAppColor_GrayBlue];
    [acController setPrimaryTextHighlightColor:KAppColor];
    GMSAutocompleteFilter *filter=[GMSAutocompleteFilter new];
    filter.type=kGMSPlacesAutocompleteTypeFilterCity;
    
    acController.autocompleteFilter=filter;
    acController.delegate = (id)self;
    [self presentViewController:acController animated:YES completion:nil];
    [App_Delegate setStatusBarBackgroundColor:KAppColor];
}

- (IBAction)btnBackPressed:(UIButton *)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
    //[App_Delegate showAlertWithMessage:@"Under Construction" inViewController:self];
}


- (IBAction)btnTouristPressed:(UIButton *)sender
{
    [self switchRole];
}

- (IBAction)btnGuidePressed:(UIButton *)sender
{
    
}

- (IBAction)btnMalePressed:(UIButton *)sender
{
    [btnMale setSelected:YES];
    [btnFemale setSelected:NO];
}

- (IBAction)btnFemalePressed:(UIButton *)sender
{
    [btnMale setSelected:NO];
    [btnFemale setSelected:YES];
}

- (IBAction)btnChangePasswordPressed:(UIButton *)sender
{
    ChangePasswordVC *changePasswordVC=[[ChangePasswordVC alloc]init];
    [self.navigationController pushViewController:changePasswordVC animated:YES];
}

- (IBAction)btnAddPhotosPressed:(UIButton *)sender
{
    AddPhotosVC *changePasswordVC=[[AddPhotosVC alloc]init];
    [self.navigationController pushViewController:changePasswordVC animated:YES];
}

- (IBAction)btnAddLanguagePressed:(UIButton *)sender
{
    isForChangeSelection=NO;
    
    PSelectionView *selectionView=[[PSelectionView alloc]init];
    selectionView.pageTitle=@"Select Language";
    selectionView.arrayData=aryLanguage;
    
    NSMutableArray *aryFilteredLanguage=[[NSMutableArray alloc]init];
    
    for (LanguageObject *object in App_Delegate.userObject.locatDetailObject.aryLanguage)
    {
        if ([object.strLanguageLevel isEqualToString:@"Fluent"])
        {
            [aryFilteredLanguage addObject:object.strLanguageName];
        }
    }
    
    selectionView.arraySelectedData=aryFilteredLanguage;
    selectionView.viewColor=KAppColor;
    selectionView.delegate=(id)self;
    [self presentViewController:selectionView animated:YES completion:nil];
    
    //LanguageLevelVC *languageLevelVC=[[LanguageLevelVC alloc]init];
    //languageLevelVC.aryLanguageData=aryLanguage;
    //[self.navigationController pushViewController:languageLevelVC animated:YES];
}

-(void)btnChangeLanguagePressed:(UIButton*)button
{
    isForChangeSelection=YES;
    isChangeLanguage=YES;
    currentIndex=(int)button.tag;
    
    PSelectionView *selectionView=[[PSelectionView alloc]init];
    selectionView.pageTitle=@"Select Language";
    selectionView.arrayData=aryLanguage;
    selectionView.viewColor=KAppColor;
    selectionView.delegate=(id)self;
    [self presentViewController:selectionView animated:YES completion:nil];
}

-(void)btnChangeLanguageLevelPressed:(UIButton*)button
{
    isForChangeSelection=YES;
    isChangeLanguage=NO;
    currentIndex=(int)button.tag;
    
    PSelectionView *selectionView=[[PSelectionView alloc]init];
    selectionView.pageTitle=@"Select Level";
    selectionView.arrayData=aryLanguageLevel;
    selectionView.viewColor=KAppColor;
    selectionView.delegate=(id)self;
    [self presentViewController:selectionView animated:YES completion:nil];
}

#pragma mark- AlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1012)
    {
        NSLog(@"Logout Alert");
        NSLog(@"Button index : %d",(int)buttonIndex);
        
        if (buttonIndex==1)
        {
            [self callSwitchRoleApi];
        }
    }
}

#pragma mark- TextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    int length =(int)[currentString length];
    
    if (textField.tag==50 && length > 2) //Month
    {
        return NO;
    }
    
    if (textField.tag==51 && length > 2) //Date
    {
        return NO;
    }
    
    if (textField.tag==55 && length > 4) //Year
    {
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.tag==50 && [textField.text intValue]>12) //Month
    {
        [App_Delegate showAlertWithMessage:@"Birth month must be between 1-12" inViewController:self];
        return NO;
    }
    
    if (textField.tag==51 && [textField.text intValue]>31) //Date
    {
        [App_Delegate showAlertWithMessage:@"Birth date must be between 1-31" inViewController:self];
        return NO;
    }
    
    return YES;
}

#pragma mark- GMSAutoComplete Delegate

- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place
{
    
    [App_Delegate setStatusBarBackgroundColor:[UIColor clearColor]];
    
    // The user has selected a place.
    [self dismissViewControllerAnimated:YES completion:^{
        
        //http://stackoverflow.com/questions/15036007/how-to-obtain-country-state-city-from-reversegeocodecoordinate
        
        strAddress=place.name;
        //[btnAddress setTitle:place.name forState:UIControlStateNormal];
        //[btnAddress setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [txtFieldCity setText:place.name];
        strLat=[NSString stringWithFormat:@"%f",place.coordinate.latitude];
        strLong=[NSString stringWithFormat:@"%f",place.coordinate.longitude];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        CLLocation *location=[[CLLocation alloc]initWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude];
        
        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            NSLog(@"Finding address");
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if (error)
            {
                NSLog(@"Error %@", error.description);
            }
            else
            {
                CLPlacemark *placemark = [placemarks lastObject];
                App_Delegate.userObject.strOffset=[NSString stringWithFormat:@"%ld",placemark.timeZone.secondsFromGMT];
                
                NSString *city = [[placemark addressDictionary] objectForKey:(NSString *)kABPersonAddressCityKey];
                NSString *countryLocal = [[placemark addressDictionary] objectForKey:(NSString *)kABPersonAddressCountryKey];
                NSString *countryISO = [[placemark addressDictionary] objectForKey:(NSString *)kABPersonAddressCountryCodeKey];
                
                if (city!=nil && city!=(id)[NSNull null])
                {
                    txtFieldCity.text=city;
                }
                
                if (countryLocal!=nil && countryLocal!=(id)[NSNull null])
                {
                    strCountry=countryLocal;
                    [btnCountry setTitle:countryLocal forState:UIControlStateNormal];
                    [btnCountry setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                }
                
                if (countryISO!=nil && countryISO!=(id)[NSNull null])
                {
                    strCountryISO=countryISO;
                }
                
            }
        }];
        
        
//        [[GMSGeocoder geocoder] reverseGeocodeCoordinate:place.coordinate completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
//            NSLog(@"reverse geocoding results:");
//            
//            if (response.results!=nil && response.results!=(id)[NSNull null] && response.results.count>0)
//            {
//                GMSAddress *address=[[response results] objectAtIndex:0];
//                
//                if (address.locality!=nil && address.locality!=(id)[NSNull null])
//                {
//                    txtFieldCity.text=address.locality;
//                }
//                
//                if (address.country!=nil && address.country!=(id)[NSNull null])
//                {
//                    txtFieldCity.text=address.country;
//                }
//            }
//        }];
        
    }];
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithError:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [App_Delegate setStatusBarBackgroundColor:[UIColor clearColor]];
}

// User pressed cancel button.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [App_Delegate setStatusBarBackgroundColor:[UIColor clearColor]];
}


#pragma mark- PSelectionDelegate

-(void)userSelectedArray:(NSMutableArray*)arySelectedData
{
    
    NSMutableArray *aryTemp=[[NSMutableArray alloc]init];
    
    NSString *strCurrentLevel=@"Fluent";
    
    //Remove the already added language in the current categaory
    for (LanguageObject *object in App_Delegate.userObject.locatDetailObject.aryLanguage)
    {
        if ([object.strLanguageLevel isEqualToString:strCurrentLevel])
        {
            //Leave them
            NSLog(@"Selected Language %@ and level %@",object.strLanguageName,object.strLanguageLevel);
        }
        else
        {
            [aryTemp addObject:object];
        }
        
    }
    
    App_Delegate.userObject.locatDetailObject.aryLanguage=aryTemp;
    
    
    for (NSString *strLanguage in arySelectedData)
    {
        LanguageObject *langObject=[[LanguageObject alloc]init];
        langObject.strLanguageName=[NSString stringWithString:strLanguage];
        langObject.strLanguageLevel=strCurrentLevel;
        [App_Delegate.userObject.locatDetailObject.aryLanguage addObject:langObject];
    }
    
    for (LanguageObject *object in App_Delegate.userObject.locatDetailObject.aryLanguage)
    {
        NSLog(@"Selected Language %@ and level %@",object.strLanguageName,object.strLanguageLevel);
    }
}

-(void)userSelected:(NSString*)string
{
    
    if (isForChangeSelection)
    {
        LanguageObject *object=[App_Delegate.userObject.locatDetailObject.aryLanguage objectAtIndex:currentIndex];
        
        if (isChangeLanguage)
        {
            object.strLanguageName=string;
        }
        else
        {
            object.strLanguageLevel=string;
        }
        
    }
    else
    {
        if (!App_Delegate.userObject.locatDetailObject.aryLanguage)
        {
            App_Delegate.userObject.locatDetailObject.aryLanguage=[[NSMutableArray alloc]init];
        }
        
        LanguageObject *langObject=[[LanguageObject alloc]init];
        langObject.strLanguageName=[NSString stringWithString:string];
        langObject.strLanguageLevel=@"Fluent";
        [App_Delegate.userObject.locatDetailObject.aryLanguage addObject:langObject];
        
    }
    
    [tblLanguage reloadData];
    [self.view setNeedsLayout];
    
}

#pragma mark- Actionsheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        //Gallery
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = (id)self;
        picker.view.tag=101;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    else if (buttonIndex==1)
    {
        //Camera
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate =(id)self;
        picker.view.tag=101;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
}

#pragma mark- UIImagePicker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    if (picker.view.tag==101)
    {
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        //Select image from gallery
        chosenImage = info[UIImagePickerControllerEditedImage];
        
        NSString *fileImagePath=[NSString stringWithFormat:@"%@/Documents/profilepic.jpeg",NSHomeDirectory()];
        
        NSData *data = UIImageJPEGRepresentation(chosenImage, 0.8);
        
        NSError *error;
        
        [data writeToFile:fileImagePath options:NSDataWritingAtomic error:&error];
        
        if(error != nil)
            NSLog(@"write error %@", error);
        
        
        [imgUserProfile setImage:chosenImage];
        
    }
}

#pragma mark- Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return App_Delegate.userObject.locatDetailObject.aryLanguage.count+1;
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==App_Delegate.userObject.locatDetailObject.aryLanguage.count)
    {
        return 70;
    }
    else
    {
        return 56;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if (indexPath.row==App_Delegate.userObject.locatDetailObject.aryLanguage.count)
    if (indexPath.row==0)
    {
        NSString *strCellIdentifier=@"AddLanguageCell";
        
        AddLanguageCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        [cell.lblLabel setTitleColor:KAppColor_Blue forState:UIControlStateNormal];
        
        return cell;
    }
    else
    {
        NSString *strCellIdentifier=@"LanguageCell";
        
        LanguageCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        LanguageObject *object=[App_Delegate.userObject.locatDetailObject.aryLanguage objectAtIndex:indexPath.row];
        
        
        [cell.btnLanguage setTitle:object.strLanguageName forState:UIControlStateNormal];
        [cell.btnLevel setTitle:object.strLanguageLevel   forState:UIControlStateNormal];
        [cell.btnLevel setTag:indexPath.row];
        [cell.btnLanguage setTag:indexPath.row];
        
        [cell.btnLanguage addTarget:self action:@selector(btnChangeLanguagePressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnLevel addTarget:self action:@selector(btnChangeLanguageLevelPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if (indexPath.row==App_Delegate.userObject.locatDetailObject.aryLanguage.count)
    if (indexPath.row==0)
    {
        [self btnAddLanguagePressed:nil];
    }
}


@end
