//
//  PageView_AboutVCViewController.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/8/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "PageView_AboutVCViewController.h"
#import "AboutPageCell.h"
#import "LanguageObject.h"
#import "BookMeCell.h"
#import "LoginVC.h"



#define Ktitle @"title"
#define KDescription @"description"



@interface PageView_AboutVCViewController ()

@end

@implementation PageView_AboutVCViewController
@synthesize guideObject;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    [tblView registerNib:[UINib nibWithNibName:@"AboutPageCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"AboutPageCell"];
    
    aryInformation=[[NSMutableArray alloc]init];
    
    if (guideObject)
    {
        [self setupViewForOtherUser];
    }
    else
    {
        [self setupViewForUserHimself];
    }
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(enableScrollview) name:@"enableTableScrolling" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(disableScrolling) name:@"disableScrolling" object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [tblView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Methods

-(void)setupViewForOtherUser
{
    
    if ([guideObject.strUserType isEqualToString:@"TRAVELER"])
    {
        
        //Other //TRAVELER
        if (guideObject.travellerDetailObject.strAbout!=nil && guideObject.travellerDetailObject.strAbout.length>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setObject:@"About Me" forKey:Ktitle];
            [dic setObject:guideObject.travellerDetailObject.strAbout forKey:KDescription];
            [aryInformation addObject:dic];
        }
        
        //Tourist Travel Interest
        if (guideObject.travellerDetailObject.strTravelIntrested!=nil && guideObject.travellerDetailObject.strTravelIntrested.length>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setObject:@"Travel Interests" forKey:Ktitle];
            [dic setObject:guideObject.travellerDetailObject.strTravelIntrested forKey:KDescription];
            [aryInformation addObject:dic];
        }
        
        //Tourist Visited Countries
        if (guideObject.travellerDetailObject.strVisitedPlaces!=nil && guideObject.travellerDetailObject.strVisitedPlaces.length>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setObject:@"Countries I've Visited" forKey:Ktitle];
            NSString *strCountries=guideObject.travellerDetailObject.strVisitedPlaces;
            NSLog(@"Countries : %@",strCountries);
            strCountries=[strCountries stringByReplacingOccurrencesOfString:@"," withString:@", "];
            NSLog(@"Countries AFter : %@",strCountries);
            [dic setObject:strCountries forKey:KDescription];
            [aryInformation addObject:dic];
        }
        
    }
    else
    {
        if (guideObject.locatDetailObject.strAbout!=nil && guideObject.locatDetailObject.strAbout.length>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setObject:@"About Me" forKey:Ktitle];
            [dic setObject:guideObject.locatDetailObject.strAbout forKey:KDescription];
            [aryInformation addObject:dic];
        }
        
        if (guideObject.locatDetailObject.strOfferTourist!=nil && guideObject.locatDetailObject.strOfferTourist.length>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setObject:@"What I Can Offer Tourists" forKey:Ktitle];
            [dic setObject:guideObject.locatDetailObject.strOfferTourist forKey:KDescription];
            [aryInformation addObject:dic];
        }
        
        if (guideObject.locatDetailObject.aryLanguage!=nil && guideObject.locatDetailObject.aryLanguage.count>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            
            
            NSString *str=@"\n";
            
            for (LanguageObject *lang in guideObject.locatDetailObject.aryLanguage)
            {
                str=[str stringByAppendingString:[NSString stringWithFormat:@"%@\n",lang.strLanguageName,lang.strLanguageLevel]];
            }
            
            //NSString *newString = [str substringToIndex:[str length]-1];
            
            [dic setObject:@"Languages Spoken Fluently" forKey:Ktitle];
            [dic setObject:str forKey:KDescription];
            [aryInformation addObject:dic];
        }
        
        if (guideObject.locatDetailObject.strTransport!=nil && guideObject.locatDetailObject.strTransport.length>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setObject:@"Transportation" forKey:Ktitle];
            [dic setObject:guideObject.locatDetailObject.strTransport forKey:KDescription];
            [aryInformation addObject:dic];
        }
    }
    
    
    
    [tblView reloadData];
}

-(void)setupViewForUserHimself
{
    
    if ([App_Delegate getCurrentUser]==Tourist)
    {
        //Tourist
        
        //About tourist
        if (App_Delegate.userObject.travellerDetailObject.strAbout!=nil && App_Delegate.userObject.travellerDetailObject.strAbout.length>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setObject:@"About Me" forKey:Ktitle];
            [dic setObject:App_Delegate.userObject.travellerDetailObject.strAbout forKey:KDescription];
            [aryInformation addObject:dic];
        }
        
        //Tourist Travel Interest
        if (App_Delegate.userObject.travellerDetailObject.strTravelIntrested!=nil && App_Delegate.userObject.travellerDetailObject.strTravelIntrested.length>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setObject:@"Travel Interests" forKey:Ktitle];
            [dic setObject:App_Delegate.userObject.travellerDetailObject.strTravelIntrested forKey:KDescription];
            [aryInformation addObject:dic];
        }
        
        //Tourist Visited Countries
        if (App_Delegate.userObject.travellerDetailObject.strVisitedPlaces!=nil && App_Delegate.userObject.travellerDetailObject.strVisitedPlaces.length>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setObject:@"Countries I've Visited" forKey:Ktitle];
            NSString *strCountries=App_Delegate.userObject.travellerDetailObject.strVisitedPlaces;
            NSLog(@"Countries : %@",strCountries);
            strCountries=[strCountries stringByReplacingOccurrencesOfString:@"," withString:@", "];
            NSLog(@"Countries AFter : %@",strCountries);
            [dic setObject:strCountries forKey:KDescription];
            [aryInformation addObject:dic];
        }
    }
    else if ([App_Delegate getCurrentUser]==Guide)
    {
        if (App_Delegate.userObject.locatDetailObject.strAbout!=nil && App_Delegate.userObject.locatDetailObject.strAbout.length>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setObject:@"About Me" forKey:Ktitle];
            [dic setObject:App_Delegate.userObject.locatDetailObject.strAbout forKey:KDescription];
            [aryInformation addObject:dic];
        }
        
        if (App_Delegate.userObject.locatDetailObject.strOfferTourist!=nil && App_Delegate.userObject.locatDetailObject.strOfferTourist.length>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setObject:@"What I Can Offer Tourists" forKey:Ktitle];
            [dic setObject:App_Delegate.userObject.locatDetailObject.strOfferTourist forKey:KDescription];
            [aryInformation addObject:dic];
        }
        
        if (App_Delegate.userObject.locatDetailObject.aryLanguage!=nil && App_Delegate.userObject.locatDetailObject.aryLanguage.count>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            
            
            NSString *str=@"\n";
            
            for (LanguageObject *lang in App_Delegate.userObject.locatDetailObject.aryLanguage)
            {
                str=[str stringByAppendingString:[NSString stringWithFormat:@"%@\n",lang.strLanguageName,lang.strLanguageLevel]];
            }
            
            //NSString *newString = [str substringToIndex:[str length]-1];
            [dic setObject:@"Languages Spoken Fluently" forKey:Ktitle];
            [dic setObject:str forKey:KDescription];
            [aryInformation addObject:dic];
        }
        
        if (App_Delegate.userObject.locatDetailObject.strTransport!=nil && App_Delegate.userObject.locatDetailObject.strTransport.length>0)
        {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setObject:@"Transportation" forKey:Ktitle];
            [dic setObject:App_Delegate.userObject.locatDetailObject.strTransport forKey:KDescription];
            [aryInformation addObject:dic];
        }
    }
    
    [tblView reloadData];
}

-(void)enableScrollview
{
    [tblView setScrollEnabled:YES];
}

-(void)disableScrolling
{
    [tblView setScrollEnabled:NO];
}

-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    
    float scrollOffset = scrollView.contentOffset.y;
    
    if (scrollOffset <=-10)
    {
        //[scrollView setContentOffset:CGPointMake(0, 0)];
        [tblView setScrollEnabled:NO];
    }
}

-(void)showGuestAlert
{
    
    NSString *strMessage=[NSString stringWithFormat:@"Please sign up or login to book local guides."];
    
    SCLAlertView *alert =[App_Delegate getAlertInstance];
    
    [alert addButton:@"No" actionBlock:^(void) {
        
    }];
    
    [alert addButton:@"Yes" actionBlock:^(void) {
        
        [App_Delegate.xmppManager disconnect];
        
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        
        LoginVC *viewController=[[LoginVC alloc]init];
        App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
        [App_Delegate.navigationController setNavigationBarHidden:YES];
        [App_Delegate.window setRootViewController:App_Delegate.navigationController];
        
    }];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:strMessage closeButtonTitle:nil duration:0.0];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==7800)
    {
        //Guest Alert
        if (buttonIndex==1)
        {
            [App_Delegate.xmppManager disconnect];
            
            NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
            [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
            
            LoginVC *viewController=[[LoginVC alloc]init];
            App_Delegate.navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
            [App_Delegate.navigationController setNavigationBarHidden:YES];
            [App_Delegate.window setRootViewController:App_Delegate.navigationController];
        }
    }
}

#pragma mark- Button Action

- (IBAction)btnAddPhotosPressed:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"openEditProfile" object:nil];
}

-(IBAction)btnBookMePressed:(id)sender
{
    if ([App_Delegate getCurrentUser]==Guest)
    {
        [self showGuestAlert];
        return;
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"goToBookGuidePage" object:nil];
}

#pragma mark- Tableview Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (guideObject && self.isForViewProfile==NO)
    {
        return 55;
    }
    else
    {
        return 0;
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIButton *btnBook=[[UIButton alloc]init];
    [btnBook addTarget:self action:@selector(btnBookMePressed:) forControlEvents:UIControlEventTouchUpInside];
    [btnBook setFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,55)];
    [btnBook setBackgroundColor:[UIColor clearColor]];
    [viewFooter addSubview:btnBook];
    return viewFooter;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (aryInformation.count==0)
    {
        return 100;
    }
    
    return 0;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (guideObject && self.isForViewProfile==NO)
    {
        //Hide button
        lblHeaderTitle.text=@"About info coming soon.";
        [btnHeaderAdd setHidden:YES];
    }
    else
    {
        //Show button
        lblHeaderTitle.text=@"You do not have any information added yet";
        [btnHeaderAdd setHidden:NO];
    }
    
    [btnHeaderAdd addTarget:self action:@selector(btnAddPhotosPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return viewHeader;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (guideObject && self.isForViewProfile==NO)
    {
        //return aryInformation.count+1;
        return aryInformation.count;
    }
    else
    {
        return aryInformation.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==aryInformation.count)
    {
        NSString *strCellIdentifier=@"BookMeCell";
        
        BookMeCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        return cell;

    }
    else
    {
        NSString *strCellIdentifier=@"AboutPageCell";
        
        AboutPageCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        return cell;

    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==aryInformation.count)
    {
        return 50.0;
    }
    else
    {
        NSString *strCellIdentifier=@"AboutPageCell";
        
        static AboutPageCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            cell = [tblView dequeueReusableCellWithIdentifier:strCellIdentifier];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        return [self calculateHeightForConfiguredSizingCell:cell];
    }
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell
{
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

- (void)setUpCell:(AboutPageCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dictionary=[aryInformation objectAtIndex:indexPath.row];
    
    [cell.lblTitle setPreferredMaxLayoutWidth:cell.lblTitle.bounds.size.width];
    [cell.lblTitle setText:[dictionary objectForKey:Ktitle]];
    [cell.lblDescription setText:[dictionary objectForKey:KDescription]];
    
    //[cell.lblDescription setText:@"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then"];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==aryInformation.count)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"goToBookGuidePage" object:nil];
    }
}



@end
