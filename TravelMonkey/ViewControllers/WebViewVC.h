//
//  WebViewVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 1/19/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewVC : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *webView;
    IBOutlet UILabel *lblTitle;
    IBOutlet UIButton *btnToggle;
    
}

@property BOOL shouldHideToggle;
@property (nonatomic,strong) NSString *strUrl;
@property (nonatomic,strong) NSString *strTitle;

@end
