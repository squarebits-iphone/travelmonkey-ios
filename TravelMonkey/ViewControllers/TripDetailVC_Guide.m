//
//  TripDetailVC_Guide.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/20/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "TripDetailVC_Guide.h"
#import "Parser.h"
#import "UIImageView+WebCache.h"


@interface TripDetailVC_Guide ()

@end

@implementation TripDetailVC_Guide
@synthesize strTripID;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    [lblPlace setTextColor:KAppColor_GrayBlue];
    [lblPlace setTextColor:KAppColor_GrayBlue];
    [btnNegative setSelected:NO];
    [btnPositive setSelected:NO];
    [scrlView setHidden:YES];
    [self callTripDetailByGuide];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

-(void)viewDidLayoutSubviews
{
    if (shouldHideReviewArea)
    {
        [scrlView setFrame:CGRectMake(0,140,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-140-btnSubmitReview.frame.size.height)];
        
        [constraintHeight setConstant:lblTotalCost.frame.origin.y+lblTotalCost.frame.size.height+50];
        [scrlView setContentSize:CGSizeMake(0,lblTotalCost.frame.origin.y+lblTotalCost.frame.size.height+50)];
    }
    else
    {
        [scrlView setFrame:CGRectMake(0,140,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-140-btnSubmitReview.frame.size.height)];
        
        [constraintHeight setConstant:imgViewCommentBackground.frame.origin.y+imgViewCommentBackground.frame.size.height+20];
        [scrlView setContentSize:CGSizeMake(0,imgViewCommentBackground.frame.origin.y+imgViewCommentBackground.frame.size.height+20)];
    }
}

#pragma mark- Methods

-(void)callTripDetailByGuide
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KGetTripDetail];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strTripID forKey:@"booking_id"];
    [dicParameter setObject:@"GUIDE" forKey:@"role"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(tripDetailResponse:) WithCallBackObject:self];
}

-(void)tripDetailResponse:(NSDictionary*)dictionary
{
    [scrlView setHidden:NO];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        if ([dictionary objectForKey:@"booking"])
        {
            isSuccess=YES;
            Parser *parser=[[Parser alloc]init];
            objectTripDetail=[parser getTripDetails:[dictionary objectForKey:@"booking"]];
            [self setupView];
        }
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"No Trip found" inViewController:self];
        }
    }
}

-(void)hideReviewOption
{
    [lblReviewPlaceHolder setText:@"Thank you for the review."];
    [lblNegativePlaceHolder setHidden:YES];
    [lblPositivePlaceHolder setHidden:YES];
    [lblOptionalCommentPlaceHolder setHidden:YES];
    [imgViewCommentBackground setHidden:YES];
    [btnPositive setHidden:YES];
    [btnNegative setHidden:YES];
    [btnSubmitReview setHidden:YES];
    [txtViewComment setHidden:YES];
}

-(void)setupView
{
    
    [self.view addSubview:scrlView];
    [imgViewProfile setClipsToBounds:YES];
    [imgViewProfile.layer setCornerRadius:125/2];
    
    [imgViewProfile sd_setImageWithURL:[NSURL URLWithString:objectTripDetail.strTravellerImageURL]];
    
    
    NSString *strName=objectTripDetail.strTravellerName;
    NSArray *aryName=[objectTripDetail.strTravellerName componentsSeparatedByString:@" "];
    if (aryName && aryName.count>0)
    {
        strName=[aryName firstObject];
    }
    
    [lblName setText:strName];
    [lblPlace setText:[NSString stringWithFormat:@"%@, %@",objectTripDetail.strTravellerCity,objectTripDetail.strTravellerCountry]];
    
    //Setting dates
    NSDateFormatter *formatter=[NSDateFormatter new];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    
    NSDate *startTime=[formatter dateFromString:objectTripDetail.strStartTime];
    NSDate *endTime=[formatter dateFromString:objectTripDetail.strEndTime];
    
    startTime=[startTime dateByAddingTimeInterval:timeZoneSeconds];
    endTime=[endTime dateByAddingTimeInterval:timeZoneSeconds];
    
    [formatter setDateFormat:@"MMM dd, yyyy"];
    [lblTripDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
    
    [formatter setDateFormat:@"hh:mm a"];
    [lblTripStartTime setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
    [lblTripEndTime setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:endTime]]];
    
    [lblTotalCost setText:[NSString stringWithFormat:@"$%@",objectTripDetail.strPrice]];
    [lblTripDeduction setText:[NSString stringWithFormat:@"$%0.2f",[objectTripDetail.strPrice intValue]*0.2]];
    [lblTripEarning setText:[NSString stringWithFormat:@"$%0.2f",[objectTripDetail.strPrice intValue]*0.8]];
    
    shouldHideReviewArea=objectTripDetail.isReviewedByGuide;

    if (shouldHideReviewArea)
    {
        [self hideReviewOption];
    }
    
}

-(void)callReviewGuideAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KPostReview];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strTripID forKey:@"booking_id"];
    [dicParameter setObject:@"GUIDE" forKey:@"role"];
    
    if (btnPositive.selected)
    {
        [dicParameter setObject:@"LIKE" forKey:@"review"];
    }
    else
    {
        [dicParameter setObject:@"UNLIKE" forKey:@"review"];
    }
    
    if (txtViewComment.text.length>0)
    {
        [dicParameter setObject:txtViewComment.text forKey:@"comment"];
    }
    else
    {
        [dicParameter setObject:@"" forKey:@"comment"];
    }
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(reviewResponse:) WithCallBackObject:self];
    
}

-(void)reviewResponse:(NSMutableDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        [App_Delegate showAlertWithMessage:@"Thank you for your feedback" inViewController:self];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Please try again later" inViewController:self];
        }
    }
    
    [self callTripDetailByGuide];
}


#pragma mark- Button Action

-(IBAction)btnSubmitReviewForGuide:(id)sender
{
    if (btnPositive.selected || btnNegative.selected)
    {
        [self callReviewGuideAPI];
    }
    else
    {
        [App_Delegate showAlertWithMessage:@"Please select Like or Dislike button above with some feedback text to review. Thank you" inViewController:self];
    }
}

-(IBAction)btnPositiveNegativePressed:(UIButton *)senderButton
{
    [btnNegative setSelected:NO];
    [btnPositive setSelected:NO];
    [senderButton setSelected:YES];
}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end