//
//  ThankYouVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 1/27/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BEMCheckBox.h"



@interface ThankYouVC : UIViewController
{
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblConfirmationTitle;
    IBOutlet UITextView *txtViewMessage;
    IBOutlet BEMCheckBox *checkBox;
    
}

@property (nonatomic,strong) NSString *strTitle;
@property (nonatomic,strong) NSString *strConfirmationTitle;
@property (nonatomic,strong) NSString *strMessage;
@property BOOL isForBooking;


@end
