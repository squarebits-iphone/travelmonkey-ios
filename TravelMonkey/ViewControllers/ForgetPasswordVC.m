//
//  ForgetPasswordVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 1/30/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "ForgetPasswordVC.h"
#import "ThankYouVC.h"


@interface ForgetPasswordVC ()

@end

@implementation ForgetPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Methods

-(void)callForgotAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",KBaseURL,KForgetPassword];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:txtFieldEmail.text forKey:@"email"];
    
    [APIsList postAPIWithURL:strUrl andParameters:dicParameter Selector:@selector(forgotAPIResponse:) WithCallBackObject:self];
}


-(void)forgotAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        ThankYouVC *view=[[ThankYouVC alloc]init];
        view.strTitle=@"Email Sent";
        view.strConfirmationTitle=@"Done!";
        view.strMessage=@"Please check your email for a link to reset your password. The link is valid for 24 hours.";
        
        [self.navigationController pushViewController:view animated:NO];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Login failed" inViewController:self];
        }
    }
}

#pragma mark- Button Action

-(IBAction)btnSubmitPressed:(id)sender
{
    if (txtFieldEmail.text.length==0)
    {
        [App_Delegate showAlertWithMessage:@"Please enter email address" inViewController:self];
        return;
    }
    
    [self callForgotAPI];
}

-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
