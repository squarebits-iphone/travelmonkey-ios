//
//  ReviewView_AboutVCViewController.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/9/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "ReviewView_AboutVCViewController.h"
#import "ReviewCell.h"
#import "TotalReviewsCell.h"
#import "BookMeCell.h"
#import "ReviewObject.h"
#import "UIImageView+WebCache.h"


#define KNotAvailable @"Not Available";


@interface ReviewView_AboutVCViewController ()

@end

@implementation ReviewView_AboutVCViewController
@synthesize guideObject,aryInformation;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    tblView.rowHeight = UITableViewAutomaticDimension;
    tblView.estimatedRowHeight = 80.0;
    
    [tblView registerNib:[UINib nibWithNibName:@"ReviewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ReviewCell"];
    [tblView registerNib:[UINib nibWithNibName:@"TotalReviewsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TotalReviewsCell"];
    [tblView registerNib:[UINib nibWithNibName:@"BookMeCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BookMeCell"];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(enableScrollview) name:@"enableTableScrolling" object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if (App_Delegate.shouldDisableScrolling==NO)
    {
       [tblView setScrollEnabled:NO];
    }
    else
    {
        [tblView setScrollEnabled:YES];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- ScrollView Delegates

-(void)enableScrollview
{
    [tblView setScrollEnabled:YES];
    [tblView setContentOffset:CGPointMake(0,1)];
}

-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollOffset = scrollView.contentOffset.y;
    if (scrollOffset <=0)
    {
        [tblView setScrollEnabled:NO];
    }
}

#pragma mark- Tableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (guideObject)
    {
        return aryInformation.count+2;
    }
    else
    {
        return aryInformation.count+1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        NSString *strCellIdentifier=@"TotalReviewsCell";
        
        TotalReviewsCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.row==aryInformation.count+1)
    {
        NSString *strCellIdentifier=@"BookMeCell";
        
        BookMeCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        NSString *strCellIdentifier=@"ReviewCell";
        ReviewCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        return 80;
    }
    else if (indexPath.row==aryInformation.count+1)
    {
        return 50;
    }
    else
    {
        NSString *strCellIdentifier=@"ReviewCell";
        
        static ReviewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            cell = [tblView dequeueReusableCellWithIdentifier:strCellIdentifier];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        return [self calculateHeightForConfiguredSizingCell:cell];
    }
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell
{
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

- (void)setUpCell:(ReviewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    ReviewObject *revObject=[aryInformation objectAtIndex:indexPath.row-1];
    
    [cell.imgProfile setClipsToBounds:YES];
    [cell.imgLikeDislike setClipsToBounds:YES];
    [cell.imgProfile.layer setCornerRadius:45/2];
    [cell.imgLikeDislike.layer setCornerRadius:20/2];
    
    [cell.imgProfile_Opposite setClipsToBounds:YES];
    [cell.imgLikeDislike_Opposite setClipsToBounds:YES];
    [cell.imgProfile_Opposite.layer setCornerRadius:45/2];
    [cell.imgLikeDislike_Opposite.layer setCornerRadius:20/2];
    
    
    if ([App_Delegate getCurrentUser]==Guide)
    {
        
        //Opposite
        if (revObject.isReviewedByGuide)
        {
            cell.lblReviewerName_Opposite.text=revObject.strGuideName;
            cell.lblReview_Opposite.text=revObject.strGuideComment;
            cell.lblReviewerAddress_Opposite.text=revObject.strGuidePlace;
            [cell.imgProfile_Opposite sd_setImageWithURL:[NSURL URLWithString:revObject.strGuideProfilePicURL]];
            [cell.imgProfile_Opposite setHidden:NO];
        }
        else
        {
            cell.lblReviewerName_Opposite.text=KNotAvailable;
            cell.lblReview_Opposite.text=KNotAvailable;
            cell.lblReviewerAddress_Opposite.text=KNotAvailable;
            [cell.imgProfile_Opposite setHidden:YES];
        }
        
        //Reviewer
        if (revObject.isReviewedByTraveller)
        {
            cell.lblReviewerName.text=revObject.strTravellerName;
            cell.lblReviewerAddress.text=revObject.strTravellerPlace;
            cell.lblReview.text=revObject.strTravellerComment;
            [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:revObject.strTravellerProfilePicURL]];
            [cell.imgProfile setHidden:NO];
        }
        else
        {
            cell.lblReviewerName.text=KNotAvailable;
            cell.lblReviewerAddress.text=KNotAvailable;
            cell.lblReview.text=KNotAvailable;
            [cell.imgProfile setHidden:YES];
        }
    
    }
    else
    {
        //self - Traveller
        //Opposite - Guide
        cell.lblReviewerName.text=revObject.strTravellerName;
        cell.lblReviewerName_Opposite.text=revObject.strGuideName;
        
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==aryInformation.count+1)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"goToBookGuidePage" object:nil];
    }
}


@end
