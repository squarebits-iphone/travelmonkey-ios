//
//  ReviewView_AboutVCViewController.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/9/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "ReviewTouristVC.h"
#import "ReviewCell.h"
#import "TotalReviewsCell.h"
#import "BookMeCell.h"
#import "ReviewObject.h"
#import "UIImageView+WebCache.h"


#define KNotAvailable @"Not Available";


@implementation ReviewTouristVC
@synthesize guideObject,aryInformation;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    tblView.rowHeight = UITableViewAutomaticDimension;
    tblView.estimatedRowHeight = 80.0;
    
    [tblView registerNib:[UINib nibWithNibName:@"ReviewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ReviewCell"];
    [tblView registerNib:[UINib nibWithNibName:@"TotalReviewsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TotalReviewsCell"];
    [tblView registerNib:[UINib nibWithNibName:@"BookMeCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BookMeCell"];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(enableScrollview) name:@"enableTableScrolling" object:nil];
    
    aryInformation=[self getFilterArray:aryInformation];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if (App_Delegate.shouldDisableScrolling==NO)
    {
        [tblView setScrollEnabled:NO];
    }
    else
    {
        [tblView setScrollEnabled:YES];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Methods

-(NSMutableArray*)getFilterArray:(NSMutableArray*)inputArray
{
    NSMutableArray *ary=[[NSMutableArray alloc]init];
    totalNegative=0;
    totalPositive=0;
    
    for (ReviewObject *reviewObj in inputArray)
    {
        
        if (reviewObj.isReviewedByGuide)
        {
            if (reviewObj.isPositiveByGuide)
            {
                totalPositive++;
            }
            else
            {
                totalNegative++;
            }
        }
        
        if (reviewObj.isReviewedByGuide==YES)
        {
            [ary addObject:reviewObj];
        }
    }
    
    return ary;
}

#pragma mark- ScrollView Delegates

-(void)enableScrollview
{
    [tblView setScrollEnabled:YES];
}

-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollOffset = scrollView.contentOffset.y;
    if (scrollOffset <=0)
    {
        [tblView setScrollEnabled:NO];
    }
}

#pragma mark- Tableview Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (aryInformation.count==0)
    {
        return 100;
    }
    
    return 0;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (guideObject)
    {
        //Hide button
        lblHeaderTitle.text=@"This tourist has no reviews yet.";
        [btnHeaderAdd setHidden:YES];
    }
    else
    {
        //Show button
        lblHeaderTitle.text=@"You do not have any reviews yet.";
        [btnHeaderAdd setHidden:YES];
    }
    
    //[btnHeaderAdd addTarget:self action:@selector(btnAddPhotosPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return viewHeader;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (aryInformation.count==0)
    {
        return 0;
    }
    
    if (guideObject && self.isForViewProfile==NO)
    {
        return aryInformation.count+2;
    }
    else
    {
        return aryInformation.count+1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        NSString *strCellIdentifier=@"TotalReviewsCell";
        
        TotalReviewsCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        [cell.lblPositiveCount setText:[NSString stringWithFormat:@"%d Positive",totalPositive]];
        [cell.lblNegativeCount setText:[NSString stringWithFormat:@"%d Negative",totalNegative]];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.row==aryInformation.count+1)
    {
        NSString *strCellIdentifier=@"BookMeCell";
        
        BookMeCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        NSString *strCellIdentifier=@"ReviewCell";
        ReviewCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        return 100;
    }
    else if (indexPath.row==aryInformation.count+1)
    {
        return 55;
    }
    else
    {
        NSString *strCellIdentifier=@"ReviewCell";
        
        static ReviewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            cell = [tblView dequeueReusableCellWithIdentifier:strCellIdentifier];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        return [self calculateHeightForConfiguredSizingCell:cell];
    }
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell
{
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

- (void)setUpCell:(ReviewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    ReviewObject *revObject=[aryInformation objectAtIndex:indexPath.row-1];
    
    [cell.imgProfile setClipsToBounds:YES];
    [cell.imgLikeDislike setClipsToBounds:YES];
    [cell.imgProfile.layer setCornerRadius:45/2];
    [cell.imgLikeDislike.layer setCornerRadius:20/2];
    
    [cell.imgProfile_Opposite setClipsToBounds:YES];
    [cell.imgLikeDislike_Opposite setClipsToBounds:YES];
    [cell.imgProfile_Opposite.layer setCornerRadius:45/2];
    [cell.imgLikeDislike_Opposite.layer setCornerRadius:20/2];
    
    [cell.viewBackground setClipsToBounds:YES];
    [cell.viewBackground.layer setCornerRadius:2];
    
    //temp
    //revObject.strTravellerComment=@"An article review is both a summary and an evaluation of another writer's article. Teachers often assign article reviews to introduce students to the work of experts in the field. Experts also are often asked to review the work of other professionals. Understanding the main points and arguments of the article is essential for an accurate summation. Logical evaluation of the article's main theme, supporting arguments, and implications for further research is an important element of a review. Here are a few guidelines for writing an article review. End";
    
    //revObject.strGuideComment=@"Before you even begin reading the article you will review, you need to understand how your article review will be set up. This will help you understand how to read the article so that you can write an effective review. Your review will be set up in the following parts. End";
    
    
    //Opposite
    if (revObject.isReviewedByTraveller)
    {
        
        cell.lblReviewerName_Opposite.text=revObject.strTravellerName;
        cell.lblReview_Opposite.text=revObject.strTravellerComment;
        [cell.imgProfile_Opposite sd_setImageWithURL:[NSURL URLWithString:revObject.strTravellerProfilePicURL]];
        [cell.imgProfile_Opposite setHidden:NO];
        [cell.lblReviewerAddress_Opposite setText:[NSString stringWithFormat:@"%@, %@",revObject.strTravellerCity,revObject.strTravellerCountry]];
        [cell.lblDate_Opposite setText:revObject.strTravellerDate];
        
        //temp
        //revObject.isPositiveByGuide=NO;
        
        if (revObject.isPositiveByTraveller)
        {
            [cell.imgLikeDislike_Opposite setImage:[UIImage imageNamed:@"icn_like.png"]];
        }
        else
        {
            [cell.imgLikeDislike_Opposite setImage:[UIImage imageNamed:@"icn_dislike.png"]];
        }
        
    }
    else
    {
        cell.lblReviewerName_Opposite.hidden=cell.lblReview_Opposite.hidden=cell.lblReviewerAddress_Opposite.hidden=cell.imgProfile_Opposite.hidden=cell.imgProfile_Opposite.hidden=cell.lblReviewerAddress_Opposite.hidden=cell.lblDate_Opposite.hidden=cell.imgLikeDislike_Opposite.hidden=cell.viewBackground.hidden=YES;
    }
    
    //Reviewer
    if (revObject.isReviewedByGuide)
    {
        cell.lblReviewerName.text=revObject.strGuideName;
        cell.lblReview.text=revObject.strGuideComment;
        [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:revObject.strGuideProfilePicURL]];
        [cell.imgProfile setHidden:NO];
        [cell.lblReviewerAddress setText:[NSString stringWithFormat:@"%@, %@",revObject.strGuideCity,revObject.strGuideCountry]];
        [cell.lblDate setText:revObject.strGuideDate];
        
        if (revObject.isPositiveByGuide)
        {
            [cell.imgLikeDislike setImage:[UIImage imageNamed:@"icn_like.png"]];
        }
        else
        {
            [cell.imgLikeDislike setImage:[UIImage imageNamed:@"icn_dislike.png"]];
        }
    }
    else
    {
        cell.lblReviewerName.text=KNotAvailable;
        cell.lblReviewerAddress.text=KNotAvailable;
        cell.lblReview.text=KNotAvailable;
        [cell.imgProfile setHidden:YES];
    }
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==aryInformation.count+1)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"goToBookGuidePage" object:nil];
    }
}


@end
