//
//  ReviewGuideVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 1/12/17.
//  Copyright © 2017 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewGuideVC : UIViewController
{
    IBOutlet UITableView *tblView;
    NSMutableArray *aryInformation;
    
    int totalPositive;
    int totalNegative;
    
    IBOutlet UIView *viewFooter;
    
    IBOutlet UIView *viewHeader;
    IBOutlet UILabel *lblHeaderTitle;
    IBOutlet UIButton *btnHeaderAdd;

}

@property (nonatomic,strong) UserObject *guideObject;
@property (nonatomic,strong) NSMutableArray *aryInformation;
@property BOOL isForViewProfile;


@end
