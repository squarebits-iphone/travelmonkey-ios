//
//  AboutUsVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/11/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUsVC : UIViewController
{
    IBOutlet UIView *viewStatusBar;
    IBOutlet UIView *viewTitle;
    IBOutlet UILabel *lblVersionInfo;
    
    NSMutableArray *aryOptions;
}

@end
