//
//  SignUpVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SocialInformation.h"

@interface SignUpVC : UIViewController
{
    IBOutlet UIView *viewTopMain;
    IBOutlet UIView *viewScrollContent;
    
    IBOutlet UIImageView *imgUserProfile;
    IBOutlet UIScrollView *scrollviewMain;
    
    UIImage *chosenImage;
    NSString *strUserType;
    NSString *strUserImageId;
    IBOutlet UIButton *btnSignUp;
    
    IBOutlet NSLayoutConstraint *constScrollContentView;
    
    IBOutlet NSLayoutConstraint *constScrollViewBottomSpace;
}

@property (strong,nonatomic) SocialInformation *socialObject;
@property (strong, nonatomic) IBOutlet UIButton *btnTourist;
@property (strong, nonatomic) IBOutlet UIButton *btnGuide;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldFirstName;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldLastName;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldConfirmPassword;
@property (strong,nonnull) IBOutlet UIImageView *imgtxtFieldPassword;
@property (strong,nonnull) IBOutlet UIImageView *imgtxtFieldConfirmPass;

@property (strong, nonatomic) IBOutlet UIButton *btnSignUp;

- (IBAction)btnTouristPressed:(UIButton *)sender;
- (IBAction)btnGuidePressed:(UIButton *)sender;
- (IBAction)btnSignUpPressed:(UIButton *)sender;
- (IBAction)btnBackPressed:(UIButton *)sender;




@end
