//
//  HelpVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpVC : UIViewController
{
    IBOutlet UIView *viewStatusBar;
    IBOutlet UIView *viewTitle;
    NSMutableArray *aryOptions;
    IBOutlet UITableView *tblView;
    
}


@property BOOL isGuide;


@end
