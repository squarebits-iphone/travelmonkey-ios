//
//  ContactUsVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"


@interface ContactUsVC : UIViewController
{
    IBOutlet UIScrollView *scrollviewMain;
    IBOutlet UIView *subViewScroll;
    
    
    IBOutlet UIButton *btnDepartment;
    IBOutlet UIButton *btnIam;
    IBOutlet UITextField *txtFieldName;
    IBOutlet UITextField *txtFieldEmail;
    IBOutlet UITextView *txtViewMessage;
    NIDropDown *dropDown;
    
    __weak IBOutlet NSLayoutConstraint *constraintTableHeight;
    
}

@end
