//
//  AddCreditCardDetailVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/20/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "AddCreditCardDetailVC.h"
#import "AddPaymentCell.h"
#import "CreditCardCell.h"
#import "CreditCard.h"

@interface AddCreditCardDetailVC ()

@end

@implementation AddCreditCardDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self refreshData];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshData) name:@"refreshData" object:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark- Methods

-(void)refreshData
{
    aryCard=[[NSMutableArray alloc]init];
    
    if (App_Delegate.userObject.travellerDetailObject.aryCards && App_Delegate.userObject.travellerDetailObject.aryCards.count>0)
    {
        aryCard=App_Delegate.userObject.travellerDetailObject.aryCards;
    }
    
    [tblCardView reloadData];
    
    if (App_Delegate.userObject.travellerDetailObject.aryCards && App_Delegate.userObject.travellerDetailObject.aryCards.count>0)
    {
        
        BOOL isDefaultCard=NO;
        
        for (CreditCard *card in App_Delegate.userObject.travellerDetailObject.aryCards)
        {
            
            if ([card.strActive isEqualToString:@"1"])
            {
                isDefaultCard=YES;
                break;
            }
        }
        
        if (isDefaultCard==NO)
        {
            [App_Delegate showAlertWithMessage:@"Please set default card." inViewController:self];
        }
        
        
    }
}

-(void)callActiveCardAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KActiveCardAPI];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:currentCard.strId forKey:@"card_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(deleteCardAPIResponse:) WithCallBackObject:self];
}

-(void)callDeleteCardAPI
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KDeleteCardAPI];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:currentCard.strId forKey:@"card_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(deleteCardAPIResponse:) WithCallBackObject:self];
}

-(void)deleteCardAPIResponse:(NSDictionary*)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        [App_Delegate internalLogin];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
            
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"There is some error" inViewController:self];
        }
    }
}

-(void)callTokenAPI:(NSString*)strToken
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KSendStripeToken];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strToken forKey:@"token"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(tokenAPIResponse:) WithCallBackObject:self];
}

-(void)tokenAPIResponse:(NSDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        [App_Delegate internalLogin];
        [App_Delegate showAlertWithMessage:@"Credit Card Detail added successfully" inViewController:self];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Credit Card Detail cannot be added this time" inViewController:self];
        }
    }
}

-(void)btnEditPressed:(UIButton*)btn
{
    currentCard=[aryCard objectAtIndex:btn.tag];
    [self callActiveCardAPI];
}

-(void)btnDeletePressed:(UIButton*)btn
{
    currentCard=[aryCard objectAtIndex:btn.tag];
    [self callDeleteCardAPI];
}

#pragma mark- Button Action

-(void)btnAddCardPressed:(id)sender
{
    STPAddCardViewController *addCardViewController = [[STPAddCardViewController alloc] init];
    addCardViewController.delegate = (id)self;
    
    // STPAddCardViewController must be shown inside a UINavigationController.
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:addCardViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}


#pragma mark- 

- (void)paymentContextDidChange:(STPPaymentContext *)paymentContext
{
    //self.activityIndicator.animating = paymentContext.loading;
    //self.paymentButton.enabled = paymentContext.selectedPaymentMethod != nil;
    //self.paymentLabel.text = paymentContext.selectedPaymentMethod.label;
    //self.paymentIcon.image = paymentContext.selectedPaymentMethod.image;
}

- (void)paymentContext:(STPPaymentContext *)paymentContext
didCreatePaymentResult:(STPPaymentResult *)paymentResult
            completion:(STPErrorBlock)completion
{
//    [self.apiClient createCharge:paymentResult.source.stripeID completion:^(NSError *error) {
//        if (error) {
//            completion(error);
//        } else {
//            completion(nil);
//        }
//    }];
}

- (void)paymentContext:(STPPaymentContext *)paymentContext didFailToLoadWithError:(NSError *)error
{
    // Show the error to your user, etc.
    NSLog(@"Payment failed : %@",error);
}

#pragma mark- STPAddCardViewControllerDelegate

-(void)addCardViewControllerDidCancel:(STPAddCardViewController *)addCardViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)addCardViewController:(STPAddCardViewController *)addCardViewController
              didCreateToken:(STPToken *)token
                  completion:(STPErrorBlock)completion
{
    
    [self hideAddCardScreen];
    NSLog(@"Token id : %@",token.tokenId);
    [self callTokenAPI:token.tokenId];
}

-(void)hideAddCardScreen
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark- Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return aryCard.count+1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==aryCard.count)
    {
        return 70;
    }
    else
    {
       return 100;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==aryCard.count)
    {
        NSString *strCellIdentifier=@"AddPaymentCell";
        
        AddPaymentCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        [cell.lblAddPayment setTextColor:KAppColor_Blue];
        
        return cell;
    }
    else
    {
        NSString *strCellIdentifier=@"CreditCardCell";
        
        CreditCardCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        CreditCard *card=[aryCard objectAtIndex:indexPath.row];
        
        NSBundle *bundle = [NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"Stripe" withExtension:@"bundle"]];
        NSString *imagePath = [bundle pathForResource:@"stp_card_visa" ofType:@"png"];
        
        [cell.lblCardNumber setText:[NSString stringWithFormat:@"%@ ending in %@",card.strBrand,card.strLastdigits]];
        //[cell.imgCardPicture setImage:[UIImage imageWithContentsOfFile:imagePath]];
        
        [cell.btnEdit setTag:indexPath.row];
        [cell.btnDelete setTag:indexPath.row];
        
        BOOL isActive=[card.strActive boolValue];
        
        if (isActive)
        {
            [cell.btnEdit setTitle:@"Default" forState:UIControlStateNormal];
            [cell.btnEdit setTitleColor:KAppColor forState:UIControlStateNormal];
        }
        else
        {
            [cell.btnEdit setTitle:@"Make Default" forState:UIControlStateNormal];
            [cell.btnEdit setTitleColor:KAppColor_GrayBlue forState:UIControlStateNormal];
        }
        
        [cell.btnEdit addTarget:self action:@selector(btnEditPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnDelete addTarget:self action:@selector(btnDeletePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnDelete setTitleColor:KAppColor_Blue forState:UIControlStateNormal];
        
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==aryCard.count)
    {
        [self btnAddCardPressed:nil];
    }
}


@end
