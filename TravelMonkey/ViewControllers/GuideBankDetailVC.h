//
//  GuideBankDetailVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/14/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuideBankDetailVC : UIViewController
{
    //Outlets
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UIScrollView *scrollviewMain;
    IBOutlet UIView *viewScrollContent;
    IBOutlet UIView *viewVerify;
    IBOutlet UITextField *txtFieldAccountNumberVerify;
    IBOutlet UITextField *txtFieldNameVerify;
    
    IBOutlet UILabel *lblIPAddress;
    IBOutlet UITextField *txtFieldAccountNumber;
    IBOutlet UITextField *txtFieldFirstName;
    IBOutlet UITextField *txtFieldLastName;
    IBOutlet UITextField *txtFieldAddress;
    IBOutlet UITextField *txtFieldCity;
    IBOutlet UITextField *txtFieldState;
    IBOutlet UITextField *txtFieldSSN;
    IBOutlet UITextField *txtFieldRouting;
    IBOutlet UITextField *txtFieldPostalCode;
    IBOutlet UITextField *txtFieldPersonalID;
    IBOutlet UITextField *txtFieldBirthDate;
    IBOutlet UITextField *txtFieldBirthMonth;
    IBOutlet UITextField *txtFieldBirthYear;
    
    NSString *strIP;
    NSString *strType;
    
    __weak IBOutlet NSLayoutConstraint *constraintHeight;
    
    UIImage *chosenImage;
    
    NSString *strFileID;
    
    IBOutlet UIButton *btnSave;
    IBOutlet UILabel *lblHeaderTitle;
    
    
    
}

@end
