//
//  HomeVC_Tourist.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 7/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeVC_Tourist : UIViewController
{
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UITextField *txtFieldPlace;
    IBOutlet UIView *viewSubNavigationBar;
    GMSPlace *touristGMSPlace;
    
}

@end
