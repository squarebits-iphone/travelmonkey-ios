//
//  VerifyEmailVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/16/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyEmailVC : UIViewController
{
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    IBOutlet UITextView *lblMessage;
    
}

@property (nonatomic,strong) NSString *strEmail;
@property (nonatomic,strong) NSString *strPassword;

@end
