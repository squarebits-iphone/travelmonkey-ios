//
//  ReviewView_AboutVCViewController.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/9/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewView_AboutVCViewController : UIViewController
{
    IBOutlet UITableView *tblView;
    NSMutableArray *aryInformation;
}

@property (nonatomic,strong) UserObject *guideObject;
@property (nonatomic,strong) NSMutableArray *aryInformation;

@end
