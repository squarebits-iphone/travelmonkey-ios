//
//  QuestionVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "QuestionVC.h"
#import "QuestionCell.h"
#import "QuestionDetailCell.h"

#define KQuestion @"question"
#define KAnswer @"answer"

@interface QuestionVC ()

@end

@implementation QuestionVC

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewStatusBar setBackgroundColor:KAppColor];
    [viewTitle setBackgroundColor:KAppColor];
    
    [self setupData];
    
    currentIndexPath=[NSIndexPath indexPathForRow:-1 inSection:0];
    
    [tbl registerNib:[UINib nibWithNibName:@"QuestionDetailCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"QuestionDetailCell"];
    
    [tbl registerNib:[UINib nibWithNibName:@"QuestionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"QuestionCell"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    //tbl.estimatedRowHeight=tbl.frame.size.width;
    [tbl reloadData];
}

#pragma mark- Methods

-(void)setupData
{
    
    //Main Array
    aryMain=[[NSMutableArray alloc]init];
    
    if ([App_Delegate getCurrentUser]==Guide)
    {
        NSMutableArray *aryQuestion=[NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"HelpLocalGuide" ofType:@"plist"]];
        //Adding sections
        [aryMain addObject:aryQuestion];
    }
    else
    {
        NSMutableArray *aryQuestion=[NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"HelpTourist" ofType:@"plist"]];
        //Adding sections
        [aryMain addObject:aryQuestion];
    }
    
}

-(void)refreshTablePosition:(NSIndexPath*)indexPath
{
    [tbl scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark- Button Action

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- Tableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return aryMain.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[aryMain objectAtIndex:section] count];
}

//- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    //Making View
//    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width-40,40)];
//    [view setBackgroundColor:[UIColor whiteColor]];
//
//    UILabel *lblTitle=[[UILabel alloc]initWithFrame:view.bounds];
//    [lblTitle setTextColor:[UIColor darkGrayColor]];
//    [lblTitle setTextAlignment:NSTextAlignmentCenter];
//    [lblTitle setFont:[UIFont fontWithName:@"Titillium-Regular" size:18]];
//
//    //Title
//    NSString *strTitle=@"";
//
//    if (section==0)
//    {
//        strTitle=@"Top Questions";
//    }
//    else
//    {
//        strTitle=@"More Questions";
//    }
//
//    [lblTitle setText:strTitle];
//    [view addSubview:lblTitle];
//
//    return view;
//
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([currentIndexPath compare:indexPath]== NSOrderedSame)
    {
        NSString *strCellIdentifier=@"QuestionDetailCell";
        
        static QuestionDetailCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            cell = [tbl dequeueReusableCellWithIdentifier:strCellIdentifier];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        return [self calculateHeightForConfiguredSizingCell:cell];
    }
    else
    {
        NSString *strCellIdentifier=@"QuestionCell";
        
        static QuestionCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            cell = [tbl dequeueReusableCellWithIdentifier:strCellIdentifier];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        });
        
        [self setUpCellQuestionCell:cell atIndexPath:indexPath];
        
        return [self calculateHeightForConfiguredSizingCell:cell];
    }
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell
{
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

- (void)setUpCellQuestionCell:(QuestionCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    [cell.lblTitle setPreferredMaxLayoutWidth:cell.lblTitle.frame.size.width];
    NSMutableDictionary *dictionary=[[aryMain objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [cell.lblTitle setText:[dictionary objectForKey:KQuestion]];
    
}

- (void)setUpCell:(QuestionDetailCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    [cell.lblQuestion setPreferredMaxLayoutWidth:cell.lblQuestion.frame.size.width];
    [cell.lblAnswer setPreferredMaxLayoutWidth:cell.lblAnswer.frame.size.width];
    
    NSMutableDictionary *dictionary=[[aryMain objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [cell.lblQuestion setText:[dictionary objectForKey:KQuestion]];
    [cell.lblAnswer setText:[dictionary objectForKey:KAnswer]];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([currentIndexPath compare:indexPath]== NSOrderedSame)
    {
        
        NSString *strCellIdentifier=@"QuestionDetailCell";
        
        QuestionDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        
        if (cell==nil)
        {
            cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        [cell.viewBackGround setClipsToBounds:YES];
        [cell.viewBackGround.layer setCornerRadius:30];
        [cell.viewBackGround.layer setBorderColor:KAppColor_Blue.CGColor];
        [cell.viewBackGround.layer setBorderWidth:1];
        
        return cell;
        
    }
    
    NSString *strCellIdentifier=@"QuestionCell";
    
    QuestionCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
    
    if (cell==nil)
    {
        cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    [self setUpCellQuestionCell:cell atIndexPath:indexPath];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([currentIndexPath compare:indexPath]== NSOrderedSame)
    {
        currentIndexPath=[NSIndexPath indexPathForRow:-1 inSection:0];
    }
    else
    {
        
        currentIndexPath=indexPath;
    }
    
    [tbl reloadData];
    [self performSelector:@selector(refreshTablePosition:) withObject:indexPath afterDelay:0.5];
}


@end
