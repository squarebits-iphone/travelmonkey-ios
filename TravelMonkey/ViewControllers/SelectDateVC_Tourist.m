//
//  SelectDateVC_Tourist.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/22/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "SelectDateVC_Tourist.h"
#import "JTCalendarMenuView.h"
#import "SelectGuideVC_Tourist.h"



@interface SelectDateVC_Tourist ()

@end

@implementation SelectDateVC_Tourist

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    [_calendarMenuView setBackgroundColor:[UIColor clearColor]];
    [_calendarContentView setBackgroundColor:[UIColor clearColor]];
    
    [self setupCalender];
    [self setTimePickersWithDate:[NSDate date]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [scrollviewMain setContentSize:CGSizeMake(0, startTimePicker.frame.origin.y+startTimePicker.frame.size.height+100)];
}

#pragma mark- Methods

-(void)setupCalender
{
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:[NSDate date]];
    
    _calendarMenuView.scrollView.scrollEnabled = NO; // Scroll not supported with JTVerticalCalendarView
    
    _dateSelected=[NSDate date];
    
    [startTimePicker setValue:KAppColor_Blue forKeyPath:@"textColor"];
    [endTimePicker setValue:KAppColor_Blue forKeyPath:@"textColor"];
    SEL selector = NSSelectorFromString( @"setHighlightsToday:" );
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature :
                                [UIDatePicker
                                 instanceMethodSignatureForSelector:selector]];
    BOOL no = NO;
    [invocation setSelector:selector];
    [invocation setArgument:&no atIndex:2];
    [invocation invokeWithTarget:startTimePicker];
    [invocation invokeWithTarget:endTimePicker];
    
}

-(void)setTimePickersWithDate:(NSDate*)inputDate
{
    
    inputDate=[self getRoundedDate:inputDate];
    
    NSTimeInterval seconds = 1 * 60 * 60; //hh*mm*sec
    NSDate *dateHoursAhead=[inputDate dateByAddingTimeInterval:seconds];
    [startTimePicker setMinimumDate:dateHoursAhead];
    [startTimePicker setDate:dateHoursAhead];
    
    
    NSTimeInterval seconds_End = 2 * 60 * 60; //hh*mm*sec
    NSDate *dateHoursAhead_End=[inputDate dateByAddingTimeInterval:seconds_End];
    [endTimePicker setMinimumDate:dateHoursAhead_End];
    [endTimePicker setDate:dateHoursAhead_End];
}

- (NSDate *)getRoundedDate:(NSDate *)inDate{
    
    NSDate *returnDate;
    NSInteger minuteInterval = 15 ;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSMinuteCalendarUnit fromDate:inDate];
    NSInteger minutes = [dateComponents minute];
    NSInteger minutesRounded = ( (NSInteger)(minutes / minuteInterval) ) * minuteInterval;
    NSDate *roundedDate = [[NSDate alloc] initWithTimeInterval:60.0 * (minutesRounded - minutes) sinceDate:inDate];
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Set the date picker's value (and the selected date on the UI display) to
    // the rounded date.
    if ([roundedDate isEqualToDate:inDate])
    {
        // We need to set the date picker's value to something different than
        // the rounded date, because the second call to set the date picker's
        // date with the same value is ignored. Which could be bad since the
        // call above to set the date picker's minute interval can leave the
        // date picker with the wrong selected date (the whole reason why we are
        // doing this).
        NSDate *diffrentDate = [[NSDate alloc] initWithTimeInterval:60 sinceDate:roundedDate];
        returnDate = diffrentDate;
        //[diffrentDate release];
    }
    
    returnDate = roundedDate;
    return returnDate;
}

-(IBAction)timeChanged:(UIDatePicker*)timePicker
{
    if (timePicker.tag==101)
    {
        //Start Time
        NSTimeInterval seconds_End = 1 * 60 * 60; //hh*mm*sec
        NSDate *dateHoursAhead_End=[timePicker.date dateByAddingTimeInterval:seconds_End];
        [endTimePicker setMinimumDate:dateHoursAhead_End];
    }
    else
    {
        //End Time
    }
}

#pragma mark- Button Action

-(IBAction)btnFindGuidePressed:(id)sender
{
    
    NSDateFormatter *formatter=[NSDateFormatter new];
    //[formatter setDateFormat:@"yyyy-MM-dd h:mm a"];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    //GMT
    //NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    //[formatter setTimeZone:gmt];
    
    SelectGuideVC_Tourist *showLocals=[[SelectGuideVC_Tourist alloc]init];
    showLocals.touristGMSPlace=self.touristGMSPlace;
    showLocals.strStartTime=[formatter stringFromDate:[App_Delegate getDateWithZeroSecond:startTimePicker.date]];
    showLocals.strEndTime=[formatter stringFromDate:[App_Delegate getDateWithZeroSecond:endTimePicker.date]];
    App_Delegate.strStartTime=showLocals.strStartTime;
    App_Delegate.strEndTime=showLocals.strEndTime;
    
    NSLog(@"Calender Date : %@",_dateSelected);
    NSLog(@"StartTime : %@",startTimePicker.date);
    NSLog(@"EndTime : %@",endTimePicker.date);
    
    [self.navigationController pushViewController:showLocals animated:YES];
    
}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    
    dayView.hidden = NO;
    
    // Hide if from another month
    if([dayView isFromAnotherMonth])
    {
        dayView.hidden = YES;
    }
    
    // Today
    else if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor lightGrayColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = KAppColor_Blue;
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Other month
    else if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Another day of the current month
    else
    {
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = KAppColor_Blue;
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    
    if([self haveEventForDay:dayView.date])
    {
        dayView.dotView.hidden = NO;
    }
    else
    {
        dayView.dotView.hidden = YES;
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    
    if(![_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date] && [dayView.date compare:[NSDate date]] == NSOrderedAscending)
    {
        [App_Delegate showAlertWithMessage:@"Booking cannot be done in past" inViewController:self];
        return;
    }
    
    _dateSelected = dayView.date;
    
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date])
    {
       [self setTimePickersWithDate:[NSDate date]];
    }
    else
    {
       [self setTimePickersWithDate:_dateSelected];
    }
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                    } completion:nil];
    
    
    // Don't change page in week mode because block the selection of days in first and last weeks of the month
    if(_calendarManager.settings.weekModeEnabled){
        return;
    }
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
            [_calendarContentView loadNextPageWithAnimation];
        }
        else{
            [_calendarContentView loadPreviousPageWithAnimation];
        }
    }
}

#pragma mark - Fake data

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    
    return NO;
    
}

- (void)createRandomEvents
{
    _eventsByDate = [NSMutableDictionary new];
    
    for(int i = 0; i < 30; ++i){
        // Generate 30 random dates between now and 60 days later
        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
        
        // Use the date as key for eventsByDate
        NSString *key = [[self dateFormatter] stringFromDate:randomDate];
        
        if(!_eventsByDate[key]){
            _eventsByDate[key] = [NSMutableArray new];
        }
        
        [_eventsByDate[key] addObject:randomDate];
    }
}



@end
