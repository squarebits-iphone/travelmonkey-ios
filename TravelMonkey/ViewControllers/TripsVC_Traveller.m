//
//  TripsVC_Traveller.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/30/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "TripsVC_Traveller.h"
#import "Parser.h"
#import "TripObject.h"
#import "UIImageView+WebCache.h"
#import "SVPullToRefresh.h"

#import "TripTravellerCell.h"
#import "TripGuideConfirmedRequestCell.h"
#import "TripGuideNewRequestCell.h"
#import "NSDate+TimeAgo.h"
#import "PastTripNormalCell.h"
#import "PastTripDetailCell.h"
#import "TripDetailVC_Tourist.h"
#import "ChatViewControllerVC_Guide.h"
#import "HomeVC_Guide.h"


@interface TripsVC_Traveller ()

@end

@implementation TripsVC_Traveller

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    tblTrips.delegate=self;
    tblTrips.dataSource=self;
    
    [viewTopStatusBar setBackgroundColor:KAppColor];
    [viewTopMain setBackgroundColor:KAppColor];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    __weak TripsVC_Traveller *weakSelf = self;
    
    //Setup pull-to-refresh
    [tblTrips addPullToRefreshWithActionHandler:^{
        
        [weakSelf refreshTripTableView];
        
    }];
    
    [self btnFutureTripsPressed:btnFutureTrips];
    
    [[NSNotificationCenter defaultCenter]addObserver:tblTrips selector:@selector(reloadData) name:kMessageUnreadReceivedNotification object:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [refreshTimer invalidate];
}

#pragma mark- Methods

-(void)callCancelBookedBookingbyGuide:(NSString*)strBookingID
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KCancelBookedRequest];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strBookingID forKey:@"booking_id"];
    [dicParameter setObject:@"TRAVELER" forKey:@"role"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(bookingRequestResponse:) WithCallBackObject:self];
    
}

-(void)bookingRequestResponse:(NSDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        
        isSuccess=YES;
        [self refreshTripTableView];
    }
    
    if (!isSuccess)
    {
        
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"Please try again, Server not responding" inViewController:self];
        }
    }
}

-(void)btnCancelBookedBookingPressed:(UIButton*)button
{
    TripObject *object;
    object=[aryFutureTrips objectAtIndex:button.tag];
    //[self callCancelBookedBookingbyGuide:object.strBookingId];
    currentObject=object;
    
    NSString *strMessage=[NSString stringWithFormat:@"Are you sure you want to cancel this booking?"];
    
    SCLAlertView *alert =[App_Delegate getAlertInstance];
    
    [alert addButton:@"No" actionBlock:^(void) {
        
    }];
    
    [alert addButton:@"Yes" actionBlock:^(void) {
        
        [self callCancelBookedBookingbyGuide:currentObject.strBookingId];
        
    }];
    
    [alert showCustom:[UIImage imageNamed:@"icn_alertview.png"] color:KAppColor_Blue title:nil subTitle:strMessage closeButtonTitle:nil duration:0.0];
    
}

-(void)btnStartTripPressed:(UIButton*)button
{
    [App_Delegate showAlertWithMessage:@"Under Construction" inViewController:self];
}

-(void)btnChatWithTouristPressed:(UIButton*)button
{
    TripObject *object=[aryFutureTrips objectAtIndex:button.tag];
    ChatViewControllerVC_Guide *chatVC=[[ChatViewControllerVC_Guide alloc]init];
    chatVC.strUserChatId=object.strUserChatID;
    chatVC.strBookingID=object.strBookingId;
    chatVC.strOtherName=[NSString stringWithFormat:@"%@",object.strFirstName];
    chatVC.strOtherPlace=[NSString stringWithFormat:@"%@, %@",object.strCity,object.strCountry];
    chatVC.strOtherImageUrl=object.strImageURL;
    [self.navigationController pushViewController:chatVC animated:YES];
    
    if ([App_Delegate.aryUnreadChat containsObject:object.strBookingId])
    {
        [App_Delegate.aryUnreadChat removeObject:object.strBookingId];
        [tblTrips reloadData];
    }

}

-(void)btnViewDetailsPressed:(UIButton*)button
{
    TripObject *object=[aryPastTrips objectAtIndex:button.tag];
    TripDetailVC_Tourist *view=[[TripDetailVC_Tourist alloc]init];
    view.strTripID=object.strBookingId;
    [self.navigationController pushViewController:view animated:YES];
}

-(void)refreshTripTableView
{
    if (btnFutureTrips.selected)
    {
        [self fetchFutureTripsAPI:YES];
    }
    else
    {
        [self fetchPastTripsAPI:YES];
    }
}

-(void)refreshSilently
{
    if (btnFutureTrips.selected)
    {
        [self fetchFutureTripsAPI:NO];
    }
    else
    {
        [self fetchPastTripsAPI:NO];
    }
}

-(void)fetchFutureTripsAPI:(BOOL)shouldShowHUD
{
    
    [refreshTimer invalidate];
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KGetFutureBooking];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:App_Delegate.userObject.strUserType forKey:@"role"];
    [dicParameter setObject:@"1" forKey:@"page"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    if (shouldShowHUD)
    {
        [tblTrips setHidden:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(tripAPIResponse:) WithCallBackObject:self];
    }
    else
    {
        [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(tripAPIResponseBackground:) WithCallBackObject:self];
    }
}

-(void)fetchPastTripsAPI:(BOOL)shouldShowHUD
{
    
    [refreshTimer invalidate];
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KGetPastBooking];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:App_Delegate.userObject.strUserType forKey:@"role"];
    [dicParameter setObject:@"1" forKey:@"page"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    
    if (shouldShowHUD)
    {
        [tblTrips setHidden:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(tripAPIResponse:) WithCallBackObject:self];
    }
    else
    {
        [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(tripAPIResponseBackground:) WithCallBackObject:self];
    }
    
    
}

-(void)tripAPIResponse:(NSDictionary *)dictionary
{
    
    [tblTrips setHidden:NO];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [tblTrips.pullToRefreshView stopAnimating];
    
    aryFutureTrips=[[NSMutableArray alloc]init];
    aryPastTrips=[[NSMutableArray alloc]init];
    
    [tblTrips setHidden:NO];
    [tblTrips reloadData];
    
    refreshTimer=[NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(refreshSilently) userInfo:nil repeats:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        
        if ([dictionary objectForKey:@"booking_detail"] && [[dictionary objectForKey:@"booking_detail"] isKindOfClass:[NSArray class]])
        {
            NSArray *ary=[dictionary objectForKey:@"booking_detail"];
            
            if (ary.count>0)
            {
                isSuccess=YES;
                
                if (showFutureTrips)
                {
                    
                    Parser *parser=[[Parser alloc]init];
                    aryFutureTrips=[parser getTripArray:ary];
                }
                else
                {
                    
                    Parser *parser=[[Parser alloc]init];
                    aryPastTrips=[parser getTripArray:ary];
                }
            }
        }
    }
    
    //temp
//    NSMutableArray *aryTemp=[[NSMutableArray alloc]init];
//    TripObject *tripObj=[[TripObject alloc]init];
//    [tripObj setStrStatus:@"ACCEPTED"];
//    [aryTemp addObject:tripObj];
//    
//    tripObj=[[TripObject alloc]init];
//    [tripObj setStrStatus:@"GUIDING"];
//    [aryTemp addObject:tripObj];
//    
//    
//    tripObj=[[TripObject alloc]init];
//    [tripObj setStrStatus:@"NEW_REQUEST"];
//    [aryTemp addObject:tripObj];
//    
//    tripObj=[[TripObject alloc]init];
//    [tripObj setStrStatus:@""];
//    [aryTemp addObject:tripObj];
//    
//    aryFutureTrips=aryTemp;
    
//        NSMutableArray *aryTemp=[[NSMutableArray alloc]init];
//        TripObject *tripObj=[[TripObject alloc]init];
//        [tripObj setStrStatus:@"TIMEOUT"];
//        [aryTemp addObject:tripObj];
//    
//        tripObj=[[TripObject alloc]init];
//        [tripObj setStrStatus:@"DECLINED"];
//        [aryTemp addObject:tripObj];
//    
//    
//        tripObj=[[TripObject alloc]init];
//        [tripObj setStrStatus:@"CANCELLED"];
//        [aryTemp addObject:tripObj];
//    
//        tripObj=[[TripObject alloc]init];
//        [tripObj setStrStatus:@"COMPLETED"];
//        [aryTemp addObject:tripObj];
//    
//        tripObj=[[TripObject alloc]init];
//        [tripObj setStrStatus:@""];
//        [aryTemp addObject:tripObj];
//    
//        aryPastTrips=aryTemp;
    
    //-------------
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate dropDownalertWithMessage:[dictionary objectForKey:@"message"]];
        }
        else
        {
            [App_Delegate dropDownalertWithMessage:@"No booking found"];
        }
    }
    
    [tblTrips reloadData];
}

-(void)tripAPIResponseBackground:(NSDictionary *)dictionary
{
    
    [tblTrips setHidden:NO];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [tblTrips.pullToRefreshView stopAnimating];
    
    aryFutureTrips=[[NSMutableArray alloc]init];
    aryPastTrips=[[NSMutableArray alloc]init];
    
    [tblTrips setHidden:NO];
    [tblTrips reloadData];
    
    refreshTimer=[NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(refreshSilently) userInfo:nil repeats:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        
        if ([dictionary objectForKey:@"booking_detail"] && [[dictionary objectForKey:@"booking_detail"] isKindOfClass:[NSArray class]])
        {
            NSArray *ary=[dictionary objectForKey:@"booking_detail"];
            
            if (ary.count>0)
            {
                isSuccess=YES;
                
                if (showFutureTrips)
                {
                    
                    Parser *parser=[[Parser alloc]init];
                    aryFutureTrips=[parser getTripArray:ary];
                }
                else
                {
                    
                    Parser *parser=[[Parser alloc]init];
                    aryPastTrips=[parser getTripArray:ary];
                }
            }
        }
    }
    
    [tblTrips reloadData];
}

-(UILabel *)getCustomBackgroundLabel
{
    UILabel *messageLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,tblTrips.bounds.size.width,tblTrips.bounds.size.height)];
    messageLbl.text = @"No booking found";
    messageLbl.textAlignment = NSTextAlignmentCenter;
    messageLbl.numberOfLines=0;
    messageLbl.font=[UIFont fontWithName:@"SourceSansPro-Regular" size:15.0];
    messageLbl.textColor=KAppColor_Blue;
    [messageLbl sizeToFit];
    
    return messageLbl;
}

-(void)addTapGuestureToImageView:(UIImageView*)imageView
{
    UITapGestureRecognizer *gesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewProfileButtonPressed:)];
    [imageView addGestureRecognizer:gesture];
}

-(void)callGuideDetailAPIWithID:(NSString*)strTouristID
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strURL=[NSString stringWithFormat:@"%@%@",KBaseURL,KGetUserDetail];
    
    NSMutableDictionary *dicParameter=[[NSMutableDictionary alloc]init];
    [dicParameter setObject:App_Delegate.userObject.strId forKey:@"user_id"];
    [dicParameter setObject:strTouristID forKey:@"otheruser_id"];
    
    NSMutableDictionary *dicHeader=[[NSMutableDictionary alloc]init];
    [dicHeader setObject:App_Delegate.userObject.strAuthKey forKey:@"auth"];
    
    [APIsList postAPIWithURL:strURL andHeader:dicHeader andParameters:dicParameter Selector:@selector(userDetailAPIResponse:) WithCallBackObject:self];
}

-(void)userDetailAPIResponse:(NSDictionary*)dictionary
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL isSuccess=NO;
    
    if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [[dictionary objectForKey:@"status"] intValue]==1)
    {
        isSuccess=YES;
        Parser *parser=[[Parser alloc]init];
        UserObject *guideObject=[parser getUserDetail:dictionary];
        
        HomeVC_Guide *filter=[[HomeVC_Guide alloc]init];
        filter.guideObject=guideObject;
        filter.isForViewProfile=YES;
        [self.navigationController pushViewController:filter animated:YES];
    }
    
    if (!isSuccess)
    {
        if (dictionary!=nil && [dictionary isKindOfClass:[NSDictionary class]] && [dictionary objectForKey:@"message"])
        {
            [App_Delegate showAlertWithMessage:[dictionary objectForKey:@"message"] inViewController:self];
        }
        else
        {
            [App_Delegate showAlertWithMessage:@"User details not found" inViewController:self];
        }
    }
    
}

#pragma mark- Actions

-(void)viewProfileButtonPressed:(UITapGestureRecognizer*)tapGesture
{
    UIImageView *imgView=(UIImageView*)[tapGesture view];
    
    TripObject *object=[aryFutureTrips objectAtIndex:imgView.tag];
    [self callGuideDetailAPIWithID:object.strGuideId];
}

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

-(IBAction)btnFutureTripsPressed:(id)sender
{
    aryFutureTrips=[[NSMutableArray alloc]init];
    aryPastTrips=[[NSMutableArray alloc]init];
    [tblTrips reloadData];
    [self fetchFutureTripsAPI:YES];
    showFutureTrips=YES;
    [btnPastTrips setSelected:NO];
    [btnFutureTrips setSelected:YES];
}

-(IBAction)btnPastTripsPressed:(id)sender
{
    aryFutureTrips=[[NSMutableArray alloc]init];
    aryPastTrips=[[NSMutableArray alloc]init];
    [tblTrips reloadData];
    [self fetchPastTripsAPI:YES];
    showFutureTrips=NO;
    [btnPastTrips setSelected:YES];
    [btnFutureTrips setSelected:NO];
}

#pragma mark- Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (showFutureTrips)
    {
        
        if (aryFutureTrips==nil || aryFutureTrips==(id)[NSNull null] || aryFutureTrips.count==0)
        {
            tblTrips.backgroundView = [self getCustomBackgroundLabel];
            return 0;
        }
        else
        {
            NSLog(@"Unread Chat Array : %@",App_Delegate.aryUnreadChat);
            tblTrips.backgroundView=nil;
            return aryFutureTrips.count;
            
        }
    }
    else
    {
        
        if (aryPastTrips==nil || aryPastTrips==(id)[NSNull null] || aryPastTrips.count==0)
        {
            tblTrips.backgroundView = [self getCustomBackgroundLabel];
            return 0;
        }
        else
        {
            tblTrips.backgroundView=nil;
            return aryPastTrips.count;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (showFutureTrips)
    {
        return 140;
    }
    else
    {
        TripObject *object=[aryPastTrips objectAtIndex:indexPath.row];
        
        if ([object.strStatus isEqualToString:@"TIMEOUT"] || [object.strStatus isEqualToString:@"DECLINED"] || [object.strStatus isEqualToString:@"CANCELLED"]) //Rejected
        {
            return 85;
        }
        else if ([object.strStatus isEqualToString:@"COMPLETED"]) //Comepleted
        {
            return 140;
        }
        else //Cancelled
        {
            return 140;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (showFutureTrips)
    {
        
        TripObject *object=[aryFutureTrips objectAtIndex:indexPath.row];
        
        if ([object.strStatus isEqualToString:@"ACCEPTED"]) //Trip accepted
        {
            NSString *strCellIdentifier=@"TripGuideNewRequestCell";
            
            TripGuideNewRequestCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
            
            if (cell==nil)
            {
                cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            //Image
            [cell.imgUserProfile setClipsToBounds:YES];
            [cell.imgUserProfile.layer setCornerRadius:20];
            [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:object.strImageURL]];
            
            [cell.imgCurrentStatus setImage:[UIImage imageNamed:@"icn_accepted.png"]];
            
            //Name
            NSString *strUserName=@"";
            
            if (object.strFirstName!=nil && object.strFirstName!=(id)[NSNull null])
            {
                strUserName=[strUserName stringByAppendingString:object.strFirstName];
                [cell.lblName setText:strUserName];
            }
            
            if (object.strLastName!=nil && object.strLastName!=(id)[NSNull null])
            {
                //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",object.strLastName]];
                //[cell.lblName setText:strUserName];
            }
            
            //Location
            NSString *strLocation=@"";
            
            if(object.strCity!=nil && object.strCity!=(id)[NSNull null] && object.strCity.length>0)
            {
                strLocation=[strLocation stringByAppendingString:object.strCity];
                [cell.lblPlace setText:strLocation];
            }
            
            if(object.strCountry!=nil && object.strCountry!=(id)[NSNull null] && object.strCountry.length>0)
            {
                strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",object.strCountry]];
                [cell.lblPlace setText:strLocation];
            }
            
            //Date
            NSDateFormatter *formatter=[NSDateFormatter new];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            
            NSDate *startTime=[formatter dateFromString:object.strStartTime];
            NSDate *endTime=[formatter dateFromString:object.strEndTime];
            [formatter setDateFormat:@"MMM dd, yyyy"];
            [cell.lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
            
            //Time
            [formatter setDateFormat:@"hh:mm a"];
            NSString *strTime=[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:startTime],[formatter stringFromDate:endTime]];
            [cell.lblTime setText:strTime];
            
            //Status
            [cell.lblCurrentStatus setText:object.strStatus];
            
            cell.btnAcceptRequest.tag=cell.btnRejectRequest.tag=indexPath.row; //Assiging tag to buttons
            
            [cell.btnAcceptRequest setTitle:@"Cancel Booking" forState:UIControlStateNormal];
            [cell.btnRejectRequest setTitle:@"Chat" forState:UIControlStateNormal];
            
            [cell.btnAcceptRequest addTarget:self action:@selector(btnCancelBookedBookingPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnRejectRequest addTarget:self action:@selector(btnChatWithTouristPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            //UnreadMessage
            if ([App_Delegate.aryUnreadChat containsObject:object.strBookingId])
            {
                [cell.imgNewChatMessage setHidden:NO];
            }
            else
            {
                [cell.imgNewChatMessage setHidden:YES];
            }
            
            //TapToViewProfile
            [cell.lblViewProfile setHidden:NO];
            cell.imgUserProfile.tag=indexPath.row;
            [cell.imgUserProfile setUserInteractionEnabled:YES];
            [self addTapGuestureToImageView:cell.imgUserProfile];
            
            [cell.lblRemainingTime setHidden:YES];
            
            return cell;
            
        }
        else if ([object.strStatus isEqualToString:@"GUIDING"])
        {
            
            //Guiding
            NSString *strCellIdentifier=@"TripGuideNewRequestCell";
            
            TripGuideNewRequestCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
            
            if (cell==nil)
            {
                cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            //Image
            [cell.imgUserProfile setClipsToBounds:YES];
            [cell.imgUserProfile.layer setCornerRadius:20];
            [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:object.strImageURL]];
            
            //Set image
            [cell.imgCurrentStatus setImage:[UIImage imageNamed:@"icn_guiding"]];
            
            //Name
            NSString *strUserName=@"";
            
            if (object.strFirstName!=nil && object.strFirstName!=(id)[NSNull null])
            {
                strUserName=[strUserName stringByAppendingString:object.strFirstName];
                [cell.lblName setText:strUserName];
            }
            
            if (object.strLastName!=nil && object.strLastName!=(id)[NSNull null])
            {
                //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",object.strLastName]];
                //[cell.lblName setText:strUserName];
            }
            
            //Location
            NSString *strLocation=@"";
            
            if(object.strCity!=nil && object.strCity!=(id)[NSNull null] && object.strCity.length>0)
            {
                strLocation=[strLocation stringByAppendingString:object.strCity];
                [cell.lblPlace setText:strLocation];
            }
            
            if(object.strCountry!=nil && object.strCountry!=(id)[NSNull null] && object.strCountry.length>0)
            {
                strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",object.strCountry]];
                [cell.lblPlace setText:strLocation];
            }
            
            //Date
            NSDateFormatter *formatter=[NSDateFormatter new];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            
            NSDate *startTime=[formatter dateFromString:object.strStartTime];
            NSDate *endTime=[formatter dateFromString:object.strEndTime];
            [formatter setDateFormat:@"MMM dd, yyyy"];
            [cell.lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
            
            //Time
            [formatter setDateFormat:@"hh:mm a"];
            NSString *strTime=[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:startTime],[formatter stringFromDate:endTime]];
            [cell.lblTime setText:strTime];
            
            //Status
            [cell.lblCurrentStatus setText:object.strStatus];
            
            //Time ago
            formatter=[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            NSDate *date=[formatter dateFromString:object.strStartTime];
            
            if (date!=nil)
            {
                strTime=[date timeAgo];
            }
            
            NSString *strCompletedTime=@"";
            
            float completedMinutes=[App_Delegate getMinutesDifferenceBetweenStartDate:object.strStartTime endDate:[formatter stringFromDate:[NSDate date]] withFormaterStyle:@"YYYY-MM-dd HH:mm:ss"];
            
            strCompletedTime=[NSString stringWithFormat:@"%d",(int)completedMinutes];
            
            if (completedMinutes<0)
            {
                [cell.btnAcceptRequest setTitle:@"Starting" forState:UIControlStateNormal];
            }
            else
            {
                [cell.btnAcceptRequest setTitle:[NSString stringWithFormat:@"%@:00",strCompletedTime] forState:UIControlStateNormal];
            }
            
            [cell.btnAcceptRequest setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
            [cell.btnRejectRequest setTitle:@"End Trip" forState:UIControlStateNormal];
            
            [cell.lblRemainingTime setHidden:YES];
            
            cell.btnAcceptRequest.tag=cell.btnRejectRequest.tag=indexPath.row;
            
            [cell.btnRejectRequest setEnabled:NO]; //temp - This is kept disable so as to keep the flow limited to the traveller.
            [cell.btnRejectRequest setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            
            //Assiging tag to buttons
            //[cell.btnAcceptRequest addTarget:self action:@selector(acceptNewRequestButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            //[cell.btnRejectRequest addTarget:self action:@selector(rejectNewRequestButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            //TapToViewProfile
            [cell.lblViewProfile setHidden:NO];
            cell.imgUserProfile.tag=indexPath.row;
            [cell.imgUserProfile setUserInteractionEnabled:YES];
            [self addTapGuestureToImageView:cell.imgUserProfile];
            
            return cell;
            
        }
        else if ([object.strStatus isEqualToString:@"NEW_REQUEST"])
        {
            NSString *strCellIdentifier=@"TripTravellerCell";
            
            TripTravellerCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
            
            if (cell==nil)
            {
                cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            //Image
            [cell.imgUserProfile setClipsToBounds:YES];
            [cell.imgUserProfile.layer setCornerRadius:20];
            [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:object.strImageURL]];
            
            //Name
            NSString *strUserName=@"";
            
            if (object.strFirstName!=nil && object.strFirstName!=(id)[NSNull null])
            {
                strUserName=[strUserName stringByAppendingString:object.strFirstName];
                [cell.lblName setText:strUserName];
            }
            
            if (object.strLastName!=nil && object.strLastName!=(id)[NSNull null])
            {
                //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",object.strLastName]];
                //[cell.lblName setText:strUserName];
            }
            
            //Location
            NSString *strLocation=@"";
            
            if(object.strCity!=nil && object.strCity!=(id)[NSNull null] && object.strCity.length>0)
            {
                strLocation=[strLocation stringByAppendingString:object.strCity];
                [cell.lblPlace setText:strLocation];
            }
            
            if(object.strCountry!=nil && object.strCountry!=(id)[NSNull null] && object.strCountry.length>0)
            {
                strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",object.strCountry]];
                [cell.lblPlace setText:strLocation];
            }
            
            //Date
            NSDateFormatter *formatter=[NSDateFormatter new];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            
            NSDate *startTime=[formatter dateFromString:object.strStartTime];
            NSDate *endTime=[formatter dateFromString:object.strEndTime];
            [formatter setDateFormat:@"MMM dd, yyyy"];
            [cell.lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
            
            
            //Time
            [formatter setDateFormat:@"hh:mm a"];
            NSString *strTime=[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:startTime],[formatter stringFromDate:endTime]];
            [cell.lblTime setText:strTime];
            
            //Status
            [cell.lblCurrentStatus setText:@"Pending"];
            [cell.imgCurrentStatus setImage:[UIImage imageNamed:@"icn_pending"]];
            cell.btnCancelBookingRequest.tag=indexPath.row;
            [cell.btnCancelBookingRequest addTarget:self action:@selector(btnCancelBookedBookingPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            //TapToViewProfile
            [cell.lblViewProfile setHidden:NO];
            cell.imgUserProfile.tag=indexPath.row;
            [cell.imgUserProfile setUserInteractionEnabled:YES];
            [self addTapGuestureToImageView:cell.imgUserProfile];
            
            return cell;
        }
        else
        {
            
            //Declined
            NSString *strCellIdentifier=@"TripTravellerCell";
            
            TripTravellerCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
            
            if (cell==nil)
            {
                cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            //Image
            [cell.imgUserProfile setClipsToBounds:YES];
            [cell.imgUserProfile.layer setCornerRadius:20];
            [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:object.strImageURL]];
            
            //Name
            NSString *strUserName=@"";
            
            if (object.strFirstName!=nil && object.strFirstName!=(id)[NSNull null])
            {
                strUserName=[strUserName stringByAppendingString:object.strFirstName];
                [cell.lblName setText:strUserName];
            }
            
            if (object.strLastName!=nil && object.strLastName!=(id)[NSNull null])
            {
                //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",object.strLastName]];
                //[cell.lblName setText:strUserName];
            }
            
            //Location
            NSString *strLocation=@"";
            
            if(object.strCity!=nil && object.strCity!=(id)[NSNull null] && object.strCity.length>0)
            {
                strLocation=[strLocation stringByAppendingString:object.strCity];
                [cell.lblPlace setText:strLocation];
            }
            
            if(object.strCountry!=nil && object.strCountry!=(id)[NSNull null] && object.strCountry.length>0)
            {
                strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",object.strCountry]];
                [cell.lblPlace setText:strLocation];
            }
            
            //Date
            NSDateFormatter *formatter=[NSDateFormatter new];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            
            NSDate *startTime=[formatter dateFromString:object.strStartTime];
            NSDate *endTime=[formatter dateFromString:object.strEndTime];
            [formatter setDateFormat:@"MMM dd, yyyy"];
            [cell.lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
            
            
            //Time
            [formatter setDateFormat:@"hh:mm a"];
            NSString *strTime=[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:startTime],[formatter stringFromDate:endTime]];
            [cell.lblTime setText:strTime];
            
            //Status
            [cell.lblCurrentStatus setText:@"Declined"];
            
            //TapToViewProfile
            [cell.lblViewProfile setHidden:NO];
            cell.imgUserProfile.tag=indexPath.row;
            [cell.imgUserProfile setUserInteractionEnabled:YES];
            [self addTapGuestureToImageView:cell.imgUserProfile];
            
            
            
            
            return cell;
        }
    }
    else
    {
        
        TripObject *object=[aryPastTrips objectAtIndex:indexPath.row];
        
        if ([object.strStatus isEqualToString:@"TIMEOUT"] || [object.strStatus isEqualToString:@"DECLINED"] || [object.strStatus isEqualToString:@"CANCELLED"]) //Rejected
        {
            
            NSString *strCellIdentifier=@"PastTripNormalCell";
            
            PastTripNormalCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
            
            if (cell==nil)
            {
                cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            //Image
            [cell.imgUserProfile setClipsToBounds:YES];
            [cell.imgUserProfile.layer setCornerRadius:20];
            [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:object.strImageURL]];
            
            //Name
            NSString *strUserName=@"";
            
            if (object.strFirstName!=nil && object.strFirstName!=(id)[NSNull null])
            {
                strUserName=[strUserName stringByAppendingString:object.strFirstName];
                [cell.lblName setText:strUserName];
            }
            
            if (object.strLastName!=nil && object.strLastName!=(id)[NSNull null])
            {
                //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",object.strLastName]];
                //[cell.lblName setText:strUserName];
            }
            
            //Location
            NSString *strLocation=@"";
            
            if(object.strCity!=nil && object.strCity!=(id)[NSNull null] && object.strCity.length>0)
            {
                strLocation=[strLocation stringByAppendingString:object.strCity];
                [cell.lblPlace setText:strLocation];
            }
            
            if(object.strCountry!=nil && object.strCountry!=(id)[NSNull null] && object.strCountry.length>0)
            {
                strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",object.strCountry]];
                [cell.lblPlace setText:strLocation];
            }
            
            //Date
            NSDateFormatter *formatter=[NSDateFormatter new];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            
            NSDate *startTime=[formatter dateFromString:object.strStartTime];
            NSDate *endTime=[formatter dateFromString:object.strEndTime];
            [formatter setDateFormat:@"MMM dd, yyyy"];
            [cell.lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
            
            //Time
            [formatter setDateFormat:@"hh:mm a"];
            NSString *strTime=[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:startTime],[formatter stringFromDate:endTime]];
            [cell.lblTime setText:strTime];
            
            if ([object.strStatus isEqualToString:@"TIMEOUT"])
            {
                [cell.lblCurrentStatus setText:@"NOT\nRESPONDED"];
                [cell.imgCurrentStatus setImage:[UIImage imageNamed:@"icn_pending.png"]];
            }
            else if ([object.strStatus isEqualToString:@"CANCELLED"])
            {
                [cell.lblCurrentStatus setText:object.strStatus];
                [cell.imgCurrentStatus setImage:[UIImage imageNamed:@"icn_canceled.png"]];
            }
            else
            {
                [cell.lblCurrentStatus setText:object.strStatus];
                [cell.imgCurrentStatus setImage:[UIImage imageNamed:@"icn_declined.png"]];
            }
            
            return cell;
            
        }
        else if ([object.strStatus isEqualToString:@"COMPLETED"]) //Comepleted
        {
            
            NSString *strCellIdentifier=@"PastTripDetailCell";
            
            PastTripDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
            
            if (cell==nil)
            {
                cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            //Image
            [cell.imgUserProfile setClipsToBounds:YES];
            [cell.imgUserProfile.layer setCornerRadius:20];
            [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:object.strImageURL]];
            
            //Name
            NSString *strUserName=@"";
            
            if (object.strFirstName!=nil && object.strFirstName!=(id)[NSNull null])
            {
                strUserName=[strUserName stringByAppendingString:object.strFirstName];
                [cell.lblName setText:strUserName];
            }
            
            if (object.strLastName!=nil && object.strLastName!=(id)[NSNull null])
            {
                //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",object.strLastName]];
                //[cell.lblName setText:strUserName];
            }
            
            //Location
            NSString *strLocation=@"";
            
            if(object.strCity!=nil && object.strCity!=(id)[NSNull null] && object.strCity.length>0)
            {
                strLocation=[strLocation stringByAppendingString:object.strCity];
                [cell.lblPlace setText:strLocation];
            }
            
            if(object.strCountry!=nil && object.strCountry!=(id)[NSNull null] && object.strCountry.length>0)
            {
                strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",object.strCountry]];
                [cell.lblPlace setText:strLocation];
            }
            
            //Date
            NSDateFormatter *formatter=[NSDateFormatter new];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            
            NSDate *startTime=[formatter dateFromString:object.strStartTime];
            NSDate *endTime=[formatter dateFromString:object.strEndTime];
            [formatter setDateFormat:@"MMM dd, yyyy"];
            [cell.lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
            
            //Time
            [formatter setDateFormat:@"hh:mm a"];
            NSString *strTime=[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:startTime],[formatter stringFromDate:endTime]];
            [cell.lblTime setText:strTime];
            
            
            cell.btnViewTripDetail.tag=indexPath.row;
            
            //Status
            [cell.lblCurrentStatus setText:object.strStatus];
            [cell.imgCurrentStatus setImage:[UIImage imageNamed:@"icn_accepted.png"]];
            [cell.btnViewTripDetail addTarget:self action:@selector(btnViewDetailsPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
        else //Cancelled
        {
            
            NSString *strCellIdentifier=@"PastTripDetailCell";
            
            PastTripDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
            
            if (cell==nil)
            {
                cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            //Image
            [cell.imgUserProfile setClipsToBounds:YES];
            [cell.imgUserProfile.layer setCornerRadius:20];
            [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:object.strImageURL]];
            
            //Name
            NSString *strUserName=@"";
            
            if (object.strFirstName!=nil && object.strFirstName!=(id)[NSNull null])
            {
                strUserName=[strUserName stringByAppendingString:object.strFirstName];
                [cell.lblName setText:strUserName];
            }
            
            if (object.strLastName!=nil && object.strLastName!=(id)[NSNull null])
            {
                //strUserName=[strUserName stringByAppendingString:[NSString stringWithFormat:@" %@",object.strLastName]];
                //[cell.lblName setText:strUserName];
            }
            
            //Location
            NSString *strLocation=@"";
            
            if(object.strCity!=nil && object.strCity!=(id)[NSNull null] && object.strCity.length>0)
            {
                strLocation=[strLocation stringByAppendingString:object.strCity];
                [cell.lblPlace setText:strLocation];
            }
            
            if(object.strCountry!=nil && object.strCountry!=(id)[NSNull null] && object.strCountry.length>0)
            {
                strLocation=[strLocation stringByAppendingString:[NSString stringWithFormat:@", %@",object.strCountry]];
                [cell.lblPlace setText:strLocation];
            }
            
            //Date
            NSDateFormatter *formatter=[NSDateFormatter new];
            [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            
            NSDate *startTime=[formatter dateFromString:object.strStartTime];
            NSDate *endTime=[formatter dateFromString:object.strEndTime];
            [formatter setDateFormat:@"MMM dd, yyyy"];
            [cell.lblDate setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:startTime]]];
            
            //Time
            [formatter setDateFormat:@"hh:mm a"];
            NSString *strTime=[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:startTime],[formatter stringFromDate:endTime]];
            [cell.lblTime setText:strTime];
            
            //Status
            [cell.lblCurrentStatus setText:object.strStatus];
            
            [cell.btnViewTripDetail setHidden:YES]; //temp
            
            return cell;
        }
        
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark- AlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==7800)
    {
        //Guest Alert
        if (buttonIndex==1)
        {
            [self callCancelBookedBookingbyGuide:currentObject.strBookingId];
        }
    }
}


@end
