//
//  PageView_AboutVCViewController.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 9/8/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserObject.h"
#import "PTableView.h"



@interface PageView_AboutVCViewController : UIViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet PTableView *tblView;
    NSMutableArray *aryInformation;
    
    IBOutlet UIView *viewHeader;
    IBOutlet UIView *viewFooter;
    IBOutlet UIButton *btnFooterBookMe;
    
    IBOutlet UILabel *lblHeaderTitle;
    IBOutlet UIButton *btnHeaderAdd;
    
}

@property (nonatomic,strong) UserObject *guideObject;
@property BOOL isForViewProfile;


@end
