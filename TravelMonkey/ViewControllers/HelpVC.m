//
//  HelpVC.m
//  TravelMonkey
//
//  Created by Purushottam Sain on 10/13/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import "HelpVC.h"
#import "HelpCellVC.h"
#import "ContactUsVC.h"
#import "QuestionVC.h"


@interface HelpVC ()

@end

#define KName @"name"

@implementation HelpVC
@synthesize isGuide;


- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [viewStatusBar setBackgroundColor:KAppColor];
    [viewTitle setBackgroundColor:KAppColor];
    
    aryOptions=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Questions" forKey:KName];
    [aryOptions addObject:dic];
    
    dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"Contact Us" forKey:KName];
    [aryOptions addObject:dic];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [tblView reloadData];
}

#pragma mark- Button Action

-(IBAction)btnTogglePressed:(id)sender
{
    [self.viewDeckController toggleLeftViewAnimated:YES];
}

#pragma mark- Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return aryOptions.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strCellIdentifier=@"HelpCellVC";
    
    HelpCellVC *cell=[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
    
    if (cell==nil)
    {
        cell=[[[NSBundle mainBundle] loadNibNamed:strCellIdentifier owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NSMutableDictionary *dic=[aryOptions objectAtIndex:indexPath.row];
    
    [cell.lblTitle setText:[dic objectForKey:KName]];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    HelpCellVC *cell=[tableView cellForRowAtIndexPath:indexPath];
    [cell.lblTitle setTextColor:[UIColor whiteColor]];
    [cell.imgViewBorder setImage:[UIImage imageNamed:@"cell_contactus_touch.png"]];
    [cell.imgViewArrow setImage:[UIImage imageNamed:@"icn_arrow_contactus_touch.png"]];
    
    if (indexPath.row==0)
    {
        QuestionVC *contactUsVC=[[QuestionVC alloc]init];
        [self.navigationController pushViewController:contactUsVC animated:YES];
    }
    else
    {
        ContactUsVC *contactUsVC=[[ContactUsVC alloc]init];
        [self.navigationController pushViewController:contactUsVC animated:YES];
    }
}


@end
