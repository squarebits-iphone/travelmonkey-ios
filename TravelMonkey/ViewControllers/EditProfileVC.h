//
//  EditProfileVC.h
//  TravelMonkey
//
//  Created by Purushottam Sain on 8/12/16.
//  Copyright © 2016 Squarebits Private Limited Jodhpur, Raj, India ( Purushottam Sain ). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileVC : UIViewController<UITextFieldDelegate>
{
    IBOutlet UIView *viewTopStatusBar;
    IBOutlet UIView *viewTopMain;
    
    UIImage *chosenImage;
    NSString *strAddress;
    NSString *strCountry;
    NSString *strUserImageId;
    NSString *strLat;
    NSString *strLong;
    NSString *strCountryISO;
    NSString *strUserType;
    
    NSMutableArray *aryLanguageLevel;
    NSMutableArray *aryLanguage;
    BOOL isForChangeSelection;
    BOOL isChangeLanguage;
    int currentIndex;
    
    
    IBOutlet UIView *viewBackButton;
    IBOutlet UIScrollView *scrlView;
    IBOutlet UIView *viewSubViewScrollView;
    IBOutlet UIImageView *imgUserProfile;
    IBOutlet UIButton *btnAddPhoto;
    IBOutlet UITableView *tblLanguage;
    
    //__weak IBOutlet UIButton *btnAddress;
    __weak IBOutlet UIButton *btnTourist;
    
    __weak IBOutlet UIButton *btnLcoal;
    __weak IBOutlet UIButton *btnCountry;
    IBOutlet UIButton *btnSave;
    
    
    
    __weak IBOutlet UIButton *btnFemale;
    __weak IBOutlet UIButton *btnMale;
    __weak IBOutlet UIButton *btnChangePassword;
    IBOutlet UIImageView *imgPasswordArrow;
    
    __weak IBOutlet UITextField *txtFieldCity;
    __weak IBOutlet UITextField *txtFieldEmail;
    __weak IBOutlet UITextField *txtFieldLastName;
    __weak IBOutlet UITextField *txtFieldFirstName;
    __weak IBOutlet UITextField *txtFieldMonth;
    __weak IBOutlet UITextField *txtFieldDate;
    
    __weak IBOutlet UITextView *txtViewTransportation;
    __weak IBOutlet UITextView *txtViewOfferTourist;
    __weak IBOutlet UITextField *txtFieldYear;
    __weak IBOutlet UITextView *txtViewAboutMe;
    
    
    __weak IBOutlet NSLayoutConstraint *viewScrlSubViewBottamLayout;
    
    __weak IBOutlet NSLayoutConstraint *constraintViewSubViewScrollView;
    __weak IBOutlet NSLayoutConstraint *constraintTableHeight;
    
    
    
    
}

- (IBAction)btnTouristPressed:(UIButton *)sender;
- (IBAction)btnGuidePressed:(UIButton *)sender;
- (IBAction)btnMalePressed:(UIButton *)sender;
- (IBAction)btnFemalePressed:(UIButton *)sender;
- (IBAction)btnSavePressed:(UIButton *)sender;
- (IBAction)btnBackPressed:(UIButton *)sender;


@end
